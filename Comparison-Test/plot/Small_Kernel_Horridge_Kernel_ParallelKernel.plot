set terminal png
set output "1.png"
set title "Horridge Vs Kernel Vs Clustered Parallel Kernel"
set xlabel "Time (ms)"
set ylabel "Kernel Size"
set key below
plot 'Small_Kernel_Horridge_Kernel_ParallelKernel.dat' using 1:2 with lines title 'Horridge' smooth csplines,'Small_Kernel_Horridge_Kernel_ParallelKernel.dat' using 1:3 with lines title 'Kernel' smooth csplines, 'Small_Kernel_Horridge_Kernel_ParallelKernel.dat' using 1:4 with lines title 'Paralel Clustered Kernel' smooth csplines
