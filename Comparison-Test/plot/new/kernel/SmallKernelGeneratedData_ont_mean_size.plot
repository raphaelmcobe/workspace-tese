set terminal png size 800,600
set output "SmallKernelGeneratedData_ont_mean_size.png"
set title "Tamanho médio de entrada do Mecanismo de Inferência"
set xlabel "Tamanho da Ontologia (No. de Axiomas)"
set ylabel "Tamanho médio (No. de Axiomas)"
set key below


plot '../../../benchmark/output/generated/kernel/small/ce_cc/out.dat' using 2:($8 > 0 ? $8 : 1/0) with lines title 'Expansão Clássica, Contração Clássica',\
'../../../benchmark/output/generated/kernel/small/ce_swc/out.dat' using 2:($8 > 0 ? $8 : 1/0) with lines title 'Expansão Clássica, Contração Sliding Window', \
'../../../benchmark/output/generated/kernel/small/ce_dacc/out.dat' using 2:($8 > 0 ? $8 : 1/0) with lines title 'Expansão Clássica, Contração Divisão e Conquista', \
'../../../benchmark/output/generated/kernel/small/sre_cc/out.dat' using 2:($8 > 0 ? $8 : 1/0) with lines title 'Expansão Relevância Sintática, Contração Clássica', \
'../../../benchmark/output/generated/kernel/small/sre_swc/out.dat' using 2:($8 > 0 ? $8 : 1/0) with lines title 'Expansão Relevância Sintática, Contração Sliding Window', \
'../../../benchmark/output/generated/kernel/small/sre_dacc/out.dat' using 2:($8 > 0 ? $8 : 1/0) with lines title 'Expansão Relevância Sintática, Contração Divisão e Conquista'
