set terminal png size 800,600
set output "SmallKernelGeneratedData_time.png"
set title "Tempo de Execução"
set xlabel "Tamanho da Ontologia (No. de Axiomas)"
set ylabel "Tempo(ms)"
set key below


plot '../../../benchmark/output/generated/kernel/small/ce_cc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Expansão Clássica, Contração Clássica',\
'../../../benchmark/output/generated/kernel/small/ce_swc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Expansão Clássica, Contração Sliding Window', \
'../../../benchmark/output/generated/kernel/small/ce_dacc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Expansão Clássica, Contração Divisão e Conquista', \
'../../../benchmark/output/generated/kernel/small/sre_cc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Expansão Relevância Sintática, Contração Clássica', \
'../../../benchmark/output/generated/kernel/small/sre_swc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Expansão Relevância Sintática, Contração Sliding Window', \
'../../../benchmark/output/generated/kernel/small/sre_dacc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Expansão Relevância Sintática, Contração Divisão e Conquista'
