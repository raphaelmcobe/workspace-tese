set terminal png size 1024,768
set output "SmallKernelXRemainderGeneratedData_RC.png"
set title "Chamadas ao Mecanismo de Inferência Kernel X Remainder"
set xlabel "Tamanho da Ontologia (No. de Axiomas)"
set ylabel "Chamadas ao Mecanismo de Inferência"
set key below


plot '../../../benchmark/output/generated/remainder/small/ce_cc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Expansão Clássica, Contração Clássica',\
'../../../benchmark/output/generated/remainder/small/ce_swc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Expansão Clássica, Contração Sliding Window', \
'../../../benchmark/output/generated/remainder/small/ce_src/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Expansão Clássica, Contração por Relevância Sintática', \
'../../../benchmark/output/generated/remainder/small/ce_trc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Expansão Clássica, Contração Trivial.',\
'../../../benchmark/output/generated/remainder/small/dace_trc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Expansão Divisão e Conquista, Contração Trivial', \
'../../../benchmark/output/generated/remainder/small/dace_src/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Expansão Divisão e Conquista, Contração por Relevância Sintática', \
'../../../benchmark/output/generated/kernel/small/ce_cc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Kernel - Expansão Clássica, Contração Clássica', \
'../../../benchmark/output/generated/kernel/small/ce_swc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Kernel - Expansão Clássica, Contração Sliding Window', \
'../../../benchmark/output/generated/kernel/small/ce_dacc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Kernel - Expansão Clássica, Contração Divisão e Conquista',\
'../../../benchmark/output/generated/kernel/small/sre_cc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Kernel - Expansão Relevância Sintática, Contração Clássica', \
'../../../benchmark/output/generated/kernel/small/sre_swc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Kernel - Expansão Relevância Sintática, Contração Sliding Window',\
'../../../benchmark/output/generated/kernel/small/sre_dacc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Kernel - Expansão Relevância Sintática, Contração Divisão e Conquista'
