set terminal png size 1024,768
set output "LargeKernelXRemainderGeneratedData_time.png"
set title "Tempo de Execução Kernel X Remainder"
set xlabel "Tamanho da Ontologia (No. de Axiomas)"
set ylabel "Tempo de Execução (ms)"
set key below


plot '../../../benchmark/output/generated/remainder/large/ce_cc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Remainder - Expansão Clássica, Contração Clássica',\
'../../../benchmark/output/generated/remainder/large/ce_swc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Remainder - Expansão Clássica, Contração Sliding Window', \
'../../../benchmark/output/generated/remainder/large/ce_src/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Remainder - Expansão Clássica, Contração por Relevância Sintática', \
'../../../benchmark/output/generated/remainder/large/ce_trc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Remainder - Expansão Clássica, Contração Trivial',\
'../../../benchmark/output/generated/remainder/large/dace_trc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Remainder - Expansão Divisão e Conquista, Contração Trivial', \
'../../../benchmark/output/generated/remainder/large/dace_src/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Remainder - Expansão Divisão e Conquista, Contração por Relevância Sintática',\
'../../../benchmark/output/generated/kernel/large/ce_cc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Kernel - Expansão Clássica, Contração Clássica', \
'../../../benchmark/output/generated/kernel/large/ce_swc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Kernel - Expansão Clássica, Contração Sliding Window', \
'../../../benchmark/output/generated/kernel/large/ce_dacc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Kernel - Expansão Clássica, Contração Divisão e Conquista',\
'../../../benchmark/output/generated/kernel/large/sre_cc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Kernel - Expansão Relevância Sintática, Contração Clássica', \
'../../../benchmark/output/generated/kernel/large/sre_swc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Kernel - Expansão Relevância Sintática, Contração Sliding Window',\
'../../../benchmark/output/generated/kernel/large/sre_dacc/out.dat' using 2:($4 > 0 ? $4 : 1/0) with lines title 'Kernel - Expansão Relevância Sintática, Contração Divisão e Conquista'
