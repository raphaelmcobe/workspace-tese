set terminal png size 800,600
set output "LargeRemainderGeneratedData_mem.png"
set title "Variação da Memória"
set xlabel "Tamanho da Ontologia (No. de Axiomas)"
set ylabel "Memória usada pela JVM (MB)"
set key below


plot '../../../benchmark/output/generated/remainder/large/ce_cc/out.dat' using 2:($11 > 0 ? $11 : 1/0) with lines title 'Expansão Clássica, Contração Clássica',\
'../../../benchmark/output/generated/remainder/large/ce_swc/out.dat' using 2:($11 > 0 ? $11 : 1/0) with lines title 'Expansão Clássica, Contração Sliding Window', \
'../../../benchmark/output/generated/remainder/large/ce_src/out.dat' using 2:($11 > 0 ? $11 : 1/0) with lines title 'Expansão Clássica, Contração por Relevância Sintática', \
'../../../benchmark/output/generated/remainder/large/ce_trc/out.dat' using 2:($11 > 0 ? $11 : 1/0) with lines title 'Expansão Clássica, Contração Trivial.',\
'../../../benchmark/output/generated/remainder/large/dace_trc/out.dat' using 2:($11 > 0 ? $11 : 1/0) with lines title 'Expansão Divisão e Conquista, Contração Trivial', \
'../../../benchmark/output/generated/remainder/large/dace_src/out.dat' using 2:($11 > 0 ? $11 : 1/0) with lines title 'Expansão Divisão e Conquista, Contração por Relevância Sintática'
