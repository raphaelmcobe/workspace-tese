set terminal png size 800,600
set output "SmallRemainderGeneratedData_RC.png"
set title "Chamadas ao Mecanismo de Inferência"
set xlabel "Tamanho da Ontologia (No. de Axiomas)"
set ylabel "Chamadas ao Mecanismo de Inferência"

set xrange [0:400]

set key below


plot '../../../benchmark/output/generated/remainder/small/ce_cc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Expansão Clássica, Contração Clássica',\
'../../../benchmark/output/generated/remainder/small/ce_swc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Expansão Clássica, Contração Sliding Window', \
'../../../benchmark/output/generated/remainder/small/ce_src/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Expansão Clássica, Contração por Relevância Sintática', \
'../../../benchmark/output/generated/remainder/small/ce_trc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Expansão Clássica, Contração Trivial.',\
'../../../benchmark/output/generated/remainder/small/dace_trc/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Expansão Divisão e Conquista, Contração Trivial', \
'../../../benchmark/output/generated/remainder/small/dace_src/out.dat' using 2:($7 > 0 ? $7 : 1/0) with lines title 'Expansão Divisão e Conquista, Contração por Relevância Sintática'
