set terminal png size 800,600
set output "SmallRemainderGeneratedData_ont_mean_size.png"
set title "Tamanho médio de entrada do Mecanismo de Inferência"
set xlabel "Tamanho da Ontologia (No. de Axiomas)"
set ylabel "Tamanho médio (No. de Axiomas)"
set key below


plot '../../../benchmark/output/generated/remainder/small/ce_cc/out.dat' using 2:($8 > 0 ? $8 : 1/0) with lines title 'Expansão Clássica, Contração Clássica',\
'../../../benchmark/output/generated/remainder/small/ce_swc/out.dat' using 2:($8 > 0 ? $8 : 1/0) with lines title 'Expansão Clássica, Contração Sliding Window', \
'../../../benchmark/output/generated/remainder/small/ce_src/out.dat' using 2:($8 > 0 ? $8 : 1/0) with lines title 'Expansão Clássica, Contração por Relevância Sintática', \
'../../../benchmark/output/generated/remainder/small/ce_trc/out.dat' using 2:($8 > 0 ? $8 : 1/0) with lines title 'Expansão Clássica, Contração Trivial.',\
'../../../benchmark/output/generated/remainder/small/dace_trc/out.dat' using 2:($8 > 0 ? $8 : 1/0) with lines title 'Expansão Divisão e Conquista, Contração Trivial', \
'../../../benchmark/output/generated/remainder/small/dace_src/out.dat' using 2:($8 > 0 ? $8 : 1/0) with lines title 'Expansão Divisão e Conquista, Contração por Relevância Sintática'
