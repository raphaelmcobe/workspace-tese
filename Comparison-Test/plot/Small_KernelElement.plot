set terminal png
set output "Small_KernelElement\.png"
set title "Horridge Vs Simple BlackBox Vs Sliding Window Vs Syntatic Relevance Expansion Vs Divide & Conquer"
set xlabel "Ontology Size (# of Axiom)"
set ylabel "Time(ms)"
set key below

set logscale x 2

plot 'Small_KernelElement.dat' using 1:3 with lines title 'Horridge' smooth csplines,\
'Small_KernelElement.dat' using 1:5 with lines title 'Simple BlackBox' smooth csplines, \
'Small_KernelElement.dat' using 1:7 with lines title 'Sliding Window' smooth csplines,\
'Small_KernelElement.dat' using 1:9 with lines title 'Syntatic Relevance Expansion' smooth csplines , \
'Small_KernelElement.dat' using 1:11 with lines title 'Divide and Conquer' smooth csplines
