set terminal png size 1024,768
set output "Small_RemainderElement_Reasoner_Calls_Time.png"
set title "Resina Vs Divide & Conquer"
set xlabel "Ontology Size (# of Axioms)"
set x2label "Remainder Set Size (# of Axioms)"
set ylabel "Mean Time for Reasoner Calls (ms)"
set key below

plot 'Small_RemainderElement.dat' using 1:6 with lines title 'Resina' smooth csplines,\
'Small_RemainderElement.dat' using 1:11 with lines title 'Divide and Conquer' smooth csplines,\
'Small_RemainderElement.dat' using 1:16 with lines title 'Divide and Conquer V2' smooth csplines
