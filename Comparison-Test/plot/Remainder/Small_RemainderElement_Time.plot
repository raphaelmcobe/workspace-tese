set terminal png size 1024,768
set output "Small_RemainderElement_Time.png"
set title "Resina Vs Divide & Conquer"
set xlabel "Ontology Size (# of Axioms)"
set x2label "Remainder Set Size (# of Axioms)"
set ylabel "Time to build a Remainder (ms)"
set key below

plot 'Small_RemainderElement.dat' using 1:2 with lines title 'Resina' smooth csplines,\
'Small_RemainderElement.dat' using 1:7 with lines title 'Divide and Conquer' smooth csplines,\
'Small_RemainderElement.dat' using 1:12 with lines title 'Divide and Conquer' smooth csplines
