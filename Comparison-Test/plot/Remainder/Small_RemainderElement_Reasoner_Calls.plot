set terminal png size 1024,768
set output "Small_RemainderElement_Reasoner_Calls.png"
set title "Resina Vs Divide & Conquer"
set xlabel "Ontology Size (# of Axioms)"
set x2label "Remainder Set Size (# of Axioms)"
set ylabel "# of Reasoner Calls"
set key below

plot 'Small_RemainderElement.dat' using 1:4 with lines title 'Resina' smooth csplines,\
'Small_RemainderElement.dat' using 1:9 with lines title 'Divide and Conquer' smooth csplines,\
'Small_RemainderElement.dat' using 1:14 with lines title 'Divide and Conquer V2' smooth csplines
