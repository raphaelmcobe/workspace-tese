import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChangeException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.RemoveAxiom;

import algorithms.SimpleReiter;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

public class TestSimpleReiterWithSlidingWindow {
	private static int reasonerCalls;




	public static void test(OWLOntology o, int windowSize) throws OWLOntologyCreationException {
		reasonerCalls=0;
		double time;
		time = System.currentTimeMillis();
		Set<Set<OWLAxiom>> kernel = kernelWithSlidingWindow(o, windowSize);
		System.out.print((System.currentTimeMillis() - time) + "\t");
		System.out.print(kernel.size() + "\t");
		System.out.print(reasonerCalls+" \t");
	}
	
	
	public static void testSingleKernelElement(OWLOntology o, int windowSize) throws OWLOntologyChangeException, OWLOntologyCreationException {
		reasonerCalls=0;
		double time;
		time = System.currentTimeMillis();
		Set<OWLAxiom> aKernel = kernelElementWithSlidingWindow(o.getAxioms(), windowSize);
		System.out.print((System.currentTimeMillis() - time) + "\t\t");
		System.out.print(aKernel.size() + "\t\t");
		System.out.print(reasonerCalls+"\t\t");
	}
	
	public static Set<Set<OWLAxiom>> kernelWithSlidingWindow(OWLOntology B, int windowSize) throws OWLOntologyCreationException, OWLOntologyChangeException {
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

		Set<Set<OWLAxiom>> kernel = new HashSet<Set<OWLAxiom>>();

		Set<Set<OWLAxiom>> cut = new HashSet<Set<OWLAxiom>>();
		PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(B);
		manager.addOntologyChangeListener(reasoner);

		Queue<Set<OWLAxiom>> queue = new LinkedList<Set<OWLAxiom>>();
		Set<OWLAxiom> element = null;
		Set<OWLAxiom> candidate = null;
		Set<OWLAxiom> hn;
		boolean haveToContinue = false;

		Set<OWLAxiom> exp = null;

		boolean isConsistent = reasoner.isConsistent();
		reasonerCalls++;
		if (isConsistent)
			return kernel;

		exp = B.getAxioms();

		element = kernelElementWithSlidingWindow(exp,windowSize);
		kernel.add(element);
		for (OWLAxiom axiom : element) {
			Set<OWLAxiom> set = new HashSet<OWLAxiom>();
			set.add(axiom);
			queue.add(set);
		}
		// Reiter's algorithm
		while (!queue.isEmpty()) {
			hn = queue.remove();

			haveToContinue = false;
			for (Set<OWLAxiom> set : cut) {
				// Check if there is an element of cut that is in hn
				if (hn.containsAll(set)) {
					haveToContinue = true;
					break;
				}
			}
			if (haveToContinue)
				continue;

			for (OWLAxiom axiom : hn) {
				RemoveAxiom removeAxiom = new RemoveAxiom(B, axiom);
				manager.applyChange(removeAxiom);
			}
			exp = B.getAxioms();
			isConsistent = reasoner.isConsistent();
			reasonerCalls++;
			if (!isConsistent) {
				candidate = kernelElementWithSlidingWindow(exp,windowSize);
//				System.out.println(candidate);
				kernel.add(candidate);
				for (OWLAxiom axiom : candidate) {
					Set<OWLAxiom> set2 = new HashSet<OWLAxiom>();
					set2.addAll(hn);
					set2.add(axiom);
					queue.add(set2);
				}
			} else
				cut.add(hn);

			// Restore to the ontology the axioms removed so it can be used
			// again
			for (OWLAxiom axiom : hn) {
				AddAxiom addAxiom = new AddAxiom(B, axiom);
				manager.applyChange(addAxiom);
			}
		}

		return kernel;
	}
	
	
	

	private static Set<OWLAxiom> kernelElementWithSlidingWindow(Set<OWLAxiom> exp, int windowSize) throws OWLOntologyCreationException,
			OWLOntologyChangeException {
		Set<OWLAxiom> X = new HashSet<OWLAxiom>();

		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology ont = manager.createOntology();
		PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
		manager.addOntologyChangeListener(reasoner);

		// First Part: EXPAND
		// Adicionamos os axiomas de exp na ontologia criada ateh que alpha
		// seja inferido (esteja contido nas consequehncias)
		for (OWLAxiom axiom : exp) {
			AddAxiom addAxiom = new AddAxiom(ont, axiom);
			manager.applyChange(addAxiom);
			boolean isConsistent = reasoner.isConsistent();
			reasonerCalls++;
			if (!isConsistent)
				break;
		}
		// System.out.println("Tamanho do Kernel "+ont.getAxiomCount());

		// Second Part: SHRINK
		List<OWLAxiom> expList = new LinkedList<OWLAxiom>(exp);
		for (int i = 0; i < expList.size();) {
			int windowEnd = (i + windowSize) < (expList.size()) ? (i + windowSize) : expList.size();
			List<OWLAxiom> subList = expList.subList(i, windowEnd);
			for (OWLAxiom owlAxiom : subList) {
				RemoveAxiom removeAxiom = new RemoveAxiom(ont, owlAxiom);
				manager.applyChange(removeAxiom);
			}
			boolean isConsistent = reasoner.isConsistent();
			reasonerCalls++;
			if (isConsistent) {
				for (OWLAxiom owlAxiom : subList) {
					AddAxiom addAxiom = new AddAxiom(ont, owlAxiom);
					manager.applyChange(addAxiom);
				}
			}
			i = windowEnd;
		}
		// System.out.println("Tamanho depois do Sliding "+ont.getAxiomCount());
		X = SimpleReiter.kernelElement(ont.getAxioms());
		return X;
	}


	

}
