package algorithms;

/**
 * Created by rocknroll on 14/07/14.
 */
public class StatisticMetric {

	private String name;

	private String format;

	public StatisticMetric(String name, String format) {
		this.name = name;
		this.format = format;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
}
