package algorithms;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChangeException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.RemoveAxiom;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;


public class SimpleReiter {

	private static int reasonerCalls;

	public static void test(OWLOntology o) throws OWLOntologyCreationException {
		reasonerCalls=0;
		double time;
		time = System.currentTimeMillis();
		Set<Set<OWLAxiom>> kernel = kernel(o);
		System.out.print((System.currentTimeMillis() - time)+"\t");
		System.out.print(kernel.size()+"\t");
		System.out.print(reasonerCalls+"\t");
	}
	
	public static void testSingleKernelElement(OWLOntology o) throws OWLOntologyChangeException, OWLOntologyCreationException {
		reasonerCalls=0;
		double time;
		time = System.currentTimeMillis();
		Set<OWLAxiom> aKernel = kernelElement(o.getAxioms());
		System.out.print((System.currentTimeMillis() - time)+"\t\t");
		System.out.print(aKernel.size()+"\t\t");
		System.out.print(reasonerCalls+" \t\t");
	}
	
	
	public static Set<Set<OWLAxiom>> kernel(OWLOntology B) throws OWLOntologyCreationException, OWLOntologyChangeException {
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

		Set<Set<OWLAxiom>> kernel = new HashSet<Set<OWLAxiom>>();

		Set<Set<OWLAxiom>> cut = new HashSet<Set<OWLAxiom>>();
		PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(B);
		manager.addOntologyChangeListener(reasoner);

		Queue<Set<OWLAxiom>> queue = new LinkedList<Set<OWLAxiom>>();
		Set<OWLAxiom> element = null;
		Set<OWLAxiom> candidate = null;
		Set<OWLAxiom> hn;
		boolean haveToContinue = false;

		Set<OWLAxiom> exp = null;

		boolean isConsistent = reasoner.isConsistent();
		reasonerCalls++;
		if (isConsistent)
			return kernel;

		exp = B.getAxioms();

		element = kernelElement(exp);
		kernel.add(element);
		for (OWLAxiom axiom : element) {
			Set<OWLAxiom> set = new HashSet<OWLAxiom>();
			set.add(axiom);
			queue.add(set);
		}
		// Reiter's algorithm
		while (!queue.isEmpty()) {
			hn = queue.remove();

			haveToContinue = false;
			for (Set<OWLAxiom> set : cut) {
				// Check if there is an element of cut that is in hn
				if (hn.containsAll(set)) {
					haveToContinue = true;
					break;
				}
			}
			if (haveToContinue)
				continue;

			for (OWLAxiom axiom : hn) {
				RemoveAxiom removeAxiom = new RemoveAxiom(B, axiom);
				manager.applyChange(removeAxiom);
			}
			exp = B.getAxioms();
			isConsistent = reasoner.isConsistent();
			reasonerCalls++;
			if (!isConsistent) {
				candidate = kernelElement(exp);
//				System.out.println(candidate);
				kernel.add(candidate);
				for (OWLAxiom axiom : candidate) {
					Set<OWLAxiom> set2 = new HashSet<OWLAxiom>();
					set2.addAll(hn);
					set2.add(axiom);
					queue.add(set2);
				}
			} else
				cut.add(hn);

			// Restore to the ontology the axioms removed so it can be used
			// again
			for (OWLAxiom axiom : hn) {
				AddAxiom addAxiom = new AddAxiom(B, axiom);
				manager.applyChange(addAxiom);
			}
		}

		return kernel;
	}

	public static Set<OWLAxiom> kernelElement(Set<OWLAxiom> exp) throws OWLOntologyCreationException, OWLOntologyChangeException {
//		System.out.println("Kernel Element" + exp.size());
		// X eh um elemento do kernel
		Set<OWLAxiom> X = new HashSet<OWLAxiom>();

		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology ont = manager.createOntology();
		PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
		manager.addOntologyChangeListener(reasoner);

		// First Part: EXPAND
		// Adicionamos os axiomas de exp na ontologia criada ateh que alpha
		// seja inferido (esteja contido nas consequencias)
		for (OWLAxiom axiom : exp) {
			AddAxiom addAxiom = new AddAxiom(ont, axiom);
			manager.applyChange(addAxiom);
			boolean isConsistent = reasoner.isConsistent();
			reasonerCalls++;
			if (!isConsistent)
				break;
		}

		// Second Part: SHRINK
		// Para cada axioma em exp, removemo-lo do conjunto ont (se contido) e
		// verificamos
		// se alpha nao eh mais valido. Nesse caso, o axioma eh necessario para
		// inferir alpha e entao ele deve fazer parte de X que pertence ao
		// kernel
		for (OWLAxiom axiom : exp) {
			if (ont.containsAxiom(axiom)) {
				RemoveAxiom removeAxiom = new RemoveAxiom(ont, axiom);
				manager.applyChange(removeAxiom);
				boolean isConsistent = reasoner.isConsistent();
				reasonerCalls++;
				if (isConsistent) {
					X.add(axiom);
					AddAxiom addAxiom = new AddAxiom(ont, axiom);
					manager.applyChange(addAxiom);
				}
			}
		}
		return X;
	}

	
	
	
}
