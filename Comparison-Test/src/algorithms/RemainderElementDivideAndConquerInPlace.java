package algorithms;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import util.Util;

import java.util.*;

public class RemainderElementDivideAndConquerInPlace extends RemainderElement {


	public OWLOntologyManager manager;

	public PelletReasoner reasoner;


	public Set<OWLAxiom> findRemainderElement(Set<OWLAxiom> kb, Set<OWLAxiom> hn, OWLAxiom entailment) throws OWLOntologyCreationException {
		Util.print(kb);
		this.reasonerCallsTimes = new ArrayList<>();
		manager = OWLManager.createOWLOntologyManager();
		OWLOntology ontology = manager.createOntology();
		List<OWLAxiom> allAxioms = new ArrayList<>();
		allAxioms.addAll(kb);
		reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ontology);
		manager.addOntologyChangeListener(reasoner);
		if (hn == null)
			hn = new HashSet<>();
		allAxioms.removeAll(hn);

		boolean useConsistencyCheck = false;

		if (entailment != null) {
			if (entailment.isOfType(AxiomType.SUBCLASS_OF)) {
				OWLSubClassOfAxiom subClassAxiom = (OWLSubClassOfAxiom) entailment;
				OWLClassExpression left = subClassAxiom.getSubClass();
				OWLClassExpression right = subClassAxiom.getSuperClass();
				if (left.isOWLThing() && right.isOWLNothing()) {
					useConsistencyCheck = true;
				}
			}
		}
		this.executionTime = System.currentTimeMillis();
		Set<OWLAxiom> toReturn = remainderElement(ontology, 0, allAxioms.size(), allAxioms, hn, entailment, useConsistencyCheck);
		toReturn.addAll(hn);
		this.executionTime = System.currentTimeMillis()- this.executionTime;
		return toReturn;

	}

	private Set<OWLAxiom> remainderElement(OWLOntology kb, int start, int end, List<OWLAxiom> allAxioms, Set<OWLAxiom> hn, OWLAxiom entailment, boolean useConsistencyCheck) throws OWLOntologyCreationException {
		manager.addAxioms(kb, hn);
		boolean condition = false;
		if (useConsistencyCheck) {
			double time = System.currentTimeMillis();
			condition = reasoner.isConsistent();
			this.reasonerCallsTimes.add(System.currentTimeMillis() - time);
		} else {
			double time = System.currentTimeMillis();
			condition = !reasoner.isEntailed(entailment);
			this.reasonerCallsTimes.add(System.currentTimeMillis() - time);
		}
		manager.removeAxioms(kb, hn);
		if (!condition) {
			return new HashSet<>();
		}

		if (end - start == 1) {
			manager.addAxioms(kb, new HashSet<>(allAxioms.subList(start, end)));
			manager.addAxioms(kb, hn);
			if (useConsistencyCheck) {
				double time = System.currentTimeMillis();
				condition = reasoner.isConsistent();
				this.reasonerCallsTimes.add(System.currentTimeMillis() - time);
			} else {
				double time = System.currentTimeMillis();
				condition = !reasoner.isEntailed(entailment);
				this.reasonerCallsTimes.add(System.currentTimeMillis() - time);
			}
			manager.removeAxioms(kb, hn);

			if (condition) {
				return kb.getAxioms();
			} else {
				manager.removeAxioms(kb, new HashSet<>(allAxioms.subList(start, end)));
			}
			return new HashSet<>();
		}
		int middle = (start + end) / 2;

		manager.addAxioms(kb, new HashSet<>(allAxioms.subList(start, middle)));
		manager.addAxioms(kb, hn);
		if (useConsistencyCheck) {
			double time = System.currentTimeMillis();
			condition = reasoner.isConsistent();
			this.reasonerCallsTimes.add(System.currentTimeMillis() - time);
		} else {
			double time = System.currentTimeMillis();
			condition = !reasoner.isEntailed(entailment);
			this.reasonerCallsTimes.add(System.currentTimeMillis() - time);
		}
		manager.removeAxioms(kb, hn);
		if (condition) {

			manager.addAxioms(kb, remainderElement(kb, middle, end, allAxioms, hn, entailment, useConsistencyCheck));
		} else {
			manager.removeAxioms(kb, new HashSet<>(allAxioms.subList(start, middle)));
			manager.addAxioms(kb, new HashSet<>(allAxioms.subList(middle, end)));

			manager.addAxioms(kb, hn);
			if (useConsistencyCheck) {
				double time = System.currentTimeMillis();
				condition = reasoner.isConsistent();
				this.reasonerCallsTimes.add(System.currentTimeMillis() - time);
			} else {
				double time = System.currentTimeMillis();
				condition = !reasoner.isEntailed(entailment);
				this.reasonerCallsTimes.add(System.currentTimeMillis() - time);
			}
			manager.removeAxioms(kb, hn);

			if (condition) {
				manager.addAxioms(kb, remainderElement(kb, start, middle, allAxioms, hn, entailment, useConsistencyCheck));
			} else {
				manager.removeAxioms(kb, new HashSet<>(allAxioms.subList(middle, end)));
				remainderElement(kb, start, middle, allAxioms, hn, entailment, useConsistencyCheck);
				reasoner.refresh();
				remainderElement(kb, middle, end, allAxioms, hn, entailment, useConsistencyCheck);
			}
		}
		return kb.getAxioms();
	}

	public Map<StatisticMetric,StatisticValue> getStatistics(){
		Map<StatisticMetric, StatisticValue> toReturn = super.getStatistics();

		StatisticMetric executionTimeMetric = new StatisticMetric(Util.DIVIDE_AND_CONQUER_REMAINDER_ELEMENT, Util.DIVIDE_AND_CONQUER_REMAINDER_ELEMENT_FORMAT);
		StatisticValue executionTimeValue = new StatisticValue(Util.DIVIDE_AND_CONQUER_REMAINDER_ELEMENT_NUMBER_FORMAT, this.executionTime);
		toReturn.put(executionTimeMetric,executionTimeValue);
		return toReturn;
	}



}
