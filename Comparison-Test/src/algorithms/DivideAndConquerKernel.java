package algorithms;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.RemoveAxiom;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;


public class DivideAndConquerKernel {
	
	
	private static int reasonerCalls;

	public static void testSingleKernelElement(OWLOntology o) throws OWLOntologyCreationException {
		reasonerCalls=0;
		double time;
		time = System.currentTimeMillis();
		Set<OWLAxiom> kernel = aDivideAndConquerKernel(o.getAxioms());
		System.out.print((System.currentTimeMillis() - time)+"\t\t");
		System.out.print(kernel.size()+"\t\t");
		System.out.print(reasonerCalls+"\t\t");
	}
	
	
	public static Set<OWLAxiom> aDivideAndConquerKernel(Set<OWLAxiom> exp) throws OWLOntologyCreationException{
		reasonerCalls=0;
		return divideAndConquerBlackBox(new ArrayList<OWLAxiom>(exp));
	}
	
	private static Set<OWLAxiom> divideAndConquerBlackBox(List<OWLAxiom> ontology) throws OWLOntologyCreationException{
		List<OWLAxiom> l1 = ontology.subList(0, ontology.size()/2);
		List<OWLAxiom> l2 = ontology.subList(ontology.size()/2, ontology.size());
		Set<OWLAxiom> toReturn = new HashSet<OWLAxiom>();
		
		if(ontology.size()<=2){
			toReturn.addAll(ontology);
			return toReturn;
		}
		
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology ont = manager.createOntology(new HashSet<OWLAxiom>(l1));
		PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
		manager.addOntologyChangeListener(reasoner);
		
		if(!reasoner.isConsistent()){
			return divideAndConquerBlackBox(l1);
		}
		else{
			ont = manager.createOntology(new HashSet<OWLAxiom>(l2));
			reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
			manager.addOntologyChangeListener(reasoner);
			if(!reasoner.isConsistent()){
				return divideAndConquerBlackBox(l2);
			}
			else{
				List<OWLAxiom> l11 = new ArrayList<OWLAxiom>(l1.subList(0, l1.size()/2));
				l11.addAll(l2);
				ont = manager.createOntology(new HashSet<>(l11));
				reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
				manager.addOntologyChangeListener(reasoner);
				if(!reasoner.isConsistent()){
					return divideAndConquerBlackBox(l11);
				}
				else{
					List<OWLAxiom> l12 = new ArrayList<OWLAxiom>(l1.subList(l1.size()/2,l1.size()));
					l12.addAll(l2);
					ont = manager.createOntology(new HashSet<OWLAxiom>(l12));
					reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
					manager.addOntologyChangeListener(reasoner);
					if(!reasoner.isConsistent()){
						return divideAndConquerBlackBox(l12);
					}
					else{
						List<OWLAxiom> l21 = new ArrayList<OWLAxiom>(l2.subList(0, l2.size()/2));
						l21.addAll(l1);
						ont = manager.createOntology(new HashSet<OWLAxiom>(l21));
						reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
						manager.addOntologyChangeListener(reasoner);
						if(!reasoner.isConsistent()){
							return divideAndConquerBlackBox(l21);
						}
						else{
							List<OWLAxiom> l22 = new ArrayList<OWLAxiom>(l2.subList(l2.size()/2, l2.size()));
							l22.addAll(l1);
							ont = manager.createOntology(new HashSet<OWLAxiom>(l22));
							reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
							manager.addOntologyChangeListener(reasoner);
							boolean isConsistent =reasoner.isConsistent();
							reasonerCalls++;
							if(!isConsistent){
								return divideAndConquerBlackBox(l22);
							}
							else{
								
								//Shrink!
								Set<OWLAxiom> exp = new HashSet<OWLAxiom>(l1);
								exp.addAll(l2);
								ont = manager.createOntology(exp);
								reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
								manager.addOntologyChangeListener(reasoner);
								for (OWLAxiom axiom : exp) {
									if (ont.containsAxiom(axiom)) {
										RemoveAxiom removeAxiom = new RemoveAxiom(ont, axiom);
										manager.applyChange(removeAxiom);
										isConsistent = reasoner.isConsistent();
										reasonerCalls++;
										if (isConsistent) {
											toReturn.add(axiom);
											AddAxiom addAxiom = new AddAxiom(ont, axiom);
											manager.applyChange(addAxiom);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return toReturn;
	}
	
	
	public static void main(String[] args) throws OWLOntologyCreationException {
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		OWLOntology o = m.loadOntologyFromOntologyDocument(new File("benchmark/horridge-data/inconsistent/ontology-for-disease-genetic-investigation/ontology-for-disease-genetic-investigation.owl.xml"));
		Set<OWLOntology> imports = o.getImportsClosure();
		Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();
		axioms.addAll(o.getABoxAxioms(false));
		axioms.addAll(o.getTBoxAxioms(false));
		for (OWLOntology imported : imports) {
			axioms.addAll(imported.getABoxAxioms(false));
			axioms.addAll(imported.getTBoxAxioms(false));
		
		}
		
		DivideAndConquerKernel.testSingleKernelElement(o);
		
	}
	

}
