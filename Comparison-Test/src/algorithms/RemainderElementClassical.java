package algorithms;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import util.Util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RemainderElementClassical extends RemainderElement{

	

	@Override
	public Set<OWLAxiom> findRemainderElement(Set<OWLAxiom> kb, Set<OWLAxiom> hn, OWLAxiom entailment) throws OWLOntologyCreationException {
		this.reasonerCallsTimes = new ArrayList<>();
		boolean useConsistencyCheck = false;
		if (entailment != null) {
			if (entailment.isOfType(AxiomType.SUBCLASS_OF)) {
				OWLSubClassOfAxiom subClassAxiom = (OWLSubClassOfAxiom) entailment;
				OWLClassExpression left = subClassAxiom.getSubClass();
				OWLClassExpression right = subClassAxiom.getSuperClass();
				if (left.isOWLThing() && right.isOWLNothing()) {
					useConsistencyCheck = true;
				}
			}
		}
		this.executionTime = System.currentTimeMillis();
		Set<OWLAxiom> toReturn = this.remainderElement(kb, hn, entailment, useConsistencyCheck);
		this.executionTime = System.currentTimeMillis() - this.executionTime;
		return toReturn;
	}
	
	public Set<OWLAxiom> remainderElement(Set<OWLAxiom> kb, Set<OWLAxiom> hn, OWLAxiom entailment, boolean useConsistencyCheck) throws OWLOntologyCreationException, OWLOntologyChangeException {

		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology ont = manager.createOntology();
		PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
		// Reasoner reasoner = new Reasoner(ont);
		manager.addOntologyChangeListener(reasoner);

		boolean condition = false;
		
		// remElem is the element of the remainder set to be returned
		Set<OWLAxiom> remElem = new HashSet<>();
		if (!(hn == null)) {
			remElem.addAll(hn);
			manager.addAxioms(ont, hn);
		}

		for (OWLAxiom axiom : kb) {
			manager.addAxiom(ont, axiom);
			
			if (useConsistencyCheck) {
				double time = System.currentTimeMillis();
				condition = !reasoner.isConsistent();
				this.reasonerCallsTimes.add(System.currentTimeMillis() - time);
			} else {
				double time = System.currentTimeMillis();
				condition = reasoner.isEntailed(entailment);
				this.reasonerCallsTimes.add(System.currentTimeMillis() - time);
			}
			if (condition) {
				manager.removeAxiom(ont, axiom);
				reasoner.refresh();
			}
			else
				remElem.add(axiom);
		}
		// At this point, remElem is one element of the remainder set
		return remElem;
	}

	public Map<StatisticMetric,StatisticValue> getStatistics(){
		Map<StatisticMetric,StatisticValue> toReturn = super.getStatistics();

		StatisticMetric executionTimeMetric = new StatisticMetric(Util.CLASSICAL_REMAINDER_ELEMENT, Util.CLASSICAL_REMAINDER_ELEMENT_FORMAT);
		StatisticValue executionTimeValue = new StatisticValue(Util.CLASSICAL_REMAINDER_ELEMENT_NUMBER_FORMAT, this.executionTime);
		toReturn.put(executionTimeMetric,executionTimeValue);

		return toReturn;
	}

}
