package algorithms;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;

import java.util.*;

import static util.Util.*;

public class ClassicalPartialMeet extends PartialMeet {


	public ClassicalPartialMeet(RemainderElement remainderElement){
		super(remainderElement);
	}
	
	
	public ClassicalPartialMeet(){
		super(new RemainderElementClassical());
	}
	
	
	
	public Map<StatisticMetric,StatisticValue> getStatistics(){
		Map<StatisticMetric, StatisticValue> toReturn = super.getStatistics();
		StatisticMetric classicalPartialMeetStatisticMetric = new StatisticMetric(CLASSICAL_PARTIAL_MEET,CLASSICAL_PARTIAL_MEET_FORMAT);
		StatisticValue classicalPartialMeetStatisticValue = new StatisticValue(CLASSICAL_PARTIAL_MEET_NUMBER_FORMAT,totalTime);
		toReturn.put(classicalPartialMeetStatisticMetric, classicalPartialMeetStatisticValue);

		return toReturn;
	}
	
	/**
	 * Method that compute all the elements of the remainder of B by alpha C
	 * using one element of the remainder (obtained by a black-box algorithm
	 * through the method remainderElement in this class)
	 * 
	 * @param B
	 *            - the ontology (belief base) on which we will apply the
	 *            contraction
	 * @param entailment
	 *            - the axiom (belief) by which we will contract
	 * 
	 * @return kernel
	 */
	public Set<Set<OWLAxiom>> partialMeet(OWLOntology B, OWLAxiom entailment) throws OWLOntologyCreationException, OWLOntologyChangeException {
		totalTime = System.currentTimeMillis();
		Set<Set<OWLAxiom>> remainderSet = new HashSet<>();
		boolean isConsistencyCheck = false;

		if (entailment != null) {
			if (entailment.isOfType(AxiomType.SUBCLASS_OF)) {
				OWLSubClassOfAxiom subClassAxiom = (OWLSubClassOfAxiom) entailment;
				OWLClassExpression left = subClassAxiom.getSubClass();
				OWLClassExpression right = subClassAxiom.getSuperClass();
				if (left.isOWLThing() && right.isOWLNothing()) {
					isConsistencyCheck = true;
				}
			}
		}
		
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(B);
		// Reasoner reasoner = new Reasoner(B);
		manager.addOntologyChangeListener(reasoner);

		Queue<Set<OWLAxiom>> queue = new LinkedList<Set<OWLAxiom>>();
		Set<OWLAxiom> element = null;
		Set<OWLAxiom> diff = null;
		Set<OWLAxiom> hn = null;
		Set<OWLAxiom> kb = null;
		boolean condition = false;
		if (isConsistencyCheck) {
			double time = System.currentTimeMillis();
			condition = reasoner.isConsistent();
			reasonerCallsTimes.add(System.currentTimeMillis() - time);
		} else {
			double time = System.currentTimeMillis();
			condition = !reasoner.isEntailed(entailment);
			reasonerCallsTimes.add(System.currentTimeMillis() - time);
		}
		if (condition) {
			return remainderSet;
		}
		kb = B.getAxioms();

		element = this.remainderElement.findRemainderElement(kb, null, entailment);
		remainderSet.add(element);
		diff = new HashSet<OWLAxiom>();
		diff.addAll(kb);
		diff.removeAll(element);
		for (OWLAxiom axiom : diff) {
			Set<OWLAxiom> set = new HashSet<OWLAxiom>();
			set.add(axiom);
			queue.add(set);
		}

		OWLOntology ont = manager.createOntology();
		reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);

		while (!queue.isEmpty()) {
			hn = queue.remove();
			manager.addAxioms(ont, hn);
			if (isConsistencyCheck) {
				double time = System.currentTimeMillis();
				condition = reasoner.isConsistent();
				reasonerCallsTimes.add(System.currentTimeMillis() - time);
			} else {
				double time = System.currentTimeMillis();
				condition = !reasoner.isEntailed(entailment);
				reasonerCallsTimes.add(System.currentTimeMillis() - time);
			}
			manager.removeAxioms(ont, hn);
			if (!condition) {
				continue;
			}
			element = this.remainderElement.findRemainderElement(kb, hn, entailment);
			remainderSet.add(element);
			diff.addAll(kb);
			diff.removeAll(element);
			for (OWLAxiom axiom : diff) {
				Set<OWLAxiom> set = new HashSet<OWLAxiom>();
				set.addAll(hn);
				set.add(axiom);
				queue.add(set);
			}
		}

		rSize = remainderSet.size();
		totalTime = System.currentTimeMillis() - totalTime;
		return remainderSet;
	}

}
