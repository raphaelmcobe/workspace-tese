package algorithms;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import util.Util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class RemainderElement {

	protected List<Double> reasonerCallsTimes;
	public double executionTime;

	public abstract Set<OWLAxiom> findRemainderElement(Set<OWLAxiom> kb, Set<OWLAxiom> hn, OWLAxiom entailment) throws OWLOntologyCreationException;
	
	public List<Double> getReasonerCallsTimes(){
		return this.reasonerCallsTimes;
	}

	public Map<StatisticMetric,StatisticValue> getStatistics(){
		Map<StatisticMetric,StatisticValue> toReturn = new HashMap<>();

		StatisticMetric reasonerCallsMetric = new StatisticMetric(Util.REASONER_CALLS, Util.REASONER_CALLS_FORMAT);
		StatisticValue reasonerCallsValue = new StatisticValue(Util.REASONER_CALLS_NUMBER_FORMAT, (double) reasonerCallsTimes.size());
		toReturn.put(reasonerCallsMetric,reasonerCallsValue);

		StatisticMetric reasonerCallsTotalTimeMetric = new StatisticMetric(Util.REASONER_CALLS_TOTAL_TIME, Util.REASONER_CALLS_TOTAL_TIME_FORMAT);
		double totalTime = 0.0;
		for(Double time : reasonerCallsTimes){
			totalTime+=time;
		}
		StatisticValue reasonerCallsTotalTimeValue = new StatisticValue(Util.REASONER_CALLS_TOTAL_TIME_NUMBER_FORMAT,totalTime);
		toReturn.put(reasonerCallsTotalTimeMetric,reasonerCallsTotalTimeValue);

		StatisticMetric reasonerCallsMeanTimeMetric = new StatisticMetric(Util.REASONER_CALLS_MEAN_TIME, Util.REASONER_CALLS_MEAN_TIME_FORMAT);
		StatisticValue reasonerCallsMeanTimeValue = new StatisticValue(Util.REASONER_CALLS_MEAN_TIME_NUMBER_FORMAT, (totalTime)/reasonerCallsTimes.size());
		toReturn.put(reasonerCallsMeanTimeMetric,reasonerCallsMeanTimeValue);

		return toReturn;
	}


}
