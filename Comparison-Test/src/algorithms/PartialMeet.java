package algorithms;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChangeException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import java.util.*;

import static util.Util.*;

public abstract class PartialMeet {

	protected List<Double> reasonerCallsTimes;
	
	protected RemainderElement remainderElement;

	protected double totalTime;

	protected double rSize;
	
	protected List<List<Double>> allRemaindersReasonerCallsTimes;
	
	public PartialMeet(RemainderElement remainderElement){
		this.reasonerCallsTimes = new ArrayList<>();
		this.rSize = 0;
		this.allRemaindersReasonerCallsTimes = new ArrayList<>();
		this.totalTime=0;
		this.remainderElement=remainderElement;
	}
	
	
	public abstract Set<Set<OWLAxiom>> partialMeet(OWLOntology B, OWLAxiom entailment)throws OWLOntologyChangeException, OWLOntologyCreationException; 
	
	public Map<StatisticMetric,StatisticValue> getStatistics(){
		Map<StatisticMetric,StatisticValue> toReturn = new HashMap<>();

		StatisticMetric remainderSizeStatisticMetric = new StatisticMetric(REMAINDER_SIZE,REMAINDER_SIZE_FORMAT);
		StatisticValue remainderSizeStatisticValue = new StatisticValue(REMAINDER_SIZE_NUMBER_FORMAT,rSize);
		toReturn.put(remainderSizeStatisticMetric, remainderSizeStatisticValue);
		
		List<Double> allReasonerCallsTimes = new ArrayList<>(reasonerCallsTimes);
		
		for (List<Double> eachRemainderReasonerCallsTimes : allRemaindersReasonerCallsTimes) {
			allReasonerCallsTimes.addAll(eachRemainderReasonerCallsTimes);
		}
		
		double reasonerCallsTotalTime=0;
		for (Double reasonerTime : allReasonerCallsTimes) {
			reasonerCallsTotalTime+=reasonerTime;
		}

		StatisticMetric reasonerCallsStatisticMetric = new StatisticMetric(REASONER_CALLS,REASONER_CALLS_FORMAT);
		StatisticValue reasonerCallsStatisticValue = new StatisticValue(REASONER_CALLS_NUMBER_FORMAT,(double) allReasonerCallsTimes.size());
		toReturn.put(reasonerCallsStatisticMetric,reasonerCallsStatisticValue);


		StatisticMetric reasonerCallsMeanTimeStatisticMetric = new StatisticMetric(REASONER_CALLS_MEAN_TIME,REASONER_CALLS_MEAN_TIME_FORMAT);
		StatisticValue reasonerCallsMeanTimeStatisticValue = new StatisticValue(REASONER_CALLS_MEAN_TIME_NUMBER_FORMAT,(reasonerCallsTotalTime/allReasonerCallsTimes.size()));
		toReturn.put(reasonerCallsMeanTimeStatisticMetric,reasonerCallsMeanTimeStatisticValue);

		StatisticMetric reasonerCallsTotalTimeStatisticMetric = new StatisticMetric(REASONER_CALLS_TOTAL_TIME, REASONER_CALLS_TOTAL_TIME_FORMAT);
		StatisticValue reasonerCallsTotalTimeStatisticValue = new StatisticValue(REASONER_CALLS_TOTAL_TIME_NUMBER_FORMAT,reasonerCallsTotalTime);
		toReturn.put(reasonerCallsTotalTimeStatisticMetric,reasonerCallsTotalTimeStatisticValue);

		return toReturn;
	}
	

}
