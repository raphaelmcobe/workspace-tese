package algorithms;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

public class RemainderElementDivideAndConquerCopyBased {
	public List<Double> reasonerCallsTimes;

	public OWLOntologyManager manager;

	public PelletReasoner reasoner;

	public int reusedNodes;

	public Set<OWLAxiom> remainderElementCopyBased(Set<OWLAxiom> kb, Set<OWLAxiom> hn) throws OWLOntologyCreationException {
		manager = OWLManager.createOWLOntologyManager();
		OWLOntology ontology = manager.createOntology();
		reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ontology);
		manager.addOntologyChangeListener(reasoner);
		if(hn==null)
			hn = new HashSet<OWLAxiom>();
		kb.removeAll(hn);
		Set<OWLAxiom> toReturn = remainderElement(ontology, kb, hn);
		toReturn.addAll(hn);
		return toReturn;
	}
	
	
	public Set<OWLAxiom> remainderElement(OWLOntology ont, Set<OWLAxiom> kb, Set<OWLAxiom> hn) throws OWLOntologyCreationException {
		double time = System.currentTimeMillis();
		boolean isConsistent = reasoner.isConsistent();
		reasonerCallsTimes.add(System.currentTimeMillis() - time);
		manager.removeAxioms(ont, hn);
		if(!isConsistent){
			return new HashSet<OWLAxiom>();
		}
		
		
		if (kb.size() == 1) {
			manager.addAxioms(ont, kb);
			manager.addAxioms(ont, hn);
			time = System.currentTimeMillis();
			isConsistent = reasoner.isConsistent();
			reasonerCallsTimes.add(System.currentTimeMillis() - time);
			manager.removeAxioms(ont, hn);
			if (isConsistent) {
				return kb;
			}
			else{
				manager.removeAxioms(ont, kb);
			}
			return new HashSet<OWLAxiom>();
		}
		List<OWLAxiom> allElements = new ArrayList<OWLAxiom>(kb);
		List<OWLAxiom> l1 = allElements.subList(0, allElements.size() / 2);
		List<OWLAxiom> l2 = allElements.subList(allElements.size() / 2, allElements.size());
		manager.addAxioms(ont, hn);
		manager.addAxioms(ont, new HashSet<OWLAxiom>(l1));
		time = System.currentTimeMillis();
		isConsistent = reasoner.isConsistent();
		reasonerCallsTimes.add(System.currentTimeMillis() - time);
		manager.removeAxioms(ont, hn);
		if (isConsistent) {
			manager.addAxioms(ont,remainderElement(ont, new HashSet<OWLAxiom>(l2), hn));
		} else {
			manager.removeAxioms(ont, new HashSet<OWLAxiom>(l1));
			manager.addAxioms(ont, new HashSet<OWLAxiom>(l2));
			manager.addAxioms(ont, hn);
			time = System.currentTimeMillis();
			isConsistent = reasoner.isConsistent();
			reasonerCallsTimes.add(System.currentTimeMillis() - time);
			manager.removeAxioms(ont, hn);
			if (isConsistent) {
				manager.addAxioms(ont,remainderElement(ont, new HashSet<OWLAxiom>(l1), hn));
			} else {
				manager.removeAxioms(ont, new HashSet<OWLAxiom>(l2));
				manager.addAxioms(ont, remainderElement(ont, new HashSet<OWLAxiom>(l2), hn));
				manager.addAxioms(ont, remainderElement(ont, new HashSet<OWLAxiom>(l1), hn));
			}
		}
		return hn;
	}
}
