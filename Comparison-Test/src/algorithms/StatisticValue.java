package algorithms;

/**
 * Created by rocknroll on 14/07/14.
 */
public class StatisticValue {

	private String format;

	private Double value;

	public StatisticValue(String format, Double value) {
		this.format = format;
		this.value = value;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}
}
