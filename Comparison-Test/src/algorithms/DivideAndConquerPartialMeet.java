package algorithms;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;

import java.io.IOException;
import java.util.*;

import static util.Util.*;

//import org.semanticweb.HermiT.Reasoner;

public class DivideAndConquerPartialMeet extends PartialMeet {

	private double reusedNodes;

	private double earlyTerminated;


	public DivideAndConquerPartialMeet(RemainderElement remainderElement) {
		super(remainderElement);
		this.reusedNodes = 0;
		this.earlyTerminated = 0;
	}
	
	public DivideAndConquerPartialMeet() {
		this(new RemainderElementDivideAndConquerInPlace());
	}
	

	public Map<StatisticMetric,StatisticValue> getStatistics(){
		Map<StatisticMetric, StatisticValue> toReturn = super.getStatistics();
		StatisticMetric divideAndConquerNodeStatisticMetric = new StatisticMetric(DIVIDE_AND_CONQUER_NODE_REUSE_EARLY_PATH_TERMINATION_PARTIAL_MEET,DIVIDE_AND_CONQUER_NODE_REUSE_EARLY_PATH_TERMINATION_PARTIAL_MEET_FORMAT);
		StatisticValue divideAndConquerNodeStatisticValue = new StatisticValue(DIVIDE_AND_CONQUER_NODE_REUSE_EARLY_PATH_TERMINATION_PARTIAL_MEET_NUMBER_FORMAT,totalTime);
		toReturn.put(divideAndConquerNodeStatisticMetric,divideAndConquerNodeStatisticValue);

		StatisticMetric reusedNodesStatisticMetric = new StatisticMetric(REUSED_NODES,REUSED_NODES_FORMAT);
		StatisticValue reusedNodesStatisticValue = new StatisticValue(REUSED_NODES_NUMBER_FORMAT,this.reusedNodes);
		toReturn.put(reusedNodesStatisticMetric,reusedNodesStatisticValue);

		StatisticMetric earlyTerminatedPathsStatisticMetric = new StatisticMetric(EARLY_TERMINATED_PATHS,EARLY_TERMINATED_PATHS_FORMAT);
		StatisticValue earlyTerminatedPathsStatisticValue = new StatisticValue(EARLY_TERMINATED_PATHS_NUMBER_FORMAT,this.earlyTerminated);
		toReturn.put(earlyTerminatedPathsStatisticMetric, earlyTerminatedPathsStatisticValue);

		return toReturn;
	}
	
	
	
	

	// public static void test(OWLOntology o) throws OWLOntologyChangeException,
	// UnsupportedOperationException, OWLException, IOException {
	// reasonerCallsTimes = new ArrayList<Double>();
	// double time;
	// time = System.currentTimeMillis();
	// Set<Set<OWLAxiom>> remainder = partialMeet(o);
	// System.out.print((System.currentTimeMillis() - time) + "\t\t");
	// System.out.print(remainder.size() + "\t\t");
	// System.out.print(reasonerCallsTimes.size() + " \t\t");
	// double total = 0;
	// for (Double callTime : reasonerCallsTimes) {
	// total += callTime;
	// }
	// System.out.printf("%.2f\t\t", total);
	// System.out.printf("%.2f\t\t", (total / reasonerCallsTimes.size()));
	// }
	//
	// public static void testV2(OWLOntology o, OWLAxiom entailment) throws
	// OWLOntologyChangeException,
	// UnsupportedOperationException, OWLException, IOException {
	// reasonerCallsTimes = new ArrayList<Double>();
	// reusedNodes = 0;
	// double time;
	// time = System.currentTimeMillis();
	// Set<Set<OWLAxiom>> remainder = partialMeet(o,entailment);
	// System.out.print((System.currentTimeMillis() - time) + "\t\t");
	// System.out.print(remainder.size() + "\t\t");
	// System.out.print(reasonerCallsTimes.size() + " \t\t");
	// double total = 0;
	// for (Double callTime : reasonerCallsTimes) {
	// total += callTime;
	// }
	// System.out.printf("%.2f\t\t", total);
	// System.out.printf("%.2f\t\t", (total / reasonerCallsTimes.size()));
	// System.out.print(reusedNodes + "\t\t");
	// }
	//
	// public static void testSingleRemainder(OWLOntology o) throws
	// OWLOntologyChangeException,
	// OWLOntologyCreationException {
	// this
	// reasonerCallsTimes = new ArrayList<Double>();
	// double time;
	// time = System.currentTimeMillis();
	// Set<OWLAxiom> remainder = remainderElementCopyBased(o.getAxioms(), null);
	// System.out.print((System.currentTimeMillis() - time) + "\t\t");
	// System.out.print(remainder.size() + "\t\t");
	// System.out.print(reasonerCallsTimes.size() + " \t\t");
	// double total = 0;
	// for (Double callTime : reasonerCallsTimes) {
	// }
	// total += callTime;
	// System.out.printf("%.2f\t\t", total);
	// System.out.printf("%.2f\t\t", (total / reasonerCallsTimes.size()));
	// }
	//
	// public static void testSingleRemainderV2(OWLOntology o, OWLAxiom
	// entailment) throws OWLOntologyChangeException,
	// OWLOntologyCreationException {
	// reasonerCallsTimes = new ArrayList<Double>();
	//
	// double time;
	// time = System.currentTimeMillis();
	// Set<OWLAxiom> remainder =
	// remainderElement.InPlace(o.getAxioms(),null,entailment);
	// System.out.print((System.currentTimeMillis() - time) + "\t\t");
	// System.out.print(remainder.size() + "\t\t");
	// System.out.print(reasonerCallsTimes.size() + " \t\t");
	// double total = 0;
	// for (Double callTime : reasonerCallsTimes) {
	// total += callTime;
	// }
	// System.out.printf("%.2f\t\t", total);
	// System.out.printf("%.2f\t\t", (total / reasonerCallsTimes.size()));
	// }

	public Set<Set<OWLAxiom>> partialMeet(OWLOntology B, OWLAxiom entailment) throws OWLOntologyChangeException, OWLOntologyCreationException {
		totalTime = System.currentTimeMillis();
		boolean isConsistencyCheck = false;

		if (entailment != null) {
			if (entailment.isOfType(AxiomType.SUBCLASS_OF)) {
				OWLSubClassOfAxiom subClassAxiom = (OWLSubClassOfAxiom) entailment;
				OWLClassExpression left = subClassAxiom.getSubClass();
				OWLClassExpression right = subClassAxiom.getSuperClass();
				if (left.isOWLThing() && right.isOWLNothing()) {
					isConsistencyCheck = true;
				}
			}
		}

		Set<Set<OWLAxiom>> remainderSet = new HashSet<Set<OWLAxiom>>();
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(B);
		manager.addOntologyChangeListener(reasoner);

		Queue<Set<OWLAxiom>> queue = new LinkedList<>();
		Set<OWLAxiom> element = null;
		Set<OWLAxiom> diff = null;
		Set<OWLAxiom> hn = null;
		Set<OWLAxiom> exp = null;
		boolean condition = false;
		if (isConsistencyCheck) {
			double time = System.currentTimeMillis();
			condition = reasoner.isConsistent();
			reasonerCallsTimes.add(System.currentTimeMillis() - time);
		} else {
			double time = System.currentTimeMillis();
			condition = !reasoner.isEntailed(entailment);
			reasonerCallsTimes.add(System.currentTimeMillis() - time);
		}
		if (condition) {
			return remainderSet;
		}
		exp = B.getAxioms();
		element = this.remainderElement.findRemainderElement(exp, null, entailment);
		this.allRemaindersReasonerCallsTimes.add(this.remainderElement.getReasonerCallsTimes());
		remainderSet.add(element);
		diff = new HashSet<OWLAxiom>(exp);
		diff.removeAll(element);
		for (OWLAxiom axiom : diff) {
			Set<OWLAxiom> set = new HashSet<OWLAxiom>();
			set.add(axiom);
			queue.add(set);
		}
		OWLOntology ont = manager.createOntology();
		reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);

		while (!queue.isEmpty()) {
			hn = queue.remove();
			manager.addAxioms(ont, hn);

			if (isConsistencyCheck) {
				double time = System.currentTimeMillis();
				condition = reasoner.isConsistent();
				reasonerCallsTimes.add(System.currentTimeMillis() - time);
			} else {
				double time = System.currentTimeMillis();
				condition = !reasoner.isEntailed(entailment);
				reasonerCallsTimes.add(System.currentTimeMillis() - time);
			}
			manager.removeAxioms(ont, hn);
			if (!condition) {
				continue;
			}
			boolean reused = false;
			for (Set<OWLAxiom> set : remainderSet) {
				if (set.containsAll(hn)) {
					element = set;
					reused = true;
					reusedNodes++;
					break;
				}
			}
			if (!reused) {
				element = this.remainderElement.findRemainderElement(exp, hn, entailment);
				this.allRemaindersReasonerCallsTimes.add(this.remainderElement.getReasonerCallsTimes());
			}
			remainderSet.add(element);
			diff.addAll(exp);
			diff.removeAll(element);
			for (OWLAxiom axiom : diff) {
				Set<OWLAxiom> set = new HashSet<OWLAxiom>();
				set.addAll(hn);
				set.add(axiom);
				if (queue.contains(set)) {
					earlyTerminated++;
				} else {
					queue.add(set);
				}
			}
		}
		rSize = remainderSet.size();
		totalTime = System.currentTimeMillis() - totalTime;
		return remainderSet;
	}

	// public static Set<OWLAxiom> remainderElement

	public static void main(String[] args) throws OWLOntologyChangeException, UnsupportedOperationException, OWLException, IOException {
		// test1();
		// testV2();
		// testV3();
		// testV4();

	}

	// private static void testV4() throws OWLOntologyChangeException,
	// UnsupportedOperationException, OWLException, IOException {
	//
	// for (int i = 4; i <= 4; i++) {
	// reasonerCallsTimes = new ArrayList<Double>();
	// OWLOntologyManager m = OWLManager.createOWLOntologyManager();
	// OWLOntology tmp = m.loadOntologyFromOntologyDocument(new File(
	// "benchmark/input/LargeKernelWith" + i + "classes.owl"));
	// Set<OWLAxiom> tbox = tmp.getTBoxAxioms(false);
	// Set<OWLAxiom> abox = tmp.getABoxAxioms(false);
	// tbox.addAll(abox);
	// OWLOntology o = m.createOntology(tbox);
	// System.out.println("Ontology: ");
	// Util.print(tbox);
	//
	// TestRemainder.reasonerCallsTimes = new ArrayList<Double>();
	// Set<Set<OWLAxiom>> remainderSet = TestRemainder.partialMeet(o);
	// System.out.println("Remainder Size: " + remainderSet.size());
	// System.out.println("Remainders: ");
	// Util.printSet(remainderSet);
	// // System.out.println("\n Remainder: "+remainder+"\n");
	// // System.out.println("Calls: " +
	// TestRemainder.reasonerCallsTimes.size());
	// // double total = 0;
	// // for (Double callTime : TestRemainder.reasonerCallsTimes) {
	// // total += callTime;
	// // }
	// // System.out.println("Total: " + total);
	// // System.out.println("Average: " + total /
	// TestRemainder.reasonerCallsTimes.size());
	// //
	// // System.out.println("Sane? "+Util.remainderSanityCheck(o.getAxioms(),
	// remainderSet));
	// System.out.println("-------------------------------------");
	// System.out.println("Partial Meet Original Implementation");
	//
	// DivideAndConquerPartialMeet partialMeet = new
	// DivideAndConquerPartialMeet();
	//
	// Set<Set<OWLAxiom>>remainderSet2 = partialMeet.partialMeet(o);
	// System.out.println("Remainder Size: "+remainderSet2.size());
	// System.out.println("Remainders: ");
	// Util.printSet(remainderSet2);
	//
	//
	//
	// // System.out.print("\n");
	// //
	// System.out.println("-------------------------------------");
	// System.out.println("Optimized Partial Meet");
	// OWLDataFactory df = m.getOWLDataFactory();
	// OWLClass top = df.getOWLThing();
	// OWLClass bottom = df.getOWLNothing();
	// OWLAxiom conflict = df.getOWLSubClassOfAxiom(top, bottom);
	// Set<Set<OWLAxiom>> remainderSet3 = partialMeetV2(o,conflict);
	// System.out.println("Remainder Size: " + remainderSet3.size());
	// System.out.println("Remainders: ");
	// Util.printSet(remainderSet3);
	// // System.out.println("Calls: " + reasonerCallsTimes.size());
	// // total = 0;
	// // for (Double callTime : reasonerCallsTimes) {
	// // total += callTime;
	// // }
	// // System.out.println("Total: " + total);
	// // System.out.println("Average: " + total / reasonerCallsTimes.size());
	// //
	// // System.out.println("Sane? "+Util.remainderSanityCheck(o.getAxioms(),
	// remainderSet2));
	// // System.out.print("\n");
	// //
	// System.out.println("3 in 1? "+(remainderSet.containsAll(remainderSet3)));
	// System.out.println("1 equals 2? "+(remainderSet.equals(remainderSet2)));
	// //
	// //
	// //
	// //
	// // System.out.println("Remainder1:");
	// // Util.printSet(remainderSet);
	// // System.out.println("Remainder2: ");
	// // Util.printSet(remainderSet2);
	// //
	// // System.out.println("Difference: ");
	// // Set<Set<OWLAxiom>> remainderCopy = new
	// HashSet<Set<OWLAxiom>>(remainderSet);
	// // remainderCopy.removeAll(remainderSet2);
	// // Util.printSet(remainderCopy);
	// //// for (Set<OWLAxiom> set : remainderCopy) {
	// //// for (Set<OWLAxiom> set2 : remainderSet2) {
	// //// if(set2.containsAll(set)){
	// //// System.out.println("Duplicate!");
	// //// Util.print(set2);
	// //// }
	// //// }
	// //// }
	// //// System.out.println();
	// ////
	// //
	// //
	// // tbox.removeAll(remainderSet.iterator().next());
	// // System.out.println("Rest: ");
	// // Util.print(tbox);
	//
	//
	//
	// }
	//
	// }

}
