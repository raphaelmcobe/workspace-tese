import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.OWLRendererException;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChangeException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import algorithms.DivideAndConquerKernel;
import algorithms.HorridgeKernel;
import algorithms.SimpleReiter;

import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

public class Principal {


	private static final int MAX_SIZE = 256;
	
	private static final int SMALL_KERNEL=1;
	private static final int LARGE_KERNEL=2;




	public static void main(String[] args) throws OWLOntologyChangeException, InterruptedException, UnsupportedOperationException, OWLException, IOException {
		
//		testHorridgeDataSingleElement(true,false,false);
		
//		testSingleElement(SMALL_KERNEL,true, false, false);
//		testSingleElement(LARGE_KERNEL,true, false, false);
//		test(SMALL_KERNEL,true, false, false);
		test(LARGE_KERNEL,true, false, false);
		
		testHorridgeData(true, false, false);
	}
	
	
	private static void testSingleElement(int testType, boolean testRemainder, boolean testKernel, boolean showRemainderOnKernelTest) throws OWLOntologyChangeException, InterruptedException, UnsupportedOperationException, OWLException, IOException{
		String type = (testType==SMALL_KERNEL)?"Small":"Large";
		if(testRemainder){
			System.out.println("Testing Remainder With "+type+" Kernel (Single Elements):");
			System.out.print("OSize\t\tRem\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tD&C\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tD&CV2\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\n");
			for(int i = 1; i<=MAX_SIZE; i++){
				testRemainderSingleElement("benchmark/input/"+type+"KernelWith"+i+"classes.owl");
			}
		}
		if(testKernel){
			System.out.println("\nTesting Kernel Set With "+type+" Kernel:");
			System.out.print("OSize\t\tH\t\t#K\t\tSR\t\t#K\t\t#RC\t\tSRW\t\t#K\t\t#RC\t\tD&C-K\t\t#K\t\t#RC");
			if(showRemainderOnKernelTest){
				System.out.println("\t\tRem\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tD&C\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tD&CV2\t\t#R\t\t#RC\t\tRCT\t\t#MRCT");
			}
			System.out.print("\n");
			for(int i = 1; i<=MAX_SIZE; i++){
				testKernelSingleElement("benchmark/input/"+type+"KernelWith"+i+"classes.owl", showRemainderOnKernelTest);
			}
		}
	}
	
	
	
	private static void test(int testType, boolean testRemainder, boolean testKernel, boolean showRemainderOnKernelTest) throws OWLOntologyChangeException, InterruptedException, UnsupportedOperationException, OWLException, IOException{
		String type = (testType==SMALL_KERNEL)?"Small":"Large";
		if(testRemainder){
			System.out.println("Testing Remainder With "+type+" Kernel (Full Set):");
			System.out.print("OSize\t\tRem\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tD&C\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tD&CV2\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tReN\n");
			for(int i = 1; i<=MAX_SIZE; i++){
				testRemainder("benchmark/input/"+type+"KernelWith"+i+"classes.owl");
			}
		}
		if(testKernel){
			System.out.println("\nTesting Kernel Set With "+type+" Kernel:");
			System.out.print("OSize\t\tH\t\t#K\t\tSR\t\t#K\t\t#RC\t\tSRW\t\t#K\t\t#RC\t\tD&C-K\t\t#K\t\t#RC");
			if(showRemainderOnKernelTest){
				System.out.println("\t\tRem\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tD&C\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tD&CV2\t\t#R\t\t#RC\t\tRCT\t\t#MRCT");
			}
			System.out.print("\n");
			for(int i = 1; i<=MAX_SIZE; i++){
				testKernel("benchmark/input/"+type+"KernelWith"+i+"classes.owl", showRemainderOnKernelTest);
			}
		}
	}
	

	
	private static void testHorridgeDataSingleElement(boolean testRemainder, boolean testKernel,boolean showRemainderOnKernelTest) throws OWLOntologyChangeException, InterruptedException, UnsupportedOperationException, OWLException, IOException {
		String rootDir = "benchmark/horridge-data/inconsistent";
		File dir = new File(rootDir);
		File[] subDirs = dir.listFiles();
		for (File file : subDirs) {
            if (file.isDirectory()) {
                File eachDir = new File(rootDir + "/" + file.getName());
                File[] ontologies = eachDir.listFiles();
                for (File ontology : ontologies) {
                    String fileName = ontology.getName();
                    if (fileName.contains(".owl.xml")) {
                        OWLOntologyManager m = OWLManager.createOWLOntologyManager();
                        OWLOntology o = m.loadOntologyFromOntologyDocument(new File(eachDir + "/"
                                + fileName));
                        Set<OWLOntology> imports = o.getImportsClosure();
                        Set<OWLAxiom> axioms = new HashSet<>();
                        axioms.addAll(o.getABoxAxioms(false));
                        axioms.addAll(o.getTBoxAxioms(false));
                        for (OWLOntology imported : imports) {
                            axioms.addAll(imported.getABoxAxioms(false));
                            axioms.addAll(imported.getTBoxAxioms(false));
                        }

                        OWLReasoner reasoner = PelletReasonerFactory.getInstance()
                                .createNonBufferingReasoner(o);

                        if (!reasoner.isConsistent()) {
                            System.out.println(fileName + ": " + axioms.size());

                            if (testRemainder) {
                                System.out.print("OSize\t\tRem\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tD&C\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tD&CV2\t\t#R\t\t#RC\t\tRCT\t\t\t#MRCT\n");
                                testRemainderSingleElement(eachDir + "/" + fileName);
                            }
                            if (testKernel) {
                                System.out.println("\nTesting Kernel Set With Small Kernel:");
                                System.out.print("OSize\t\tH\t\t#K\t\tSR\t\t#K\t\t#RC\t\tSRW\t\t#K\t\t#RC\t\tD&C-K\t\t#K\t\t#RC");
                                if (showRemainderOnKernelTest) {
                                    System.out.println("\t\tRem\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tD&C\t\t#R\t\t#RC\t\tRCT\t\t#MRCT\t\tD&CV2\t\t#R\t\t#RC\t\tRCT\t\t#MRCT");
                                }
                                System.out.print("\n");
                                for (int i = 1; i <= MAX_SIZE; i++) {
                                    testKernelSingleElement(eachDir + "/" + fileName, showRemainderOnKernelTest);
                                }
                            }
                        }
                    }

                }
            }
        }
	}
	
	
	
	
	
	private static void testHorridgeData(boolean testRemainder, boolean testKernel,	boolean showRemainderOnKernelTest) throws OWLOntologyChangeException, InterruptedException, UnsupportedOperationException, OWLException, IOException {
		String rootDir = "benchmark/horridge-data/inconsistent";
		File dir = new File(rootDir);
		File[] subDirs = dir.listFiles();
		for (File file : subDirs) {
			if (file.isDirectory()) {
				File eachDir = new File(rootDir + "/" + file.getName());
				File[] ontologies = eachDir.listFiles();
				for (File ontology : ontologies) {
					String fileName = ontology.getName();
					if (fileName.contains(".owl.xml")) {
						OWLOntologyManager m = OWLManager.createOWLOntologyManager();
						OWLOntology o = m.loadOntologyFromOntologyDocument(new File(eachDir + "/"
								+ fileName));
						Set<OWLOntology> imports = o.getImportsClosure();
						Set<OWLAxiom> axioms = new HashSet<>();
						axioms.addAll(o.getABoxAxioms(false));
						axioms.addAll(o.getTBoxAxioms(false));
						for (OWLOntology imported : imports) {
							axioms.addAll(imported.getABoxAxioms(false));
							axioms.addAll(imported.getTBoxAxioms(false));
						}

						OWLReasoner reasoner = PelletReasonerFactory.getInstance()
								.createNonBufferingReasoner(o);

						if (!reasoner.isConsistent()) {
							System.out.println(fileName + ": " + axioms.size());
							if (testKernel)
						 		testKernel(eachDir + "/" + fileName, showRemainderOnKernelTest);
							if (testRemainder)
								testRemainder(eachDir + "/" + fileName);
						}
					}

				}
			}
		}
	}
	
	private static void testKernel(String fileName, boolean showRemainderOnKernelTest) throws OWLOntologyCreationException, OWLOntologyChangeException, InterruptedException, OWLRendererException {
		
			OWLOntologyManager m = OWLManager.createOWLOntologyManager();
			OWLOntology o = m.loadOntologyFromOntologyDocument(new File(fileName));
			Set<OWLOntology> imports = o.getImportsClosure();
			Set<OWLAxiom> axioms = new HashSet<>();
			axioms.addAll(o.getABoxAxioms(false));
			axioms.addAll(o.getTBoxAxioms(false));
			for (OWLOntology imported : imports) {
				axioms.addAll(imported.getABoxAxioms(false));
				axioms.addAll(imported.getTBoxAxioms(false));
			}
		
			
			OWLDataFactory df = m.getOWLDataFactory();
			OWLClass top = df.getOWLThing();
			OWLClass bottom = df.getOWLNothing();
			OWLAxiom conflict = df.getOWLSubClassOfAxiom(top, bottom);
			
			System.out.print(o.getAxiomCount()+"\t");
			
			
			HorridgeKernel.test(axioms, conflict);
	
			
			SimpleReiter.test(o);
			
			TestSimpleReiterWithSlidingWindow.test(o,30);
			
			TestParallelReiter.test(o);
			System.out.print("\n");
		}

	
	
	private static void testRemainder(String fileName) throws OWLOntologyChangeException, InterruptedException, UnsupportedOperationException, OWLException, IOException {
		
			OWLOntologyManager m = OWLManager.createOWLOntologyManager();
			OWLOntology o = m.loadOntologyFromOntologyDocument(new File(fileName));
			Set<OWLOntology> imports = o.getImportsClosure();
			Set<OWLAxiom> axioms = new HashSet<>();
			axioms.addAll(o.getABoxAxioms(false));
			axioms.addAll(o.getTBoxAxioms(false));
			for (OWLOntology imported : imports) {
				axioms.addAll(imported.getABoxAxioms(false));
				axioms.addAll(imported.getTBoxAxioms(false));
			}
			
			
			OWLDataFactory df = m.getOWLDataFactory();
			OWLClass top = df.getOWLThing();
			OWLClass bottom = df.getOWLNothing();
			OWLAxiom conflict = df.getOWLSubClassOfAxiom(top, bottom);
			
			System.out.print(o.getAxiomCount()+"\t\t");
			
			
			TestRemainder.test(o);
			
//			DivideAndConquerPartialMeet.test(o);
			
//			DivideAndConquerPartialMeet.testV2(o,conflict);
			
			System.out.print("\n");
		}
	
	

	private static void testKernelSingleElement(String fileName, boolean showRemainderOnKernelTest) throws OWLOntologyChangeException, InterruptedException, UnsupportedOperationException, OWLException, IOException {
		
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		OWLOntology o = m.loadOntologyFromOntologyDocument(new File(fileName));
		Set<OWLOntology> imports = o.getImportsClosure();
		Set<OWLAxiom> axioms = new HashSet<>();
		axioms.addAll(o.getABoxAxioms(false));
		axioms.addAll(o.getTBoxAxioms(false));
		for (OWLOntology imported : imports) {
			axioms.addAll(imported.getABoxAxioms(false));
			axioms.addAll(imported.getTBoxAxioms(false));
		}

		OWLDataFactory df = m.getOWLDataFactory();
		OWLClass top = df.getOWLThing();
		OWLClass bottom = df.getOWLNothing();
		OWLAxiom conflict = df.getOWLSubClassOfAxiom(top, bottom);

		System.out.print(o.getAxiomCount() + "\t\t");


		HorridgeKernel.testSingleKernelElement(axioms, conflict);

		// testLundberg(axioms);

//		TestSimpleReiter.testSingleKernelElement(o);

//		TestSimpleReiterWithSlidingWindow.testSingleKernelElement(o, 10);

		// TestParallelReiter.testSingleKernelElement(o);

		DivideAndConquerKernel.testSingleKernelElement(o);

		if (showRemainderOnKernelTest) {
			TestRemainder.testSingleRemainder(o);
//			TestDivideAndConquerRemainder.testSingleRemainder(o);
//			DivideAndConquerPartialMeet.testSingleRemainderV2(o,conflict);
		}

		System.out.print("\n");
	}
	
	
	
	private static void testRemainderSingleElement(String fileName) throws OWLOntologyChangeException, InterruptedException, OWLOntologyCreationException{
		
		
			OWLOntologyManager m = OWLManager.createOWLOntologyManager();
			OWLOntology o = m.loadOntologyFromOntologyDocument(new File(fileName));
			Set<OWLOntology> imports = o.getImportsClosure();
			Set<OWLAxiom> axioms = new HashSet<>();
			axioms.addAll(o.getABoxAxioms(false));
			axioms.addAll(o.getTBoxAxioms(false));
			for (OWLOntology imported : imports) {
				axioms.addAll(imported.getABoxAxioms(false));
				axioms.addAll(imported.getTBoxAxioms(false));
			}
			
			OWLDataFactory df = m.getOWLDataFactory();
			OWLClass top = df.getOWLThing();
			OWLClass bottom = df.getOWLNothing();
			OWLAxiom conflict = df.getOWLSubClassOfAxiom(top, bottom);
			
			System.out.print(o.getAxiomCount()+"\t\t");
			
			TestRemainder.testSingleRemainder(o);
			
//			DivideAndConquerPartialMeet.testSingleRemainder(o);
//			DivideAndConquerPartialMeet.testSingleRemainderV2(o,conflict);
			
			System.out.print("\n");
		}
	
	
	

//	private static void testLundberg(Set<OWLAxiom> axioms) {
//		double time;
//		ISet<OWLSentence> base = ISets.empty();
//		for (OWLAxiom axiom : axioms) {
//			base = base.union(new OWLSentence(axiom));
//		}
//
//		OWLHermitReasoner r = new OWLHermitReasoner();
//		BlackboxKernelOperator<OWLSentence> blackbox = new BlackboxKernelOperator<OWLSentence>(r);
//
//		time = System.currentTimeMillis();
//		Kernel<OWLSentence> kernelSet = blackbox.eval(base);
//		System.out.print((System.currentTimeMillis() - time)+"\t");
//	}

	
	
}


