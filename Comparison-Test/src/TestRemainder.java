
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
















//import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChangeException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import com.clarkparsia.owlapi.explanation.io.manchester.ManchesterSyntaxExplanationRenderer;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

public class TestRemainder {
	
	

	public static List<Double> reasonerCallsTimes;
	
	
	
	public static void test(OWLOntology o) throws OWLOntologyChangeException, UnsupportedOperationException, OWLException, IOException {
		reasonerCallsTimes = new ArrayList<Double>();
		double time;
		time = System.currentTimeMillis();
		Set<Set<OWLAxiom>> remainder = partialMeet(o);
		System.out.print((System.currentTimeMillis() - time)+"\t\t");
		System.out.print(remainder.size()+"\t\t");
		System.out.print(reasonerCallsTimes.size()+"\t\t");
		double total = 0;
		for (Double callTime : reasonerCallsTimes) {
			total+=callTime;
		}
		System.out.printf("%.2f\t\t",total);
		System.out.printf("%.2f\t\t",(total/reasonerCallsTimes.size()));
		
	}
	
	
	public static void testSingleRemainder(OWLOntology o) throws OWLOntologyChangeException, OWLOntologyCreationException{
		reasonerCallsTimes = new ArrayList<Double>();
		double time;
		time = System.currentTimeMillis();
		Set<OWLAxiom> remainder = remainderElement(o.getAxioms(),null);
		System.out.print((System.currentTimeMillis() - time)+"\t\t");
		System.out.print(remainder.size()+"\t\t");
		System.out.print(reasonerCallsTimes.size()+"\t\t");
		double total = 0;
		for (Double callTime : reasonerCallsTimes) {
			total+=callTime;
		}
		System.out.printf("%.2f\t\t",total);
		System.out.printf("%.2f\t\t",(total/reasonerCallsTimes.size()));
		
	}
	
	
	/**
	 * Method that compute all the elements of the remainder of B by alpha C using
	 * one element of the remainder (obtained by a black-box algorithm through
	 * the method remainderElement in this class) 
	 * 
	 * @param B - the ontology (belief base) on which we will apply the contraction
	 * @param alpha - the axiom (belief) by which we will contract
	 * 
	 * @return kernel
	 * @throws IOException 
	 * @throws OWLException 
	 * @throws UnsupportedOperationException 
	 */
	public static Set<Set<OWLAxiom>> partialMeet(OWLOntology B) throws OWLOntologyChangeException, UnsupportedOperationException, OWLException, IOException{
		Set<Set<OWLAxiom>> remainderSet = new HashSet<Set<OWLAxiom>>();
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(B);
		//Reasoner reasoner = new Reasoner(B);
		manager.addOntologyChangeListener(reasoner);
				
    	Queue<Set<OWLAxiom>> queue = new LinkedList<Set<OWLAxiom>>();
    	Set<OWLAxiom> element = null;
    	Set<OWLAxiom> diff = null;
    	Set<OWLAxiom> hn = null;    
    	Set<OWLAxiom> exp = null;
		
		// If alpha is not entailed by the ontology, the contraction is done
    	double time = System.currentTimeMillis();
    	boolean isConsistent = reasoner.isConsistent();
    	reasonerCallsTimes.add(System.currentTimeMillis()-time);
    	if (isConsistent) {
    		return remainderSet;
    	}		
		exp = B.getAxioms();
				
		element = remainderElement(exp,null);
		remainderSet.add(element);
		diff = new HashSet<OWLAxiom>();
		diff.addAll(exp);
		diff.removeAll(element);
		for(OWLAxiom axiom : diff){
    		Set<OWLAxiom> set = new HashSet<OWLAxiom>();
    		set.add(axiom);
    		queue.add(set);
    	}
		
		OWLOntology ont = manager.createOntology();
		reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
		
		while(!queue.isEmpty()) {
			hn = queue.remove();
			manager.addAxioms(ont,hn);
			time = System.currentTimeMillis();
	    	isConsistent = reasoner.isConsistent();
	    	reasonerCallsTimes.add(System.currentTimeMillis()-time);
			manager.removeAxioms(ont,hn);
			if(!isConsistent) 
				continue;
    		
			element = remainderElement(exp,hn);
    		remainderSet.add(element);
    		diff.addAll(exp);
    		diff.removeAll(element);
    		for(OWLAxiom axiom : diff){
        		Set<OWLAxiom> set = new HashSet<OWLAxiom>();
        		set.addAll(hn);
        		set.add(axiom);
        		queue.add(set);
        	}
    	}
		
		return remainderSet;
	}		
		
	/**
	 * Method that computes, for contraction, one element of the remainder set of a
	 * belief set by alpha, starting from X.
	 * 
	 * @param set - the set of axioms from which we will extract one element of its remainder set
	 * @param hn - the axiom (belief) by which we want contract
	 * @return remElem - a remainder element
	 * @throws OWLOntologyCreationException 
	 * @throws IOException 
	 * @throws OWLException 
	 * @throws UnsupportedOperationException 
	 */
	public static Set<OWLAxiom> remainderElement(Set<OWLAxiom> set, Set<OWLAxiom> X) throws OWLOntologyChangeException, OWLOntologyCreationException{
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology ont = manager.createOntology();
		PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
		//Reasoner reasoner = new Reasoner(ont);
		manager.addOntologyChangeListener(reasoner);
		
		//remElem is the element of the remainder set to be returned
		Set<OWLAxiom> remElem = new HashSet<OWLAxiom>();
		if(!(X == null)) {
			remElem.addAll(X);
			manager.addAxioms(ont,X);
		}
		
		for (OWLAxiom axiom: set){				
			manager.addAxiom(ont, axiom);
			double time = System.currentTimeMillis();
			boolean isConsistent = reasoner.isConsistent(); 
			reasonerCallsTimes.add(System.currentTimeMillis()-time);
			if (!isConsistent)
				manager.removeAxiom(ont, axiom);
			else
				remElem.add(axiom);
		}
		// At this point, remElem is one element of the remainder set	
		return remElem;	
	}
	
	public static void main(String[] args) throws OWLOntologyChangeException, UnsupportedOperationException, OWLException, IOException {
		for(int i = 4; i<=4; i++){
			OWLOntologyManager m = OWLManager.createOWLOntologyManager();
			OWLOntology o = m.loadOntologyFromOntologyDocument(new File("benchmark/input/LargeKernelWith"+i+"classes.owl"));
			Set<OWLOntology> imports = o.getImportsClosure();
			Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();
			axioms.addAll(o.getABoxAxioms(false));
			axioms.addAll(o.getTBoxAxioms(false));
			for (OWLOntology imported : imports) {
				axioms.addAll(imported.getABoxAxioms(false));
				axioms.addAll(imported.getTBoxAxioms(false));
			}
		
			Set<Set<OWLAxiom>> remainder = partialMeet(o);
			reasonerCallsTimes = new ArrayList<Double>();
			OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
//			OWLOntology ont = manager.createOntology(remainder);
//			PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
			System.out.println("Size: "+remainder.size());
			System.out.println("\n Remainder: "+remainder+"\n");
			System.out.println(reasonerCallsTimes.size());
//			System.out.println("Consistent? "+reasoner.isConsistent()+"\t");

		}
	}
	

}

