package tests;

import algorithms.RemainderElementClassical;
import algorithms.RemainderElementDivideAndConquerInPlace;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import util.SanityChecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;

public class CompareClassicalRemainderElementWithDivideAndConquerRemainderElement {

	public static void main(String[] args) throws OWLOntologyCreationException, FileNotFoundException {
//		testConsistencyBigKernel();
//		testConsistencySmallKernel();
		testEntailmentBigKernel();
//		testHorridgeDataSingleElementConsistencyCheck();
//		testHorridgeDataSingleElementEntailed();
	}




	private static void testHorridgeDataSingleElementEntailed() throws OWLOntologyCreationException {
		String rootDir = "benchmark/horridge-data/entailed";
		File dir = new File(rootDir);
		File[] subDirs = dir.listFiles();
//		Collections.shuffle(subDirs);
		for (File file : subDirs) {
			if (file.isDirectory()) {
				File eachDir = new File(rootDir + "/" + file.getName());
				if(file.getName().contains("harder")) continue;
				OWLOntologyManager m = OWLManager.createOWLOntologyManager();

				OWLOntology entailments = m.loadOntologyFromOntologyDocument(new File(eachDir + "/" + "selectedentailments.xml"));
				OWLOntology o = m.loadOntologyFromOntologyDocument(new File(eachDir + "/" + file.getName()+".owl.xml"));

				PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(o);

				Set<OWLSubClassOfAxiom> entailmentAxioms = entailments.getAxioms(AxiomType.SUBCLASS_OF);

				int i = 0;
				System.out.println(file.getName()+ " - "+entailments.getAxiomCount());
				System.out.format("%-20s %-10s %-15s %-20s %-20s %-15s %-15s %-15s %-15s %-20s %-20s %-20s %-20s %-20s %-20s%n", "File", "I", "Size", "C", "D&C", "C-RCalls-T", "D&C-RCalls-T", "C-Size", "D&C-Size", "D&C in C", "C In D&C", "SCheck C", "SCheck D&C", "C-RCalls", "D&C-RCalls");
				for (OWLSubClassOfAxiom entailedAxiom : entailmentAxioms) {
					RemainderElementClassical classicalRemainder = new RemainderElementClassical();
					RemainderElementDivideAndConquerInPlace dacRemainder = new RemainderElementDivideAndConquerInPlace();
					double totalClassical = System.currentTimeMillis();
					Set<OWLAxiom> remainder1 = classicalRemainder.findRemainderElement(o.getAxioms(), null, entailedAxiom);
					totalClassical = System.currentTimeMillis() - totalClassical;
					double totalDaC = System.currentTimeMillis();
					Set<OWLAxiom> remainder2 = dacRemainder.findRemainderElement(o.getAxioms(), null, entailedAxiom);
					totalDaC = System.currentTimeMillis() - totalDaC;
					double rCallsRemainder1 = 0;
					for (Double callTime : classicalRemainder.getReasonerCallsTimes()) {
						rCallsRemainder1 += callTime;
					}

					double rCallsRemainder2 = 0;
					for (Double callTime : dacRemainder.getReasonerCallsTimes()) {
						rCallsRemainder2 += callTime;
					}


					System.out.format("%-20s %-10d %-15d %-20f %-20f %-15f %-15f %-15d %-15d %-20s %-20s %-20s %-20s %-20d %-20d%n",file.getName().substring(0,((file.getName().length()>=15)?15:file.getName().length())),i,o.getAxiomCount(),totalClassical,totalDaC,rCallsRemainder1,rCallsRemainder2,
							remainder1.size(),remainder2.size(),remainder1.containsAll(remainder2),remainder2.containsAll(remainder1),
							SanityChecker.checkSingleRemainderSanity(o, remainder1, entailedAxiom),
							SanityChecker.checkSingleRemainderSanity(o, remainder2, entailedAxiom),
							classicalRemainder.getReasonerCallsTimes().size(),dacRemainder.getReasonerCallsTimes().size());
					i++;
				}
//				File[] ontologies = eachDir.listFiles();

//				for (File ontology : ontologies) {
//					String fileName = ontology.getName();
//					System.out.println(eachDir+"/"+fileName);
//					OWLOntologyManager m = OWLManager.createOWLOntologyManager();
//					if (fileName.contains("selectedentailments.xml")) {
//						OWLOntology entailments = m.loadOntologyFromOntologyDocument(new File(eachDir + "/" + fileName));
//						Set<OWLAxiom> axioms = new HashSet<>();
//						axioms.addAll(o.getABoxAxioms(false));
//						axioms.addAll(o.getTBoxAxioms(false));
//
//						OWLReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(o);
//						System.out.println(fileName);
//					}else{
//						OWLOntology o = m.loadOntologyFromOntologyDocument(new File(eachDir + "/" + fileName));
//					}
//				}
			}
		}
	}

	private static void testHorridgeDataSingleElementConsistencyCheck() throws OWLOntologyCreationException, FileNotFoundException {
		System.setErr(new PrintStream("/tmp/err.log"));

		System.out.format("%-20s %-15s %-20s %-20s %-15s %-15s %-15s %-15s %-20s %-20s %-20s %-20s %-20s %-20s%n", "I", "Size", "C", "D&C", "C-RCalls-T","D&C-RCalls-T", "C-Size", "D&C-Size","D&C in C", "C In D&C", "SCheck C","SCheck D&C","C-RCalls","D&C-RCalls");

		String rootDir = "benchmark/horridge-data/inconsistent";
		File dir = new File(rootDir);
		File[] ontologies = dir.listFiles();
		for (File ontologyFile : ontologies) {
			String ontologyFileName = ontologyFile.getName();
			if (ontologyFileName.contains(".owl.xml")) {
				OWLOntologyManager m = OWLManager.createOWLOntologyManager();
				OWLDataFactory df = m.getOWLDataFactory();
				OWLOntology ontology = m.loadOntologyFromOntologyDocument(new File(rootDir+"/"+ontologyFileName));
				Set<OWLAxiom> axioms = new HashSet<>();
				axioms.addAll(ontology.getABoxAxioms(false));
				axioms.addAll(ontology.getTBoxAxioms(false));


				OWLClass top = df.getOWLThing();
				OWLClass bottom = df.getOWLNothing();
				OWLAxiom conflict = df.getOWLSubClassOfAxiom(top, bottom);


				PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ontology);
				m.addOntologyChangeListener(reasoner);



				RemainderElementClassical classicalRemainder = new RemainderElementClassical();
				RemainderElementDivideAndConquerInPlace dacRemainder = new RemainderElementDivideAndConquerInPlace();
				double totalClassical = System.currentTimeMillis();
				Set<OWLAxiom> remainder1 = classicalRemainder.findRemainderElement(ontology.getAxioms(), null, conflict);
				totalClassical = System.currentTimeMillis() - totalClassical;
				double totalDaC = System.currentTimeMillis();
				Set<OWLAxiom> remainder2 = dacRemainder.findRemainderElement(ontology.getAxioms(), null, conflict);
				totalDaC = System.currentTimeMillis() - totalDaC;
				double rCallsRemainder1 = 0;
				for (Double callTime : classicalRemainder.getReasonerCallsTimes()) {
					rCallsRemainder1 += callTime;
				}

				double rCallsRemainder2 = 0;
				for (Double callTime : dacRemainder.getReasonerCallsTimes()) {
					rCallsRemainder2 += callTime;
				}

				System.out.format("%-20s %-15d %-20f %-20f %-15f %-15f %-15d %-15d %-20s %-20s %-20s %-20s %-20d %-20d%n",ontologyFileName.substring(0,((ontologyFileName.length()>=15)?15:ontologyFileName.length())),ontology.getAxiomCount(),totalClassical,totalDaC,rCallsRemainder1,rCallsRemainder2,
						remainder1.size(),remainder2.size(),remainder1.containsAll(remainder2),remainder2.containsAll(remainder1),
						SanityChecker.checkSingleRemainderSanity(ontology, remainder1, conflict),
						SanityChecker.checkSingleRemainderSanity(ontology, remainder2, conflict),
						classicalRemainder.getReasonerCallsTimes().size(),dacRemainder.getReasonerCallsTimes().size());


//				System.out.println(ontologyFileName + "\t\t\t\t" + ontology.getAxiomCount() + "\t\t" + totalClassical + "\t\t" + totalDaC + "\t\t" + rCallsRemainder1 + "\t\t\t" + rCallsRemainder2 +
//						"\t\t\t" + remainder1.size() + "\t\t" + remainder2.size() + "\t\t\t" + remainder1.containsAll(remainder2) + "\t\t\t" + remainder2.containsAll(remainder1) +
//						"\t\t\t" + SanityChecker.checkSingleRemainderSanity(ontology, remainder1, conflict) +
//						"\t\t\t" + SanityChecker.checkSingleRemainderSanity(ontology, remainder2, conflict) +
//						"\t\t\t" + classicalRemainder.getReasonerCallsTimes().size() + "\t\t\t" + dacRemainder.getReasonerCallsTimes().size());
			}
		}
	}


	public static void testConsistencySmallKernel() throws OWLOntologyCreationException{
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		System.out.format("%-20s %-15s %-20s %-20s %-15s %-15s %-15s %-15s %-20s %-20s %-20s %-20s %-20s %-20s%n", "I", "Size", "C", "D&C", "C-RCalls-T","D&C-RCalls-T", "C-Size", "D&C-Size","D&C in C", "C In D&C", "SCheck C","SCheck D&C","C-RCalls","D&C-RCalls");
		for (int i = 1; i <= 128; i++) {
			OWLOntology o = m.loadOntologyFromOntologyDocument(new File("benchmark/input/SmallKernelWith" + i + "classes.owl"));
			Set<OWLAxiom> axioms = new HashSet<>();
			axioms.addAll(o.getABoxAxioms(false));
			axioms.addAll(o.getTBoxAxioms(false));

			OWLOntology ontology = m.createOntology(axioms);
			OWLDataFactory df = m.getOWLDataFactory();

			OWLClass top = df.getOWLThing();
			OWLClass bottom = df.getOWLNothing();
			OWLAxiom conflict = df.getOWLSubClassOfAxiom(top, bottom);

			PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ontology);
			m.addOntologyChangeListener(reasoner);

			RemainderElementClassical classicalRemainder = new RemainderElementClassical();
			RemainderElementDivideAndConquerInPlace dacRemainder = new RemainderElementDivideAndConquerInPlace();
			double totalClassical = System.currentTimeMillis();
			Set<OWLAxiom> remainder1 = classicalRemainder.findRemainderElement(ontology.getAxioms(), null, conflict);
			totalClassical = System.currentTimeMillis() - totalClassical;
			double totalDaC = System.currentTimeMillis();
			Set<OWLAxiom> remainder2 = dacRemainder.findRemainderElement(ontology.getAxioms(), null, conflict);
			totalDaC = System.currentTimeMillis() - totalDaC;
			double rCallsRemainder1 = 0;
			for (Double callTime : classicalRemainder.getReasonerCallsTimes()) {
				rCallsRemainder1 += callTime;
			}

			double rCallsRemainder2 = 0;
			for (Double callTime : dacRemainder.getReasonerCallsTimes()) {
				rCallsRemainder2 += callTime;
			}


			System.out.format("%-20d %-15d %-20f %-20f %-15f %-15f %-15d %-15d %-20s %-20s %-20s %-20s %-20d %-20d%n",i,ontology.getAxiomCount(),totalClassical,totalDaC,rCallsRemainder1,rCallsRemainder2,
					remainder1.size(),remainder2.size(),remainder1.containsAll(remainder2),remainder2.containsAll(remainder1),
					SanityChecker.checkSingleRemainderSanity(ontology, remainder1, conflict),
					SanityChecker.checkSingleRemainderSanity(ontology, remainder2, conflict),
					classicalRemainder.getReasonerCallsTimes().size(),dacRemainder.getReasonerCallsTimes().size());


		}
	}

	public static void testConsistencyBigKernel() throws OWLOntologyCreationException {
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
//		System.out.println("I\t\tSize\t\tC\t\tD&C\t\tC-Time\t\t\tD&C-Time\t\tC-Size\t\tD&C-Size\t\tD&C in C\t\tC in D&C\t\tSCheck C\t\t SCheck D&C\t\tC-RCalls\t\tD&C-RCalls");
		System.out.format("%-20s %-15s %-20s %-20s %-15s %-15s %-15s %-15s %-20s %-20s %-20s %-20s %-20s %-20s%n", "I", "Size", "C", "D&C", "C-RCalls-T","D&C-RCalls-T", "C-Size", "D&C-Size","D&C in C", "C In D&C", "SCheck C","SCheck D&C","C-RCalls","D&C-RCalls");
		for (int i = 1; i <= 128; i++) {
			OWLOntology o = m.loadOntologyFromOntologyDocument(new File("benchmark/input/LargeKernelWith" + i + "classes.owl"));
			Set<OWLAxiom> axioms = new HashSet<>();
			axioms.addAll(o.getABoxAxioms(false));
			axioms.addAll(o.getTBoxAxioms(false));

			OWLOntology ontology = m.createOntology(axioms);
			OWLDataFactory df = m.getOWLDataFactory();

			OWLClass top = df.getOWLThing();
			OWLClass bottom = df.getOWLNothing();
			OWLAxiom conflict = df.getOWLSubClassOfAxiom(top, bottom);


			PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ontology);
			m.addOntologyChangeListener(reasoner);

			RemainderElementClassical classicalRemainder = new RemainderElementClassical();
			RemainderElementDivideAndConquerInPlace dacRemainder = new RemainderElementDivideAndConquerInPlace();
			double totalClassical = System.currentTimeMillis();
			Set<OWLAxiom> remainder1 = classicalRemainder.findRemainderElement(ontology.getAxioms(), null, conflict);
			totalClassical = System.currentTimeMillis() - totalClassical;
			double totalDaC = System.currentTimeMillis();
			Set<OWLAxiom> remainder2 = dacRemainder.findRemainderElement(ontology.getAxioms(), null, conflict);
			totalDaC = System.currentTimeMillis() - totalDaC;
			double rCallsRemainder1 = 0;
			for (Double callTime : classicalRemainder.getReasonerCallsTimes()) {
				rCallsRemainder1 += callTime;
			}

			double rCallsRemainder2 = 0;
			for (Double callTime : dacRemainder.getReasonerCallsTimes()) {
				rCallsRemainder2 += callTime;
			}


			System.out.format("%-20d %-15d %-20f %-20f %-15f %-15f %-15d %-15d %-20s %-20s %-20s %-20s %-20d %-20d%n",i,ontology.getAxiomCount(),totalClassical,totalDaC,rCallsRemainder1,rCallsRemainder2,
					remainder1.size(),remainder2.size(),remainder1.containsAll(remainder2),remainder2.containsAll(remainder1),
					SanityChecker.checkSingleRemainderSanity(ontology, remainder1, conflict),
					SanityChecker.checkSingleRemainderSanity(ontology, remainder2, conflict),
					classicalRemainder.getReasonerCallsTimes().size(),dacRemainder.getReasonerCallsTimes().size());



//			System.out.println(i + "\t\t" + ontology.getAxiomCount() + "\t\t" + totalClassical + "\t\t" + totalDaC + "\t\t" + rCallsRemainder1 + "\t\t\t" + rCallsRemainder2 +
//					"\t\t\t" + remainder1.size() + "\t\t" + remainder2.size() + "\t\t\t" + remainder1.containsAll(remainder2) + "\t\t\t" + remainder2.containsAll(remainder1) +
//					"\t\t\t" + SanityChecker.checkSingleRemainderSanity(ontology, remainder1, conflict) +
//					"\t\t\t" + SanityChecker.checkSingleRemainderSanity(ontology, remainder2, conflict) +
//					"\t\t\t" + classicalRemainder.getReasonerCallsTimes().size() + "\t\t\t" + dacRemainder.getReasonerCallsTimes().size());
		}
	}


	public static void testEntailmentBigKernel() throws OWLOntologyCreationException {
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		System.out.format("%-20s %-15s %-20s %-20s %-15s %-15s %-15s %-15s %-20s %-20s %-20s %-20s %-20s %-20s%n", "I", "Size", "C", "D&C", "C-RCalls-T","D&C-RCalls-T", "C-Size", "D&C-Size","D&C in C", "C In D&C", "SCheck C","SCheck D&C","C-RCalls","D&C-RCalls");
//		System.out.println("Size\t\tC\t\tD&C\t\tC-Time\t\t\tD&C-Time\t\tC-Size\t\tD&C-Size\t\tD&C in C\t\tC in D&C\t\tSCheck C\t\t SCheck D&C\t\tC-RCalls\t\tD&C-RCalls");
		for (int i = 1; i <= 16; i++) {
			OWLOntology o = m.loadOntologyFromOntologyDocument(new File("benchmark/input/SmallKernelEntailment/SmallKernelWith" + i + "classes.owl"));
			Set<OWLOntology> imports = o.getImportsClosure();
			Set<OWLAxiom> axioms = new HashSet<>();
			axioms.addAll(o.getABoxAxioms(false));
			axioms.addAll(o.getTBoxAxioms(false));
			for (OWLOntology imported : imports) {
				axioms.addAll(imported.getABoxAxioms(false));
				axioms.addAll(imported.getTBoxAxioms(false));
			}

			OWLOntology ontology = m.createOntology(axioms);

			OWLDataFactory df = m.getOWLDataFactory();
//			OWLIndividual aIndividual = df.getOWLNamedIndividual(IRI.create("http://www.ime.usp.br/liamf/ontologies/LargeKernelWith" + i + "Classes.owl#a"));
//			OWLClass bn = df.getOWLClass(IRI.create("http://www.ime.usp.br/liamf/ontologies/LargeKernelWith" + i + "Classes.owl#B" + i));
//			OWLClass bnPrime = df.getOWLClass(IRI.create("http://www.ime.usp.br/liamf/ontologies/LargeKernelWith" + i + "Classes.owl#B" + i + "Prime"));
//			OWLObjectUnionOf bnORbnPrime = df.getOWLObjectUnionOf(bn, bnPrime);
//			OWLAxiom aIndividualSubClassOfBnOrBnPrime = df.getOWLClassAssertionAxiom(bnORbnPrime, aIndividual);
//
//			OWLAxiom aIndividualSubClassOfNotBnOrBnPrime = df.getOWLClassAssertionAxiom(df.getOWLObjectComplementOf(bnORbnPrime), aIndividual);
//			m.removeAxiom(ontology, aIndividualSubClassOfNotBnOrBnPrime);
			OWLClass clsA = df.getOWLClass(IRI.create("http://www.ime.usp.br/liamf/ontologies/SmallKernelWith" + i + "Classes.owl" + "#A"));
			OWLClass clsC = df.getOWLClass(IRI.create("http://www.ime.usp.br/liamf/ontologies/SmallKernelWith" + i + "Classes.owl" + "#C"));
			OWLAxiom entailment = df.getOWLSubClassOfAxiom(clsA, clsC);

			PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ontology);
			m.addOntologyChangeListener(reasoner);

			RemainderElementClassical classicalRemainder = new RemainderElementClassical();
			RemainderElementDivideAndConquerInPlace dacRemainder = new RemainderElementDivideAndConquerInPlace();
			double totalClassical = System.currentTimeMillis();
			Set<OWLAxiom> remainder1 = classicalRemainder.findRemainderElement(ontology.getAxioms(), null, entailment);
			totalClassical = System.currentTimeMillis() - totalClassical;
			double totalDaC = System.currentTimeMillis();
			Set<OWLAxiom> remainder2 = dacRemainder.findRemainderElement(ontology.getAxioms(), null, entailment);
			totalDaC = System.currentTimeMillis() - totalDaC;
			double rCallsRemainder1 = 0;
			for (Double callTime : classicalRemainder.getReasonerCallsTimes()) {
				rCallsRemainder1 += callTime;
			}

			double rCallsRemainder2 = 0;
			for (Double callTime : dacRemainder.getReasonerCallsTimes()) {
				rCallsRemainder2 += callTime;
			}


			System.out.format("%-20d %-15d %-20f %-20f %-15f %-15f %-15d %-15d %-20s %-20s %-20s %-20s %-20d %-20d%n",i,ontology.getAxiomCount(),totalClassical,totalDaC,rCallsRemainder1,rCallsRemainder2,
					remainder1.size(),remainder2.size(),remainder1.containsAll(remainder2),remainder2.containsAll(remainder1),
					SanityChecker.checkSingleRemainderSanity(ontology, remainder1, entailment),
					SanityChecker.checkSingleRemainderSanity(ontology, remainder2, entailment),
					classicalRemainder.getReasonerCallsTimes().size(),dacRemainder.getReasonerCallsTimes().size());

//			System.out.println(ontology.getAxiomCount() + "\t\t" + totalClassical + "\t\t" + totalDaC + "\t\t" + rCallsRemainder1 + "\t\t\t" + rCallsRemainder2 +
//					"\t\t\t" + remainder1.size() + "\t\t" + remainder2.size() + "\t\t\t" + remainder1.containsAll(remainder2) + "\t\t\t" + remainder2.containsAll(remainder1) +
//					"\t\t\t" + SanityChecker.checkSingleRemainderSanity(ontology, remainder1, aIndividualSubClassOfBnOrBnPrime) +
//					"\t\t\t" + SanityChecker.checkSingleRemainderSanity(ontology, remainder2, aIndividualSubClassOfBnOrBnPrime) +
//					"\t\t\t" + classicalRemainder.getReasonerCallsTimes().size() + "\t\t\t" + dacRemainder.getReasonerCallsTimes().size());
		}
	}


}
