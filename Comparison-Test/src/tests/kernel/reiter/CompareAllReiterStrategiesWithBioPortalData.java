package tests.kernel.reiter;

import algorithms.blackbox.kernel.BlackBoxKernel;
import algorithms.blackbox.kernel.DivideAndConquerBlackBoxKernelContractionStrategy;
import algorithms.blackbox.kernel.SyntaticRelevanceBlackboxKernelExpansionStrategy;
import algorithms.blackbox.kernel.reiter.ClassicalReiter;
import algorithms.blackbox.kernel.reiter.HorridgeReiter;
import algorithms.blackbox.kernel.reiter.OptimizedReiter;
import algorithms.blackbox.kernel.reiter.Reiter;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import util.SanityChecker;
import util.Util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

/**
 * Created by rocknroll on 25/08/14.
 */
public class CompareAllReiterStrategiesWithBioPortalData {

    private static Map<String, Reiter> operators;

    private static OWLOntologyManager manager = null;


    public static void main(String[] args) throws OWLOntologyCreationException, InterruptedException, FileNotFoundException {
        operators = new HashMap<String, Reiter>();
        manager = OWLManager.createOWLOntologyManager();
        operators.put("cr_sre_dacc", new ClassicalReiter(new BlackBoxKernel(new SyntaticRelevanceBlackboxKernelExpansionStrategy(manager), new DivideAndConquerBlackBoxKernelContractionStrategy(manager)), manager));
        operators.put("or_sre_dacc", new OptimizedReiter(new BlackBoxKernel(new SyntaticRelevanceBlackboxKernelExpansionStrategy(manager), new DivideAndConquerBlackBoxKernelContractionStrategy(manager)), manager));
        operators.put("hr_sre_dacc", new HorridgeReiter(null, manager));


        String ontologyFileName = args[0];

        String blackBoxName = args[1];

        int entailmentIndex = Integer.parseInt(args[2])-1;

        Reiter selectedReiter = operators.get(blackBoxName);

        System.setErr(new PrintStream("/tmp/reitertest.log"));


        System.out.format("%-70s", "#Axiom String");

        System.out.format("%-10s", "i");

        System.out.format("%-20s", "No. Axioms");

        System.out.format("%-15s", "size");

        System.out.format("%-15s", "time");

        System.out.format("%-15s", "RC");

        System.out.format("%-20s", "RC_total_time");

        System.out.format("%-15s", "mem");

        System.out.format("%-10s", "sanity");

        if (selectedReiter instanceof OptimizedReiter) {
            System.out.format("%-15s", "ReusedN");

            System.out.format("%-15s", "EarlyPT");
        }

        System.out.println();


        OWLOntology entailments = manager.loadOntologyFromOntologyDocument(new File("../BioPortal/entailed/" + ontologyFileName + "/" + "selectedentailments.xml"));
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File("../BioPortal/entailed/" + ontologyFileName + "/" + ontologyFileName + ".owl.xml"));
        Set<OWLAxiom> kb = ontology.getABoxAxioms(true);
        kb.addAll(ontology.getTBoxAxioms(true));


        Set<OWLSubClassOfAxiom> entailmentAxioms = entailments.getAxioms(AxiomType.SUBCLASS_OF);
        Map<String, OWLAxiom> entailmentMap = new HashMap<>();

        for (OWLAxiom entailment : entailmentAxioms) {
            entailmentMap.put(Util.getManchesterSyntaxAxiom(entailment), entailment);
        }


        List<String> entailmentKeys = new ArrayList<String>(entailmentMap.keySet());


        Collections.sort(entailmentKeys);

        OWLAxiom entailment = entailmentMap.get(entailmentKeys.get(entailmentIndex));

		String axiomString = Util.getManchesterSyntaxAxiom(entailment);

		axiomString = axiomString.replaceAll(" ","_");

        selectedReiter.reset();

        Set<Set<OWLAxiom>> kernelSet = selectedReiter.kernelSet(kb, entailment);


        System.out.format("%-70s", axiomString);

        System.out.format("%-10s", entailmentIndex+1);

        System.out.format("%-20s", kb.size());

        System.out.format("%-15d", ((kernelSet != null) ? kernelSet.size() : -1));

        System.out.format("%-15.2f", selectedReiter.getTotalTime());

        System.out.format("%-15d", selectedReiter.getNumberOfReasonerCalls());

        System.out.format("%-20.2f", selectedReiter.getTotalReasonerCallTime());

        System.out.format("%-15d", selectedReiter.getUsedMemory());

		//manager = OWLManager.createOWLOntologyManager();

        //System.out.format("%-10s", SanityChecker.checkKernelSanity(kernelSet, entailment));

        //if (selectedReiter instanceof OptimizedReiter) {
        //    OptimizedReiter optimizedReiter = (OptimizedReiter) selectedReiter;

        //    System.out.format("%-15d", optimizedReiter.getReusedNodes());

        //    System.out.format("%-15d", optimizedReiter.getEarlyPathTermination());

        //}
        System.out.println();
        System.exit(0);
    }
}
