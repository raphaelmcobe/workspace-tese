package tests;

import algorithms.RemainderElementClassical;
import algorithms.RemainderElementDivideAndConquerInPlace;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import util.SanityChecker;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class CompareClassicalPartialMeetWithDivideAndConquerNodeReuseEarlyPathTermination {

	public static void main(String[] args) throws OWLOntologyCreationException {
		testConsistencyBigKernel();
//		testEntailmentBigKernel();

	}
	
	public static void testConsistencyBigKernel() throws OWLOntologyCreationException{
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		System.out.println("I\t\tSize\t\tC\t\tD&C\t\tC-Time\t\t\tD&C-Time\t\tC-Size\t\tD&C-Size\t\tD&C in C\t\tC in D&C\t\tSCheck C\t\t SCheck D&C\t\tC-RCalls\t\tD&C-RCalls");
		for (int i = 1; i <= 128; i++) {
			OWLOntology o = m.loadOntologyFromOntologyDocument(new File("benchmark/input/LargeKernelWith" + i + "classes.owl"));
			Set<OWLOntology> imports = o.getImportsClosure();
			Set<OWLAxiom> axioms = new HashSet<>();
			axioms.addAll(o.getABoxAxioms(false));
			axioms.addAll(o.getTBoxAxioms(false));
			for (OWLOntology imported : imports) {
				axioms.addAll(imported.getABoxAxioms(false));
				axioms.addAll(imported.getTBoxAxioms(false));
			}

			OWLOntology ontology = m.createOntology(axioms);
			OWLDataFactory df = m.getOWLDataFactory();

			OWLClass top = df.getOWLThing();
			OWLClass bottom = df.getOWLNothing();
			OWLAxiom conflict = df.getOWLSubClassOfAxiom(top, bottom);


			PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ontology);
			m.addOntologyChangeListener(reasoner);

			RemainderElementClassical classicalRemainder = new RemainderElementClassical();
			RemainderElementDivideAndConquerInPlace dacRemainder = new RemainderElementDivideAndConquerInPlace();
			double totalClassical = System.currentTimeMillis();
			Set<OWLAxiom> remainder1 = classicalRemainder.findRemainderElement(ontology.getAxioms(), null, conflict);
			totalClassical = System.currentTimeMillis()-totalClassical;
			double totalDaC = System.currentTimeMillis();
			Set<OWLAxiom> remainder2 = dacRemainder.findRemainderElement(ontology.getAxioms(), null, conflict);
			totalDaC = System.currentTimeMillis()-totalDaC;
			double rCallsRemainder1 = 0;
			for (Double callTime : classicalRemainder.getReasonerCallsTimes()) {
				rCallsRemainder1+=callTime;
			}

			double rCallsRemainder2 = 0;
			for (Double callTime : dacRemainder.getReasonerCallsTimes()) {
				rCallsRemainder2+=callTime;
			}


			System.out.println(i+"\t\t"+ontology.getAxiomCount()+"\t\t"+totalClassical+"\t\t"+totalDaC+"\t\t"+rCallsRemainder1+"\t\t\t"+rCallsRemainder2+
					"\t\t\t"+remainder1.size()+"\t\t"+remainder2.size()+"\t\t\t"+remainder1.containsAll(remainder2)+"\t\t\t"+remainder2.containsAll(remainder1)+
					"\t\t\t"+ SanityChecker.checkSingleRemainderSanity(ontology, remainder1, conflict)+
					"\t\t\t"+ SanityChecker.checkSingleRemainderSanity(ontology, remainder2, conflict)+
					"\t\t\t"+classicalRemainder.getReasonerCallsTimes().size()+"\t\t\t"+dacRemainder.getReasonerCallsTimes().size());
		}
	}
	
	
	public static void testEntailmentBigKernel() throws OWLOntologyCreationException{
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		System.out.println("Size\t\tC\t\tD&C\t\tC-Time\t\t\tD&C-Time\t\tC-Size\t\tD&C-Size\t\tD&C in C\t\tC in D&C\t\tSCheck C\t\t SCheck D&C\t\tC-RCalls\t\tD&C-RCalls");
		for (int i = 1; i <= 128; i++) {
			OWLOntology o = m.loadOntologyFromOntologyDocument(new File("benchmark/input/LargeKernelWith" + i + "classes.owl"));
			Set<OWLOntology> imports = o.getImportsClosure();
			Set<OWLAxiom> axioms = new HashSet<>();
			axioms.addAll(o.getABoxAxioms(false));
			axioms.addAll(o.getTBoxAxioms(false));
			for (OWLOntology imported : imports) {
				axioms.addAll(imported.getABoxAxioms(false));
				axioms.addAll(imported.getTBoxAxioms(false));
			}

			OWLOntology ontology = m.createOntology(axioms);

			OWLDataFactory df = m.getOWLDataFactory();
			OWLIndividual aIndividual = df.getOWLNamedIndividual(IRI.create("http://www.ime.usp.br/liamf/ontologies/LargeKernelWith" + i + "Classes.owl#a"));
			OWLClass bn = df.getOWLClass(IRI.create("http://www.ime.usp.br/liamf/ontologies/LargeKernelWith" + i + "Classes.owl#B" + i));
			OWLClass bnPrime = df.getOWLClass(IRI.create("http://www.ime.usp.br/liamf/ontologies/LargeKernelWith" + i + "Classes.owl#B" + i + "Prime"));
			OWLObjectUnionOf bnORbnPrime = df.getOWLObjectUnionOf(bn, bnPrime);
			OWLAxiom aIndividualSubClassOfBnOrBnPrime = df.getOWLClassAssertionAxiom(bnORbnPrime, aIndividual);

			OWLAxiom aIndividualSubClassOfNotBnOrBnPrime = df.getOWLClassAssertionAxiom(df.getOWLObjectComplementOf(bnORbnPrime), aIndividual);
			m.removeAxiom(ontology, aIndividualSubClassOfNotBnOrBnPrime);


			PelletReasoner reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ontology);
			m.addOntologyChangeListener(reasoner);

			RemainderElementClassical classicalRemainder = new RemainderElementClassical();
			RemainderElementDivideAndConquerInPlace dacRemainder = new RemainderElementDivideAndConquerInPlace();
			double totalClassical = System.currentTimeMillis();
			Set<OWLAxiom> remainder1 = classicalRemainder.findRemainderElement(ontology.getAxioms(), null, aIndividualSubClassOfBnOrBnPrime);
			totalClassical = System.currentTimeMillis()-totalClassical;
			double totalDaC = System.currentTimeMillis();
			Set<OWLAxiom> remainder2 = dacRemainder.findRemainderElement(ontology.getAxioms(), null, aIndividualSubClassOfBnOrBnPrime);
			totalDaC = System.currentTimeMillis()-totalDaC;
			double rCallsRemainder1 = 0;
			for (Double callTime : classicalRemainder.getReasonerCallsTimes()) {
				rCallsRemainder1+=callTime;
			}
			
			double rCallsRemainder2 = 0;
			for (Double callTime : dacRemainder.getReasonerCallsTimes()) {
				rCallsRemainder2+=callTime;
			}
			
			
			System.out.println(ontology.getAxiomCount()+"\t\t"+totalClassical+"\t\t"+totalDaC+"\t\t"+rCallsRemainder1+"\t\t\t"+rCallsRemainder2+
					"\t\t\t"+remainder1.size()+"\t\t"+remainder2.size()+"\t\t\t"+remainder1.containsAll(remainder2)+"\t\t\t"+remainder2.containsAll(remainder1)+
					"\t\t\t"+ SanityChecker.checkSingleRemainderSanity(ontology, remainder1, aIndividualSubClassOfBnOrBnPrime)+
					"\t\t\t"+ SanityChecker.checkSingleRemainderSanity(ontology, remainder2, aIndividualSubClassOfBnOrBnPrime)+
					"\t\t\t"+classicalRemainder.getReasonerCallsTimes().size()+"\t\t\t"+dacRemainder.getReasonerCallsTimes().size());
		}
	}
	
	
	
	
}
