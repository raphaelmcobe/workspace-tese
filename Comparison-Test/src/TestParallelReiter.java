import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.OWLRendererException;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChangeException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.RemoveAxiom;

import algorithms.SimpleReiter;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

public class TestParallelReiter {
	
	private static int reasonerCalls = 0;
	
	public static void test(OWLOntology o) throws OWLOntologyChangeException, OWLOntologyCreationException, InterruptedException, OWLRendererException {
		reasonerCalls=0;
		double time = System.currentTimeMillis();
		Set<Set<OWLAxiom>> kernel = parallelKernel(o);
		System.out.print((System.currentTimeMillis() - time) + "\t");
		System.out.print(kernel.size() + "\t");
		System.out.print(reasonerCalls+" \t");
	}

	public static void testSingleKernelElement(OWLOntology o) throws OWLOntologyChangeException, OWLOntologyCreationException, InterruptedException {
		reasonerCalls=0;
		double time = System.currentTimeMillis();
		Set<OWLAxiom> aKernel = parallelKernelElement(o);
		System.out.print((System.currentTimeMillis() - time) + "\t\t");
		System.out.print(aKernel.size() + "\t\t");
		System.out.print(reasonerCalls+"\t\t");
	}

	public static Set<Set<OWLAxiom>> parallelKernel(OWLOntology B) throws OWLOntologyCreationException, OWLOntologyChangeException, InterruptedException {
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		Map<OWLAxiom, Set<OWLAxiom>> cache = new HashMap<OWLAxiom, Set<OWLAxiom>>();
		Set<OWLAxiom> allAxioms = B.getAxioms();
		for (OWLAxiom owlAxiom : allAxioms) {
			Set<OWLClass> signature = owlAxiom.getClassesInSignature();
			for (OWLClass owlClass : signature) {
				for (OWLAxiom otherAxiom : B.getAxioms()) {
					if (otherAxiom.getClassesInSignature().contains(owlClass)) {
						Set<OWLAxiom> relatedAxioms = cache.get(owlAxiom);
						if (relatedAxioms == null) {
							relatedAxioms = new HashSet<OWLAxiom>();
						}
						relatedAxioms.add(otherAxiom);
						cache.put(owlAxiom, relatedAxioms);
					}
				}
			}
		}
		final Set<Set<OWLAxiom>> toReturn = new HashSet<Set<OWLAxiom>>();
		Set<OWLAxiom> keys = cache.keySet();
		List<Thread> threads = new ArrayList<Thread>();

		for (OWLAxiom key : keys) {
			final OWLOntology o = manager.createOntology(cache.get(key));
			Thread t = new Thread() {
				public void run() {
					try {
						toReturn.addAll(SimpleReiter.kernel(o));
					} catch (OWLOntologyChangeException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (OWLOntologyCreationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			};
			threads.add(t);
			t.run();

		}
		for (Thread thread : threads) {
			thread.join();
		}

		return toReturn;
	}

	public static Set<OWLAxiom> parallelKernelElement(OWLOntology B) throws OWLOntologyCreationException, OWLOntologyChangeException, InterruptedException {
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		Map<OWLAxiom, Set<OWLAxiom>> cache = new HashMap<OWLAxiom, Set<OWLAxiom>>();
		Set<OWLAxiom> allAxioms = B.getAxioms();
		for (OWLAxiom owlAxiom : allAxioms) {
			Set<OWLClass> signature = owlAxiom.getClassesInSignature();
			for (OWLClass owlClass : signature) {
				for (OWLAxiom otherAxiom : B.getAxioms()) {
					if (otherAxiom.getClassesInSignature().contains(owlClass)) {
						Set<OWLAxiom> relatedAxioms = cache.get(owlAxiom);
						if (relatedAxioms == null) {
							relatedAxioms = new HashSet<OWLAxiom>();
						}
						relatedAxioms.add(otherAxiom);
						cache.put(owlAxiom, relatedAxioms);
					}
				}
			}
		}
		Set<OWLAxiom> X = new HashSet<OWLAxiom>();
		Set<OWLAxiom> keys = cache.keySet();
		Set<OWLAxiom> exp = null;
		OWLOntology o = null;
		PelletReasoner reasoner = null;
		for (OWLAxiom key : keys) {
			o = manager.createOntology(cache.get(key));
			reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(o);
			boolean isConsistent = reasoner.isConsistent();
			reasonerCalls++;
			if (!isConsistent) {
				exp = o.getAxioms();
				break;
			}
		}

		for (OWLAxiom axiom : exp) {
			RemoveAxiom removeAxiom = new RemoveAxiom(o, axiom);
			manager.applyChange(removeAxiom);
			boolean isConsistent = reasoner.isConsistent();
			reasonerCalls++;
			if (isConsistent) {
				X.add(axiom);
				AddAxiom addAxiom = new AddAxiom(o, axiom);
				manager.applyChange(addAxiom);
			}
		}
		return X;

	}

}
