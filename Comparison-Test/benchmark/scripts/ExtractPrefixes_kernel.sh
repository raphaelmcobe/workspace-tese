#!/bin/bash

KERNEL_OPERATORS="ce_cc ce_dacc ce_swc hr_sre_dacc sre_cc sre_dacc sre_swc";

DATA_PATH="../BioPortal/entailed/";

for i in $(ls ${DATA_PATH}); do
#for i in "computer-based-patient-record-ontology"; do
	echo "Ontology: ${i}";
	sort ${DATA_PATH}/${i}/entailmentsStrings.txt | sed 's/ /_/g' > /tmp/tmp_entailment.list
	while read entailment
	do
		echo "Entailment: ${entailment}";
		for kernel_operator in ${KERNEL_OPERATORS}; do
			echo "Kernel: ${kernel_operator}"
			cat ../output/BioPortal/kernel/${i}/${kernel_operator}/out.dat | grep -v "#" | \
				sed "s/^${entailment}\s*//" | sed 's/^[^0-9]*\s*//' | sed 's/^1\s\+-1.*/NaN/g' | sed 's/^\s*$/NaN/g' >> \
					../output/BioPortal/kernel/${i}/${kernel_operator}/output.dat;
			mv ../output/BioPortal/kernel/${i}/${kernel_operator}/output.dat \
				../output/BioPortal/kernel/${i}/${kernel_operator}/out.dat
		done;
	done < /tmp/tmp_entailment.list;
done;
