#!/bin/bash

#operators="ce_cc ce_swc ce_dacc sre_cc sre_swc sre_dacc hr_sre_dacc";
#operators="cr_sre_dacc or_sre_dacc hr_sre_dacc";
#operators="hr_sre_dacc";

ontologies=$(ls ../BioPortal/entailed/)

timeout=330;

for j in $ontologies; do
	echo "Extracting Entailments from ${j}"
	java -Xms512m -Xmx2024m -cp "../../lib/*:../../bin/" util.ExtractEntailments $j > "../BioPortal/entailed/${j}/entailmentsStrings.txt"
done
