#!/bin/bash

REITER_OPERATORS="hr_sre_dacc";

DATA_PATH="../BioPortal/entailed/";

for i in $(ls ${DATA_PATH}); do
#for i in "computer-based-patient-record-ontology"; do
	echo "Ontology: ${i}";
	sort ${DATA_PATH}/${i}/entailmentsStrings.txt | sed 's/ /_/g' > /tmp/tmp_entailment.list
	while read entailment
	do
		echo "Entailment: ${entailment}";
		for reiter_operator in ${REITER_OPERATORS}; do
			echo "Remainder: ${reiter_operator}"
			cat ../output/BioPortal/reiter/${i}/${reiter_operator}/out.dat | grep -v "#" | \
				sed "s/^${entailment}\s*//" | sed 's/^[^0-9]*\s*//' | sed 's/^1\s\+-1.*/NaN/g' | sed 's/^\s*$/NaN/g' >> \
					../output/BioPortal/reiter/${i}/${reiter_operator}/output.dat;
			mv ../output/BioPortal/reiter/${i}/${reiter_operator}/output.dat \
				../output/BioPortal/reiter/${i}/${reiter_operator}/out.dat
		done;
	done < /tmp/tmp_entailment.list;
done;
