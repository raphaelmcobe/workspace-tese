#!/bin/bash

operators="ce_cc ce_swc ce_dacc sre_cc sre_swc sre_dacc";

tests="small large"

timeout=45;

testSize=128;

for j in $tests; do
	echo "Testing Mode: ${j}";
	if [ ! -d "../output/generated/kernel/${j}" ]; then
		mkdir -p "../output/generated/kernel/${j}";
	fi
	for i in $operators; do
		echo "Operator: ${i}";
		#Check if Kerneltest Output Exists
		if [ ! -d "../output/generated/kernel/${j}/${i}" ]; then
			mkdir -p "../output/generated/kernel/${j}/${i}";
		fi
		#Run Kernel Test;
		if [ -f "../output/generated/kernel/${j}/${i}/out.dat" ]; then
			rm "../output/generated/kernel/${j}/${i}/out.dat";
		fi
		for k in $(seq 1 ${testSize}); do
			echo -n "${k} ";
			timeout ${timeout} java -Xms512m -Xmx2048m -cp "../../lib/*:../../bin/" tests.kernel.CompareAllKernelStrategiesWithGeneratedData -$j $k $i >> "../output/generated/kernel/${j}/${i}/out.dat"
			result=$?;
			if [ ! $result -eq 0 ]; then
				head -n -2 "../output/generated/kernel/${j}/${i}/out.dat"  > /tmp/test.tmp;
				mv /tmp/test.tmp "../output/generated/kernel/${j}/${i}/out.dat"; 
				printf "%-10s%-10s%-15s%-15s%-15s%-15s%-15s%-20s%-20s%-20s%-15s%-15s\n" "#i" "Axioms" "size" "time" "exp_time" "cont_time" "RC" "ont_mean_size" "RC_mean_time" "RC_total_time" "mem" "Sanity" >>"../output/generated/kernel/${j}/${i}/out.dat";
				printf "%-10d%-10d%-15d%-15d%-15d%-15d%-15d%-20d%-20d%-20d%-15d%-15s\n" ${k} -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 "false">> "../output/generated/kernel/${j}/${i}/out.dat" ;
			fi
		done
		echo " ";
	done
done;
