#!/bin/bash

tests="remainder kernel reiter"


for i in $tests; do
	for m in $(seq 1 10); do
		echo "Running Test Number ${m}"
		operator=${i}
		operator=`echo ${operator:0:1} | tr  '[a-z]' '[A-Z]'`${operator:1} #Uppercase First Letter
		bash "CompareAll${operator}WithBioPortal.sh"
		mkdir -p ../experiments/BioPortal/${i}/${m}/
		mv ../output/BioPortal/${i}/* ../experiments/BioPortal/${i}/${m}/
	done;
done



