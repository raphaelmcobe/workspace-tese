#!/bin/bash

REMAINDER_OPERATORS="ce_src dace_src dace_trc ce_trc ce_swc ce_cc";
REITER_OPERATORS="hr_sre_dacc";
KERNEL_OPERATORS="ce_cc ce_dacc ce_swc hr_sre_dacc sre_cc sre_dacc sre_swc";

DATA_PATH="../BioPortal/entailed/";

for i in $(ls ${DATA_PATH}); do
#for i in "computer-based-patient-record-ontology"; do
	echo "Ontology: ${i}";
	total_entailment=$(cat ${DATA_PATH}/${i}/entailmentsStrings.txt | wc -l);
	for kernel_operator in ${KERNEL_OPERATORS}; do
		rm ../output/BioPortal/kernel/${i}/${kernel_operator}/output.dat;
		for entailment_index in $(seq 1 ${total_entailment}); do
			test_line=$(cat ../output/BioPortal/kernel/${i}/${kernel_operator}/out.dat | grep -v "^#" | sed  's/^1\s\+\-1.*$//' |  head -${entailment_index} | tail -1); 
			test_line_token_counter=$(echo ${test_line} | wc -w);
			echo "Token: ${test_line_token_counter}";
			if [[ "$test_line_token_counter" -eq "12" ]]; then
				print_line=$(echo ${test_line} | sed 's/\s\+/ /g' | cut -d' ' -f3-);
				printf "%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s\n" $(echo "${print_line}" | cut -d' ' -f1-) > /tmp/out.tmp;
				print_line=$(cat /tmp/out.tmp);
			else
				if [[ "$test_line_token_counter" -eq "11" ]]; then
					print_line=$(echo ${test_line} | sed 's/\s\+/ /g' | cut -d' ' -f2-);
					printf "%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s\n" $(echo "${print_line}" | cut -d' ' -f1-) > /tmp/out.tmp;
					print_line=$(cat /tmp/out.tmp);
				else
					if [[ "$test_line_token_counter" -eq "0" ]]; then
						printf "%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s\n" "-1" "-1" "-1" "-1" "-1" "-1" "-1" "-1" "-1" "-1" > /tmp/out.tmp;
						print_line=$(cat /tmp/out.tmp);
					fi
				fi
			fi
			echo -e "${entailment_index}\t\t\t${print_line}" >> \
					../output/BioPortal/kernel/${i}/${kernel_operator}/output.dat;
		done;
	done;

	for remainder_operator in ${REMAINDER_OPERATORS}; do
		rm ../output/BioPortal/remainder/${i}/${remainder_operator}/output.dat;
		for entailment_index in $(seq 1 ${total_entailment}); do
			test_line=$(cat ../output/BioPortal/remainder/${i}/${remainder_operator}/out.dat | grep -v "^#" | sed  's/^1\s\+\-1.*$//' | head -${entailment_index} | tail -1); 
			test_line_token_counter=$(echo ${test_line} | wc -w);
			echo "Token: ${test_line_token_counter}";
			if [[ "$test_line_token_counter" -eq "12" ]]; then
				print_line=$(echo ${test_line} | sed 's/\s\+/ /g' | cut -d' ' -f3-);
				printf "%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s\n" $(echo "${print_line}" | cut -d' ' -f1-) > /tmp/out.tmp;
				print_line=$(cat /tmp/out.tmp);
			else
				if [[ "$test_line_token_counter" -eq "11" ]]; then
					print_line=$(echo ${test_line} | sed 's/\s\+/ /g' | cut -d' ' -f2-);
					printf "%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s\n" $(echo "${print_line}" | cut -d' ' -f1-) > /tmp/out.tmp;
					print_line=$(cat /tmp/out.tmp);
				else
					if [[ "$test_line_token_counter" -eq "0" ]]; then
						printf "%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s\n" "-1" "-1" "-1" "-1" "-1" "-1" "-1" "-1" "-1" "-1" > /tmp/out.tmp;
						print_line=$(cat /tmp/out.tmp);
					fi
				fi
			fi
			echo -e "${entailment_index}\t\t\t${print_line}" >> \
					../output/BioPortal/remainder/${i}/${remainder_operator}/output.dat;
		done;
	done;


	for reiter_operator in ${REITER_OPERATORS}; do
		rm ../output/BioPortal/reiter/${i}/${reiter_operator}/output.dat;
		for entailment_index in $(seq 1 ${total_entailment}); do
			test_line=$(cat ../output/BioPortal/reiter/${i}/${reiter_operator}/out.dat | grep -v "^#" | sed  's/^\-1\s\+\-1.*$//' | head -${entailment_index} | tail -1); 
			test_line_token_counter=$(echo ${test_line} | wc -w);
			echo "Token: ${test_line_token_counter}";
			if [[ "$test_line_token_counter" -eq "8" ]]; then
				print_line=$(echo ${test_line} | sed 's/\s\+/ /g' | cut -d' ' -f3-);
				printf "%-20s%-20s%-20s%-20s%-20s%-20s\n" $(echo "${print_line}" | cut -d' ' -f1-) > /tmp/out.tmp;
				print_line=$(cat /tmp/out.tmp);
			else
				if [[ "$test_line_token_counter" -eq "7" ]]; then
					print_line=$(echo ${test_line} | sed 's/\s\+/ /g' | cut -d' ' -f2-);
					printf "%-20s%-20s%-20s%-20s%-20s%-20s\n" $(echo "${print_line}" | cut -d' ' -f1-) > /tmp/out.tmp;
					print_line=$(cat /tmp/out.tmp);
				else
					if [[ "$test_line_token_counter" -eq "0" ]]; then
						printf "%-20s%-20s%-20s%-20s%-20s%-20s\n" "-1" "-1" "-1" "-1" "-1" "-1" > /tmp/out.tmp;
						print_line=$(cat /tmp/out.tmp);
					fi
				fi
			fi
			echo -e "${entailment_index}\t\t\t${print_line}" >> \
					../output/BioPortal/reiter/${i}/${reiter_operator}/output.dat;
		done;
	done;
done;
