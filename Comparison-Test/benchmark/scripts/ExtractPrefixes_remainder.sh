#!/bin/bash

REMAINDER_OPERATORS="ce_src dace_src dace_trc ce_trc ce_swc ce_cc";

DATA_PATH="../BioPortal/entailed/";

for i in $(ls ${DATA_PATH}); do
#for i in "computer-based-patient-record-ontology"; do
	echo "Ontology: ${i}";
	sort ${DATA_PATH}/${i}/entailmentsStrings.txt | sed 's/ /_/g' > /tmp/tmp_entailment.list
	while read entailment
	do
		echo "Entailment: ${entailment}";
		for remainder_operator in ${REMAINDER_OPERATORS}; do
			echo "Remainder: ${remainder_operator}"
			cat ../output/BioPortal/remainder/${i}/${remainder_operator}/out.dat | grep -v "#" | \
				sed "s/^${entailment}\s*//" | sed 's/^[^0-9]*\s*//' | sed 's/^1\s\+-1.*/NaN/g' | sed 's/^\s*$/NaN/g' >> \
					../output/BioPortal/remainder/${i}/${remainder_operator}/output.dat;
			mv ../output/BioPortal/remainder/${i}/${remainder_operator}/output.dat \
				../output/BioPortal/remainder/${i}/${remainder_operator}/out.dat
		done;
	done < /tmp/tmp_entailment.list;
done;
