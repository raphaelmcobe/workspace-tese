#!/bin/bash

#operators="cr_sre_dacc or_sre_dacc hr_sre_dacc";
operators="hr_sre_dacc";

tests="small large"

timeout=45;

testSize=16;

for j in $tests; do
	echo "Testing Mode: ${j}";
	if [ ! -d "../output/generated/reiter/${j}" ]; then
		mkdir -p "../output/generated/reiter/${j}";
	fi
	for i in $operators; do
		echo "Operator: ${i}";
		#Check if Reiter test Output Exists
		if [ ! -d "../output/generated/reiter/${j}/${i}" ]; then
			mkdir -p "../output/generated/reiter/${j}/${i}";
		fi
		#Run Reiter Test;
		if [ -f "../output/generated/reiter/${j}/${i}/out.dat" ]; then
			rm "../output/generated/reiter/${j}/${i}/out.dat";
		fi
		for k in $(seq 1 ${testSize}); do
			echo -n "${k} ";
			timeout ${timeout} java -Xms512m -Xmx2048m -cp "../../lib/*:../../bin/" tests.kernel.reiter.CompareAllReiterStrategiesWithGeneratedData -$j $k $i >> "../output/generated/reiter/${j}/${i}/out.dat"
			result=$?;
			if [ ! $result -eq 0 ]; then
				head -n -2 "../output/generated/reiter/${j}/${i}/out.dat"  > /tmp/test.tmp;
				mv /tmp/test.tmp "../output/generated/reiter/${j}/${i}/out.dat"; 
				printf "%-10s%-10s%-15s%-15s%-15s%-20s%-15s%-10s\n" "#i" "Axioms" "size" "time" "RC" "RC_total_time" "mem" "Sanity" >>"../output/generated/reiter/${j}/${i}/out.dat";
				printf "%-10d%-10d%-15d%-15d%-15d%-20d%-15d%-10s\n" ${k} -1 -1 -1 -1 -1 -1 "false">> "../output/generated/reiter/${j}/${i}/out.dat" ;
			fi
		done
		echo " ";
	done
done;
