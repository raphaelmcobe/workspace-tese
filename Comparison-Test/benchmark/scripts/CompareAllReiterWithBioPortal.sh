#!/bin/bash

#operators="cr_sre_dacc or_sre_dacc hr_sre_dacc";
operators="hr_sre_dacc";

number='^[0-9]+$'

ontologies=$(ls ../BioPortal/entailed/)

timeout=330;

for j in $ontologies; do
	echo "Testing ${j}";
	if [ ! -d "../output/BioPortal/reiter/${j}" ]; then
		mkdir -p "../output/BioPortal/reiter/${j}";
	fi
	for i in $operators; do
		echo "Operator: ${i}";
		#Check if Kernel test Output Exists
		if [ ! -d "../output/BioPortal/reiter/${j}/${i}" ]; then
			mkdir -p "../output/BioPortal/reiter/${j}/${i}";
		fi
		#Run Kernel Test;
		if [ -f "../output/BioPortal/reiter/${j}/${i}/out.dat" ]; then
			rm "../output/BioPortal/reiter/${j}/${i}/out.dat";
		fi
		number_of_entailments=$(wc -l < ../BioPortal/entailed/$j/entailmentsStrings.txt)
		for k in $(seq 1 ${number_of_entailments}); do
			echo "Testing Entailment ${k}"
			(timeout ${timeout} java -Xms2048m -Xmx16384m -cp "../../lib/*:../../bin/" tests.kernel.reiter.CompareAllReiterStrategiesWithBioPortalData $j $i $k >> "../output/BioPortal/reiter/${j}/${i}/out.dat")
			result=$?;
			if [ $result -ne 0 ]; then
				cat "../output/BioPortal/reiter/${j}/${i}/out.dat"  > /tmp/test.tmp;
				first_field=$(tail -1 /tmp/test.tmp | awk '{print $1}')
				if [[ ${first_field} =~ ${number} ]]; then
					echo -en "\n" >> "../output/BioPortal/reiter/${j}/${i}/out.dat"; 
				else
					head -n -1 /tmp/test.tmp > "../output/BioPortal/reiter/${j}/${i}/out.dat"; 
					printf "%-10s%-10s%-15s%-15s%-15s%-20s%-15s%-10s\n" "#i" "Axioms" "size" "time" "RC" "RC_total_time" "mem" "Sanity" >>"../output/BioPortal/reiter/${j}/${i}/out.dat";
					printf "%-10d%-10d%-15d%-15d%-15d%-20d%-15d%-10s\n" -1 -1 -1 -1 -1 -1 -1 "false" >> "../output/BioPortal/reiter/${j}/${i}/out.dat" ;
				fi
			fi
		done
	done
done
