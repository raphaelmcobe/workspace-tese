#!/bin/bash

operators="ce_src dace_src dace_trc ce_trc ce_swc ce_cc"

ontologies=$(ls ../BioPortal/entailed/)
#ontologies="adverse-event-ontology"

timeout=330;

number='^[0-9]+$'

for j in $ontologies; do
	echo "Testing ${j}";
	if [ ! -d "../output/BioPortal/remainder/${j}" ]; then
		mkdir -p "../output/BioPortal/remainder/${j}";
	fi
	for i in $operators; do
		echo "Operator: ${i}";
		#Check if Remainder test Output Exists
		if [ ! -d "../output/BioPortal/remainder/${j}/${i}" ]; then
			mkdir -p "../output/BioPortal/remainder/${j}/${i}";
		fi
		#Run Remainder Test;
		if [ -f "../output/BioPortal/remainder/${j}/${i}/out.dat" ]; then
			rm "../output/BioPortal/remainder/${j}/${i}/out.dat";
		fi
		number_of_entailments=$(wc -l < ../BioPortal/entailed/$j/entailmentsStrings.txt)
		#for k in $(seq 49 ${number_of_entailments}); do
		for k in $(seq 1 ${number_of_entailments}); do
			echo "Testing Entailment ${k}"
			(timeout ${timeout} java -Xms2048m -Xmx16384m -cp "../../lib/*:../../bin/" tests.remainder.CompareAllRemainderStrategiesWithBioPortalData $j $i $k >> "../output/BioPortal/remainder/${j}/${i}/out.dat")
			result=$?;
			if [ $result -ne 0 ]; then
				cat "../output/BioPortal/remainder/${j}/${i}/out.dat"  > /tmp/test.tmp;
				first_field=$(tail -1 /tmp/test.tmp | awk '{print $1}')
				if [[ ${first_field} =~ ${number} ]]; then
					echo -en "\n" >> "../output/BioPortal/remainder/${j}/${i}/out.dat";
				else
					head -n -1 /tmp/test.tmp > "../output/BioPortal/remainder/${j}/${i}/out.dat"; 
					printf "%-10s%-10s%-15s%-15s%-15s%-15s%-15s%-20s%-20s%-20s%-15s%-15s\n" "#i" "Axioms" "size" "time" "exp_time" "cont_time" "RC" "ont_mean_size" "RC_mean_time" "RC_total_time" "mem" "Sanity" >>"../output/BioPortal/remainder/${j}/${i}/out.dat";
					printf "%-10d%-10d%-15d%-15d%-15d%-15d%-15d%-20d%-20d%-20d%-15d%-15s\n" 1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 "false" >> "../output/BioPortal/remainder/${j}/${i}/out.dat" ;
				fi
			fi
		done
	done
done
