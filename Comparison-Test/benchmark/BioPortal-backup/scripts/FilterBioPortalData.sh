#!/bin/bash


DATA_PATH="../../../BioPortal/entailed/";
REMAINDER_EXPERIMENT_PATH="../remainder/2/";
REMAINDER_OPERATORS="ce_src dace_src dace_trc ce_trc ce_swc ce_cc";

REITER_OPERATORS="hr_sre_dacc"
REITER_EXPERIMENT_PATH="../reiter/1/";

for i in $(ls ${DATA_PATH}); do
	echo $i;
	total_entailments=$(cat ${DATA_PATH}/$i/entailmentsStrings.txt | wc -l);
	#echo ${total_entailments};
	for j in $(seq 1 ${total_entailments}); do
		printf "%-10s" ${j};
		for k in ${REMAINDER_OPERATORS}; do
			time=$(cat ${REMAINDER_EXPERIMENT_PATH}/${i}/${k}/out.dat | grep -v -i "#i");
			each_time=$(echo "${time}" | head -${j} | tail -1 | awk '{print $4}');
			printf "%-15s" ${each_time};
		done;
		for k in ${REITER_OPERATORS}; do
			new_j=$(echo ${j}+1|bc -lq)
			time=$(cat ${REITER_EXPERIMENT_PATH}/${i}/${k}/out.dat | grep -v -i "^#Axio" | grep -vi "^#i");
			each_time=$(echo "${time}" | head -${new_j} | tail -1 | awk '{print $5}');
			printf "%-15s" ${each_time};
		done;
		printf "\n";
	done;
done;

