#!/usr/bin/env ruby

BIOPORTAL_PATH="../../../BioPortal/entailed/";
DATA_PATH="../filtered/";
type=ARGV[0];

@ontologies = %x(ls #{BIOPORTAL_PATH}).split("\n");

latex_header = %q{
\begin{center} 
\begin{longtable}{|c||c|c|c|c|c|c|c|} 
\caption{Ontologies Used}
\label{tab:ontologies} 
\\\\
\hline \hline }
puts "#{latex_header}";
puts 'i & Ontologia & Inferencias nao triviais\\\\ \\hline\\hline';
@ontologies.each_with_index do |ontology_path,i|
	entailments = %x(wc -l #{BIOPORTAL_PATH}/#{ontology_path}/entailmentsStrings.txt).split()[0]
	puts "#{i+1} & #{ontology_path} & #{entailments} \\\\ \\hline"
end

puts '\\end{table}';
