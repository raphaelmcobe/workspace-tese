#!/bin/bash


DATA_PATH="../../../BioPortal/entailed/";
DATA_OUTPUT_PATH="../filtered/time/data";

REMAINDER_EXPERIMENT_PATH="../remainder/3/";
REMAINDER_OPERATORS="ce_src dace_src dace_trc ce_trc ce_swc ce_cc";

REITER_OPERATORS="hr_sre_dacc"
REITER_EXPERIMENT_PATH="../reiter/2/";

for i in $(ls ${DATA_PATH}); do
	echo $i;
	total_entailments=$(cat ${DATA_PATH}/$i/entailmentsStrings.txt | wc -l);
	for j in $(seq 1 ${total_entailments}); do
		printf "%-10s" ${j} >> ${DATA_OUTPUT_PATH}/${i}.dat;
		for k in ${REMAINDER_OPERATORS}; do
			time=$(cat ${REMAINDER_EXPERIMENT_PATH}/${i}/${k}/output.dat);
			each_time=$(echo "${time}" | head -${j} | tail -1 | awk '{print $4}');
			printf "%-15s" ${each_time} >> ${DATA_OUTPUT_PATH}/${i}.dat;
		done;
		for k in ${REITER_OPERATORS}; do
			time=$(cat ${REITER_EXPERIMENT_PATH}/${i}/${k}/output.dat);
			each_time=$(echo "${time}" | head -${j} | tail -1 | awk '{print $4}');
			printf "%-15s" ${each_time} >> ${DATA_OUTPUT_PATH}/${i}.dat;
		done;
		printf "\n" >> ${DATA_OUTPUT_PATH}/${i}.dat;
	done;
done;

