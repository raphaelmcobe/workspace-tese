#!/usr/bin/env ruby

BIOPORTAL_PATH="../../../BioPortal/entailed/";
DATA_PATH="../filtered/";
type=ARGV[0];

@ontologies = %x(ls #{BIOPORTAL_PATH}).split("\n");

@REMAINDER_OPERATORS="ce_src dace_src dace_trc ce_trc ce_swc ce_cc".split();

@REITER_OPERATORS="hr_sre_dacc".split();

@operators=@REMAINDER_OPERATORS.concat(@REITER_OPERATORS);

latex_header = %q{
\begin{landscape}
\begin{center} 
\begin{longtable}{|l||c|c|c|c|c|c|c|} 
\caption{Experiment Data}
\label{tab:Experiment_Data} 
\\\\
\hline \hline }
puts "#{latex_header}";
#puts '\multicolumn{8}{|c|}{\textbf{'+ontology_path+'}}\\\\ \hline \hline';
print "Ontologia &";
@REMAINDER_OPERATORS.each_with_index do |each_operator,i|
	print '\textbf{'+each_operator.gsub("_","\\_")+'}'
	if i != (@REMAINDER_OPERATORS.size() -1) then
		print '&  ';
	else
		print '\\\\ \hline \hline ';
		print "\n";
	end
end

@ontologies.each do |ontology_path|
	print "#{ontology_path} &"
	experiment_data_file=File.open("#{DATA_PATH}/#{type}/data/#{ontology_path}.dat", "r");
	best_counter = Hash.new(0);
	
	difference_to_the_best_map = Hash.new {|h,k| h[k]=[]};
	experiment_counter=0;
	experiment_data_file.readlines.each do |experiment_data|
		experiment_counter+=1;
		@experiment_fields=experiment_data.split();
		@not_null_fields = Array.new(@experiment_fields);
		@not_null_fields.keep_if do |field|
			field.to_f != -1;
		end
		
		@not_null_fields=@not_null_fields[1..-1];
		@not_null_fields.map! do |field|
			Float(field);
		end
		best_result = @not_null_fields.min;
		@experiment_fields.each_with_index do |field, i|
			if i != 0 then
				if field.to_i != -1 then
					difference_to_horridge = field.to_f - @experiment_fields[7].to_f;
					difference_to_horridge = (difference_to_horridge.to_f)/(@experiment_fields[7].to_f)* 100;
				else
					difference_to_horridge=100;	
				end
				difference_to_the_best_map[@operators[i-1]] << difference_to_horridge;
			end
			if field.to_f == best_result then
				best_counter[@operators[i-1]]+=1;
			end
		end
	end

	@operators.each_with_index do |operator,i|
		print "#{best_counter[operator]} ";
		if i < (@operators.size() -1) then
			print " &";
		else
			print '\\\\ \\hline';
			print "\n";
		end

	end

end
puts '\end{longtable}'+ "\n" +'\\end{center} \end{landscape}';	
