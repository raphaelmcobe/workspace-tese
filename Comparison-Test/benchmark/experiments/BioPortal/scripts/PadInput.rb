#!/usr/bin/env ruby

BIOPORTAL_PATH="../../../BioPortal/entailed/";
REMAINDER_EXPERIMENT_PATH="../remainder/1/";
@REMAINDER_OPERATORS="ce_src dace_src dace_trc ce_trc ce_swc ce_cc".split();
#@REMAINDER_OPERATORS="ce_cc".split();



@ontologies = %x(ls #{BIOPORTAL_PATH}).split("\n");

#@ontologies = Array.new(1,@ontologies[7]);

ontology_entailment_map = Hash.new;

@ontologies.each do |ontology_path| 
	total_entailments = 
		%x(wc -l #{BIOPORTAL_PATH}/#{ontology_path}/entailmentsStrings.txt).split()[0].to_i;
	ontology_entailment_map[ontology_path]=total_entailments;
end

ontology_entailment_map.keys.each do |each_ontology|
	@REMAINDER_OPERATORS.each do |each_operator|
		linecounter = 1;
		file=
			File.open("#{REMAINDER_EXPERIMENT_PATH}/#{each_ontology}/#{each_operator}/out.dat","r");
		file.readlines.each do |line|
			if line =~ /^#/ then
				next;
			end
			if line.split()[0].to_i == linecounter then
				puts line;
				linecounter = linecounter+1;
			else
				if line.split()[0].to_i == 1 or line.split()[0] == -1 then
					print "#{linecounter}\t\t\t";
					10.times do 
							print "-1\t\t";
					end
					print "\n";
					linecounter = linecounter+1;
				else
					difference=line.split()[0].to_i - linecounter;
					difference.times do
						print "AQUI #{linecounter}\t\t\t";
						10.times do 
							print "-1\t\t";
						end
						print "\n";
						linecounter = linecounter+1;
					end
				end
			end
		end
	end
end

