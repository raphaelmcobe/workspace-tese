#!/bin/bash


for i in remainder/*; do
	ontology=$(echo "${i}" | rev | cut -d/ -f1 | rev);
	echo "${ontology}: "
	echo "\nRemainder: "
	for j in $i/*; do
		test=$(echo "${j}" | rev | cut -d/ -f1 | rev);
		printf "%-8s \t" ${test};
		tail -1 ${j}/out.dat;
	done;
	echo "\nKernel: "
	dir="kernel/$(echo "${i}" | cut -d/ -f2-)";
	for j in ${dir}/*; do
		dir=$(echo "${j}" | cut -d/ -f2-);
		test=$(echo "${j}" | rev | cut -d/ -f1 | rev);
		printf "%-8s \t" ${test};
		tail -1 "kernel/${dir}/out.dat";
	done;
	echo " \n\n";
done;
