#!/bin/bash

pellet=${PELLET_HOME}/pellet.sh

for i in *; do
	for j in $i/*.owl.xml; do
		if [ -f "${j}" ]
		then
			echo $j;
			$pellet consistency $j > /tmp/pellet.out
			isConsistent=$(cat /tmp/pellet.out | grep -i "No");
			if [ -n "${isConsistent}" ]
			then
				echo "${j} is inconsistent!";
				mv $i inconsistent/;
			else
				echo "${j} is consistent.";
				mv $i consistent/;
			fi;
		fi;
	done;
done;
