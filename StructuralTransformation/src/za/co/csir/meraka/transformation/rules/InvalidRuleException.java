package za.co.csir.meraka.transformation.rules;

public class InvalidRuleException extends Exception {

	public InvalidRuleException(String message) {
		super(message);
	}

}
