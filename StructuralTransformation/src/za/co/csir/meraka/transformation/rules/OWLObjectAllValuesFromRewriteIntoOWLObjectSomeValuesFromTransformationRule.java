package za.co.csir.meraka.transformation.rules;

import java.util.Deque;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;

class OWLObjectAllValuesFromRewriteIntoOWLObjectSomeValuesFromTransformationRule extends TransformationRule<OWLClassExpression> {

	public OWLObjectAllValuesFromRewriteIntoOWLObjectSomeValuesFromTransformationRule(OWLDataFactory datafactory) {
		super(datafactory);
	}

	@Override
	public boolean canExecute(OWLClassExpression classExpression) {
		return this.searchForExpressionOfGivenType(ClassExpressionType.OBJECT_ALL_VALUES_FROM, classExpression) != null;
	}

	@Override
	public void execute(OWLClassExpression classExpression, Deque<OWLClassExpression> stack) throws InvalidRuleException {
		OWLObjectAllValuesFrom allValuesFrom = (OWLObjectAllValuesFrom) this.searchForExpressionOfGivenType(ClassExpressionType.OBJECT_ALL_VALUES_FROM, classExpression);
		OWLClassExpression filler = allValuesFrom.getFiller();
		OWLObjectPropertyExpression property = allValuesFrom.getProperty();
		OWLClassExpression newAxiomExistentialClassExpression = dataFactory.getOWLObjectSomeValuesFrom(property, filler.getObjectComplementOf().getNNF()).getObjectComplementOf();
		stack.push(newAxiomExistentialClassExpression);
	}
	
	public String toString(){
		return "UniversalTransformationRule";
	}
}