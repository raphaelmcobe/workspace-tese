package za.co.csir.meraka.transformation.rules;

import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;

class OWLObjectUnionOfTransformationRule extends TransformationRule<OWLClassExpression> {

	public OWLObjectUnionOfTransformationRule(OWLDataFactory datafactory) {
		super(datafactory);
	}

	public boolean canExecute(OWLClassExpression classExpression) {
		if (classExpression.getClassExpressionType() == ClassExpressionType.OBJECT_UNION_OF) {
			OWLObjectUnionOf disjunction = (OWLObjectUnionOf) classExpression;
			Set<OWLClassExpression> operands = disjunction.asDisjunctSet();
			for (OWLClassExpression operand : operands) {
				Set<OWLClassExpression> conjunctions = operand.asConjunctSet();
				if (conjunctions.size() > 1) {
					return true;
				}
			}
		}
		return false;
	}

	public void execute(OWLClassExpression classExpression, Deque<OWLClassExpression> stack) throws InvalidRuleException {
		try {
			OWLObjectUnionOf disjunction = (OWLObjectUnionOf) classExpression;
			Set<OWLClassExpression> clauses = this.disjunctionCrossProduct(disjunction).asConjunctSet();
			for (OWLClassExpression clause : clauses) {
				stack.push(clause.getNNF());
			}
		} catch (ClassCastException ccex) {
			throw new InvalidRuleException("Attempt to Execute Invalid Transformation Rule!");
		}
	}

	private OWLClassExpression disjunctionCrossProduct(OWLClassExpression classExpression) {
		Set<OWLClassExpression> disjunctionClauses = classExpression.asDisjunctSet();
		Set<OWLClassExpression> allConjunctions = new HashSet<OWLClassExpression>();

		for (OWLClassExpression owlClassExpression : disjunctionClauses) {
			allConjunctions.add(owlClassExpression);
		}
		Iterator<OWLClassExpression> it = allConjunctions.iterator();
		OWLClassExpression leftHandClassExpressions = it.next();
		for (; it.hasNext();) {
			leftHandClassExpressions = disjunctionCrossProduct(leftHandClassExpressions, it.next());
		}
		return leftHandClassExpressions;
	}

	private OWLClassExpression disjunctionCrossProduct(OWLClassExpression leftHandOperand, OWLClassExpression rightHandOperand) {
		Set<OWLObjectUnionOf> clauses = new HashSet<OWLObjectUnionOf>();
		for (OWLClassExpression leftHandClassExpression : leftHandOperand.asConjunctSet()) {
			for (OWLClassExpression rightHandClassExpression : rightHandOperand.asConjunctSet()) {
				OWLObjectUnionOf newDisjunction = dataFactory.getOWLObjectUnionOf(leftHandClassExpression, rightHandClassExpression);
				newDisjunction = dataFactory.getOWLObjectUnionOf(newDisjunction.asDisjunctSet());
				clauses.add(newDisjunction);
			}
		}
		return dataFactory.getOWLObjectIntersectionOf(clauses);
	}
	
	public String toString(){
		return "ORTransformationRule";
	}

}