package za.co.csir.meraka.transformation.rules;

import java.util.Deque;
import java.util.Set;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;

class OWLObjectIntersectionOfTransformationRule extends TransformationRule<OWLClassExpression> {

	public OWLObjectIntersectionOfTransformationRule(OWLDataFactory datafactory) {
		super(datafactory);

	}

	@Override
	public boolean canExecute(OWLClassExpression classExpression) {
		return classExpression.getClassExpressionType() == ClassExpressionType.OBJECT_INTERSECTION_OF;
	}

	@Override
	public void execute(OWLClassExpression classExpression, Deque<OWLClassExpression> stack) throws InvalidRuleException {
		OWLObjectIntersectionOf intersection = (OWLObjectIntersectionOf) classExpression;
		Set<OWLClassExpression> conjuntcions = intersection.asConjunctSet();
		if (conjuntcions.size() <= 1)
			throw new InvalidRuleException("Attempt to Execute Invalid Transformation Rule!");
		for (OWLClassExpression conjunction : conjuntcions) {
			stack.push(conjunction.getNNF());
		}
	}
	
	public String toString(){
		return "ANDTransformationRule";
	}
}