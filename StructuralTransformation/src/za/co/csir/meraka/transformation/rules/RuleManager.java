package za.co.csir.meraka.transformation.rules;

import java.util.ArrayList;
import java.util.List;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;


public class RuleManager {

	private static List<TransformationRule<OWLAxiom>> availableAxiomTransformationRules = new ArrayList<TransformationRule<OWLAxiom>>();
	private static List<TransformationRule<OWLClassExpression>> availableClassTransformationRules = new ArrayList<TransformationRule<OWLClassExpression>>();

	public static List<TransformationRule<OWLAxiom>> getAvailableAxiomTransformationRules() {
		return availableAxiomTransformationRules;
	}

	public static void setAvailableAxiomTransformationRules(List<TransformationRule<OWLAxiom>> availableAxiomTransformationRules) {
		RuleManager.availableAxiomTransformationRules = availableAxiomTransformationRules;
	}

	public static List<TransformationRule<OWLClassExpression>> getAvailableClassTransformationRules() {
		return availableClassTransformationRules;
	}

	public static void setAvailableClassTransformationRules(List<TransformationRule<OWLClassExpression>> availableClassTransformationRules) {
		RuleManager.availableClassTransformationRules = availableClassTransformationRules;
	}

	public static TransformationRule<OWLClassExpression> selectSuitableRuleForClass(OWLClassExpression owlObject) {
		for (TransformationRule<OWLClassExpression> rule : availableClassTransformationRules) {
			if (rule.canExecute(owlObject))
				return rule;
		}
		return null;
	}

	public static TransformationRule<OWLAxiom> selectSuitableRuleForAxiom(OWLAxiom owlObject) {
		for (TransformationRule<OWLAxiom> rule : availableAxiomTransformationRules) {
			if (rule.canExecute(owlObject)){
				return rule;
			}
		}
		return null;
	}

}