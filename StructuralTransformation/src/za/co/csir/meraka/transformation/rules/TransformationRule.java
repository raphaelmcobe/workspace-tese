package za.co.csir.meraka.transformation.rules;

import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import org.semanticweb.owlapi.model.OWLQuantifiedObjectRestriction;

public abstract class TransformationRule<T extends OWLObject> {
	protected OWLDataFactory dataFactory;

	protected Map<OWLClassExpression, OWLClassExpression> cache = new HashMap<OWLClassExpression, OWLClassExpression>();

	public TransformationRule(OWLDataFactory dataFactory) {
		this.dataFactory = dataFactory;
	}

	public abstract boolean canExecute(T owlObject);

	public abstract void execute(T owlObject, Deque<T> stack) throws InvalidRuleException;

	protected OWLClassExpression searchForExpressionOfGivenType(ClassExpressionType givenType, OWLClassExpression classExpression) {
		Set<OWLClassExpression> components = new HashSet<OWLClassExpression>();

		ClassExpressionType expressionType = classExpression.getClassExpressionType();

		if (expressionType == givenType)
			return classExpression;

		if (expressionType == ClassExpressionType.OBJECT_UNION_OF) {
			components = ((OWLObjectUnionOf) classExpression).asDisjunctSet();
			for (OWLClassExpression owlClassExpression : components) {
				OWLClassExpression searchedClass = searchForExpressionOfGivenType(givenType, owlClassExpression);
				if (searchedClass != null)
					return searchedClass;
			}
		}

		if (expressionType == ClassExpressionType.OBJECT_INTERSECTION_OF) {
			components = ((OWLObjectUnionOf) classExpression).asConjunctSet();
			for (OWLClassExpression owlClassExpression : components) {
				OWLClassExpression searchedClass = searchForExpressionOfGivenType(givenType, owlClassExpression);
				if (searchedClass != null)
					return searchedClass;
			}
		}

		if (expressionType == ClassExpressionType.OBJECT_ALL_VALUES_FROM || expressionType == ClassExpressionType.DATA_SOME_VALUES_FROM) {
			OWLQuantifiedObjectRestriction objectQuantifiedRestriction = (OWLQuantifiedObjectRestriction) classExpression;
			OWLClassExpression searchedClass = searchForExpressionOfGivenType(givenType, objectQuantifiedRestriction.getFiller());
			if (searchedClass != null)
				return searchedClass;
		}
		return null;
	}
	
	
	protected OWLClassExpression replace(OWLClassExpression actualClass, OWLClassExpression originalClass, OWLClassExpression newClass){
		if(actualClass==originalClass) return newClass;
		else if(actualClass.getClassExpressionType() == ClassExpressionType.OBJECT_UNION_OF){
			Set<OWLClassExpression> newOperands = new HashSet<OWLClassExpression>();
			Set<OWLClassExpression> operands = ((OWLObjectUnionOf)actualClass).asDisjunctSet();
			for (OWLClassExpression owlClassExpression : operands) {
				newOperands.add(replace(owlClassExpression, originalClass, newClass));
			}
			return dataFactory.getOWLObjectUnionOf(newOperands);
		}
		else if(actualClass.getClassExpressionType() == ClassExpressionType.OBJECT_INTERSECTION_OF){
			Set<OWLClassExpression> newOperands = new HashSet<OWLClassExpression>();
			Set<OWLClassExpression> operands = ((OWLObjectIntersectionOf)actualClass).asConjunctSet();
			for (OWLClassExpression owlClassExpression : operands) {
				newOperands.add(replace(owlClassExpression, originalClass, newClass));
			}
			return dataFactory.getOWLObjectIntersectionOf(newOperands);
		}
		else if(actualClass.getClassExpressionType() == ClassExpressionType.OBJECT_SOME_VALUES_FROM){
			OWLObjectSomeValuesFrom oldSomeValuesFrom = (OWLObjectSomeValuesFrom) actualClass;
			return dataFactory.getOWLObjectSomeValuesFrom(oldSomeValuesFrom.getProperty(), replace(oldSomeValuesFrom.getFiller(),originalClass,newClass));
		}
		else if(actualClass.getClassExpressionType() == ClassExpressionType.OBJECT_ALL_VALUES_FROM){
			OWLObjectAllValuesFrom oldAllValuesFrom = (OWLObjectAllValuesFrom) actualClass;
			return dataFactory.getOWLObjectSomeValuesFrom(oldAllValuesFrom.getProperty(), replace(oldAllValuesFrom.getFiller(),originalClass,newClass));
		}
		else{
			return actualClass;
		}
	}
	

}