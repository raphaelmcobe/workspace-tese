package za.co.csir.meraka.transformation.rules;

import java.io.File;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLPropertyExpression;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;

public class StructuralTransformation {


	public static void main(String[] args) throws OWLOntologyCreationException, InvalidRuleException {
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		OWLOntology o = m.loadOntologyFromOntologyDocument(new File("inputs/Ontology5.owl"));
		Set<OWLAxiom> axioms = o.getAxioms();
		
		StructuralTransformation transformation = new StructuralTransformation(m.getOWLDataFactory());
		List<TransformationRule<OWLAxiom>> availableAxiomRules = new ArrayList<TransformationRule<OWLAxiom>>();
		TransformationRule<OWLAxiom> subsumptionRule = new OWLSubClassOfAxiomTransformationRule(m.getOWLDataFactory());
		RuleManager.setAvailableAxiomTransformationRules(availableAxiomRules);
		
		TransformationRule<OWLClassExpression> orRule = new OWLObjectUnionOfTransformationRule(m.getOWLDataFactory());
		TransformationRule<OWLClassExpression> andRule = new OWLObjectIntersectionOfTransformationRule(m.getOWLDataFactory());
		TransformationRule<OWLClassExpression> existsRule = new OWLObjectSomeValuesFromTarnsformationRule(m.getOWLDataFactory());
		TransformationRule<OWLClassExpression> forAllRule = new OWLObjectAllValuesFromRewriteIntoOWLObjectSomeValuesFromTransformationRule(m.getOWLDataFactory());
		
		List<TransformationRule<OWLClassExpression>> availableClassRules = new ArrayList<TransformationRule<OWLClassExpression>>();
		
		availableAxiomRules.add(subsumptionRule);
		availableClassRules.add(orRule);
		availableClassRules.add(andRule);
		availableClassRules.add(existsRule);
		availableClassRules.add(forAllRule);
		
		RuleManager.setAvailableClassTransformationRules(availableClassRules);
		
		
		Set<OWLAxiom> result = transformation.transform(axioms);
		
		
		System.out.println("Results: ");
		
		for (OWLAxiom owlAxiom : result) {
			System.out.println(owlAxiom);
		}
	}
	
	
	
	
	private OWLDataFactory dataFactory;
	
	public StructuralTransformation(OWLDataFactory dataFactory) {
		this.dataFactory = dataFactory;
	}

	public Set<OWLAxiom> transform(Set<OWLAxiom> kb) throws InvalidRuleException {
		Set<OWLAxiom> toReturn = new HashSet<OWLAxiom>();
		Deque<OWLClassExpression> stack = new ArrayDeque<OWLClassExpression>();

		for (OWLAxiom owlAxiom : kb) {
			Deque<OWLAxiom> axiomStack = new ArrayDeque<OWLAxiom>();
			TransformationRule<OWLAxiom> rule = RuleManager.selectSuitableRuleForAxiom(owlAxiom);
			if(rule != null){
				rule.execute(owlAxiom, axiomStack);
				OWLSubClassOfAxiom subClassAxiom = (OWLSubClassOfAxiom) axiomStack.pop().getNNF();
				OWLClassExpression classExpression = subClassAxiom.getSuperClass();
				stack.push(classExpression);
			}
		}

		while (!stack.isEmpty()) {
			OWLClassExpression classExpression = stack.pop();
			TransformationRule<OWLClassExpression> rule = RuleManager.selectSuitableRuleForClass(classExpression);
			if (rule == null) {
				toReturn.add(this.createTopSubSumption(classExpression));
				continue;
			}else{
				System.out.println(rule);
				System.out.println(classExpression);
				rule.execute(classExpression, stack);
				System.out.println(stack.peek());
			}
		}
		return toReturn;
	}

	private OWLAxiom createTopSubSumption(OWLClassExpression classExpression) {
		return dataFactory.getOWLSubClassOfAxiom(dataFactory.getOWLThing(), classExpression);
	}
}
