package za.co.csir.meraka.transformation.rules;

import java.util.Deque;

import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;

class OWLSubClassOfAxiomTransformationRule extends TransformationRule<OWLAxiom> {

	public OWLSubClassOfAxiomTransformationRule(OWLDataFactory datafactory) {
		super(datafactory);
	}

	public boolean canExecute(OWLAxiom axiom) {
		if (axiom.isOfType(AxiomType.SUBCLASS_OF)) {
			OWLSubClassOfAxiom subClassAxiom = (OWLSubClassOfAxiom) axiom;
			return !subClassAxiom.getSubClass().isOWLThing();
		}
		return false;
	}

	@Override
	public void execute(OWLAxiom owlObject, Deque<OWLAxiom> stack) throws InvalidRuleException {
		OWLSubClassOfAxiom subClassAxiom = (OWLSubClassOfAxiom) owlObject;
		OWLObjectComplementOf negatedSubClass = dataFactory.getOWLObjectComplementOf(subClassAxiom.getSubClass());
		OWLObjectUnionOf newAxiom = dataFactory.getOWLObjectUnionOf(negatedSubClass, subClassAxiom.getSuperClass());
		stack.push(dataFactory.getOWLSubClassOfAxiom(dataFactory.getOWLThing(), newAxiom).getNNF());
	}
	
	public String toString(){
		return "SubsumptionTransformationRule";
	}
}