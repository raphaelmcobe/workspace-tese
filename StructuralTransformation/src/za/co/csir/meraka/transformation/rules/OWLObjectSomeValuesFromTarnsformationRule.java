package za.co.csir.meraka.transformation.rules;

import java.util.Deque;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;

class OWLObjectSomeValuesFromTarnsformationRule extends TransformationRule<OWLClassExpression> {

		public OWLObjectSomeValuesFromTarnsformationRule(OWLDataFactory datafactory) {
			super(datafactory);
		}

		@Override
		public boolean canExecute(OWLClassExpression classExpression) {

			OWLClassExpression existentialQuantifier = searchForExpressionOfGivenType(ClassExpressionType.OBJECT_SOME_VALUES_FROM, classExpression);
			
			
			
			if (existentialQuantifier != null) {
				OWLObjectSomeValuesFrom existentialClass = (OWLObjectSomeValuesFrom) existentialQuantifier;
				OWLClassExpression filler = existentialClass.getFiller();
				return filler.getClassExpressionType() == ClassExpressionType.OBJECT_INTERSECTION_OF 
						|| filler.getClassExpressionType() == ClassExpressionType.OBJECT_UNION_OF 
						|| filler.getClassExpressionType() == ClassExpressionType.OBJECT_SOME_VALUES_FROM 
						|| filler.getClassExpressionType() == ClassExpressionType.OBJECT_ALL_VALUES_FROM;
			}
			return false;
		}

		@Override
		public void execute(OWLClassExpression classExpression, Deque<OWLClassExpression> stack) throws InvalidRuleException {
			OWLObjectSomeValuesFrom existentialClassExpression = (OWLObjectSomeValuesFrom) this.searchForExpressionOfGivenType(ClassExpressionType.OBJECT_SOME_VALUES_FROM, classExpression);
//			existentialClassExpression.
			OWLClassExpression filler = existentialClassExpression.getFiller();
			OWLObjectPropertyExpression property = existentialClassExpression.getProperty();
			
			//Check if a new concept name hasn't already been created for the Existential axiom Filler;
			if (this.cache.get(filler) == null) {

				// Generate Random name for the newly introduced Class and store it in the cache;
				Random r = new Random();
				char c1 = (char) (r.nextInt(26) + 'A');
				char c2 = (char) (r.nextInt(26) + 'A');
				char c3 = (char) (r.nextInt(26) + 'A');

				OWLClass newClass = dataFactory.getOWLClass(IRI.create(c1 + "" + c2 + "" + c3));
				this.cache.put(filler, newClass);
			}
			
			OWLClassExpression newClass = this.cache.get(filler);

			OWLObjectSomeValuesFrom newSomeValuesFromAxiom = dataFactory.getOWLObjectSomeValuesFrom(property, newClass);
			
			
			
			OWLObjectUnionOf newUnionOfAxiom1 = dataFactory.getOWLObjectUnionOf(newClass.getObjectComplementOf(), filler);

			OWLObjectUnionOf newUnionOfAxiom2 = dataFactory.getOWLObjectUnionOf(filler.getObjectComplementOf(), newClass);

			stack.push(this.replace(classExpression, existentialClassExpression, newSomeValuesFromAxiom));
			stack.push(newUnionOfAxiom1.getNNF());
			System.out.println("AH: "+newUnionOfAxiom2.getNNF());
			stack.push(newUnionOfAxiom2.getNNF());


		}
		
		public String toString(){
			return "ExistentialTarnsformationRule";
		}
	}