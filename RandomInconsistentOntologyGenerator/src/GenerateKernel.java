import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.util.DefaultPrefixManager;


public class GenerateKernel {


	public static void main(String[] args) throws OWLOntologyCreationException, OWLOntologyStorageException, FileNotFoundException {

//		if(args.length <2){
//			System.out.println("Usage: java GenerateKernel number_of_classes destination_path");
//			System.exit(0);
//		}
		GenerateKernel generator = new GenerateKernel();
//		generator.generateSmallKernel(Integer.parseInt(args[0]),args[1]);
//		generator.generateLargeKernel(Integer.parseInt(args[0]),args[1]);
		for(int i = 1; i <= 512; i++){
			generator.generateLargeKernel(i, "../Comparison-Test/benchmark/input");
//			generator.generateSmallKernel(i, "../Comparison-Test/benchmark/input");
		}
	}

	public void generateSmallKernel(int n, String path) throws OWLOntologyCreationException, OWLOntologyStorageException, FileNotFoundException{
		System.out.println("Generating Ontology with "+n+" classes (Small Kernel)...");
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		IRI ontologyIRI = IRI.create("http://www.ime.usp.br/liamf/ontologies/SmallKernelWith"+n+"Classes.owl");
		OWLOntology ontology = manager.createOntology(ontologyIRI);
		OWLDataFactory factory = manager.getOWLDataFactory();
		OWLClass clsA = factory.getOWLClass(IRI.create(ontologyIRI + "#A"));
		OWLClass clsC = factory.getOWLClass(IRI.create(ontologyIRI + "#C"));
		for(int i = 0; i < n; i++){
			OWLClass clsB = factory.getOWLClass(IRI.create(ontologyIRI + "#B"+(i+1)));
			OWLAxiom axiom = factory.getOWLSubClassOfAxiom(clsA, clsB);
			AddAxiom addAxiom = new AddAxiom(ontology, axiom);
			manager.applyChange(addAxiom);
			axiom = factory.getOWLSubClassOfAxiom(clsB, clsC);
			addAxiom = new AddAxiom(ontology, axiom);
			manager.applyChange(addAxiom);
		}
//		OWLAxiom disjointAxiom = factory.getOWLDisjointClassesAxiom(clsA,clsC);
//		AddAxiom addAxiom = new AddAxiom(ontology, disjointAxiom);
//		manager.applyChange(addAxiom);
//		PrefixManager pm = new DefaultPrefixManager(ontologyIRI.toString());
//		OWLIndividual indA = factory.getOWLNamedIndividual("#a",pm);
//		OWLClassAssertionAxiom classAssertion = factory.getOWLClassAssertionAxiom(clsA,indA);
//		addAxiom = new AddAxiom(ontology, classAssertion);
//		manager.applyChange(addAxiom);

		manager.saveOntology(ontology, new FileOutputStream(new File(path+"/SmallKernelEntailment/SmallKernelWith"+n+"classes.owl")));
			
	}

	public void generateLargeKernel(int n, String path) throws OWLOntologyCreationException, OWLOntologyStorageException, FileNotFoundException{
		System.out.println("Generating Ontology with "+n+" classes (Large Kernel)...");
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		IRI ontologyIRI = IRI.create("http://www.ime.usp.br/liamf/ontologies/LargeKernelWith"+n+"Classes.owl");
		PrefixManager pm = new DefaultPrefixManager(ontologyIRI.toString());
		OWLOntology ontology = manager.createOntology(ontologyIRI);
		OWLDataFactory factory = manager.getOWLDataFactory();
		OWLClass clsB = factory.getOWLClass(IRI.create(ontologyIRI + "#B0"));
		OWLClass clsBPrime = factory.getOWLClass(IRI.create(ontologyIRI + "#B0Prime"));
		OWLIndividual indA = factory.getOWLNamedIndividual("#a",pm);
		OWLClassExpression union = factory.getOWLObjectUnionOf(clsB,clsBPrime);
		OWLClassAssertionAxiom classAssertion = factory.getOWLClassAssertionAxiom(union,indA);

		AddAxiom addAx = new AddAxiom(ontology, classAssertion);
		manager.applyChange(addAx);

		for(int i = 0; i < n; i++){
			OWLClass ithClass = factory.getOWLClass(IRI.create(ontologyIRI+"#B"+i));
			OWLClass ithClassPrime = factory.getOWLClass(IRI.create(ontologyIRI+"#B"+i+"Prime"));
			OWLClass ithPlusOneClass = factory.getOWLClass(IRI.create(ontologyIRI+"#B"+(i+1)));
			OWLClass ithPlusOneClassPrime = factory.getOWLClass(IRI.create(ontologyIRI+"#B"+(i+1)+"Prime"));
			OWLClassExpression ithUnion = factory.getOWLObjectUnionOf(ithClass,ithClassPrime);
			OWLAxiom ithSubsumption = factory.getOWLSubClassOfAxiom(ithUnion, ithPlusOneClass);
			OWLAxiom ithSubsumptionPrime = factory.getOWLSubClassOfAxiom(ithUnion, ithPlusOneClassPrime);
			manager.addAxiom(ontology, ithSubsumption);
			manager.addAxiom(ontology, ithSubsumptionPrime);

		}
//		OWLClass nthClass = factory.getOWLClass(IRI.create(ontologyIRI + "#B"+n));
//		OWLClass nthClassPrime = factory.getOWLClass(IRI.create(ontologyIRI + "#B"+n+"Prime"));
//		OWLClassExpression nthUnion = factory.getOWLObjectUnionOf(nthClass,nthClassPrime);
//		OWLClassExpression nthNegatedClassAssertion = factory.getOWLObjectComplementOf(nthUnion);
//		OWLClassAssertionAxiom nthNegatedUnionClassAssertion = factory.getOWLClassAssertionAxiom(nthNegatedClassAssertion,indA);
//		addAx = new AddAxiom(ontology, nthNegatedUnionClassAssertion);
//		manager.applyChange(addAx);

		manager.saveOntology(ontology, new FileOutputStream(new File(path+"/LargeKernelEntailment/LargeKernelWith"+n+"classes.owl")));
	}
}
