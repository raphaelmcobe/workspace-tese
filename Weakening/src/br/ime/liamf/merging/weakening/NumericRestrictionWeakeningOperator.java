package br.ime.liamf.merging.weakening;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectMaxCardinality;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.base.ISets;
import bcontractor.dl.owl.OWLSentence;
import bcontractor.dl.owl.hermit.OWLHermitReasoner;
import bcontractor.kernel.Kernel;

public class NumericRestrictionWeakeningOperator implements WeakeningOperator{

	@Override
	public ISet<OWLSentence> weakenBase(OWLSentence axiomToBeWeakened, ISet<OWLSentence> knowledgeBase, Kernel<OWLSentence> kernels) {
		List<ISet<OWLSentence>> orderedSentences = orderKernelsByFrequency(kernels);
		OWLSubClassOfAxiom subClassAxiomToBeWeaken = (OWLSubClassOfAxiom) axiomToBeWeakened.getAxiom();
		ISet<OWLSentence> firstSentenceSet = orderedSentences.iterator().next();
		int valueToIncrease = 0;
		for (OWLSentence owlSentence : firstSentenceSet) {
			OWLAxiom axiom = owlSentence.getAxiom();
			if(axiom.getAxiomType().equals(AxiomType.SUBCLASS_OF)){
				if(((OWLSubClassOfAxiom) axiom).getSuperClass().getClassExpressionType().equals(ClassExpressionType.OBJECT_MAX_CARDINALITY)){
					OWLObjectMaxCardinality subClassAxiom = (OWLObjectMaxCardinality) ((OWLSubClassOfAxiom) axiom).getSuperClass();
					if(owlSentence.equals(axiomToBeWeakened)){
						for (ISet<OWLSentence> eachKernel : kernels) {
							if(eachKernel.contains(owlSentence)) valueToIncrease++;
						}
					}
				}
			}
		}
		knowledgeBase = knowledgeBase.minus(axiomToBeWeakened);
		OWLObjectMaxCardinality subClassAxiom = (OWLObjectMaxCardinality) ((OWLSubClassOfAxiom) axiomToBeWeakened.getAxiom()).getSuperClass();
		int newCardinality = subClassAxiom.getCardinality()+valueToIncrease;
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		OWLDataFactory df = m.getOWLDataFactory();
		OWLSubClassOfAxiom newSubClassAxiom = null;
		OWLSubClassOfAxiom previousSubClassAxiom = null;
		SATReasoner<OWLSentence> reasoner = new OWLHermitReasoner();
		
		//Now we have to change the value of the new Cardinality restriction iteratively
		
		for(int i = newCardinality; i>=subClassAxiom.getCardinality(); i--){
			OWLObjectMaxCardinality newNumberedRestriction = df.getOWLObjectMaxCardinality(i, subClassAxiom.getProperty());
			newSubClassAxiom = df.getOWLSubClassOfAxiom(subClassAxiomToBeWeaken.getSubClass(), newNumberedRestriction);
			if(reasoner.isSatisfiable(knowledgeBase.union(new OWLSentence(newSubClassAxiom)))){
				previousSubClassAxiom = newSubClassAxiom;
			}
			else return knowledgeBase.union(new OWLSentence(previousSubClassAxiom));;
		}
		return knowledgeBase;
	}
	
	
	public List<ISet<OWLSentence>> orderKernelsByFrequency(Kernel<OWLSentence> kernels){
		List<ISet<OWLSentence>> toReturn = new ArrayList<ISet<OWLSentence>>();
		Map<Integer,Set<OWLSentence>> axiomFrequence = new HashMap<Integer,Set<OWLSentence>>();
		Set<OWLSentence> allSentences = new HashSet<OWLSentence>();
		for (Iterator<ISet<OWLSentence>> it = kernels.iterator();it.hasNext();){
			for (OWLSentence eachSentence : it.next()) {
				allSentences.add(eachSentence);
			}
		}
		for (OWLSentence owlSentence : allSentences) {
			int occurenceCounter = 0;
			for(Iterator<ISet<OWLSentence>> it = kernels.iterator(); it.hasNext();){
				if(it.next().contains(owlSentence)){
					occurenceCounter++;
				}
			}
			Set<OWLSentence> strata = axiomFrequence.get(occurenceCounter);
			if(strata == null) strata = new HashSet<OWLSentence>();
			strata.add(owlSentence);
			axiomFrequence.put(occurenceCounter, strata);
		}
		List<Integer> axiomCounters = new ArrayList<Integer>(axiomFrequence.keySet());
		Collections.sort(axiomCounters);
		Collections.reverse(axiomCounters);
		for (Integer axiomCounter : axiomCounters) {
			toReturn.add(ISets.asISet(axiomFrequence.get(axiomCounter)));
		}
		return toReturn;
	}
}
