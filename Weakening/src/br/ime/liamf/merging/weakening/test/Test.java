package br.ime.liamf.merging.weakening.test;

import java.util.HashSet;
import java.util.Set;

import bcontractor.api.ISet;
import bcontractor.base.ISets;
import bcontractor.dl.owl.OWLSentence;

public class Test {

	
	public static void main(String[] args) {
		ISet<OWLSentence> sentences = ISets.empty();
		
		Set<OWLSentence> sentences2 = new HashSet<OWLSentence>();
		
		for(int i = 0; i<10; i++){
			OWLSentence sentence = new OWLSentence(null);
			sentences2.add(sentence);
			sentences.union(sentence);
		}
		System.out.println(sentences.size());
		sentences = ISets.asISet(sentences2);
		System.out.println(sentences.size());
	}
	
	
}
