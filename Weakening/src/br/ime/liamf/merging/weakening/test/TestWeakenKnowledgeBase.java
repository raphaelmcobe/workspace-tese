package br.ime.liamf.merging.weakening.test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLObjectMaxCardinality;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;

import bcontractor.api.ISet;
import bcontractor.base.ISets;
import bcontractor.dl.owl.OWLSentence;
import bcontractor.dl.owl.hermit.OWLHermitReasoner;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.operators.BlackboxKernelOperator;
import br.ime.liamf.merging.weakening.NumericRestrictionWeakeningOperator;

public class TestWeakenKnowledgeBase {

	@Test
	public void test() throws OWLOntologyCreationException {
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		OWLOntology o = m.loadOntologyFromOntologyDocument(new File("src/br/ime/liamf/merging/weakening/test/inputs/NumberedRestriction1.owl"));
		ISet<OWLSentence> base = ISets.empty();
		
		for (OWLAxiom axiom : o.getAxioms()) {
			base = base.union(new OWLSentence(axiom));
		}
		
		OWLHermitReasoner reasoner = new OWLHermitReasoner();
		BlackboxKernelOperator<OWLSentence> blackbox = new BlackboxKernelOperator<OWLSentence>(reasoner);
		Kernel<OWLSentence> kernelSet =  blackbox.eval(base);
		assertThat(kernelSet.getElements().size(), is(2));
		
		List<ISet<OWLSentence>> rankedAxioms = new NumericRestrictionWeakeningOperator().orderKernelsByFrequency(kernelSet);

		assertThat(rankedAxioms.size(), is(2));
		
		Iterator<ISet<OWLSentence>> it = rankedAxioms.iterator(); 
		
		assertThat(it.next().size(), is(3));
		
		assertThat(it.next().size(), is(4));
		
		OWLSentence numericRestrictionSubsumption = null;
		it = rankedAxioms.iterator();
		ISet<OWLSentence> firstStrata = it.next();
		for (OWLSentence owlSentence : firstStrata) {
			OWLAxiom axiom = owlSentence.getAxiom();
			if(axiom.getAxiomType().equals(AxiomType.SUBCLASS_OF)){
				if(((OWLSubClassOfAxiom) axiom).getSuperClass().getClassExpressionType().equals(ClassExpressionType.OBJECT_MAX_CARDINALITY)){
					numericRestrictionSubsumption = owlSentence; break;
				}
				
			}
		}
		
		if(numericRestrictionSubsumption != null){
			ISet<OWLSentence> weakenBase = new NumericRestrictionWeakeningOperator().weakenBase(numericRestrictionSubsumption, base, kernelSet);
			assertThat(weakenBase.size(), is(base.size()));
			
			numericRestrictionSubsumption = null;
			
			
			for (OWLSentence owlSentence : weakenBase) {
				OWLAxiom axiom = owlSentence.getAxiom();
				System.out.println("Achou!");
				if(axiom.getAxiomType().equals(AxiomType.SUBCLASS_OF)){
					if(((OWLSubClassOfAxiom) axiom).getSuperClass().getClassExpressionType().equals(ClassExpressionType.OBJECT_MAX_CARDINALITY)){
						numericRestrictionSubsumption = owlSentence; 
						break;
					}
					
				}
			}
			
			
			OWLObjectMaxCardinality subClassAxiom = (OWLObjectMaxCardinality) ((OWLSubClassOfAxiom) numericRestrictionSubsumption.getAxiom()).getSuperClass();
			int newCardinality = subClassAxiom.getCardinality();
			assertThat(newCardinality, is(2));
			
			
			
			
			
		}
		
	}

}
