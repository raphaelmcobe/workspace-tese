package br.ime.liamf.merging.weakening;

import bcontractor.api.ISet;
import bcontractor.dl.owl.OWLNumberedRestrictionSentence;
import bcontractor.dl.owl.OWLSentence;
import bcontractor.kernel.Kernel;

public interface WeakeningOperator {
	public abstract ISet<OWLSentence> weakenBase(OWLSentence axiomToBeWeakened, ISet<OWLSentence> knowledgeBase, Kernel<OWLSentence> kernels);

}
