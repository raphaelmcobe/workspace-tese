/*    */ package tools.ontologyrepositoryapi.tools;
/*    */ 
/*    */ import java.io.File;
/*    */
/*    */ import org.semanticweb.owlapi.apibinding.OWLManager;
/*    */ import org.semanticweb.owlapi.model.IRI;
/*    */ import org.semanticweb.owlapi.model.OWLClass;
/*    */ import org.semanticweb.owlapi.model.OWLOntology;
/*    */ import org.semanticweb.owlapi.model.OWLOntologyCreationException;
/*    */ import org.semanticweb.owlapi.model.OWLOntologyManager;
/*    */ 
/*    */ public class CheckForErrors
/*    */ {
/*    */   private static final String ONTOLOGIES_DIRECTORY_NAME = "ontologies";
/*    */   private static final String IMPORTS_DIRECTORY_NAME = "imports";
/*    */   private static final String FAILED_TO_LOAD_DIRECTORY_NAME = "failed-to-load";
/*    */   private static final String ONTOLOGY_METADATA_PREFIX = "http://owl.cs.manchester.ac.uk/ontology#";
/* 24 */   private static final IRI SOURCE_ONTOLOGY_IRI = IRI.create("http://owl.cs.manchester.ac.uk/ontology#sourceOntology");
/*    */ 
/*    */   public static void main(String[] args)
/*    */   {
/* 28 */     File ontologiesDirectory = new File("ontologies");
/* 29 */     if (!ontologiesDirectory.exists()) {
/* 30 */       throw new RuntimeException("Ontologies Directory does not exist");
/*    */     }
/* 32 */     for (File ontologyDir : ontologiesDirectory.listFiles())
/* 33 */       if (ontologyDir.isDirectory()) {
/* 34 */         File[] contents = ontologyDir.listFiles();
/* 35 */         if (contents != null)
/* 36 */           for (File content : contents)
/* 37 */             if (content.getName().endsWith("owl.xml"))
/* 38 */               loadOntology(content);
/*    */       }
/*    */   }
/*    */ 
/*    */   private static void loadOntology(File file)
/*    */   {
/*    */     try
/*    */     {
/* 49 */       OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
/* 50 */       OWLOntology ont = manager.loadOntologyFromOntologyDocument(file);
/*    */ 
/* 52 */       int errorCount = 0;
/* 53 */       for (OWLClass cls : ont.getClassesInSignature()) {
/* 54 */         if (cls.getIRI().toString().contains("Error")) {
/* 55 */           errorCount++;
/*    */         }
/*    */       }
/* 58 */       if (errorCount > 0)
/* 59 */         System.out.println(file + " contains " + errorCount + " errors");
/*    */     }
/*    */     catch (OWLOntologyCreationException e)
/*    */     {
/* 63 */       e.printStackTrace();
/*    */     }
/*    */     catch (RuntimeException e)
/*    */     {
/* 67 */       System.err.println(file);
/* 68 */       e.printStackTrace();
/*    */     }
/*    */   }
/*    */ }

/* Location:           /tmp/repositorytools.jar
 * Qualified Name:     tools.ontologyrepositoryapi.tools.CheckForErrors
 * JD-Core Version:    0.6.2
 */