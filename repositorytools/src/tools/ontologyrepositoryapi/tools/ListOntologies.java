/*    */ package tools.ontologyrepositoryapi.tools;
/*    */ 
/*    */

/*    */ import tools.ontologyrepositoryapi.BioPortalOntologyRepository;
/*    */ import tools.ontologyrepositoryapi.OntologyRepositoryEntry;
/*    */ 
/*    */ public class ListOntologies
/*    */ {
/*    */   public static void main(String[] args)
/*    */   {
/* 15 */     BioPortalOntologyRepository repository = new BioPortalOntologyRepository();
/* 16 */     int count = 0;
/* 17 */     int owlCompatibleCount = 0;
/* 18 */     int oboCount = 0;
/* 19 */     int owlCount = 0;
/* 20 */     for (OntologyRepositoryEntry entry : repository.getEntries()) {
/* 21 */       count++;
/* 22 */       if (entry.isOWLCompatibleFormat()) {
/* 23 */         System.out.println(entry);
/* 24 */         owlCompatibleCount++;
/*    */       }
/* 26 */       if (entry.getFormat().toLowerCase().contains("owl")) {
/* 27 */         owlCount++;
/*    */       }
/* 29 */       else if (entry.getFormat().toLowerCase().contains("obo")) {
/* 30 */         oboCount++;
/*    */       }
/*    */     }
/* 33 */     System.out.println("-----------------------------------------------------------------------");
/* 34 */     System.out.println(count + " ontologies in BioPortal");
/* 35 */     System.out.println(owlCompatibleCount + " OWL Compatible ontologies");
/* 36 */     System.out.println(owlCount + " OWL ontologies");
/* 37 */     System.out.println(oboCount + " OBO ontologies");
/*    */   }
/*    */ }

/* Location:           /tmp/repositorytools.jar
 * Qualified Name:     tools.ontologyrepositoryapi.tools.ListOntologies
 * JD-Core Version:    0.6.2
 */