/*     */ package tools.ontologyrepositoryapi.tools;
/*     */ 
/*     */ import java.io.BufferedOutputStream;
/*     */ import java.io.File;
/*     */ import java.io.FileNotFoundException;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.IOException;
/*     */
/*     */ import java.io.PrintWriter;
/*     */ import java.util.Arrays;
/*     */ import java.util.HashSet;
/*     */ import java.util.Set;
/*     */ import tools.ontologyrepositoryapi.BioPortalOntologyRepository;
/*     */ import tools.ontologyrepositoryapi.OntologyRepositoryEntry;
/*     */ 
/*     */ public class DownloadOntologyDocuments
/*     */ {
/*     */   private static final String ONTOLOGIES_DIRECTORY_NAME = "ontologydocuments";
/*     */   private static final String FAILED_TO_LOAD_DIRECTORY_NAME = "failed";
/*  28 */   private Set<String> formats = new HashSet();
/*     */ 
/*     */   public DownloadOntologyDocuments(String[] formats) {
/*  31 */     for (String format : Arrays.asList(formats))
/*  32 */       this.formats.add(format.toUpperCase());
/*     */   }
/*     */ 
/*     */   public DownloadOntologyDocuments()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void download() {
/*  40 */     BioPortalOntologyRepository repository = new BioPortalOntologyRepository();
/*  41 */     int count = 0;
/*  42 */     for (OntologyRepositoryEntry entry : repository.getEntries()) {
/*  43 */       if ((this.formats.isEmpty()) || (this.formats.contains(entry.getFormat().toUpperCase()))) {
/*  44 */         count++;
/*  45 */         System.out.println("Processing ontology " + count + " (" + entry.getAbbreviation() + ")");
/*  46 */         downloadOntology(entry);
/*     */       }
/*     */     }
/*  49 */     System.out.println(count + " ontologies");
/*     */   }
/*     */ 
/*     */   private void downloadOntology(OntologyRepositoryEntry entry) {
/*  53 */     File ontologiesDirectory = new File("ontologydocuments");
/*  54 */     String entryName = getEntryName(entry);
/*  55 */     File entryDirectory = new File(ontologiesDirectory, entryName);
/*  56 */     if (entryDirectory.exists()) {
/*  57 */       System.out.println("\t\t" + entryDirectory + "Directory already exists. Skipping.");
/*  58 */       return;
/*     */     }
/*     */     try {
/*  61 */       File ontologyFile = new File(entryDirectory, entryName.replace("\"", "") + ".owl");
/*  62 */       entryDirectory.mkdirs();
/*  63 */       BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(ontologyFile));
/*  64 */       entry.getOntologyDocument(bos);
/*  65 */       bos.close();
/*     */     }
/*     */     catch (FileNotFoundException e) {
/*  68 */       saveException(entry, e);
/*     */     }
/*     */     catch (IOException e) {
/*  71 */       saveException(entry, e);
/*     */     }
/*     */     catch (RuntimeException e) {
/*  74 */       saveException(entry, e);
/*     */     }
/*     */   }
/*     */ 
/*     */   private String getEntryName(OntologyRepositoryEntry entry)
/*     */   {
/*  80 */     return entry.getAbbreviation().replaceAll("\\W+", "-").toLowerCase();
/*     */   }
/*     */ 
/*     */   private void saveException(OntologyRepositoryEntry repositoryEntry, Exception e)
/*     */   {
/*     */     try {
/*  86 */       File failedToLoad = new File("failed");
/*  87 */       failedToLoad.mkdirs();
/*  88 */       File exceptionFile = new File(failedToLoad, getEntryName(repositoryEntry) + "---loader-exception.txt");
/*  89 */       PrintWriter pw = new PrintWriter(exceptionFile);
/*  90 */       e.printStackTrace(pw);
/*  91 */       pw.flush();
/*  92 */       pw.close();
/*     */     }
/*     */     catch (FileNotFoundException e1) {
/*  95 */       e1.printStackTrace();
/*     */     }
/*     */   }
/*     */ 
/*     */   public static void main(String[] args)
/*     */   {
/* 101 */     DownloadOntologyDocuments downloadOntologyDocuments = new DownloadOntologyDocuments(new String[] { "OBO" });
/* 102 */     downloadOntologyDocuments.download();
/*     */   }
/*     */ }

/* Location:           /tmp/repositorytools.jar
 * Qualified Name:     tools.ontologyrepositoryapi.tools.DownloadOntologyDocuments
 * JD-Core Version:    0.6.2
 */