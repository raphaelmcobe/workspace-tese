/*    */ package tools.ontologyrepositoryapi.tools;
/*    */ 
/*    */

/*    */

import java.util.*;

/*    */
/*    */
/*    */
/*    */
/*    */ 
/*    */ public class CommandLineArgs
/*    */ {
/*    */   private String[] args;
/* 15 */   private Map<String, List<String>> argsMap = new LinkedHashMap();
/*    */ 
/*    */   public CommandLineArgs(String[] args) {
/* 18 */     this.args = args;
/* 19 */     processCommandLineArguments();
/*    */   }
/*    */ 
/*    */   private void processCommandLineArguments() {
/* 23 */     List currentSwitchArgs = new ArrayList();
/* 24 */     for (String s : this.args) {
/* 25 */       s = s.trim();
/* 26 */       if (s.startsWith("-"))
/*    */       {
/* 28 */         currentSwitchArgs = new ArrayList();
/* 29 */         this.argsMap.put(s.substring(1), currentSwitchArgs);
/*    */       }
/*    */       else {
/* 32 */         currentSwitchArgs.add(unescape(s));
/*    */       }
/*    */     }
/*    */   }
/*    */ 
/*    */   private String unescape(String s) {
/* 38 */     if ((s.startsWith("\"")) && (s.endsWith("\"")) && (s.length() > 2)) {
/* 39 */       return s.substring(1, s.length() - 1);
/*    */     }
/*    */ 
/* 42 */     return s;
/*    */   }
/*    */ 
/*    */   public List<String> getArguments(String sw)
/*    */   {
/* 47 */     List args = (List)this.argsMap.get(sw);
/* 48 */     if (args == null) {
/* 49 */       return Collections.emptyList();
/*    */     }
/*    */ 
/* 52 */     return args;
/*    */   }
/*    */ 
/*    */   public String getFirstArgument(String sw, String defaultValue)
/*    */   {
/* 57 */     List args = getArguments(sw);
/* 58 */     if (args.isEmpty()) {
/* 59 */       return defaultValue;
/*    */     }
/*    */ 
/* 62 */     return (String)args.get(0);
/*    */   }
/*    */ 
/*    */   public boolean hasSwitch(String sw)
/*    */   {
/* 67 */     return this.argsMap.containsKey(sw);
/*    */   }
/*    */ 
/*    */   public void dump() {
/* 71 */     System.out.println("----------------------------------------------");
/* 72 */     System.out.println("COMMAND LINE ARGUMENTS");
/* 73 */     System.out.println("----------------------------------------------");
/* 74 */     for (String sw : this.argsMap.keySet()) {
/* 75 */       System.out.println(sw);
/* 76 */       for (String arg : (List<String>) this.argsMap.get(sw)) {
/* 77 */         System.out.println("\t" + arg);
/*    */       }
/* 79 */       System.out.println("----------------------------------------------");
/*    */     }
/*    */   }
/*    */ 
/*    */   public static void main(String[] args) {
/* 84 */     CommandLineArgs cmd = new CommandLineArgs(args);
/* 85 */     cmd.dump();
/* 86 */     System.out.println("Ontologies: " + cmd.getFirstArgument("ontologies", "ontologies"));
/*    */   }
/*    */ }

/* Location:           /tmp/repositorytools.jar
 * Qualified Name:     tools.ontologyrepositoryapi.tools.CommandLineArgs
 * JD-Core Version:    0.6.2
 */