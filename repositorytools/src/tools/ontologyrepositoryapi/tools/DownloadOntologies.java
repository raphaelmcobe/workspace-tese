package tools.ontologyrepositoryapi.tools;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.OWLXMLOntologyFormat;
import org.semanticweb.owlapi.model.*;
import tools.ontologyrepositoryapi.BioPortalOntologyRepository;
import tools.ontologyrepositoryapi.MissingImportsTreatment;
import tools.ontologyrepositoryapi.OntologyRepositoryEntry;

import java.io.*;
import java.util.Collections;
import java.util.Iterator;

public class DownloadOntologies {
	private static final String ONTOLOGIES_DIRECTORY_NAME = "ontologies";
	private static final String IMPORTS_DIRECTORY_NAME = "imports";
	private static final String FAILED_TO_LOAD_DIRECTORY_NAME = "failed-to-load";
	private static final String ONTOLOGY_METADATA_PREFIX = "http://owl.cs.manchester.ac.uk/ontology#";
	private static final IRI SOURCE_ONTOLOGY_IRI = IRI.create("http://owl.cs.manchester.ac.uk/ontology#sourceOntology");

	public void download() {
		BioPortalOntologyRepository repository = new BioPortalOntologyRepository();
		int count = 0;


		for (OntologyRepositoryEntry entry : repository.getEntries()) {
			System.out.println("Downloading");
			if (entry.isOWLCompatibleFormat()) {
				count++;
				System.out.println("Processing ontology " + count + " (" + entry.getAbbreviation() + ")");
				downloadOntology(entry);
			}
		}
		System.out.println(count + " ontologies");
	}

	private void downloadOntology(OntologyRepositoryEntry entry) {
		File ontologiesDirectory = new File("ontologies");
		String entryName = getEntryName(entry);
		File entryDirectory = new File(ontologiesDirectory, entryName);
		if (entryDirectory.exists()) {
			System.out.println("\t\t" + entryDirectory + "Directory already exists. Skipping.");
			return;
		}
		File ontologyFile = new File(entryDirectory, entryName.replace("\"", "") + ".owl.xml");
		entryDirectory.mkdirs();
		try {
			OWLOntology ontology = entry.loadOntology(MissingImportsTreatment.IGNORE);
			OWLOntologyManager mergedManager = OWLManager.createOWLOntologyManager();
			OWLOntology mergedOntology = mergedManager.createOntology(ontology.getOntologyID());
			int anonOntologyCount = 0;
			for (Iterator i = ontology.getImportsClosure().iterator(); i.hasNext(); ) {
				OWLOntology ont = (OWLOntology) i.next();
				for (OWLAxiom ax : ont.getLogicalAxioms()) {
					OWLDataFactory df = mergedManager.getOWLDataFactory();
					OWLAnnotationProperty sourceOntologyProperty = df.getOWLAnnotationProperty(SOURCE_ONTOLOGY_IRI);
					OWLAnnotation annotation;
					if (ont.isAnonymous()) {
						OWLLiteral anonName = df.getOWLLiteral("anonymous" + anonOntologyCount);
						annotation = df.getOWLAnnotation(sourceOntologyProperty, anonName);
						anonOntologyCount++;
					} else {
						annotation = df.getOWLAnnotation(sourceOntologyProperty, ont.getOntologyID().getOntologyIRI());
					}
					OWLAxiom annotatedAxiom = ax.getAnnotatedAxiom(Collections.singleton(annotation));
					mergedManager.addAxiom(mergedOntology, annotatedAxiom);
				}
			}
			OWLOntology ont;
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(ontologyFile));
			OWLXMLOntologyFormat ontologyFormat = new OWLXMLOntologyFormat();
			ontologyFormat.setPrefix("ont", "http://www.ime.usp.br/ontology#");
			mergedManager.saveOntology(mergedOntology, ontologyFormat, bos);
			bos.close();
		} catch (OWLOntologyCreationException e) {
			saveException(entry, e);
		} catch (OWLOntologyStorageException e) {
			saveException(entry, e);
		} catch (FileNotFoundException e) {
			saveException(entry, e);
		} catch (IOException e) {
			saveException(entry, e);
		} catch (RuntimeException e) {
			saveException(entry, e);
		}
	}

	private String getEntryName(OntologyRepositoryEntry entry) {
		return entry.getAbbreviation().replaceAll("\\W+", "-").toLowerCase();
	}

	private void saveException(OntologyRepositoryEntry repositoryEntry, Exception e) {
		try {
			File failedToLoad = new File("failed-to-load");
			failedToLoad.mkdirs();
			File exceptionFile = new File(failedToLoad, getEntryName(repositoryEntry) + "---loader-exception.txt");
			PrintWriter pw = new PrintWriter(exceptionFile);
			e.printStackTrace(pw);
			pw.flush();
			pw.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}

	public static void main(String[] args) {
		DownloadOntologies downloadOntologies = new DownloadOntologies();
		downloadOntologies.download();
	}
}