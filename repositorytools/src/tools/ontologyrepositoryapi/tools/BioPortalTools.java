package tools.ontologyrepositoryapi.tools;


import tools.ontologyrepositoryapi.BioPortalOntologyRepository;
import tools.ontologyrepositoryapi.OntologyRepositoryEntry;

public class BioPortalTools {
	private static final String DOWNLOAD_SWITCH = "download";
	private static final String LIST_SWITCH = "list";

	public static void main(String[] args) {
//		CommandLineArgs cmdArgs = new CommandLineArgs(args);
//		if (cmdArgs.hasSwitch("download")) {
//			downloadOntologies();
//		} else if (cmdArgs.hasSwitch("list")) {
//			listOntologies();
//		} else {
//			System.out.println("Expected one of:");
//			System.out.println("\t-download (Downloads ontologies from bioportal.  Each ontology is downloaded into a separate folder.  The imports closure of each ontology is merged into a single OWL/XML, with each axiom annotated with its source ontology.)");
//			System.out.println("\t-list (Lists the ontologies in bioportal)");
//		}
		listOntologies();
	}

	private static void downloadOntologies() {
		DownloadOntologies downloadOntologies = new DownloadOntologies();
		downloadOntologies.download();
	}

	private static void listOntologies() {
		BioPortalOntologyRepository repository = new BioPortalOntologyRepository();
		int count = 0;
		for (OntologyRepositoryEntry entry : repository.getEntries()) {
			if (entry.isOWLCompatibleFormat()) {
				System.out.println(entry);
				count++;
			}
		}
		System.out.println(count + " ontologies");
	}
}