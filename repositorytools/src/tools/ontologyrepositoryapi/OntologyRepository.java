package tools.ontologyrepositoryapi;

import java.util.List;

public abstract interface OntologyRepository
{
  public abstract String getName();

  public abstract int getEntriesCount();

  public abstract OntologyRepositoryEntry getEntry(int paramInt);

  public abstract List<OntologyRepositoryEntry> getEntries();
}

/* Location:           /tmp/repositorytools.jar
 * Qualified Name:     tools.ontologyrepositoryapi.OntologyRepository
 * JD-Core Version:    0.6.2
 */