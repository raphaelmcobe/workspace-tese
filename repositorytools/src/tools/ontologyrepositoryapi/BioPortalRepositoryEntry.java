package tools.ontologyrepositoryapi;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public class BioPortalRepositoryEntry extends OntologyRepositoryEntryImpl {
	private String id;
	private String abbrev;
	private String format;

	public BioPortalRepositoryEntry(OntologyRepository repository, String displayName, String id, String abbrev, String format) {
		super(repository, displayName);
		this.id = id;
		this.abbrev = abbrev;
		this.format = format;
	}

	public String getId() {
		return this.id;
	}

	public String getAbbreviation() {
		return this.abbrev;
	}

	public String getFormat() {
		return this.format;
	}

	public boolean isOWLCompatibleFormat() {
		String upperCaseFormat = this.format.toUpperCase();
		return (upperCaseFormat.contains("OWL")) || (upperCaseFormat.contains("OBO"));
	}

	public OWLOntology loadOntology(MissingImportsTreatment missingImportsTreatment) throws OWLOntologyCreationException {
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		if (missingImportsTreatment.equals(MissingImportsTreatment.IGNORE)) {
			manager.setSilentMissingImportsHandling(true);
		}
		return manager.loadOntology(getDocumentIRI());
	}

	private IRI getDocumentIRI() {
		return IRI.create(this.getId()+"/download/?apikey="+BioPortalOntologyRepository.API_KEY);
	}

	public void getOntologyDocument(OutputStream outputStream) throws IOException {
		IRI documentIRI = getDocumentIRI();
		URL url = documentIRI.toURI().toURL();
		InputStream is = url.openStream();
		BufferedInputStream bis = new BufferedInputStream(is);
		byte[] buffer = new byte[4098];
		int read;
		while ((read = bis.read(buffer)) != -1) {
			outputStream.write(buffer, 0, read);
		}
		bis.close();
		outputStream.flush();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("RepositoryEntry( ");
		sb.append(this.id);
		sb.append(" ");
		sb.append(this.format);
		sb.append(" \"");
		sb.append(this.abbrev);
		sb.append("\" \"");
		sb.append(getDisplayName());
		sb.append("\" )");
		return sb.toString();
	}
}
