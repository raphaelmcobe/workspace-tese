/*     */
package tools.ontologyrepositoryapi;
/*     */ 
/*     */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import tools.ontologyrepositoryapi.tools.util.HTTPUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class BioPortalOntologyRepository
		implements OntologyRepository {

	public static final String REST_URL = "http://data.bioontology.org";
	public static final String API_KEY = "634e131a-75c5-4ed1-8936-cdc64d9b672f";

	private List<BioPortalRepositoryEntry> entries = new ArrayList();

	public BioPortalOntologyRepository() {
		this.entries = new ArrayList();
	}

	public String getName() {
		return null;
	}

	public List<OntologyRepositoryEntry> getEntries() {
		if (this.entries.isEmpty()) {
			refill();
		}
		return new ArrayList(this.entries);
	}

	public int getEntriesCount() {
		return this.entries.size();
	}

	public OntologyRepositoryEntry getEntry(int index) {
		return (OntologyRepositoryEntry) this.entries.get(index);
	}

	public void refill() {
//			HTTPUtil.get()
		ResponseParser responseParser = new ResponseParser();
		responseParser.parse();
//			parser.parse(is, responseParser);
		List entries = responseParser.getEntries();
		Collections.sort(entries, new Comparator<OntologyRepositoryEntry>() {
			@Override
			public int compare(OntologyRepositoryEntry o1, OntologyRepositoryEntry o2) {
				return o1.getDisplayName().compareToIgnoreCase(o2.getDisplayName());
			}

		});
		this.entries.clear();
		this.entries.addAll(entries);
	}

	private class ResponseParser extends DefaultHandler {
		private StringBuilder sb = new StringBuilder();
		private String ontologyId;
		private String displayName;
		private String abbrevName;
		private String format;
		private List<BioPortalRepositoryEntry> entries = new ArrayList();

		private boolean inACL = false;

		private int userEntryCount = 0;

		private ResponseParser() {
		}

		public void startElement(String uri, String localName, String qName, Attributes attributes)
				throws SAXException {
			this.sb = new StringBuilder();
			if (qName.equals("userAcl")) {
				this.inACL = true;
				this.userEntryCount = 0;
			}
		}

		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			String content = this.sb.toString().trim();
			if (qName.equals("ontologyId")) {
				this.ontologyId = content;
			} else if (qName.equals("displayLabel")) {
				this.displayName = content;
			} else if (qName.equals("abbreviation")) {
				this.abbrevName = content;
			} else if (qName.equals("ontologyBean")) {
				if (this.userEntryCount == 0) {
					this.entries.add(new BioPortalRepositoryEntry(BioPortalOntologyRepository.this, this.displayName, this.ontologyId, this.abbrevName, this.format));
				}
			} else if (qName.equals("format")) {
				this.format = content;
			} else if (qName.equals("userAcl")) {
				this.inACL = false;
			} else if ((qName.equals("userEntry")) &&
					(this.inACL))
				this.userEntryCount += 1;
		}

		public void characters(char[] ch, int start, int length)
				throws SAXException {
			this.sb.append(ch, start, length);
		}

		public List<BioPortalRepositoryEntry> getEntries() {
			return this.entries;
		}

		public void parse() {

			String resourcesString = HTTPUtil.get(REST_URL + "/");

			JsonNode resources = jsonToNode(resourcesString);

			// Follow the ontologies link by looking for the media type in the list of links
			String link = resources.get("links").findValue("ontologies").asText();

			// Get the ontologies from the link we found
			JsonNode ontologies = jsonToNode(HTTPUtil.get(link));

			// Get the name and ontology id from the returned list
			int i = 0;
			for (JsonNode ontology : ontologies) {


				JsonNode eachOntology = jsonToNode(HTTPUtil.get(ontology.get("@id").asText() + "/latest_submission"));
				JsonNode fileTypeNode = eachOntology.get("hasOntologyLanguage");

				if(fileTypeNode == null) continue;

				BioPortalRepositoryEntry entry = new BioPortalRepositoryEntry(BioPortalOntologyRepository.this, ontology.get("name").asText(),
						ontology.get("@id").asText(),
						ontology.get("acronym").asText(),
						fileTypeNode.asText());
			}

		}

		private JsonNode jsonToNode(String json) {
			JsonNode root = null;
			try {
				root = new ObjectMapper().readTree(json);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return root;
		}
	}
}
