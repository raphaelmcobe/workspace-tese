package tools.ontologyrepositoryapi;

import java.io.IOException;
import java.io.OutputStream;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

public abstract interface OntologyRepositoryEntry
{
  public abstract OntologyRepository getRepository();

  public abstract String getDisplayName();

  public abstract String getAbbreviation();

  public abstract String getFormat();

  public abstract boolean isOWLCompatibleFormat();

  public abstract OWLOntology loadOntology(MissingImportsTreatment paramMissingImportsTreatment)
    throws OWLOntologyCreationException;

  public abstract void getOntologyDocument(OutputStream paramOutputStream)
    throws IOException;
}

/* Location:           /tmp/repositorytools.jar
 * Qualified Name:     tools.ontologyrepositoryapi.OntologyRepositoryEntry
 * JD-Core Version:    0.6.2
 */