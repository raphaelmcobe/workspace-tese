/*    */ package tools.ontologyrepositoryapi;
/*    */ 
/*    */ public abstract class OntologyRepositoryEntryImpl
/*    */   implements OntologyRepositoryEntry
/*    */ {
/*    */   private OntologyRepository repository;
/*    */   private String displayName;
/*    */ 
/*    */   protected OntologyRepositoryEntryImpl(OntologyRepository repository, String displayName)
/*    */   {
/* 16 */     this.repository = repository;
/* 17 */     this.displayName = displayName;
/*    */   }
/*    */ 
/*    */   public OntologyRepository getRepository() {
/* 21 */     return this.repository;
/*    */   }
/*    */ 
/*    */   public String getDisplayName() {
/* 25 */     return this.displayName;
/*    */   }
/*    */ }

/* Location:           /tmp/repositorytools.jar
 * Qualified Name:     tools.ontologyrepositoryapi.OntologyRepositoryEntryImpl
 * JD-Core Version:    0.6.2
 */