/*     */ import java.io.PrintStream;
/*     */ import java.util.HashSet;
/*     */ import java.util.Set;
/*     */ import org.semanticweb.owlapi.apibinding.OWLManager;
/*     */ import org.semanticweb.owlapi.model.IRI;
/*     */ import org.semanticweb.owlapi.model.OWLDataFactory;
/*     */ import org.semanticweb.owlapi.model.OWLOntology;
/*     */ import org.semanticweb.owlapi.model.OWLOntologyCreationException;
/*     */ import org.semanticweb.owlapi.model.OWLOntologyManager;
/*     */ import org.semanticweb.owlapi.reasoner.BufferingMode;
/*     */ import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
/*     */ import org.semanticweb.owlapi.reasoner.structural.StructuralReasoner;
/*     */ 
/*     */ public class Test
/*     */ {
/*     */   public static void main(String[] args)
/*     */   {
/*     */     try
/*     */     {
/*  61 */       long tt0 = System.currentTimeMillis();
/*  62 */       OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
/*  63 */       OWLOntology ont = manager.loadOntologyFromOntologyDocument(IRI.create("http://www.co-ode.org/ontologies/pizza/pizza.owl"));
/*     */ 
/*  70 */       ont.getClassesInSignature();
/*  71 */       ont.getSubClassAxiomsForSubClass(ont.getOWLOntologyManager().getOWLDataFactory().getOWLThing());
/*  72 */       ont.getSubClassAxiomsForSuperClass(ont.getOWLOntologyManager().getOWLDataFactory().getOWLThing());
/*  73 */       ont.getEquivalentClassesAxioms(ont.getOWLOntologyManager().getOWLDataFactory().getOWLThing());
/*  74 */       long tt1 = System.currentTimeMillis();
/*  75 */       System.gc();
/*  76 */       System.gc();
/*  77 */       System.gc();
/*  78 */       dumpMemory();
/*  79 */       System.out.println("Loaded and cached in " + (tt1 - tt0));
/*     */ 
/*  81 */       StructuralReasoner reasoner = new StructuralReasoner(ont, new SimpleConfiguration(), BufferingMode.NON_BUFFERING);
/*  82 */       long t0 = System.currentTimeMillis();
/*     */ 
/*  84 */       reasoner.prepareReasoner();
/*  85 */       long t1 = System.currentTimeMillis();
/*  86 */       System.out.println("Done in " + (t1 - t0));
/*  87 */       System.gc();
/*  88 */       System.gc();
/*  89 */       System.gc();
/*  90 */       dumpMemory();
/*     */ 
/*  92 */       Set axioms = new HashSet();
/*  93 */       axioms.addAll(ont.getAxioms());
/*     */ 
/*  95 */       ont = null;
/*  96 */       System.gc();
/*  97 */       System.gc();
/*  98 */       System.gc();
/*  99 */       dumpMemory();
/* 100 */       System.out.println("Axioms: " + axioms.size());
/*     */     }
/*     */     catch (OWLOntologyCreationException e)
/*     */     {
/* 105 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */ 
/*     */   private static void dumpMemory()
/*     */   {
/* 113 */     Runtime r = Runtime.getRuntime();
/* 114 */     long totalMem = r.totalMemory();
/* 115 */     long freeMem = r.freeMemory();
/* 116 */     long usedMem = totalMem - freeMem;
/* 117 */     System.out.println("Memory Used: " + usedMem / 1048576L);
/*     */   }
/*     */ }

/* Location:           /tmp/repositorytools.jar
 * Qualified Name:     Test
 * JD-Core Version:    0.6.2
 */