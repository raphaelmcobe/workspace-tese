import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.TimeoutException;

public class KBMerging {

    /**
     * @param args
     */
    public static void main(String[] args) {

        try {
            Set<int[]> result = buildAllPossibleValues(3);
            int[] model1 = { 0, 1, 3, 1 };
            int[] model2 = { 1, 0, 0, 1 };
            System.out.println("Distancia entre modelos: "
                    + distanceBetweenModels(model1, model2));

            Set<int[]> kb1 = new HashSet<int[]>();

            int[] c1 = { 1, -2, 3 };
            int[] c2 = { -1, 2, -3 };
            kb1.add(c1);
            kb1.add(c2);

            Set<int[]> kb2 = new HashSet<int[]>();

            int[] c3 = {1, 2, 3};
            int[] c4 = {-1,-2,-3};

            kb2.add(c3);
            kb2.add(c4);

            Set<Set<int[]>> knowledgeSet = new HashSet<Set<int[]>>();

            knowledgeSet.add(kb1);
            knowledgeSet.add(kb2);

            Set<int[]> mergedSet = mergeKB(knowledgeSet);

        } catch (Exception e) {
            System.out.println("Problems!");
            e.printStackTrace();
        }

    }

    private static Set<int[]> mergeKB(Set<Set<int[]>> knowledgeSet)
            throws ContradictionException, TimeoutException {
        Set<int[]> toReturn = new HashSet<int[]>();
        Map<Set<int[]>, Set<int[]>> formulaModels = new HashMap<Set<int[]>, Set<int[]>>();
        for (Set<int[]> kb : knowledgeSet) {
            formulaModels.put(kb, Util.calculateModels(kb));
        }

        Set<Set<int[]>> keys = formulaModels.keySet();
        for (Set<int[]> eachKey : keys) {
            System.out.println("KB: ");
            for (int[] formula : eachKey) {
                System.out.print(Arrays.toString(formula)+", ");
            }
            System.out.println();
            Set<int[]> models = formulaModels.get(eachKey);
            for (int[] eachModel : models) {
                System.out.print(Arrays.toString(eachModel)+", ");
            }
            System.out.println();
        }
        
        int formulaSize = (formulaModels.values().iterator().next().iterator().next()).length;
        System.out.println("Tamanhos das Fórmulas: "+formulaSize);
 


        return toReturn;
    }

    private static Set<int[]> dMax(Map<Set<int[]>,Set<int[]>> formulaModels){
        int formulaSize = (formulaModels.values().iterator().next().iterator().next()).length;
       
        Map<Integer,Set<int[]>> distances = new HashMap<Integer,Set<int[]>>();
        
        Set<int[]> allPossibleValues = buildAllPossibleValues(formulaSize);
        Set<Set<int[]>> knowledgeBases = formulaModels.keySet();
        for (int[] eachModel : allPossibleValues) {
            int maxDistance = Integer.MIN_VALUE;
            for (Set<int[]> set : knowledgeBases) {
                int minDistanceBetweenModelAndAModelSet = minDistanceBetweenModelAndAModelSet(eachModel, formulaModels.get(set));
                if(minDistanceBetweenModelAndAModelSet >= maxDistance) {
                    maxDistance = minDistanceBetweenModelAndAModelSet;
                }
            }
            Set<int[]> modelWithDistance = distances.remove(maxDistance);
            if(modelWithDistance == null || modelWithDistance.size() == 0) {
                modelWithDistance = new HashSet<int[]>();
            }
            
            modelWithDistance.add(eachModel);
            distances.put(maxDistance, modelWithDistance);
        }

        Set<Integer> allDistances = distances.keySet();
        
        Integer[] sortedDistances = allDistances.toArray(new Integer[0]);
        Arrays.sort(sortedDistances);
        
        return distances.get(sortedDistances[0]);
    }
    
    private static int minDistanceBetweenModelAndAModelSet(int[] model, Set<int[]> modelSet) {
        int toReturn = Integer.MAX_VALUE;
        
        for (int[] is : modelSet) {
            int distanceBetweenModels = distanceBetweenModels(model, is);
            if(distanceBetweenModels <= toReturn) {
                toReturn = distanceBetweenModels;
            }
        }
        return toReturn;
    }
    
    

    private static int[] calculateFormula(int[] model) {
        int size = 0;
        for (int i = 0; i < model.length; i++) {
            if(model[i] == 3)
                size++;
        }
        int[] toReturn = new int[size];
        int formulaIndex = 0;
        for (int i = 0; i < model.length; i++) {
            if(model[i] == 0) {
                toReturn[formulaIndex++] = -i;
            }
            else if(model[i] == 1) {
                toReturn[formulaIndex++] = i;
            }
        }
        return toReturn;
    }

   

    public static Set<int[]> buildAllPossibleValues(int numberOfAtoms) {
        int amountOfEvaluations = (int) Math.pow(2, numberOfAtoms);
        Set<int[]> toReturn = new HashSet<int[]>();

        for (int i = 0; i < amountOfEvaluations; i++) {
            String binaryString = Integer.toBinaryString(i);
            toReturn.add(fillWithZeros(binaryString, numberOfAtoms));
        }
        return toReturn;
    }

    private static int[] fillWithZeros(String binaryString, int numberOfAtoms) {
        int[] toReturn = new int[numberOfAtoms];
        int fillingSize = numberOfAtoms - binaryString.length();
        for (int i = 0; i < fillingSize; i++) {
            toReturn[i] = 0;
        }
        int stringIndex = 0;
        for (int i = fillingSize; i < numberOfAtoms; i++) {
            toReturn[i] = Integer.parseInt(binaryString.substring(
                    stringIndex++, stringIndex));
        }
        return toReturn;
    }

    private static int distanceBetweenModels(int[] model1, int[] model2) {
        int toReturn = 0;
        if (model1.length != model2.length)
            return Integer.MAX_VALUE;
        for (int i = 0; i < model2.length; i++) {
            if ((model1[i] != 3 || model2[i] != 3) && (model1[i] != model2[i]))
                toReturn++;
        }
        return toReturn;
    }
}
