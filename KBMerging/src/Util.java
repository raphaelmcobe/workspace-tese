import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;
import org.sat4j.tools.ModelIterator;

public class Util {

    static Set<int[]> calculateModels(Set<int[]> kb)
            throws ContradictionException, TimeoutException {
        Set<int[]> toReturn = new HashSet<int[]>();
        ISolver solver = SolverFactory.newDefault();
        solver.setExpectedNumberOfClauses(kb.size());
        for (int[] formula : kb) {
            solver.newVar(formula.length);
            Arrays.sort(formula);
            solver.addClause(new VecInt(formula));
        }
        ModelIterator mi = new ModelIterator(solver);
        while (mi.isSatisfiable()) {
            int[] model = mi.model();
            int[] binaryModel = buildBinaryModel(model, model.length);
            toReturn.add(binaryModel);
        }
        return toReturn;
    }

    private static int[] buildBinaryModel(int[] model, int maxSize) {
        int[] toReturn = new int[maxSize];
        Arrays.fill(toReturn, 3);
        for (int i = 0; i < model.length; i++) {
            if (model[i] < 0) {
                int index = (model[i] * -1);
                toReturn[index - 1] = 0;
            } else {
                toReturn[model[i] - 1] = 1;
            }
        }
        return toReturn;
    }
}