<?xml version="1.0"?>


<!DOCTYPE rdf:RDF [
    <!ENTITY owl "http://www.w3.org/2002/07/owl#" >
    <!ENTITY xsd "http://www.w3.org/2001/XMLSchema#" >
    <!ENTITY rdfs "http://www.w3.org/2000/01/rdf-schema#" >
    <!ENTITY rdf "http://www.w3.org/1999/02/22-rdf-syntax-ns#" >
]>


<rdf:RDF xmlns="http://www.co-ode.org/ontologies/pizza/pizza.owl#"
     xml:base="http://www.co-ode.org/ontologies/pizza/pizza.owl"
     xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
     xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
     xmlns:owl="http://www.w3.org/2002/07/owl#">
    <owl:Ontology rdf:about="">
        <owl:versionInfo xml:lang="en"
            >v.1.4. Added Food class (used in domain/range of hasIngredient), Added several hasCountryOfOrigin restrictions on pizzas, Made hasTopping invers functional</owl:versionInfo>
        <owl:versionInfo rdf:datatype="&xsd;string">version 1.5</owl:versionInfo>
        <owl:versionInfo xml:lang="en"
            >v.1.5. Removed protege.owl import and references. Made ontology URI date-independent</owl:versionInfo>
        <rdfs:comment xml:lang="en"
            >An example ontology that contains all constructs required for the various versions of the Pizza Tutorial run by Manchester University (see http://www.co-ode.org/resources/tutorials/)</rdfs:comment>
    </owl:Ontology>
    


    <!-- 
    ///////////////////////////////////////////////////////////////////////////
    //
    //   OWL Classes
    //
    ///////////////////////////////////////////////////////////////////////////
     -->

    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#American -->

    <owl:Class rdf:about="#American">
        <rdfs:label xml:lang="pt">Americana</rdfs:label>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#TomatoTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#PeperoniSausageTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasCountryOfOrigin"/>
                <owl:hasValue rdf:resource="#America"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#MozzarellaTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#NamedPizza"/>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:allValuesFrom>
                    <owl:Class>
                        <owl:unionOf rdf:parseType="Collection">
                            <owl:Class rdf:about="#MozzarellaTopping"/>
                            <owl:Class rdf:about="#PeperoniSausageTopping"/>
                            <owl:Class rdf:about="#TomatoTopping"/>
                        </owl:unionOf>
                    </owl:Class>
                </owl:allValuesFrom>
            </owl:Restriction>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#AmericanHot -->

    <owl:Class rdf:about="#AmericanHot">
        <rdfs:label xml:lang="pt"
            >AmericanaPicante</rdfs:label>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#HotGreenPepperTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#JalapenoPepperTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#TomatoTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasCountryOfOrigin"/>
                <owl:hasValue rdf:resource="#America"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#PeperoniSausageTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#MozzarellaTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#NamedPizza"/>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:allValuesFrom>
                    <owl:Class>
                        <owl:unionOf rdf:parseType="Collection">
                            <owl:Class rdf:about="#MozzarellaTopping"/>
                            <owl:Class rdf:about="#PeperoniSausageTopping"/>
                            <owl:Class rdf:about="#JalapenoPepperTopping"/>
                            <owl:Class rdf:about="#TomatoTopping"/>
                            <owl:Class rdf:about="#HotGreenPepperTopping"/>
                        </owl:unionOf>
                    </owl:Class>
                </owl:allValuesFrom>
            </owl:Restriction>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#AnchoviesTopping -->

    <owl:Class rdf:about="#AnchoviesTopping">
        <rdfs:label xml:lang="pt"
            >CoberturaDeAnchovies</rdfs:label>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#FishTopping"/>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#ArtichokeTopping -->

    <owl:Class rdf:about="#ArtichokeTopping">
        <rdfs:label xml:lang="pt"
            >CoberturaDeArtichoke</rdfs:label>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#VegetableTopping"/>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasSpiciness"/>
                <owl:someValuesFrom rdf:resource="#Mild"/>
            </owl:Restriction>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#AsparagusTopping -->

    <owl:Class rdf:about="#AsparagusTopping">
        <rdfs:label xml:lang="pt"
            >CoberturaDeAspargos</rdfs:label>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#VegetableTopping"/>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasSpiciness"/>
                <owl:someValuesFrom rdf:resource="#Mild"/>
            </owl:Restriction>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#Cajun -->

    <owl:Class rdf:about="#Cajun">
        <rdfs:label xml:lang="pt">Cajun</rdfs:label>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#TomatoTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#TobascoPepperSauce"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:allValuesFrom>
                    <owl:Class>
                        <owl:unionOf rdf:parseType="Collection">
                            <owl:Class rdf:about="#PrawnsTopping"/>
                            <owl:Class rdf:about="#TobascoPepperSauce"/>
                            <owl:Class rdf:about="#MozzarellaTopping"/>
                            <owl:Class rdf:about="#TomatoTopping"/>
                            <owl:Class rdf:about="#OnionTopping"/>
                            <owl:Class rdf:about="#PeperonataTopping"/>
                        </owl:unionOf>
                    </owl:Class>
                </owl:allValuesFrom>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#MozzarellaTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#OnionTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#PeperonataTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#NamedPizza"/>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#PrawnsTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#CajunSpiceTopping -->

    <owl:Class rdf:about="#CajunSpiceTopping">
        <rdfs:label xml:lang="pt"
            >CoberturaDeCajun</rdfs:label>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasSpiciness"/>
                <owl:someValuesFrom rdf:resource="#Hot"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#HerbSpiceTopping"/>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#CaperTopping -->

    <owl:Class rdf:about="#CaperTopping">
        <rdfs:label xml:lang="pt"
            >CoberturaDeCaper</rdfs:label>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#VegetableTopping"/>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasSpiciness"/>
                <owl:someValuesFrom rdf:resource="#Mild"/>
            </owl:Restriction>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#Capricciosa -->

    <owl:Class rdf:about="#Capricciosa">
        <rdfs:label xml:lang="pt">Capricciosa</rdfs:label>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#TomatoTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#HamTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:allValuesFrom>
                    <owl:Class>
                        <owl:unionOf rdf:parseType="Collection">
                            <owl:Class rdf:about="#AnchoviesTopping"/>
                            <owl:Class rdf:about="#MozzarellaTopping"/>
                            <owl:Class rdf:about="#TomatoTopping"/>
                            <owl:Class rdf:about="#PeperonataTopping"/>
                            <owl:Class rdf:about="#HamTopping"/>
                            <owl:Class rdf:about="#CaperTopping"/>
                            <owl:Class rdf:about="#OliveTopping"/>
                        </owl:unionOf>
                    </owl:Class>
                </owl:allValuesFrom>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#OliveTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#MozzarellaTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#AnchoviesTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#PeperonataTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#NamedPizza"/>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#CaperTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#Caprina -->

    <owl:Class rdf:about="#Caprina">
        <rdfs:label xml:lang="pt">Caprina</rdfs:label>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#TomatoTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#SundriedTomatoTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#MozzarellaTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#NamedPizza"/>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:allValuesFrom>
                    <owl:Class>
                        <owl:unionOf rdf:parseType="Collection">
                            <owl:Class rdf:about="#MozzarellaTopping"/>
                            <owl:Class rdf:about="#TomatoTopping"/>
                            <owl:Class rdf:about="#SundriedTomatoTopping"/>
                            <owl:Class rdf:about="#GoatsCheeseTopping"/>
                        </owl:unionOf>
                    </owl:Class>
                </owl:allValuesFrom>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasTopping"/>
                <owl:someValuesFrom rdf:resource="#GoatsCheeseTopping"/>
            </owl:Restriction>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#CheeseTopping -->

    <owl:Class rdf:about="#CheeseTopping">
        <rdfs:label xml:lang="pt"
            >CoberturaDeQueijo</rdfs:label>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#PizzaTopping"/>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#CheeseyPizza -->

    <owl:Class rdf:about="#CheeseyPizza">
        <rdfs:comment xml:lang="en"
            >Any pizza that has at least 1 cheese topping.</rdfs:comment>
        <rdfs:label xml:lang="pt">PizzaComQueijo</rdfs:label>
        <owl:equivalentClass>
            <owl:Class>
                <owl:intersectionOf rdf:parseType="Collection">
                    <owl:Class rdf:about="#Pizza"/>
                    <owl:Restriction>
                        <owl:onProperty rdf:resource="#hasTopping"/>
                        <owl:someValuesFrom rdf:resource="#CheeseTopping"/>
                    </owl:Restriction>
                </owl:intersectionOf>
            </owl:Class>
        </owl:equivalentClass>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#CheeseyVegetableTopping -->

    <owl:Class rdf:about="#CheeseyVegetableTopping">
        <rdfs:label xml:lang="pt"
            >CoberturaDeQueijoComVegetais</rdfs:label>
        <rdfs:comment xml:lang="en"
            >This class will be inconsistent. This is because we have given it 2 disjoint parents, which means it could never have any members (as nothing can simultaneously be a CheeseTopping and a VegetableTopping). NB Called ProbeInconsistentTopping in the ProtegeOWL Tutorial.</rdfs:comment>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#CheeseTopping"/>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#VegetableTopping"/>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#ChickenTopping -->

    <owl:Class rdf:about="#ChickenTopping">
        <rdfs:label xml:lang="pt"
            >CoberturaDeFrango</rdfs:label>
        <rdfs:subClassOf>
            <owl:Class rdf:about="#MeatTopping"/>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="#hasSpiciness"/>
                <owl:someValuesFrom rdf:resource="#Mild"/>
            </owl:Restriction>
        </rdfs:subClassOf>
    </owl:Class>
    


    <!-- Class: http://www.co-ode.org/ontologies/pizza/pizza.owl#Country -->

    <owl:Class rdf:about="#Country">
        <rdfs:label xml:lang="pt">Pais</rdfs:label>
        <rdfs:comment xml:lang="en"
            >A class that is equivalent to the set of individuals that are described in the enumeration - ie Countries can only be either America, England, France, Germany or Italy and nothing else. Note that these individuals have been asserted to be allDifferent from each other.</rdfs:comment>
        <owl:equivalentClass>
            <owl:Class>
                <owl:intersectionOf rdf:parseType="Collection">
                    <owl:Class>
                        <owl:oneOf rdf:parseType="Collection">
                            <owl:Thing rdf:about="#America"/>
                            <owl:Thing rdf:about="#Italy"/>
                            <owl:Thing rdf:about="#Germany"/>
                            <owl:Thing rdf:about="#France"/>
                            <owl:Thing rdf:about="#England"/>
                        </owl:oneOf>
                    </owl:Class>
                    <owl:Class rdf:about="#DomainConcept"/>
                </owl:intersectionOf>
            </owl:Class>
        </owl:equivalentClass>
    </owl:Class>
    

</rdf:RDF>
