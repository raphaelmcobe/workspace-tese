package bcontractor.base;

import static bcontractor.base.BitFieldUtils.createField;
import static junit.framework.Assert.assertEquals;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class BitFieldUtilsTest {

	@Test
	public void workWithOrderZero() {
		checkSingleIndex(createField(0, elements(0x0000)), 1, 0);
		checkSingleIndex(createField(0, elements(0x0001)), 1, 1);
		checkSingleIndex(createField(0, elements(0x0002)), 1, 2);
		checkSingleIndex(createField(0, elements(0x0003)), 1, 3);
		checkSingleIndex(createField(0, elements(0x003F)), 1, 63);
		checkSingleIndex(createField(0, elements(0x0040)), 1, 0);
	}
	
	@Test
	public void workWithOrderOne() {
		checkSingleIndex(createField(1, elements(0x0000)), 2, 0);
		checkSingleIndex(createField(1, elements(0x003F)), 2, 63);
		checkSingleIndex(createField(1, elements(0x0040)), 2, 64);
		checkSingleIndex(createField(1, elements(0x007F)), 2, 127);
		checkSingleIndex(createField(1, elements(0x0080)), 2, 0);
	}
	
	@Test
	public void workWithOrderTwo() {
		checkSingleIndex(createField(2, elements(0x0000)), 4, 0);
		checkSingleIndex(createField(2, elements(0x00FF)), 4, 255);
		checkSingleIndex(createField(2, elements(0x0100)), 4, 0);
	}
	
	@Test
	public void suggestsOrder() {
		assertThat(BitFieldUtils.suggestOrder(0), is(0));
		assertThat(BitFieldUtils.suggestOrder(31), is(0));
		assertThat(BitFieldUtils.suggestOrder(32), is(1));
		assertThat(BitFieldUtils.suggestOrder(63), is(1));
		assertThat(BitFieldUtils.suggestOrder(64), is(2));
		assertThat(BitFieldUtils.suggestOrder(127), is(2));
		assertThat(BitFieldUtils.suggestOrder(128), is(3));
		assertThat(BitFieldUtils.suggestOrder(255), is(3));
		assertThat(BitFieldUtils.suggestOrder(256), is(4));
		assertThat(BitFieldUtils.suggestOrder(511), is(4));
		assertThat(BitFieldUtils.suggestOrder(512), is(5));
		assertThat(BitFieldUtils.suggestOrder(1023), is(5));
		assertThat(BitFieldUtils.suggestOrder(1024), is(6));
		assertThat(BitFieldUtils.suggestOrder(2047), is(6));
		assertThat(BitFieldUtils.suggestOrder(2048), is(7));
		assertThat(BitFieldUtils.suggestOrder(4095), is(7));
		assertThat(BitFieldUtils.suggestOrder(4096), is(8));
		assertThat(BitFieldUtils.suggestOrder(Integer.MAX_VALUE), is(8));
	}
	
	@Test
	public void efficiency() {
		Iterable<?> elements = elements(Double.valueOf(Math.random()).intValue(), Double.valueOf(Math.random()).intValue(), Double.valueOf(Math.random()).intValue(), Double.valueOf(Math.random()).intValue());
		for(int i = 0; i < 10000000; i++) {
			createField(1, elements);
		}
	}
	
	
	private void checkSingleIndex(long[] actual, int subfields, int index) {
		assertEquals(subfields, actual.length);
		int subfield = index / 64;
		for(int i = 0; i < actual.length; i++) {
			if(i == subfield) {
				assertEquals(Long.toBinaryString(0x00000001L << index % 64), Long.toBinaryString(actual[i]));
			} else {
				assertEquals(0x00000000L, actual[i]);
			}
		}
	}

	private Iterable<?> elements(int... hashCodes) {
		List<MockHashCode> elements = new ArrayList<MockHashCode>();
		for(int hashCode : hashCodes) {
			elements.add(new MockHashCode(hashCode));
		}
		return elements;
	}

	static class MockHashCode {
		private final int hashCode;
		
		public MockHashCode( int hashCode) {
			this.hashCode = hashCode;
		}
		
		@Override
		public int hashCode() {
			return hashCode;
		}
	}
	
	
}

