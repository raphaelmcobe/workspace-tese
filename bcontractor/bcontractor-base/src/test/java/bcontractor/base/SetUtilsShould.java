package bcontractor.base;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import bcontractor.api.ISet;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.PropositionalSentenceParser;

/**
 * Tests static helper methods on SetUtils
 * 
 * @author lundberg
 * 
 */
public class SetUtilsShould {

	private PropositionalSentenceParser parser;

	@Before
	public void init() {
		this.parser = new PropositionalSentenceParser();
	}

	@Test
	public void intersectionOfNoSetsIsEmpty() {
		Set<ISet<PropositionalSentence>> setOfSets = new HashSet<ISet<PropositionalSentence>>();
		assertThat(ISets.intersection(setOfSets).isEmpty(), is(true));
	}

	@Test
	public void intersectionOfSingleSetIsTheSingleSet() {
		Set<ISet<PropositionalSentence>> setOfSets = new HashSet<ISet<PropositionalSentence>>();
		setOfSets.add(this.parser.createSet("a", "a v b"));
		assertThat(ISets.intersection(setOfSets), is(equalTo(this.parser.createSet("a", "a v b"))));
	}

	@Test
	public void intersectionOfManySetsContainsCommonElements() {
		Set<ISet<PropositionalSentence>> setOfSets = new HashSet<ISet<PropositionalSentence>>();
		setOfSets.add(this.parser.createSet("a", "a v b", "d"));
		setOfSets.add(this.parser.createSet("a", "a v b", "d", "c"));
		setOfSets.add(this.parser.createSet("a v b", "d", "c"));
		assertThat(ISets.intersection(setOfSets), is(equalTo(this.parser.createSet("a v b", "d"))));
	}
}
