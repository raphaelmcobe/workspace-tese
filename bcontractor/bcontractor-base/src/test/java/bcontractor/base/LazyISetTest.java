package bcontractor.base;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import bcontractor.api.ISet;

public class LazyISetTest {

	@Test
	public void empty() {
		assertThat(ISets.empty().size(), is(0));
	}

	@Test
	public void union() {
		MockElement element = new MockElement();
		ISet<Object> set = ISets.empty().union(element);
		assertThat(set.size(), is(1));
		assertThat(set.contains(element), is(true));
	}

	@Test
	public void unionAll() {
		MockElement element = new MockElement();
		ISet<MockElement> set = ISets.<MockElement>empty().union(ISets.singleton(element));
		assertThat(set.size(), is(1));
		assertThat(set.contains(element), is(true));
	}


	@Test
	public void minus() {
		MockElement element = new MockElement();
		ISet<Object> set = ISets.empty().union(element).minus(element);
		assertThat(set.size(), is(0));
		assertThat(set.contains(element), is(false));
	}


	@Test
	public void minusAll() {
		MockElement element = new MockElement();
		ISet<Object> set = ISets.empty().union(element).minus(ISets.singleton(element));
		assertThat(set.size(), is(0));
		assertThat(set.contains(element), is(false));
	}


	@Test
	public void intersection() {
		MockElement a = new MockElement();
		MockElement b = new MockElement();
		MockElement c = new MockElement();
		ISet<Object> l = ISets.empty().union(a).union(c);
		ISet<Object> r = ISets.empty().union(b).union(c);
		ISet<Object> i = l.intersection(r);
		assertThat(i.size(), is(1));
		assertThat(i.contains(c), is(true));
	}

	@Test
	public void fastUnionAll() {
		ISet<Object> l = ISets.empty();
		ISet<Object> r = ISets.empty();
		for(int i = 0; i < 1000; i++) {
			l = l.union(new MockElement());
			r = r.union(new MockElement());
		}
		for(int i = 0; i < 100000000; i++) {
			l.containsAll(r);
		}
	}

	@Test
	public void avoidsStackOverflow() {
		ISet<Object> l = ISets.empty();
		for(int i = 0; i < 10000; i++) {
			l = l.union(i);
		}
		assertThat(l.size(), is(10000));
	}


	static class MockElement {
		private final int hashCode;

		public MockElement() {
			this.hashCode = super.hashCode();
		}

		public MockElement( int hashCode) {
			this.hashCode = hashCode;
		}

		@Override
		public int hashCode() {
			return hashCode;
		}
	}

}
