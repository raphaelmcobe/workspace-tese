package bcontractor.partialmeet.operators;

import static bcontractor.propositional.sat4j.PropositionalSAT4JSATReasonerBuilder.aReasoner;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import bcontractor.api.SATReasoner;
import bcontractor.partialmeet.RemainderSet;
import bcontractor.partialmeet.RemainderSetOperator;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.inputfactory.ArbitraryClauseFactory;
import bcontractor.propositional.inputfactory.ContractionCase;
import bcontractor.propositional.inputfactory.ContractionCaseFactory;
import bcontractor.propositional.inputfactory.PropositionalSentenceSetFactory;

/**
 * Runs several remainder set algorithms on random generated problemas, checking
 * if all reach the same answer.
 * 
 * @author Lundberg
 * 
 */
public class RandomProblemsRemainderSetTest {

	private static final int samples = 10;

	private static final int minSize = 5;

	private static final int maxSize = 8;

	private static final int nVariables = 10;

	private static final int nClauses = 50;

	private ContractionCaseFactory<PropositionalSentence> factory;

	private SATReasoner<PropositionalSentence> reasoner;

	private List<RemainderSetOperator<PropositionalSentence>> operators = new ArrayList<RemainderSetOperator<PropositionalSentence>>();

	@Before
	public void initialize() {
		Random random = new Random(System.currentTimeMillis());
		ArbitraryClauseFactory sentenceFactory = new ArbitraryClauseFactory(random, nVariables, minSize, maxSize);
		PropositionalSentenceSetFactory sentenceSetFactory = new PropositionalSentenceSetFactory(sentenceFactory, nVariables, nClauses);
		reasoner = aReasoner().build();
		factory = new ContractionCaseFactory<PropositionalSentence>(sentenceSetFactory, sentenceFactory, reasoner);
		operators.add(BlackboxRemainderSetOperatorBuilder.aRemainderSetOperator().build());
		operators.add(MaxSATRemainderSetOperatorBuilder.aRemainderSetOperator().build());
	}

	@Test
	public void testRandomProblems() {
		for (int i = 0; i < samples; i++) {
			ContractionCase<PropositionalSentence> contractionCase = factory.create();
			List<RemainderSet<PropositionalSentence>> remainders = new ArrayList<RemainderSet<PropositionalSentence>>();
			for (RemainderSetOperator<PropositionalSentence> operator : operators) {
				remainders.add(operator.eval(contractionCase.getSentences(), contractionCase.getSentence()));
			}
			if (new HashSet<RemainderSet<PropositionalSentence>>(remainders).size() != 1) {
				System.out.println(contractionCase.getSentences());
				System.out.println(contractionCase.getSentence());
				fail("All kernels should be equal. " + remainders);
			}
		}
	}
}
