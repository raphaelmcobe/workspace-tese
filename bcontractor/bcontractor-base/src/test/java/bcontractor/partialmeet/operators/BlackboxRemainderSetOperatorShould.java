package bcontractor.partialmeet.operators;

import static bcontractor.partialmeet.operators.BlackboxRemainderSetOperatorBuilder.aRemainderSetOperator;
import bcontractor.partialmeet.RemainderSetOperator;
import bcontractor.propositional.PropositionalSentence;

/**
 * Unit test for blackbox kernel operator. All tests for kernel operators should
 * pass.
 * 
 * @author lundberg
 * 
 */
public class BlackboxRemainderSetOperatorShould extends ARemainderSetOperatorShould {

    /**
     * {@inheritDoc}
     */
    @Override
    protected RemainderSetOperator<PropositionalSentence> createRemainderSetOperator() {
        return aRemainderSetOperator().build();
    }
}
