package bcontractor.partialmeet.operators;

import bcontractor.api.MaxSATReasoner;
import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.builder.Builder;
import bcontractor.builders.BuilderHelper;
import bcontractor.builders.NullBuilder;
import bcontractor.partialmeet.operators.MaxSATRemainderSetOperator;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.sat4j.PropositionalSAT4JMaxSATReasonerBuilder;
import bcontractor.propositional.sat4j.PropositionalSAT4JSATReasonerBuilder;

public class MaxSATRemainderSetOperatorBuilder<S extends Sentence<S>> implements Builder<MaxSATRemainderSetOperator<S>> {

	private Builder<SATReasoner<S>> sat = new NullBuilder<SATReasoner<S>>();

	private Builder<MaxSATReasoner<S>> maxsat = new NullBuilder<MaxSATReasoner<S>>();

	/**
	 * 
	 * @return BlackboxKernelResolverBuilder
	 */
	public static MaxSATRemainderSetOperatorBuilder<PropositionalSentence> aRemainderSetOperator() {
		return new MaxSATRemainderSetOperatorBuilder<PropositionalSentence>().sat(new PropositionalSAT4JSATReasonerBuilder()).maxsat(
				new PropositionalSAT4JMaxSATReasonerBuilder());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MaxSATRemainderSetOperator<S> build() {
		return new MaxSATRemainderSetOperator<S>(this.sat.build(), this.maxsat.build());
	}

	/**
	 * @param aReasoner
	 */
	public MaxSATRemainderSetOperatorBuilder<S> sat(Builder<? extends SATReasoner<S>> aReasoner) {
		return BuilderHelper.copyWith(this, "sat", aReasoner);
	}

	/**
	 * @param aReasoner
	 */
	public MaxSATRemainderSetOperatorBuilder<S> maxsat(Builder<? extends MaxSATReasoner<S>> aReasoner) {
		return BuilderHelper.copyWith(this, "maxsat", aReasoner);
	}
}
