package bcontractor.partialmeet;

import bcontractor.api.Sentence;
import bcontractor.builder.Builder;
import bcontractor.builders.BuilderHelper;
import bcontractor.builders.DoneBuilder;
import bcontractor.builders.NullBuilder;
import bcontractor.partialmeet.operators.BlackboxRemainderSetOperatorBuilder;
import bcontractor.partialmeet.selection.FullSelectionFunction;
import bcontractor.propositional.PropositionalSentence;

/**
 * Builder for PartialMeetContractionOperator
 * 
 * @author lundberg
 * 
 */
public class PartialMeetContractionOperatorBuilder<S extends Sentence<S>> implements Builder<PartialMeetContractionOperator<S>> {

    private Builder<? extends RemainderSetOperator<S>> remainder = new NullBuilder<RemainderSetOperator<S>>();

    private Builder<? extends SelectionFunction<S>> selection = new NullBuilder<SelectionFunction<S>>();

    public static PartialMeetContractionOperatorBuilder<PropositionalSentence> aContractionOperator() {
        PartialMeetContractionOperatorBuilder<PropositionalSentence> op = new PartialMeetContractionOperatorBuilder<PropositionalSentence>();
        op = op.withRemainderSetOperator(BlackboxRemainderSetOperatorBuilder.aRemainderSetOperator());
        return op.withSelectionFunction(new DoneBuilder<SelectionFunction<PropositionalSentence>>(new FullSelectionFunction<PropositionalSentence>()));
    }

    @Override
    public PartialMeetContractionOperator<S> build() {
        return new PartialMeetContractionOperator<S>(this.remainder.build(), this.selection.build());
    }

    public PartialMeetContractionOperatorBuilder<S> withRemainderSetOperator(Builder<? extends RemainderSetOperator<S>> aRemainder) {
        return BuilderHelper.copyWith(this, "remainder", aRemainder);
    }

    public PartialMeetContractionOperatorBuilder<S> withSelectionFunction(Builder<? extends SelectionFunction<S>> aSelection) {
        return BuilderHelper.copyWith(this, "selection", aSelection);
    }
}
