package bcontractor.partialmeet;

import bcontractor.api.Sentence;
import bcontractor.builder.Builder;
import bcontractor.builders.BuilderHelper;
import bcontractor.builders.DoneBuilder;
import bcontractor.builders.NullBuilder;
import bcontractor.partialmeet.operators.BlackboxRemainderSetOperatorBuilder;
import bcontractor.partialmeet.selection.FullSelectionFunction;
import bcontractor.propositional.PropositionalSentence;

/**
 * Builder for PartialMeetRevisionOperator
 * 
 * @author lundberg
 * 
 */
public class PartialMeetRevisionOperatorBuilder<S extends Sentence<S>> implements Builder<PartialMeetRevisionOperator<S>> {

    private Builder<? extends RemainderSetOperator<S>> remainder = new NullBuilder<RemainderSetOperator<S>>();

    private Builder<? extends SelectionFunction<S>> selection = new NullBuilder<SelectionFunction<S>>();

    public static PartialMeetRevisionOperatorBuilder<PropositionalSentence> aRevisionOperator() {
        PartialMeetRevisionOperatorBuilder<PropositionalSentence> op = new PartialMeetRevisionOperatorBuilder<PropositionalSentence>();
        op = op.withRemainderSetOperator(BlackboxRemainderSetOperatorBuilder.aRemainderSetOperator());
        return op.withSelectionFunction(new DoneBuilder<SelectionFunction<PropositionalSentence>>(new FullSelectionFunction<PropositionalSentence>()));
    }

    @Override
    public PartialMeetRevisionOperator<S> build() {
        return new PartialMeetRevisionOperator<S>(this.remainder.build(), this.selection.build());
    }

    public PartialMeetRevisionOperatorBuilder<S> withRemainderSetOperator(Builder<? extends RemainderSetOperator<S>> aRemainderSetOperator) {
        return BuilderHelper.copyWith(this, "remainder", aRemainderSetOperator);
    }

    public PartialMeetRevisionOperatorBuilder<S> withSelectionFunction(Builder<? extends SelectionFunction<S>> aSelectionFunction) {
        return BuilderHelper.copyWith(this, "selection", aSelectionFunction);
    }
}
