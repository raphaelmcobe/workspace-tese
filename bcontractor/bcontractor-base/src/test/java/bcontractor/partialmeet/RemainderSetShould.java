package bcontractor.partialmeet;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import org.junit.Before;
import org.junit.Test;

import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.PropositionalSentenceParser;

public class RemainderSetShould {

    private RemainderSetBuilder<PropositionalSentence> builder;

    private PropositionalSentenceParser parser;

    @Before
    public void init() {
        this.parser = new PropositionalSentenceParser();
        this.builder = RemainderSetBuilder.aRemainderSet().with(this.parser.createSet("a"));
    }

    @Test
    public void implementEqualsAndHashCode() {
        RemainderSet<PropositionalSentence> built = this.builder.build();
        assertThat(built.hashCode(), not(System.identityHashCode(built.hashCode())));
        assertThat(built.equals(built), is(true));
        assertThat(built.equals(new Object()), is(false));

        assertThat(built.equals(this.builder.with(this.parser.createSet("b")).build()), is(false));

        assertThat(built.hashCode(), not(this.builder.with(this.parser.createSet("b")).build().hashCode()));

        assertThat(built.toString(), not(containsString("@")));
    }
}
