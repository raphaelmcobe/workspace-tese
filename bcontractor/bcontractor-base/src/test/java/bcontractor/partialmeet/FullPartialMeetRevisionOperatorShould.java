package bcontractor.partialmeet;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import bcontractor.api.ISet;
import bcontractor.api.RevisionOperator;
import bcontractor.api.SATReasoner;
import bcontractor.base.ISets;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.PropositionalSentenceParser;
import bcontractor.propositional.sat4j.PropositionalSAT4JSATReasonerBuilder;

/**
 * Unit test for partial meet revision operator using full selection function.
 * 
 * @author lundberg
 * 
 */
public class FullPartialMeetRevisionOperatorShould {

	private RevisionOperator<PropositionalSentence> revision;

	private SATReasoner<PropositionalSentence> reasoner;

	private PropositionalSentenceParser parser;

	@Before
	public void init() {
		this.revision = PartialMeetRevisionOperatorBuilder.aRevisionOperator().build();
		this.reasoner = PropositionalSAT4JSATReasonerBuilder.aReasoner().build();
		this.parser = new PropositionalSentenceParser();
	}

	@Test
	public void stripSentenceStraightforward() {
		ISet<PropositionalSentence> base = this.parser.createSet("a", "b", "a v b");
		PropositionalSentence target = this.parser.create("¬a");
		ISet<PropositionalSentence> result = this.revision.revise(base, target);
		assertThat(this.reasoner.isSatisfiable(result), is(true));
		assertThat(this.reasoner.entails(result, target), is(true));
		assertThat(result, equalTo(this.parser.createSet("¬a", "b", "a v b")));
	}

	@Test
	public void stripSentencesThatEntailed() {
		ISet<PropositionalSentence> base = this.parser.createSet("¬a v b", "a", "c");
		PropositionalSentence target = this.parser.create("¬b");
		ISet<PropositionalSentence> result = this.revision.revise(base, target);
		assertThat(this.reasoner.isSatisfiable(result), is(true));
		assertThat(this.reasoner.entails(result, target), is(true));
		assertThat(result, equalTo(this.parser.createSet("¬b", "c")));
	}

	@Test
	public void stripSentencesFromAllKernels() {
		ISet<PropositionalSentence> base = ISets.empty();
		base = base.union(this.parser.createSet("¬b v a", "b"));
		base = base.union(this.parser.createSet("¬b v a", "¬c v ¬d v b", "¬e v d", "c", "e"));
		base = base.union(this.parser.createSet("j v k", "m v n v ¬q", "m v ¬n v q v j"));

		PropositionalSentence target = this.parser.create("¬a");

		ISet<PropositionalSentence> result = this.revision.revise(base, target);
		assertThat(this.reasoner.isSatisfiable(result), is(true));
		assertThat(this.reasoner.entails(result, target), is(true));

		assertThat(result, equalTo(this.parser.createSet("j v k", "m v n v ¬q", "m v ¬n v q v j", "¬a")));
	}

	@Test
	public void doNothingIfNotNecessary() {
		ISet<PropositionalSentence> base = ISets.empty();
		base = base.union(this.parser.createSet("¬b v a"));
		base = base.union(this.parser.createSet("¬b v a", "¬c v ¬d v b", "¬e v d", "e"));
		base = base.union(this.parser.createSet("j v k", "m v n v ¬q", "m v ¬n v q v j"));

		PropositionalSentence target = this.parser.create("¬a");

		ISet<PropositionalSentence> result = this.revision.revise(base, target);

		assertThat(this.reasoner.isSatisfiable(result), is(true));
		assertThat(this.reasoner.entails(result, target), is(true));

		assertThat(result, equalTo(base.union(target)));
	}
}
