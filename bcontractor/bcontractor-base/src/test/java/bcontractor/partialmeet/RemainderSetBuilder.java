package bcontractor.partialmeet;

import bcontractor.api.ISet;
import bcontractor.api.Sentence;
import bcontractor.base.ISets;
import bcontractor.builder.Builder;
import bcontractor.builders.BuilderHelper;
import bcontractor.propositional.PropositionalSentence;

/**
 * 
 * Builder for a RemainderSet
 * 
 * @author lundberg
 * 
 */
public class RemainderSetBuilder<S extends Sentence<S>> implements Builder<RemainderSet<S>> {

	private ISet<ISet<S>> elements = ISets.empty();

	@Override
	public RemainderSet<S> build() {
		return new RemainderSet<S>(this.elements);
	}

	public static RemainderSetBuilder<PropositionalSentence> aRemainderSet() {
		return new RemainderSetBuilder<PropositionalSentence>();
	}

	public RemainderSetBuilder<S> with(ISet<S> element) {
		return BuilderHelper.copyWith(this, "elements", this.elements.union(element));
	}
}
