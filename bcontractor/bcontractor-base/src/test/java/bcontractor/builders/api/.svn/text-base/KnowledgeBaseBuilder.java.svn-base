package bcontractor.builders.api;

import bcontractor.api.ISet;
import bcontractor.api.Sentence;
import bcontractor.base.ISets;
import bcontractor.builder.Builder;
import bcontractor.builders.BuilderHelper;
import bcontractor.propositional.PropositionalSentence;

/**
 * 
 * Builder for knowledge bases bases
 * 
 * @author lundberg
 * 
 */
public class KnowledgeBaseBuilder<S extends Sentence<S>> implements Builder<ISet<S>> {

	private ISet<S> sentences;

	/**
	 * Construtor
	 */
	public KnowledgeBaseBuilder() {
		this.sentences = ISets.empty();
	}

	public static KnowledgeBaseBuilder<PropositionalSentence> aKnowledgeBase() {
		return new KnowledgeBaseBuilder<PropositionalSentence>();
	}

	@Override
	public ISet<S> build() {
		return sentences;
	}

	public KnowledgeBaseBuilder<S> with(S sentence) {
		return BuilderHelper.copyWith(this, "sentences", this.sentences.union(sentence));
	}

	public KnowledgeBaseBuilder<S> with(ISet<S> someSentences) {
		return BuilderHelper.copyWith(this, "sentences", this.sentences.union(someSentences));
	}
}
