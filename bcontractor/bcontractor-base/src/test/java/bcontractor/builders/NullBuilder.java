package bcontractor.builders;

import bcontractor.builder.Builder;

/**
 * Builder that always returns null.
 * 
 * @author lundberg
 * 
 */
public class NullBuilder<T> implements Builder<T> {

    /**
     * {@inheritDoc}
     */
    @Override
    public T build() {
        return null;
    }
}
