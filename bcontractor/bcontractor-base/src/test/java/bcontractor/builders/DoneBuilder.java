package bcontractor.builders;

import bcontractor.builder.Builder;

/**
 * Builder that wrapps any object as it's builder
 * 
 * @author lundberg
 * 
 * @param <T>
 */
public class DoneBuilder<T> implements Builder<T> {

    private final T result;

    public DoneBuilder(T result) {
        this.result = result;
    }

    @Override
    public T build() {
        return this.result;
    }
}
