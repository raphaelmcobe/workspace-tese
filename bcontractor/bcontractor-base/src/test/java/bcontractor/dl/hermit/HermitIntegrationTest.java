package bcontractor.dl.hermit;

import java.io.File;

import org.junit.Test;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import bcontractor.api.ISet;
import bcontractor.base.ISets;
import bcontractor.dl.owl.OWLSentence;
import bcontractor.dl.owl.hermit.OWLHermitReasoner;
import bcontractor.partialmeet.operators.BlackboxRemainderSetOperator;

public class HermitIntegrationTest {

	@Test
	public void basic() throws Exception {
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		
		OWLOntology o = m.loadOntologyFromOntologyDocument(new File("bcontractor-base/src/test/resources/bcontractor/dl/hermit/pizza.owl"));
		ISet<OWLSentence> base = ISets.empty();
		for (OWLAxiom axiom : o.getTBoxAxioms(false)) {
			base = base.union(new OWLSentence(axiom));
		}
		OWLHermitReasoner reasoner = new OWLHermitReasoner();
		BlackboxRemainderSetOperator<OWLSentence> remainder = new BlackboxRemainderSetOperator<OWLSentence>(reasoner);

		remainder.eval(base, new OWLSentence(o.getTBoxAxioms(false).iterator().next()));

	}
}
