package bcontractor.dl.hermit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;

import org.junit.Test;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import bcontractor.api.ISet;
import bcontractor.base.ISets;
import bcontractor.dl.owl.OWLSentence;
import bcontractor.dl.owl.hermit.OWLHermitReasoner;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.operators.BlackboxKernelOperator;

public class HermitFlyingPenguinConflictTest {

	@Test
	public void basic() throws Exception {
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		OWLOntology o = m.loadOntologyFromOntologyDocument(new File("bcontractor-base/src/test/resources/bcontractor/dl/hermit/flying_penguin_incoherent.owl"));
		ISet<OWLSentence> base = ISets.empty();
		for (OWLAxiom axiom : o.getTBoxAxioms(false)) {
			base = base.union(new OWLSentence(axiom));
		}
		
		for (OWLAxiom axiom : o.getABoxAxioms(false)) {
			System.out.println("AxiomType: "+axiom);
			base = base.union(new OWLSentence(axiom));
		}
		OWLDataFactory df = m.getOWLDataFactory();
		OWLClass flies = df.getOWLClass(IRI.create("http://www.ime.usp.br/ontologies/FlyingPenguin#Flies"));
		OWLClass penguin = df.getOWLClass(IRI.create("http://www.ime.usp.br/ontologies/FlyingPenguin#Penguin"));
		OWLNamedIndividual tweety = df.getOWLNamedIndividual(IRI.create("http://www.ime.usp.br/ontologies/FlyingPenguin#Tweety"));
		


		OWLSentence penguinDoesntFly = new OWLSentence(df.getOWLDisjointClassesAxiom(penguin, flies));
		OWLSentence tweetyIsAPenguin = new OWLSentence(df.getOWLClassAssertionAxiom(penguin, tweety));
		OWLSentence tweetyDoesntFly = new OWLSentence(df.getOWLClassAssertionAxiom(df.getOWLObjectComplementOf(flies),tweety));
		OWLHermitReasoner reasoner = new OWLHermitReasoner();
		BlackboxKernelOperator<OWLSentence> blackbox = new BlackboxKernelOperator<OWLSentence>(reasoner);
		Kernel<OWLSentence> kernelSet =  blackbox.eval(base);
		assertThat(kernelSet.getElements().size(), is(0));
		
		
		kernelSet = blackbox.eval(base.union(penguinDoesntFly));
		assertThat(kernelSet.getElements().size(), is(1));
		
		kernelSet = blackbox.eval(base.union(tweetyDoesntFly).union(tweetyIsAPenguin));
		assertThat(kernelSet.getElements().size(), is(1));
		
		
		kernelSet = blackbox.eval(base.union(tweetyIsAPenguin)
				.union(penguinDoesntFly).union(tweetyDoesntFly));

		
		int i = 0;
		for (ISet<OWLSentence> kernel : kernelSet) {
			System.out.println("Kernel"+ i++);
			for (OWLSentence owlSentence : kernel) {
				System.out.println(owlSentence.getAxiom());
			}
			System.out.println("---------------");
		}
		
		assertThat(kernelSet.getElements().size(), is(2));

	}
}
