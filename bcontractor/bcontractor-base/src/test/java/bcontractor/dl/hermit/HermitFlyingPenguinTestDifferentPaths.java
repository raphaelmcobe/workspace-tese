package bcontractor.dl.hermit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;

import org.junit.Test;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import bcontractor.api.ISet;
import bcontractor.base.ISets;
import bcontractor.dl.owl.OWLSentence;
import bcontractor.dl.owl.hermit.OWLHermitReasoner;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.operators.BlackboxKernelOperator;

public class HermitFlyingPenguinTestDifferentPaths {

	@Test
	public void basic() throws Exception {
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		OWLOntology o = m.loadOntologyFromOntologyDocument(new File("bcontractor-base/src/test/resources/bcontractor/dl/hermit/test_different_paths.owl"));
		ISet<OWLSentence> base = ISets.empty();
		for (OWLAxiom axiom : o.getTBoxAxioms(false)) {
			base = base.union(new OWLSentence(axiom));
		}
		OWLDataFactory df = m.getOWLDataFactory();
		OWLClass animalVoador = df.getOWLClass(IRI.create("http://www.ime.usp.br/ontologies/test2#A"));
		OWLClass pinguim = df.getOWLClass(IRI.create("http://www.ime.usp.br/ontologies/test2#D"));
		OWLSentence DSubClassOfA = new OWLSentence(df.getOWLSubClassOfAxiom(pinguim, animalVoador));

		OWLHermitReasoner reasoner = new OWLHermitReasoner();

		assertThat(reasoner.isSatisfiable(base.union(DSubClassOfA)), is(true));
		assertThat(reasoner.entails(base, DSubClassOfA), is(true));

		BlackboxKernelOperator<OWLSentence> blackbox = new BlackboxKernelOperator<OWLSentence>(reasoner);
		
		Kernel<OWLSentence> kernelSet = blackbox.eval(base,DSubClassOfA);
		
		assertThat(kernelSet.getElements().size(), is(2));
		

	}
}
