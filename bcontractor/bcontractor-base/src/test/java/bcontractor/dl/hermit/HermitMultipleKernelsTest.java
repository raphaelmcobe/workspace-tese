package bcontractor.dl.hermit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;

import org.junit.Test;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import bcontractor.api.ContractionOperator;
import bcontractor.api.ISet;
import bcontractor.base.ISets;
import bcontractor.dl.owl.OWLSentence;
import bcontractor.dl.owl.hermit.OWLHermitReasoner;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.operators.BlackboxKernelOperator;
import bcontractor.partialmeet.PartialMeetContractionOperator;
import bcontractor.partialmeet.operators.BlackboxRemainderSetOperator;
import bcontractor.partialmeet.selection.FullSelectionFunction;

public class HermitMultipleKernelsTest {

	@Test
	public void basic() throws Exception {
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		OWLOntology o = m.loadOntologyFromOntologyDocument(new File("bcontractor-base/src/test/resources/bcontractor/dl/hermit/flying_penguin.owl"));
		ISet<OWLSentence> base = ISets.empty();
		for (OWLAxiom axiom : o.getTBoxAxioms(false)) {
			base = base.union(new OWLSentence(axiom));
		}
		OWLDataFactory df = m.getOWLDataFactory();
		OWLClass animalVoador = df.getOWLClass(IRI.create("http://www.semanticweb.org/ontologies/2010/10/penguin2-Resina.owl#AnimalVoador"));
		OWLClass pinguim = df.getOWLClass(IRI.create("http://www.semanticweb.org/ontologies/2010/10/penguin2-Resina.owl#Pinguim"));
		OWLSentence pinguimNaoVoa = new OWLSentence(df.getOWLDisjointClassesAxiom(animalVoador, pinguim));
		OWLSentence pinguimVoa = new OWLSentence(df.getOWLSubClassOfAxiom(pinguim, animalVoador));

		OWLHermitReasoner reasoner = new OWLHermitReasoner();

		assertThat(reasoner.isSatisfiable(base.union(pinguimVoa)), is(true));
		assertThat(reasoner.isUnsatisfiable(base.union(pinguimNaoVoa)), is(true));
		assertThat(reasoner.entails(base, pinguimVoa), is(true));

		
		
		BlackboxRemainderSetOperator<OWLSentence> remainderSetOperator = new BlackboxRemainderSetOperator<OWLSentence>(reasoner);
		FullSelectionFunction<OWLSentence> selectionFunction = new FullSelectionFunction<OWLSentence>();
		ContractionOperator<OWLSentence> contractionOperator = new PartialMeetContractionOperator<OWLSentence>(remainderSetOperator, selectionFunction);

		ISet<OWLSentence> contraction = contractionOperator.contract(base, pinguimVoa);

		assertThat(reasoner.isSatisfiable(contraction.union(pinguimVoa)), is(true));
		assertThat(reasoner.isSatisfiable(contraction.union(pinguimNaoVoa)), is(true));
		assertThat(reasoner.entails(contraction, pinguimVoa), is(false));

		assertThat(reasoner.isSatisfiable(contraction.union(pinguimNaoVoa)), is(true));

	}
}
