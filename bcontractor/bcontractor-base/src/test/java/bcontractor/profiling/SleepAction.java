package bcontractor.profiling;

import org.hamcrest.Description;
import org.jmock.api.Action;
import org.jmock.api.Invocation;

final class SleepAction implements Action {

	private long sleep;

	private Object result;

	public SleepAction(long sleep, Object result) {
		super();
		this.sleep = sleep;
		this.result = result;
	}

	@Override
	public Object invoke(Invocation arg0) throws Throwable {
		Thread.sleep(sleep);
		return result;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("Sleeping");
	}
}