package bcontractor.profiling;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.junit.Test;
import org.junit.runner.RunWith;

import bcontractor.api.SATReasoner;
import bcontractor.base.ISets;
import bcontractor.propositional.PropositionalSentence;

@RunWith(JMock.class)
public class ProfilingSATReasonerShould {

	private Mockery context = new Mockery();

	@SuppressWarnings("unchecked")
	private SATReasoner<PropositionalSentence> delegate = context.mock(SATReasoner.class);

	private ProfilingSATReasoner<PropositionalSentence> profiling = new ProfilingSATReasoner<PropositionalSentence>(delegate);

	@Test
	public void shouldDelegateIsSatisfiable() {
		context.checking(new Expectations() {
			{
				one(delegate).isSatisfiable(ISets.<PropositionalSentence> empty());
				will(returnValue(true));
			}
		});
		assertTrue(profiling.isSatisfiable(ISets.<PropositionalSentence> empty()));
	}

	@Test
	public void shouldDelegateIsUnsatisfiable() {
		context.checking(new Expectations() {
			{
				one(delegate).isUnsatisfiable(ISets.<PropositionalSentence> empty());
				will(returnValue(false));
			}
		});
		assertFalse(profiling.isUnsatisfiable(ISets.<PropositionalSentence> empty()));
	}

	@Test
	public void shouldDelegateEntails() {
		context.checking(new Expectations() {
			{
				one(delegate).entails(ISets.<PropositionalSentence> empty(), null);
				will(returnValue(true));
			}
		});
		assertTrue(profiling.entails(ISets.<PropositionalSentence> empty(), null));
	}

	@Test
	public void shouldCountCalls() {
		context.checking(new Expectations() {
			{
				allowing(delegate).entails(ISets.<PropositionalSentence> empty(), null);
				allowing(delegate).isUnsatisfiable(ISets.<PropositionalSentence> empty());
				allowing(delegate).isSatisfiable(ISets.<PropositionalSentence> empty());
			}
		});
		assertThat(profiling.getCalls(), is(0L));
		profiling.entails(ISets.<PropositionalSentence> empty(), null);
		assertThat(profiling.getCalls(), is(1L));
		profiling.isUnsatisfiable(ISets.<PropositionalSentence> empty());
		profiling.isSatisfiable(ISets.<PropositionalSentence> empty());
		assertThat(profiling.getCalls(), is(3L));
	}
}
