package bcontractor.profiling;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.junit.Test;
import org.junit.runner.RunWith;

import bcontractor.api.MUSReasoner;
import bcontractor.base.ISets;
import bcontractor.propositional.PropositionalSentence;

@RunWith(JMock.class)
public class ProfilingMUSReasonerShould {

	private Mockery context = new Mockery();

	@SuppressWarnings("unchecked")
	private MUSReasoner<PropositionalSentence> delegate = context.mock(MUSReasoner.class);

	private ProfilingMUSReasoner<PropositionalSentence> profiling = new ProfilingMUSReasoner<PropositionalSentence>(delegate);

	@Test
	public void shouldDelegateFindUnsatisfiableCore() {
		context.checking(new Expectations() {
			{
				one(delegate).findUnsatisfiableCore(ISets.<PropositionalSentence> empty());
				will(returnValue(ISets.<PropositionalSentence> empty()));
			}
		});
		assertThat(profiling.findUnsatisfiableCore(ISets.<PropositionalSentence> empty()), is(ISets.<PropositionalSentence> empty()));
	}

	@Test
	public void shouldCountCalls() {
		context.checking(new Expectations() {
			{
				one(delegate).findUnsatisfiableCore(ISets.<PropositionalSentence> empty());
				one(delegate).findUnsatisfiableCore(ISets.<PropositionalSentence> empty());
				one(delegate).findUnsatisfiableCore(ISets.<PropositionalSentence> empty());
			}
		});
		assertThat(profiling.getCalls(), is(0L));
		profiling.findUnsatisfiableCore(ISets.<PropositionalSentence> empty());
		assertThat(profiling.getCalls(), is(1L));
		profiling.findUnsatisfiableCore(ISets.<PropositionalSentence> empty());
		profiling.findUnsatisfiableCore(ISets.<PropositionalSentence> empty());
		assertThat(profiling.getCalls(), is(3L));
	}
}
