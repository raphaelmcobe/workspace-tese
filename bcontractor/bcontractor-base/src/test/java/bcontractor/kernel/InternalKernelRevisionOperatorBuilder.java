package bcontractor.kernel;

import bcontractor.api.Sentence;
import bcontractor.builder.Builder;
import bcontractor.builders.BuilderHelper;
import bcontractor.builders.NullBuilder;
import bcontractor.kernel.incision.SmallestFirstIncisionFunctionBuilder;
import bcontractor.kernel.operators.MMRBlackboxKernelOperatorBuilder;
import bcontractor.propositional.PropositionalSentence;

/**
 * Builder for KernelRevisionOperator
 * 
 * @author lundberg
 * 
 */
public class InternalKernelRevisionOperatorBuilder<S extends Sentence<S>> implements Builder<InternalKernelRevisionOperator<S>> {

    private Builder<? extends KernelOperator<S>> kernel = new NullBuilder<KernelOperator<S>>();

    private Builder<? extends IncisionFunction<S>> incision = new NullBuilder<IncisionFunction<S>>();

    public static InternalKernelRevisionOperatorBuilder<PropositionalSentence> aContractionOperator() {
        InternalKernelRevisionOperatorBuilder<PropositionalSentence> op = new InternalKernelRevisionOperatorBuilder<PropositionalSentence>();
        op = op.withKernelOperator(MMRBlackboxKernelOperatorBuilder.aKernelOperator());
        return op.withIncisionFunction(SmallestFirstIncisionFunctionBuilder.anIncisionFunction());
    }

    @Override
    public InternalKernelRevisionOperator<S> build() {
        return new InternalKernelRevisionOperator<S>(this.kernel.build(), this.incision.build());
    }

    public InternalKernelRevisionOperatorBuilder<S> withKernelOperator(Builder<? extends KernelOperator<S>> aKernel) {
        return BuilderHelper.copyWith(this, "kernel", aKernel);
    }

    public InternalKernelRevisionOperatorBuilder<S> withIncisionFunction(Builder<? extends IncisionFunction<S>> anUncision) {
        return BuilderHelper.copyWith(this, "incision", anUncision);
    }
}
