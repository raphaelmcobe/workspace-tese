package bcontractor.kernel;

import bcontractor.api.Sentence;
import bcontractor.builder.Builder;
import bcontractor.builders.BuilderHelper;
import bcontractor.builders.NullBuilder;
import bcontractor.kernel.incision.SmallestFirstIncisionFunctionBuilder;
import bcontractor.kernel.operators.MMRBlackboxKernelOperatorBuilder;
import bcontractor.propositional.PropositionalSentence;

/**
 * Builder for KernelContractionOperator
 * 
 * @author lundberg
 * 
 */
public class KernelContractionOperatorBuilder<S extends Sentence<S>> implements Builder<KernelContractionOperator<S>> {

    private Builder<? extends KernelOperator<S>> kernel = new NullBuilder<KernelOperator<S>>();

    private Builder<? extends IncisionFunction<S>> incision = new NullBuilder<IncisionFunction<S>>();

    public static KernelContractionOperatorBuilder<PropositionalSentence> aContractionOperator() {
        KernelContractionOperatorBuilder<PropositionalSentence> op = new KernelContractionOperatorBuilder<PropositionalSentence>();
        op = op.withKernelOperator(MMRBlackboxKernelOperatorBuilder.aKernelOperator());
        return op.withIncisionFunction(SmallestFirstIncisionFunctionBuilder.anIncisionFunction());
    }

    @Override
    public KernelContractionOperator<S> build() {
        return new KernelContractionOperator<S>(this.kernel.build(), this.incision.build());
    }

    public KernelContractionOperatorBuilder<S> withKernelOperator(Builder<? extends KernelOperator<S>> aKernel) {
        return BuilderHelper.copyWith(this, "kernel", aKernel);
    }

    public KernelContractionOperatorBuilder<S> withIncisionFunction(Builder<? extends IncisionFunction<S>> anIncision) {
        return BuilderHelper.copyWith(this, "incision", anIncision);
    }
}
