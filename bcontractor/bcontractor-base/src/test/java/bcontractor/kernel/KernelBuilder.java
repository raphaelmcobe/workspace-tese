package bcontractor.kernel;

import bcontractor.api.ISet;
import bcontractor.api.Sentence;
import bcontractor.base.ISets;
import bcontractor.builder.Builder;
import bcontractor.builders.BuilderHelper;
import bcontractor.propositional.PropositionalSentence;

/**
 * 
 * Builder for a Kernel
 * 
 * @author lundberg
 * 
 */
public class KernelBuilder<S extends Sentence<S>> implements Builder<Kernel<S>> {

	private ISet<ISet<S>> elements = ISets.empty();

	@Override
	public Kernel<S> build() {
		return new Kernel<S>(this.elements);
	}

	public static KernelBuilder<PropositionalSentence> aKernel() {
		return new KernelBuilder<PropositionalSentence>();
	}

	public KernelBuilder<S> with(ISet<S> element) {
		return BuilderHelper.copyWith(this, "elements", this.elements.union(element));
	}
}
