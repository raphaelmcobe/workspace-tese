package bcontractor.kernel.incision;

import bcontractor.builder.Builder;

/**
 * Builder for SmallestFirstIncisionFunction
 * 
 * @author lundberg
 * 
 */
public class SmallestFirstIncisionFunctionBuilder implements Builder<SmallestFirstIncisionFunction> {

    public static SmallestFirstIncisionFunctionBuilder anIncisionFunction() {
        return new SmallestFirstIncisionFunctionBuilder();
    }

    @Override
    public SmallestFirstIncisionFunction build() {
        return new SmallestFirstIncisionFunction();
    }
}
