package bcontractor.kernel.incision;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import bcontractor.kernel.IncisionFunction;
import bcontractor.kernel.KernelBuilder;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.PropositionalSentenceParser;

/**
 * Tests the SmallestFirstIncisionFunction implementation isolatedly
 * 
 * @author lundberg
 * 
 */
public class SmallestFirstIncisionFunctionShould {

    private IncisionFunction<PropositionalSentence> incision;

    private PropositionalSentenceParser parser;

    private KernelBuilder<PropositionalSentence> kernel;

    @Before
    public void init() {
        this.parser = new PropositionalSentenceParser();
        this.incision = new SmallestFirstIncisionFunctionBuilder().build();
        this.kernel = KernelBuilder.aKernel();
    }

    @Test
    public void selectAtomicSentenceFromKernelWithSingleElement() {
        this.kernel = this.kernel.with(this.parser.createSet("a v b", "b", "a v b ^ c v d", "a"));
        assertThat(this.incision.eval(this.kernel.build()), equalTo(this.parser.createSet("a")));
    }

    @Test
    public void notBeFooledByNegation() {
        this.kernel = this.kernel.with(this.parser.createSet("a v b", "¬b", "a"));
        assertThat(this.incision.eval(this.kernel.build()), equalTo(this.parser.createSet("a")));
    }

    @Test
    public void selectSmallestSentenceFromKernelWithSingleElement() {
        this.kernel = this.kernel.with(this.parser.createSet("a v b v c v d", "a v d", "a v b ^ c v d", "a v b", "a v c"));
        assertThat(this.incision.eval(this.kernel.build()), equalTo(this.parser.createSet("a v b")));
    }

    @Test
    public void seekSmallestNumberOfClausesBeforeLiterals() {
        this.kernel = this.kernel.with(this.parser.createSet("a ^ b ^ c ^ d", "a v b v c v d v e v f", "(a v b) ^ (c v d)"));
        assertThat(this.incision.eval(this.kernel.build()), equalTo(this.parser.createSet("a v b v c v d v e v f")));
    }

    @Test
    public void findIncisionAmongManyElements() {
        this.kernel = this.kernel.with(this.parser.createSet("a v b", "¬b", "a"));
        this.kernel = this.kernel.with(this.parser.createSet("a v b v c v d v e v f", "(a v b) ^ (c v d)", "a v b"));
        this.kernel = this.kernel.with(this.parser.createSet("a v b v c v d", "a v d", "a v b ^ c v d", "a v b", "a v c"));
        this.kernel = this.kernel.with(this.parser.createSet("a v c", "¬b", "a"));
        assertThat(this.incision.eval(this.kernel.build()), equalTo(this.parser.createSet("a v b", "a")));
    }
}
