package bcontractor.kernel.operators;

import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.api.MUSReasoner;
import bcontractor.builder.Builder;
import bcontractor.builders.BuilderHelper;
import bcontractor.builders.NullBuilder;
import bcontractor.kernel.operators.UnsatKernelOperator;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.sat4j.PropositionalSAT4JSATReasonerBuilder;
import bcontractor.propositional.sat4j.PropositionalSAT4MUSReasonerBuilder;

/**
 * Builder for UnsatKernelOperator
 * 
 * @author lundberg
 * 
 */
public class UnsatKernelOperatorBuilder<S extends Sentence<S>> implements Builder<UnsatKernelOperator<S>> {

    private Builder<SATReasoner<S>> sat = new NullBuilder<SATReasoner<S>>();

    private Builder<MUSReasoner<S>> unsat = new NullBuilder<MUSReasoner<S>>();

    /**
     * 
     * @return BlackboxKernelResolverBuilder
     */
    public static UnsatKernelOperatorBuilder<PropositionalSentence> aKernelOperator() {
        UnsatKernelOperatorBuilder<PropositionalSentence> builder = new UnsatKernelOperatorBuilder<PropositionalSentence>();
        builder = builder.sat(new PropositionalSAT4JSATReasonerBuilder());
        builder = builder.unsat(new PropositionalSAT4MUSReasonerBuilder());
        return builder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnsatKernelOperator<S> build() {
        return new UnsatKernelOperator<S>(this.sat.build(), this.unsat.build());
    }

    /**
     * @param aSatReasoner
     */
    public UnsatKernelOperatorBuilder<S> sat(Builder<? extends SATReasoner<S>> aSatReasoner) {
        return BuilderHelper.copyWith(this, "sat", aSatReasoner);
    }

    /**
     * @param sat
     */
    public UnsatKernelOperatorBuilder<S> unsat(Builder<? extends MUSReasoner<S>> anUnsatReasoner) {
        return BuilderHelper.copyWith(this, "unsat", anUnsatReasoner);
    }
}
