package bcontractor.kernel.operators;

import static bcontractor.propositional.sat4j.PropositionalSAT4JSATReasonerBuilder.aReasoner;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import bcontractor.api.SATReasoner;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.KernelOperator;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.inputfactory.ArbitraryClauseFactory;
import bcontractor.propositional.inputfactory.ContractionCase;
import bcontractor.propositional.inputfactory.ContractionCaseFactory;
import bcontractor.propositional.inputfactory.PropositionalSentenceSetFactory;

/**
 * Runs several kernel algorithms on random generated problemas, checking if all
 * reach the same answer.
 * 
 * @author Lundberg
 * 
 */
public class RandomProblemsKernelTest {

	private static final int samples = 10;

	private static final int minSize = 5;

	private static final int maxSize = 8;

	private static final int nVariables = 10;

	private static final int nClauses = 50;

	private ContractionCaseFactory<PropositionalSentence> factory;

	private SATReasoner<PropositionalSentence> reasoner;

	private List<KernelOperator<PropositionalSentence>> operators = new ArrayList<KernelOperator<PropositionalSentence>>();

	@Before
	public void initialize() {
		Random random = new Random(System.currentTimeMillis());
		ArbitraryClauseFactory sentenceFactory = new ArbitraryClauseFactory(random, nVariables, minSize, maxSize);
		PropositionalSentenceSetFactory sentenceSetFactory = new PropositionalSentenceSetFactory(sentenceFactory, nVariables, nClauses);
		reasoner = aReasoner().build();
		factory = new ContractionCaseFactory<PropositionalSentence>(sentenceSetFactory, sentenceFactory, reasoner);
		operators.add(MMRBlackboxKernelOperatorBuilder.aKernelOperator().build());
		operators.add(BlackboxKernelOperatorBuilder.aKernelOperator().build());
		operators.add(UnsatKernelOperatorBuilder.aKernelOperator().build());
	}

	@Test
	public void testRandomProblems() {
		for (int i = 0; i < samples; i++) {
			ContractionCase<PropositionalSentence> contractionCase = factory.create();
			List<Kernel<PropositionalSentence>> kernels = new ArrayList<Kernel<PropositionalSentence>>();
			for (KernelOperator<PropositionalSentence> operator : operators) {
				kernels.add(operator.eval(contractionCase.getSentences(), contractionCase.getSentence()));
			}
			if (new HashSet<Kernel<PropositionalSentence>>(kernels).size() != 1) {
				System.out.println(contractionCase.getSentences());
				System.out.println(contractionCase.getSentence());
				fail("All kernels should be equal. " + kernels);
			}
		}
	}
}
