package bcontractor.kernel.operators;

import static bcontractor.builders.api.KnowledgeBaseBuilder.aKnowledgeBase;
import static bcontractor.kernel.KernelBuilder.aKernel;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Before;
import org.junit.Test;

import bcontractor.api.ISet;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.KernelBuilder;
import bcontractor.kernel.KernelOperator;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.PropositionalSentenceParser;

/**
 * Unit test for any implementation of kernel operator.
 * 
 * As the operation is defined precisely, any implementation should have the
 * same results on the same operands.
 * 
 * @author lundberg
 * 
 */
public abstract class AKernelResolverShould {

	private KernelOperator<PropositionalSentence> resolver;

	private PropositionalSentenceParser parser;

	@Before
	public void init() {
		this.resolver = this.createKernelOperator();
		this.parser = new PropositionalSentenceParser();
	}

	protected abstract KernelOperator<PropositionalSentence> createKernelOperator();

	@Test
	public void findSingleOneElementKernelAloneOnKB() {
		ISet<PropositionalSentence> base = aKnowledgeBase().with(this.parser.create("a")).build();
		PropositionalSentence alpha = this.parser.create("a");
		Kernel<PropositionalSentence> actual = this.resolver.eval(base, alpha);
		Kernel<PropositionalSentence> expected = aKernel().with(this.parser.createSet("a")).build();
		assertThat(actual, equalTo(expected));
	}

	@Test
	public void findSingleDirectEntailment() {
		ISet<PropositionalSentence> base = aKnowledgeBase().with(this.parser.createSet("¬a v b", "a")).build();
		PropositionalSentence alpha = this.parser.create("b");
		Kernel<PropositionalSentence> actual = this.resolver.eval(base, alpha);
		Kernel<PropositionalSentence> expected = aKernel().with(this.parser.createSet("¬a v b", "a")).build();
		assertThat(actual, equalTo(expected));
	}

	@Test
	public void findManyEntailments() {
		PropositionalSentence alpha = this.parser.create("a");
		ISet<PropositionalSentence> k1 = this.parser.createSet("¬b v a", "b");
		ISet<PropositionalSentence> k2 = this.parser.createSet("¬b v a", "¬c v ¬d v b", "¬e v d", "c", "e");
		ISet<PropositionalSentence> nonrelated = this.parser.createSet("j v k", "m v n v ¬q", "m v ¬n v q v j");
		ISet<PropositionalSentence> base = aKnowledgeBase().with(k1).with(k2).with(nonrelated).build();
		Kernel<PropositionalSentence> actual = this.resolver.eval(base, alpha);
		KernelBuilder<PropositionalSentence> expectedBuilder = aKernel();
		expectedBuilder = expectedBuilder.with(k1).with(k2);
		Kernel<PropositionalSentence> expected = expectedBuilder.build();
		assertThat(actual, equalTo(expected));
	}

	@Test
	public void findEmptyKernelIfBaseDoesntEntail() {
		PropositionalSentence alpha = this.parser.create("a");
		ISet<PropositionalSentence> k1 = this.parser.createSet("¬b v a");
		ISet<PropositionalSentence> k2 = this.parser.createSet("¬b v a", "¬c v ¬d v b", "¬e v d", "c");
		ISet<PropositionalSentence> nonrelated = this.parser.createSet("j v k", "m v n v ¬q", "m v ¬n v q v j");
		ISet<PropositionalSentence> base = aKnowledgeBase().with(k1).with(k2).with(nonrelated).build();
		Kernel<PropositionalSentence> actual = this.resolver.eval(base, alpha);
		KernelBuilder<PropositionalSentence> expectedBuilder = aKernel();
		Kernel<PropositionalSentence> expected = expectedBuilder.build();
		assertThat(actual, equalTo(expected));
	}
}
