package bcontractor.kernel.operators;

import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.builder.Builder;
import bcontractor.builders.BuilderHelper;
import bcontractor.builders.NullBuilder;
import bcontractor.kernel.operators.MMRKernelOperator;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.sat4j.PropositionalSAT4JSATReasonerBuilder;

/**
 * Builder for BlackboxKernelOperator
 * 
 * @author lundberg
 * 
 */
public class MMRBlackboxKernelOperatorBuilder<S extends Sentence<S>> implements Builder<MMRKernelOperator<S>> {

    private Builder<SATReasoner<S>> reasoner = new NullBuilder<SATReasoner<S>>();

    /**
     * 
     * @return BlackboxKernelResolverBuilder
     */
    public static MMRBlackboxKernelOperatorBuilder<PropositionalSentence> aKernelOperator() {
        return new MMRBlackboxKernelOperatorBuilder<PropositionalSentence>().having(new PropositionalSAT4JSATReasonerBuilder());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MMRKernelOperator<S> build() {
        return new MMRKernelOperator<S>(this.reasoner.build());
    }

    /**
     * @param aReasoner
     */
    public MMRBlackboxKernelOperatorBuilder<S> having(Builder<? extends SATReasoner<S>> aReasoner) {
        return BuilderHelper.copyWith(this, "reasoner", aReasoner);
    }
}
