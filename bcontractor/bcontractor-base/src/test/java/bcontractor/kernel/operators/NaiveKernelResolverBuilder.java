package bcontractor.kernel.operators;

import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.builder.Builder;
import bcontractor.builders.BuilderHelper;
import bcontractor.builders.NullBuilder;
import bcontractor.kernel.operators.NaiveKernelOperator;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.sat4j.PropositionalSAT4JSATReasonerBuilder;

/**
 * Builder for NaiveKernelResolverBuilder
 * 
 * @author lundberg
 * 
 */
public class NaiveKernelResolverBuilder<S extends Sentence<S>> implements Builder<NaiveKernelOperator<S>> {

    private Builder<? extends SATReasoner<S>> reasoner = new NullBuilder<SATReasoner<S>>();

    /**
     * {@inheritDoc}
     */
    @Override
    public NaiveKernelOperator<S> build() {
        return new NaiveKernelOperator<S>(this.reasoner.build());
    }

    public static NaiveKernelResolverBuilder<PropositionalSentence> aKernelOperator() {
        return new NaiveKernelResolverBuilder<PropositionalSentence>().having(new PropositionalSAT4JSATReasonerBuilder());
    }

    public NaiveKernelResolverBuilder<S> having(Builder<? extends SATReasoner<S>> aReasoner) {
        return BuilderHelper.copyWith(this, "reasoner", aReasoner);
    }
}
