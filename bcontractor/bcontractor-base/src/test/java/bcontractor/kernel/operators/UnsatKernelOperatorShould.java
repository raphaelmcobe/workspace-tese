package bcontractor.kernel.operators;

import static bcontractor.kernel.operators.UnsatKernelOperatorBuilder.aKernelOperator;
import bcontractor.kernel.KernelOperator;
import bcontractor.propositional.PropositionalSentence;

/**
 * Unit test for UNSAT kernel operator. All tests for kernel operators should
 * pass.
 * 
 * @author lundberg
 * 
 */
public class UnsatKernelOperatorShould extends AKernelResolverShould {

    /**
     * {@inheritDoc}
     */
    @Override
    protected KernelOperator<PropositionalSentence> createKernelOperator() {
        return aKernelOperator().build();
    }
}
