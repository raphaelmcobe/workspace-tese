package bcontractor.kernel.operators;

import static bcontractor.kernel.operators.NaiveKernelResolverBuilder.aKernelOperator;
import bcontractor.kernel.KernelOperator;
import bcontractor.propositional.PropositionalSentence;

/**
 * Unit test for naïve kernel operator. All tests for kernel operators should
 * pass.
 * 
 * @author lundberg
 * 
 */
public class NaiveKernelOperatorShould extends AKernelResolverShould {

    /**
     * {@inheritDoc}
     */
    @Override
    protected KernelOperator<PropositionalSentence> createKernelOperator() {
        return aKernelOperator().build();
    }
}
