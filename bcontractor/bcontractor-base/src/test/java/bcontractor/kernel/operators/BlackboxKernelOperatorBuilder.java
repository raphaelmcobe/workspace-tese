package bcontractor.kernel.operators;

import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.builder.Builder;
import bcontractor.builders.BuilderHelper;
import bcontractor.builders.NullBuilder;
import bcontractor.kernel.operators.BlackboxKernelOperator;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.sat4j.PropositionalSAT4JSATReasonerBuilder;

/**
 * Builder for MinimalSubsetsBlackboxKernelOperator
 * 
 * @author lundberg
 * 
 */
public class BlackboxKernelOperatorBuilder<S extends Sentence<S>> implements Builder<BlackboxKernelOperator<S>> {

	private Builder<SATReasoner<S>> reasoner = new NullBuilder<SATReasoner<S>>();

	/**
	 * 
	 * @return BlackboxKernelResolverBuilder
	 */
	public static BlackboxKernelOperatorBuilder<PropositionalSentence> aKernelOperator() {
		return new BlackboxKernelOperatorBuilder<PropositionalSentence>().having(new PropositionalSAT4JSATReasonerBuilder());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BlackboxKernelOperator<S> build() {
		return new BlackboxKernelOperator<S>(this.reasoner.build());
	}

	/**
	 * @param aReasoner
	 */
	public BlackboxKernelOperatorBuilder<S> having(Builder<? extends SATReasoner<S>> aReasoner) {
		return BuilderHelper.copyWith(this, "reasoner", aReasoner);
	}
}
