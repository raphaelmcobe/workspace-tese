package bcontractor.kernel.operators;

import static bcontractor.kernel.operators.BlackboxKernelOperatorBuilder.aKernelOperator;
import bcontractor.kernel.KernelOperator;
import bcontractor.propositional.PropositionalSentence;

/**
 * Unit test for blackbox kernel operator. All tests for kernel operators should
 * pass.
 * 
 * @author lundberg
 * 
 */
public class BlackboxKernelOperatorShould extends AKernelResolverShould {

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected KernelOperator<PropositionalSentence> createKernelOperator() {
		return aKernelOperator().build();
	}
}
