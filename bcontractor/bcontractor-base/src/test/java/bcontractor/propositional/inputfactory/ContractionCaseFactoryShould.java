package bcontractor.propositional.inputfactory;

import static bcontractor.propositional.minisat.MiniSatReasonerBuilder.aMiniSatReasoner;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.propositional.PropositionalSentence;

/**
 * Tests a contraction case factory build using propositional factories
 * 
 * @author lundberg
 * 
 */
public class ContractionCaseFactoryShould {

	private static final int samples = 10;

	private static final int minSize = 5;

	private static final int maxSize = 8;

	private static final int nVariables = 10;

	private static final int nClauses = 50;

	private ContractionCaseFactory<PropositionalSentence> factory;

	private SATReasoner<PropositionalSentence> reasoner;

	@Before
	public void initialize() {
		Random random = new Random(System.currentTimeMillis());
		ArbitraryClauseFactory sentenceFactory = new ArbitraryClauseFactory(random, nVariables, minSize, maxSize);
		PropositionalSentenceSetFactory sentenceSetFactory = new PropositionalSentenceSetFactory(sentenceFactory, nVariables, nClauses);
		reasoner = aMiniSatReasoner().build();
		factory = new ContractionCaseFactory<PropositionalSentence>(sentenceSetFactory, sentenceFactory, reasoner);
	}

	@Test
	public void shouldGenerateReasonableCases() {
		for (int i = 0; i < samples; i++) {
			ContractionCase<PropositionalSentence> contractionCase = factory.create();
			ISet<PropositionalSentence> sentences = contractionCase.getSentences();
			PropositionalSentence sentence = contractionCase.getSentence();
			assertThat(reasoner.isSatisfiable(sentences), is(true));
			assertThat(reasoner.entails(sentences, sentence), is(true));
			assertThat(reasoner.isUnsatisfiable(sentences.union(sentence.negate())), is(true));
		}
	}
}
