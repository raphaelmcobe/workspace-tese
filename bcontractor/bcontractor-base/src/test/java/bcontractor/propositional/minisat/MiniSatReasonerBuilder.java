package bcontractor.propositional.minisat;

import bcontractor.builder.Builder;
import bcontractor.propositional.minisat.MiniSatReasoner;

/**
 * 
 * @author lundberg
 * 
 */
public class MiniSatReasonerBuilder implements Builder<MiniSatReasoner> {

	public static MiniSatReasonerBuilder aMiniSatReasoner() {
		return new MiniSatReasonerBuilder();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MiniSatReasoner build() {
		return new MiniSatReasoner();
	}

	public static MiniSatReasonerBuilder aReasoner() {
		return new MiniSatReasonerBuilder();
	}
}
