package bcontractor.propositional.sat4j;

import bcontractor.builder.Builder;

/**
 * 
 * @author lundberg
 * 
 */
public class PropositionalSAT4MUSReasonerBuilder implements Builder<PropositionalSAT4JMUSReasoner> {

    /**
     * {@inheritDoc}
     */
    @Override
    public PropositionalSAT4JMUSReasoner build() {
        return new PropositionalSAT4JMUSReasoner();
    }

    public static PropositionalSAT4MUSReasonerBuilder aReasoner() {
        return new PropositionalSAT4MUSReasonerBuilder();
    }
}
