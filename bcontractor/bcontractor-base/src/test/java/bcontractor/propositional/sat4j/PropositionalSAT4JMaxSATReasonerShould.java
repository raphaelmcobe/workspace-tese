package bcontractor.propositional.sat4j;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import bcontractor.api.ISet;
import bcontractor.base.ISets;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.PropositionalSentenceParser;

/**
 * Simple tests to ensure the SAT4J MaxSAT reasoner is correctly implemented.
 * 
 * @author lundberg
 * 
 */
public class PropositionalSAT4JMaxSATReasonerShould {

	private PropositionalSAT4JMaxSATReasoner reasoner;

	private PropositionalSentenceParser parser;

	@Before
	public void init() {
		this.reasoner = new PropositionalSAT4JMaxSATReasonerBuilder().build();
		this.parser = new PropositionalSentenceParser();
	}

	@Test
	public void findsMaximumWithObviousConflicts() {
		ISet<PropositionalSentence> max = this.reasoner.findMaximumSatisfiableSubset(this.parser.createSet("a", "b", "c", "¬a"),
				ISets.<PropositionalSentence> empty());
		assertTrue(this.parser.createSet("a", "b", "c", "¬a").containsAll(max));
		assertTrue(max.containsAll(parser.createSet("b", "c")));
		assertThat(max.size(), is(3));
	}

	@Test
	public void findsMaximumWithObviousConflicts2() {
		ISet<PropositionalSentence> max = this.reasoner.findMaximumSatisfiableSubset(this.parser.createSet("¬a v b", "¬b", "a"),
				ISets.<PropositionalSentence> empty());
		assertTrue(this.parser.createSet("¬a v b", "¬b", "a").containsAll(max));
		assertThat(max.size(), is(2));
	}
}
