package bcontractor.propositional.sat4j;

import bcontractor.builder.Builder;

/**
 * 
 * @author lundberg
 * 
 */
public class PropositionalSAT4JSATReasonerBuilder implements Builder<PropositionalSAT4JSATReasoner> {

    /**
     * {@inheritDoc}
     */
    @Override
    public PropositionalSAT4JSATReasoner build() {
        return new PropositionalSAT4JSATReasoner();
    }

    public static PropositionalSAT4JSATReasonerBuilder aReasoner() {
        return new PropositionalSAT4JSATReasonerBuilder();
    }
}
