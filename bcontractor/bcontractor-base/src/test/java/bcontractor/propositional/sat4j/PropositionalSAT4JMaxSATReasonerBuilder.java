package bcontractor.propositional.sat4j;

import bcontractor.builder.Builder;

/**
 * 
 * @author lundberg
 * 
 */
public class PropositionalSAT4JMaxSATReasonerBuilder implements Builder<PropositionalSAT4JMaxSATReasoner> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PropositionalSAT4JMaxSATReasoner build() {
		return new PropositionalSAT4JMaxSATReasoner();
	}

	public static PropositionalSAT4JMaxSATReasonerBuilder aReasoner() {
		return new PropositionalSAT4JMaxSATReasonerBuilder();
	}
}
