package bcontractor.propositional.sat4j;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import bcontractor.api.ISet;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.PropositionalSentenceParser;

/**
 * Simple tests to ensure the SAT4J UNSAT reasoner is correctly implemented.
 * 
 * @author lundberg
 * 
 */
public class PropositionalSAT4JMUSReasonerShould {

    private PropositionalSAT4JMUSReasoner reasoner;

    private PropositionalSentenceParser parser;

    @Before
    public void init() {
        this.reasoner = new PropositionalSAT4MUSReasonerBuilder().build();
        this.parser = new PropositionalSentenceParser();
    }

    @Test
    public void findObviousConflicts() {
        ISet<PropositionalSentence> conflict = this.reasoner.findUnsatisfiableCore(this.parser.createSet("a", "¬a"));
        assertThat(conflict, is(this.parser.createSet("a", "¬a")));
    }

    @Test
    public void findEntailmentConflicts() {
        ISet<PropositionalSentence> conflict = this.reasoner.findUnsatisfiableCore(this.parser.createSet("a", "¬b", "c", "¬a v b", "a v c v d"));
        assertThat(conflict, is(this.parser.createSet("a", "¬b", "¬a v b")));
    }

    @Test
    public void findEntailmentConflictsInMultipleSentences() {
        ISet<PropositionalSentence> conflict = this.reasoner.findUnsatisfiableCore(this.parser.createSet("¬a v b ^ c v d", "e v g ^ ¬a v b", "a",
                "¬b", "c"));
        assertThat(conflict, is(this.parser.createSet("¬a v b ^ c v d", "e v g ^ ¬a v b", "a", "¬b")));
    }
}
