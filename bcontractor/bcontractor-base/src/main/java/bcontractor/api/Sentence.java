package bcontractor.api;

/**
 * Interface for sentenses
 * 
 * @author lundberg
 * 
 * @param <S>
 *            the type of sentence created by negation. If a hierarchy of
 *            classes is needed to represent a certain domain, this should be
 *            the supertype.
 */
public interface Sentence<S extends Sentence<S>> {

    /**
     * Obtains a sentence that is the negation of the current
     * 
     * @return negation
     */
    S negate();
}