package bcontractor.api;

/**
 * An exception that should be thrown if anything unusual happens during
 * reasoner operations.
 * 
 * @author lundberg
 * 
 */
public class ReasonerException extends RuntimeException {

	private static final long serialVersionUID = 605841615197317356L;

	/**
     * Constructor
     * 
     * @param message
     *            message
     * @param cause
     *            cause
     */
    public ReasonerException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor
     * 
     * @param message
     *            message
     */
    public ReasonerException(String message) {
        super(message);
    }
}
