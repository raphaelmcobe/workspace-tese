package bcontractor.api;

/**
 * Interface for a contraction operation upon a sentence set and a sentence
 * 
 * @author lundberg
 * 
 * @param <S>
 */
public interface ContractionOperator<S extends Sentence<S>> {

    /**
     * Contracts a given set of sentences, removing the given sentence
     * 
     * @param sentences
     *            set of sentences that must be contracted
     * @param sentence
     *            sentence that should be excluded
     * @return contraction result
     */
    ISet<S> contract(ISet<S> sentences, S sentence);
}
