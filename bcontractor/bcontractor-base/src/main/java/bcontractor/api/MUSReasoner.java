package bcontractor.api;

/**
 * Reasoner based on unsatisfiability problem.
 * 
 * @author lundberg
 * 
 * @param <S>
 */
public interface MUSReasoner<S extends Sentence<S>> {

    /**
     * Verifies if the given sentence set is unsatisfiable, returns it's MUS
     * (Minimal Unsatisfiable Subset, or Unsatisfiable Core) . If the sentence
     * set is satisfiable, returns an empty set of sentences.
     * 
     * @param sentences
     *            sentences
     * @return sentences involved in a conflict
     */
    ISet<S> findUnsatisfiableCore(ISet<S> sentences);
}
