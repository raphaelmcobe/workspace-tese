package bcontractor.api;

import bcontractor.kernel.Kernel;
import bcontractor.merging.stratification.Strata;

public interface StratificationOperator<S extends Sentence<S>> {
	public Strata<S> stratify(Kernel<S> kernelSet);

}
