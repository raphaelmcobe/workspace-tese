package bcontractor.api;

/**
 * Interface for any immutable set.
 * 
 * @author lundberg
 * 
 * @param <E>
 */
public interface ISet<E> extends Iterable<E> {

	ISet<E> union(ISet<E> elements);

	ISet<E> union(E element);

	ISet<E> minus(E element);

	ISet<E> minus(ISet<?> elements);

	ISet<E> intersection(ISet<?> elements);

	int size();

	boolean contains(Object element);

	boolean containsAll(ISet<?> elements);

	boolean isEmpty();
}
