package bcontractor.api;

/**
 * Reasoner based on MaxSAT problem.
 * 
 * @author lundberg
 * 
 * @param <S>
 */
public interface MaxSATReasoner<S extends Sentence<S>> {

	/**
	 * Returns the satisfiable subset of the given sentence set with the maximum
	 * number of clauses.
	 * 
	 * @param softSentences softSentences
	 * @param hardSentences hardSentences
	 * @return maximum satisfiable subset
	 */
	ISet<S> findMaximumSatisfiableSubset(ISet<S> softSentences, ISet<S> hardSentences);
}
