package bcontractor.api;


public interface MergeOperator<S extends Sentence<S>>  {

	ISet<S> merge(ISet<S> base1, ISet<S> base2);
}
