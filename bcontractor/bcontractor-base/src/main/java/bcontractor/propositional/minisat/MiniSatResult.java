package bcontractor.propositional.minisat;

/**
 * Resultado do solver
 * 
 * @author lundberg
 * 
 */
public class MiniSatResult {

    private Boolean satisfiable;

    private Integer variables;

    private Integer clauses;

    private Double parseTime;

    private Integer restarts;

    private Integer conflicts;

    private Integer decisions;

    private Integer propagations;

    private Integer conflictLiterals;

    private Double memoryUsed;

    private Double cpuTime;

    /**
     * @return the satisfiable
     */
    public Boolean isSatisfiable() {
        return this.satisfiable;
    }

    /**
     * @param satisfiable
     *            the satisfiable to set
     */
    public void setSatisfiable(Boolean satisfiable) {
        this.satisfiable = satisfiable;
    }

    /**
     * @return the variables
     */
    public Integer getVariables() {
        return this.variables;
    }

    /**
     * @param variables
     *            the variables to set
     */
    public void setVariables(Integer variables) {
        this.variables = variables;
    }

    /**
     * @return the clauses
     */
    public Integer getClauses() {
        return this.clauses;
    }

    /**
     * @param clauses
     *            the clauses to set
     */
    public void setClauses(Integer clauses) {
        this.clauses = clauses;
    }

    /**
     * @return the parseTime
     */
    public Double getParseTime() {
        return this.parseTime;
    }

    /**
     * @param parseTime
     *            the parseTime to set
     */
    public void setParseTime(Double parseTime) {
        this.parseTime = parseTime;
    }

    /**
     * @return the restarts
     */
    public Integer getRestarts() {
        return this.restarts;
    }

    /**
     * @param restarts
     *            the restarts to set
     */
    public void setRestarts(Integer restarts) {
        this.restarts = restarts;
    }

    /**
     * @return the conflicts
     */
    public Integer getConflicts() {
        return this.conflicts;
    }

    /**
     * @param conflicts
     *            the conflicts to set
     */
    public void setConflicts(Integer conflicts) {
        this.conflicts = conflicts;
    }

    /**
     * @return the decisions
     */
    public Integer getDecisions() {
        return this.decisions;
    }

    /**
     * @param decisions
     *            the decisions to set
     */
    public void setDecisions(Integer decisions) {
        this.decisions = decisions;
    }

    /**
     * @return the propagations
     */
    public Integer getPropagations() {
        return this.propagations;
    }

    /**
     * @param propagations
     *            the propagations to set
     */
    public void setPropagations(Integer propagations) {
        this.propagations = propagations;
    }

    /**
     * @return the conflictLiterals
     */
    public Integer getConflictLiterals() {
        return this.conflictLiterals;
    }

    /**
     * @param conflictLiterals
     *            the conflictLiterals to set
     */
    public void setConflictLiterals(Integer conflictLiterals) {
        this.conflictLiterals = conflictLiterals;
    }

    /**
     * @return the memoryUsed
     */
    public Double getMemoryUsed() {
        return this.memoryUsed;
    }

    /**
     * @param memoryUsed
     *            the memoryUsed to set
     */
    public void setMemoryUsed(Double memoryUsed) {
        this.memoryUsed = memoryUsed;
    }

    /**
     * @return the cpuTime
     */
    public Double getCpuTime() {
        return this.cpuTime;
    }

    /**
     * @param cpuTime
     *            the cpuTime to set
     */
    public void setCpuTime(Double cpuTime) {
        this.cpuTime = cpuTime;
    }
}
