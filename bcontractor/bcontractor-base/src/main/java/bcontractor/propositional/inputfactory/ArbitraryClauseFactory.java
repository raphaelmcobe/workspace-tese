package bcontractor.propositional.inputfactory;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import bcontractor.propositional.PropositionalSentence;

/**
 * Factory for arbitrary clauses, no constraints applied
 * 
 * @author lundberg
 * 
 */
public class ArbitraryClauseFactory implements SentenceFactory<PropositionalSentence> {

	private Random random;

	private int nVariables;

	private int minSize;

	private int maxSize;

	/**
	 * Constructor
	 * 
	 * @param random random
	 */
	public ArbitraryClauseFactory(Random random, int nVariables, int minSize, int maxSize) {
		if (maxSize > nVariables) {
			throw new IllegalArgumentException("maxSize should be less or equal to the number of variables.");
		}
		if (minSize > maxSize) {
			throw new IllegalArgumentException("minSize should be less or equal to maxSize.");
		}
		this.random = random;
		this.nVariables = nVariables;
		this.minSize = minSize;
		this.maxSize = maxSize;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PropositionalSentence create(Integer... forcedAtoms) {
		if (forcedAtoms.length > maxSize) {
			throw new IllegalArgumentException("Forced atoms number exceeds maximum clause size.");
		}
		Set<Integer> atoms = new HashSet<Integer>(Arrays.asList(forcedAtoms));
		int minimal = Math.max(this.minSize, forcedAtoms.length);
		int clauseSize = minimal + random.nextInt(maxSize - minimal + 1);
		while (atoms.size() < clauseSize) {
			Integer atom = random.nextInt(nVariables) + 1;
			atoms.add(atom);
		}
		int[] clause = new int[clauseSize];
		int index = 0;
		for (Integer atom : atoms) {
			boolean negate = random.nextBoolean();
			clause[index++] = negate ? -atom : atom;
		}
		return new PropositionalSentence(clause);
	}

}
