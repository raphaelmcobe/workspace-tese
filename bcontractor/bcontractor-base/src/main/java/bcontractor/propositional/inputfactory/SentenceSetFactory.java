package bcontractor.propositional.inputfactory;

import bcontractor.api.Sentence;
import bcontractor.api.ISet;


/**
 * Factory that creates SentenceSets according to some criteria.
 * @author lundberg
 *
 */
public interface SentenceSetFactory<S extends Sentence<S>> {

	/**
	 * Creates a new sentence set
	 * @return sentence set
	 */
	ISet<S> create();
}
