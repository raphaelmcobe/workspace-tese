package bcontractor.propositional.inputfactory;

import bcontractor.api.ISet;
import bcontractor.api.Sentence;

/**
 * A contraction case
 * 
 * @author lundberg
 * @param <S> type of sentence
 * 
 */
public class ContractionCase<S extends Sentence<S>> {

	private final ISet<S> sentences;

	private final S sentence;

	/**
	 * Constructor
	 * 
	 * @param sentences sentences
	 * @param sentence sentence
	 */
	public ContractionCase(ISet<S> sentences, S sentence) {
		this.sentences = sentences;
		this.sentence = sentence;
	}

	public ISet<S> getSentences() {
		return sentences;
	}

	public S getSentence() {
		return sentence;
	}
}
