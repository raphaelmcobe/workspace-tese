package bcontractor.propositional.inputfactory;

import bcontractor.api.Sentence;

/**
 * Common interface for clause factories
 * 
 * @author lundberg
 * 
 */
public interface SentenceFactory<S extends Sentence<S>> {

	/**
	 * Creates a new clause that MUST have the given atoms.
	 * 
	 * @return clause
	 */
	S create(Integer... forcedAtoms);
}
