package bcontractor.propositional.inputfactory;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;

/**
 * A factory for contraction cases
 * 
 * @author lundberg
 * 
 */
public class ContractionCaseFactory<S extends Sentence<S>> {

	private static final int REPLACEMENT_LIMIT = 1000000;

	private final SentenceSetFactory<S> sentenceSetFactory;

	private final SentenceFactory<S> sentenceFactory;

	private final SATReasoner<S> reasoner;

	/**
	 * Constructor
	 * 
	 * @param sentenceSetFactory sentenceSetFactory
	 * @param sentenceFactory sentenceFactory
	 * @param reasoner reasoner
	 */
	public ContractionCaseFactory(SentenceSetFactory<S> sentenceSetFactory, SentenceFactory<S> sentenceFactory, SATReasoner<S> reasoner) {
		this.sentenceSetFactory = sentenceSetFactory;
		this.sentenceFactory = sentenceFactory;
		this.reasoner = reasoner;
	}

	/**
	 * Creates a contraction case using the given factories
	 * 
	 * @return contraction case
	 */
	public ContractionCase<S> create() {
		int retries = 0;
		ISet<S> sentences = null;
		S sentence = null;
		do {
			retries++;
			if (retries > REPLACEMENT_LIMIT) {
				throw new IllegalArgumentException(String.format("Unable to create satisfiable set."));
			}
			sentences = this.sentenceSetFactory.create();
		} while (retries < REPLACEMENT_LIMIT && !reasoner.isSatisfiable(sentences));
		do {
			retries++;
			if (retries > REPLACEMENT_LIMIT) {
				throw new IllegalArgumentException(String.format("Unable to create entailed sentence."));
			}
			sentence = this.sentenceFactory.create();
		} while (retries < REPLACEMENT_LIMIT && !reasoner.entails(sentences, sentence));
		return new ContractionCase<S>(sentences, sentence);
	}
}
