package bcontractor.propositional.inputfactory;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.base.ISets;
import bcontractor.propositional.PropositionalSentence;

/**
 * Factory for propositional sentence sets.
 * 
 * @author lundberg
 * 
 */
public class SatisfiablePropositionalSentenceSetFactory implements SentenceSetFactory<PropositionalSentence> {

	private static final int REPLACEMENT_LIMIT = 1000000;

	private final SATReasoner<PropositionalSentence> reasoner;

	private final SentenceFactory<PropositionalSentence> clauseFactory;

	private final int nVariables;

	private final int nClauses;

	/**
	 * Constructor
	 * 
	 * @param clauseFactory clauseFactory
	 */
	public SatisfiablePropositionalSentenceSetFactory(SATReasoner<PropositionalSentence> reasoner, SentenceFactory<PropositionalSentence> clauseFactory,
			int nVariables, int nClauses) {
		this.reasoner = reasoner;
		this.clauseFactory = clauseFactory;
		this.nVariables = nVariables;
		this.nClauses = nClauses;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISet<PropositionalSentence> create() {
		int replacements = 0;
		PropositionalProblemBuilder builder = new PropositionalProblemBuilder(nVariables);
		while (builder.getNClauses() < nClauses) {
			PropositionalSentence sentence;
			do {
				sentence = clauseFactory.create();
			} while (builder.hasClause(sentence));
			if (reasoner.isSatisfiable(ISets.asISet(builder.getClauses()).union(sentence))) {
				builder.add(sentence);
			} else {
				replacements++;
				if (replacements > REPLACEMENT_LIMIT) {
					throw new IllegalArgumentException(String.format("Unable to create sentence set with %s variables and %s clauses.", nVariables, nClauses));
				}
			}
		}
		while (builder.hasMissingAtoms()) {
			replacements++;
			if (replacements > REPLACEMENT_LIMIT) {
				throw new IllegalArgumentException(String.format("Unable to create sentence set with %s variables and %s clauses.", nVariables, nClauses));
			}
			PropositionalSentence candidate = clauseFactory.create(builder.nextMissingAtom());
			if (!builder.hasClause(candidate) && reasoner.isSatisfiable(ISets.asISet(builder.getClauses()).minus(builder.getFirstClause()).union(candidate))) {
				builder.remove(builder.getFirstClause());
				builder.add(candidate);
			}
		}
		return ISets.asISet(builder.getClauses());
	}
}
