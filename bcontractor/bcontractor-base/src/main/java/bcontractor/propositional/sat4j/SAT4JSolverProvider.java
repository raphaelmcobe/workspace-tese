package bcontractor.propositional.sat4j;

import org.sat4j.minisat.SolverFactory;
import org.sat4j.pb.IPBSolver;
import org.sat4j.specs.ISolver;

public class SAT4JSolverProvider {

	private static final SAT4JSolverProvider INSTANCE = new SAT4JSolverProvider();

	private ThreadLocal<ISolver> satSolver = new ThreadLocal<ISolver>();

	private ThreadLocal<IPBSolver> pbSolver = new ThreadLocal<IPBSolver>();

	public ISolver getSATSolver() {
		if (satSolver.get() == null) {
			satSolver.set(SolverFactory.newDefault());
			satSolver.get().setTimeoutOnConflicts(Integer.MAX_VALUE);
			satSolver.get().setDBSimplificationAllowed(true);
			return satSolver.get();
		}
		satSolver.get().reset();
		return satSolver.get();
	}

	public IPBSolver getPBSolver() {
		if (pbSolver.get() == null) {
			pbSolver.set(org.sat4j.maxsat.SolverFactory.newDefault());
			pbSolver.get().setTimeoutOnConflicts(Integer.MAX_VALUE);
			return pbSolver.get();
		}
		pbSolver.get().reset();
		return pbSolver.get();
	}

	public static SAT4JSolverProvider getInstance() {
		return INSTANCE;
	}
}
