package bcontractor.propositional.sat4j;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.sat4j.core.LiteralsUtils;
import org.sat4j.core.VecInt;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.IVecInt;
import org.sat4j.specs.TimeoutException;
import org.sat4j.tools.xplain.Xplain;

import bcontractor.api.ISet;
import bcontractor.api.ReasonerException;
import bcontractor.base.AbstractMUSReasoner;
import bcontractor.base.ISets;
import bcontractor.propositional.PropositionalSentence;

/**
 * Reasoner on PropositionalSentences that uses SAT4J to solve the problem.
 * 
 * Note that SAT4J implements UNSAT in a not very efficient way, using selector
 * literals, causing the extinction of all unit clauses, increasing problem
 * complexity.
 * 
 * Hence, this UNSAT reasoner should not be used for benchmarks. A specialized
 * UNSAT solver should be used, instead.
 * 
 * @author lundberg
 * 
 */
public class PropositionalSAT4JMUSReasoner extends AbstractMUSReasoner<PropositionalSentence> {

	private SAT4JSolverProvider provider = SAT4JSolverProvider.getInstance();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISet<PropositionalSentence> findUnsatisfiableCore(ISet<PropositionalSentence> sentences) {
		int maxvar = 0;
		for (PropositionalSentence sentence : sentences) {
			maxvar = Math.max(maxvar, sentence.getMaxvar());
		}
		try {
			ISolver baseSolver = provider.getSATSolver();
			Xplain<ISolver> solver = new Xplain<ISolver>(baseSolver);
			solver.newVar(maxvar);
			Map<Set<Integer>, Set<PropositionalSentence>> sentenceMap = new HashMap<Set<Integer>, Set<PropositionalSentence>>();
			for (PropositionalSentence sentence : sentences) {
				for (int[] clause : sentence.getClauses()) {
					int[] lit = new int[clause.length];
					System.arraycopy(clause, 0, lit, 0, clause.length);
					IVecInt vec = new VecInt(lit);
					IConstr constr = solver.addClause(vec);
					Set<Integer> clauseKey = this.createClauseKey(constr, maxvar);
					if (!sentenceMap.containsKey(clauseKey)) {
						sentenceMap.put(clauseKey, new HashSet<PropositionalSentence>());
					}
					sentenceMap.get(clauseKey).add(sentence);
				}
			}
			if (solver.isSatisfiable()) {
				return ISets.empty();
			}
			ISet<PropositionalSentence> core = ISets.empty();
			for (IConstr constr : solver.explain()) {
				core = core.union(ISets.asISet(sentenceMap.get(this.createClauseKey(constr, maxvar))));
			}
			return core;
		} catch (ContradictionException e) {
			throw new ReasonerException("UNSAT should not have obvious contradictions, as there should be no unit clauses.");
		} catch (TimeoutException e) {
			throw new ReasonerException("SAT solver time out.", e);
		}
	}

	private Set<Integer> createClauseKey(IConstr constr, int maxvar) {
		Set<Integer> key = new HashSet<Integer>();
		for (int i = 0; i < constr.size(); i++) {
			int literal = LiteralsUtils.toDimacs(constr.get(i));
			if (Math.abs(literal) <= maxvar) {
				key.add(literal);
			}
		}
		return key;
	}
}
