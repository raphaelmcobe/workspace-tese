package bcontractor.propositional.sat4j;

import org.sat4j.core.ReadOnlyVecInt;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;
import org.sat4j.specs.TimeoutException;

import bcontractor.api.ISet;
import bcontractor.api.ReasonerException;
import bcontractor.base.AbstractSATReasoner;
import bcontractor.propositional.PropositionalSentence;

/**
 * Reasoner on PropositionalSentences that uses SAT4J to solve the problem.
 * 
 * @author lundberg
 * 
 */
public class PropositionalSAT4JSATReasoner extends AbstractSATReasoner<PropositionalSentence> {

	private SAT4JSolverProvider provider = SAT4JSolverProvider.getInstance();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isSatisfiable(ISet<PropositionalSentence> sentences) {
		int maxvar = 0;
		IVec<IVecInt> clauses = new Vec<IVecInt>(sentences.size());
		for (PropositionalSentence sentence : sentences) {
			for (int[] clause : sentence.getClauses()) {
				IVecInt vec = new ReadOnlyVecInt(new VecInt(clause));
				maxvar = Math.max(maxvar, Math.abs(clause[clause.length - 1]));
				clauses.push(vec);
			}
		}
		try {
			ISolver solver = provider.getSATSolver();
			solver.newVar(maxvar);
			solver.addAllClauses(clauses);
			return solver.isSatisfiable();
		} catch (ContradictionException e) {
			// SAT4J throws this exception if an obvious contradiction is found
			// while adding clauses.
			return false;
		} catch (TimeoutException e) {
			throw new ReasonerException("SAT solver time out.", e);
		}
	}
}
