package bcontractor.hittingsets;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import bcontractor.api.ISet;
import bcontractor.base.ISets;
import bcontractor.dl.owl.OWLSentence;

/**
 * Iterates over all minimal subsets that correspond to some rule, represented
 * by the matcher.
 * 
 * @author Lundberg
 * 
 */
public class HittingSets<E> implements Iterable<ISet<E>> {

	private final ISet<E> base;

	private final HittingSetsMatcher<E> matcher;

	/**
	 * Constructor
	 * 
	 * @param base base
	 * @param matcher matcher
	 */
	public HittingSets(ISet<E> base, HittingSetsMatcher<E> matcher) {
		this.base = base;
		this.matcher = matcher;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<ISet<E>> iterator() {
		return new MinimalSubsetsIterator();
	}

	/**
	 * Iterator over minimal subsets
	 * 
	 * @author Lundberg
	 * 
	 */
	private class MinimalSubsetsIterator implements Iterator<ISet<E>> {

		private Map<String, Integer> counts = new HashMap<String, Integer>();
		
		private Set<ISet<E>> found = new HashSet<ISet<E>>();

		private Set<ISet<E>> cuts = new HashSet<ISet<E>>();

		private Queue<ISet<E>> exclusions = new LinkedList<ISet<E>>();

		private Set<ISet<E>> exclusionsSet = new HashSet<ISet<E>>();

		private ISet<E> next = null;

		/**
		 * Constructor
		 */
		private MinimalSubsetsIterator() {
			exclusions.add(ISets.<E> empty());
			evalNext();
		}

		/**
		 * Evaluates the next value.
		 */
		private void evalNext() {
			next = null;
			while (next == null && !exclusions.isEmpty()) {
				ISet<E> exclusion = exclusions.poll();
				if (!exclusionIsCut(exclusion)) {
					next = matcher.findHitting(base.minus(exclusion));
					if (next == null) {
						cuts.add(exclusion);
					} else {
						for (E element : next) {
							ISet<E> newExclusion = exclusion.union(element);
							if (!exclusionIsCut(newExclusion) && exclusionsSet.add(newExclusion)) {
								exclusions.add(newExclusion);
							}
						}
						if (found.contains(next)) {
							next = null;
						} else {
							found.add(next);
						}
					}
				}
			}
//			printOnThreshhold("Found", found.size());
//			printOnThreshhold("cuts", cuts.size());
//			printOnThreshhold("exclusionsSet", exclusionsSet.size());
//			printOnThreshhold("exclusions", exclusions.size());
		}

		private void printOnThreshhold(String name, int size) {
			if(!counts.containsKey(name)) {
				counts.put(name, size);
			}
			if(Math.abs(size - counts.get(name)) > 1000) {
				System.out.println(name + ": " + size);
				counts.put(name, size);
			}
		}

		/**
		 * Verifies if the cut already contains the exclusion, meaning searching
		 * upon it would be useless.
		 * 
		 * @param exclusion exclusion
		 * @return boolean
		 */
		private boolean exclusionIsCut(ISet<E> exclusion) {
			for (ISet<E> c : cuts) {
				if (exclusion.containsAll(c)) {
					return true;
				}
			}
			return false;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext() {
			return next != null;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ISet<E> next() {
			ISet<E> result = next;
			evalNext();
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

	}
}
