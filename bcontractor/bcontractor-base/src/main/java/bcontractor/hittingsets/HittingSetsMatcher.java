package bcontractor.hittingsets;

import bcontractor.api.ISet;

/**
 * Matches a minimal subset that corresponds to some property
 * 
 * @author Lundberg
 * 
 * @param <E> type of element of the set
 */
public interface HittingSetsMatcher<E> {

	/**
	 * Searches on the given base a subset that corresponds to some property and
	 * is minimal.
	 * 
	 * If no subset of the given base corresponds to the desired property, null
	 * must be returned.
	 * 
	 * @param base base
	 * @return minimal subset that corresponds to some property.
	 */
	ISet<E> findHitting(ISet<E> base);
}
