package bcontractor.kernel.incision;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.base.ISets;
import bcontractor.dl.owl.OWLSentence;
import bcontractor.kernel.IncisionFunction;
import bcontractor.kernel.Kernel;

public class MostSpecificFirstIncisionFunction implements IncisionFunction<OWLSentence> {

	
	
	private SATReasoner<OWLSentence> reasoner = null;
	private ISet<OWLSentence> knowledgeBase = null;

	public void MostFrequentFirstIncisionFunction(SATReasoner<OWLSentence> reasoner, ISet<OWLSentence> knowledgeBase){
		this.reasoner=reasoner;
		this.knowledgeBase = knowledgeBase;
	}
	
	@Override
	public ISet<OWLSentence> eval(Kernel<OWLSentence> kernel) {
		ISet<OWLSentence> toReturn = ISets.empty();
		for (ISet<OWLSentence> eachKernel : kernel) {
			toReturn = toReturn.union(this.getMostSpecificAxiom(eachKernel));
		}
		return toReturn;
	}
	
	
	
	private OWLSentence getMostSpecificAxiom(ISet<OWLSentence> kernel) {
		ISet<List<OWLSentence>> strata = ISets.EMPTY_SET;
		Map<OWLSentence, Integer> map = new HashMap<OWLSentence,Integer>();
		for (OWLSentence owlSentence : kernel) {
			OWLAxiom axiom = owlSentence.getAxiom();
			knowledgeBase = knowledgeBase.minus(owlSentence); //Remove axiom from Knowledge Base
			if(axiom.isOfType(AxiomType.SUBCLASS_OF)){
				OWLSubClassOfAxiom subClassAxiom = (OWLSubClassOfAxiom) axiom;
				OWLClassExpression subClass = subClassAxiom.getSubClass(); //get the subclass part of the axiom
				for(OWLSentence toCompare : kernel.minus(owlSentence)){
					OWLAxiom axiomToCompare = toCompare.getAxiom();
					if(axiomToCompare.isOfType(AxiomType.SUBCLASS_OF)){
						OWLClassExpression subClassToCompare = ((OWLSubClassOfAxiom)axiomToCompare).getSubClass();
						OWLSubClassOfAxiom newAxiom = OWLManager.createOWLOntologyManager().getOWLDataFactory().getOWLSubClassOfAxiom(subClass, subClassToCompare); //Checks whether C_1\sqsubseteq C_2 and not the other way around;
						if(reasoner.entails(knowledgeBase, new OWLSentence(newAxiom))){
							OWLSubClassOfAxiom newAxiomReverse = OWLManager.createOWLOntologyManager().getOWLDataFactory().getOWLSubClassOfAxiom(subClassToCompare, subClass);
							if(!reasoner.entails(knowledgeBase, new OWLSentence(newAxiomReverse))){
								Integer occurrences = map.get(owlSentence);
								if(occurrences != null){
									map.put(owlSentence, occurrences++);
								}
							}
						}
					}
				}
			}
			knowledgeBase = knowledgeBase.union(owlSentence);
		}
		List<Integer> values =  Arrays.asList(map.values().toArray(new Integer[0]));
		Collections.sort(values);
		Integer occurence = values.get(values.size()-1); // Retrieves the highest number of occurrences;
		for (OWLSentence sentence : kernel) {
			if(map.get(sentence).equals(occurence)){
				return sentence;
			}
		}
		return null;
	}
}
