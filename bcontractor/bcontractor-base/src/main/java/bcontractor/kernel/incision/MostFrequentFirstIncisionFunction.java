package bcontractor.kernel.incision;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import bcontractor.api.ISet;
import bcontractor.base.ISets;
import bcontractor.dl.owl.OWLSentence;
import bcontractor.kernel.IncisionFunction;
import bcontractor.kernel.Kernel;

public class MostFrequentFirstIncisionFunction implements IncisionFunction<OWLSentence> {

	@Override
	public ISet<OWLSentence> eval(Kernel<OWLSentence> kernel) {
		ISet<ISet<OWLSentence>> kernels = kernel.getElements();
		ISet<OWLSentence> toReturn = ISets.empty();
		List<ISet<OWLSentence>> stratifiedKernels = orderKernelsByFrequency(kernel);
		List<OWLSentence> flattenedStratifiedKernels = this.flattenStrata(stratifiedKernels);
		for (OWLSentence owlSentence : flattenedStratifiedKernels) {
			for (ISet<OWLSentence> eachKernel : kernel) {
				if(eachKernel.contains(owlSentence)){
					toReturn = toReturn.union(owlSentence);
					kernels = kernels.minus(eachKernel);
				}
			}
			if(kernels.isEmpty()){
				break;
			}
		}
		return toReturn;
	}
	
	private List<OWLSentence> flattenStrata(List<ISet<OWLSentence>> kernels){
		List<OWLSentence> toReturn = new ArrayList<OWLSentence>();
		for (ISet<OWLSentence> eachKernel : kernels) {
			for (OWLSentence owlSentence : eachKernel) {
				toReturn.add(owlSentence);
			}
		}
		return toReturn;
		
	}
	
	public List<ISet<OWLSentence>> orderKernelsByFrequency(Kernel<OWLSentence> kernels){
		List<ISet<OWLSentence>> toReturn = new ArrayList<ISet<OWLSentence>>();
		Map<Integer,Set<OWLSentence>> axiomFrequence = new HashMap<Integer,Set<OWLSentence>>();
		Set<OWLSentence> allSentences = new HashSet<OWLSentence>();
		for (Iterator<ISet<OWLSentence>> it = kernels.iterator();it.hasNext();){
			for (OWLSentence eachSentence : it.next()) {
				allSentences.add(eachSentence);
			}
		}
		for (OWLSentence owlSentence : allSentences) {
			int occurenceCounter = 0;
			for(Iterator<ISet<OWLSentence>> it = kernels.iterator(); it.hasNext();){
				if(it.next().contains(owlSentence)){
					occurenceCounter++;
				}
			}
			Set<OWLSentence> strata = axiomFrequence.get(occurenceCounter);
			if(strata == null) strata = new HashSet<OWLSentence>();
			strata.add(owlSentence);
			axiomFrequence.put(occurenceCounter, strata);
		}
		List<Integer> axiomCounters = new ArrayList<Integer>(axiomFrequence.keySet());
		Collections.sort(axiomCounters);
		Collections.reverse(axiomCounters);
		for (Integer axiomCounter : axiomCounters) {
			toReturn.add(ISets.asISet(axiomFrequence.get(axiomCounter)));
		}
		return toReturn;
	}
	

}
