package bcontractor.kernel.incision;

import bcontractor.api.ISet;
import bcontractor.base.ISets;
import bcontractor.kernel.IncisionFunction;
import bcontractor.kernel.Kernel;
import bcontractor.propositional.PropositionalSentence;

/**
 * An implementation of incision function that takes the smallest sentence of
 * each kernel element. All elements are seen isolatedly, this strategy DOES NOT
 * search for a smallest incision, just the smallest sentence of each element.
 * 
 * @author lundberg
 * 
 */
public class SmallestFirstIncisionFunction implements IncisionFunction<PropositionalSentence> {

    @Override
    public ISet<PropositionalSentence> eval(Kernel<PropositionalSentence> kernel) {
        ISet<PropositionalSentence> incision = ISets.empty();
        for (ISet<PropositionalSentence> e : kernel) {
            PropositionalSentence chosen = null;
            for (PropositionalSentence s : e) {
                chosen = this.chooseBetter(chosen, s);
            }
            incision = incision.union(chosen);
        }
        return incision;
    }

    /**
     * Chooses the best sentence among the two candidates.
     * 
     * @param c1
     *            previously chosen sentence, may be null if no one was chosen
     * @param c2
     *            new candidate sentence
     * @return the 'better' among c1 and c2
     */
    private PropositionalSentence chooseBetter(PropositionalSentence c1, PropositionalSentence c2) {
        if (c1 == null) {
            return c2;
        }
        int[][] c1Clauses = c1.getClauses();
        int[][] c2Clauses = c2.getClauses();
        if (c1Clauses.length != c2Clauses.length) {
            return (c1Clauses.length < c2Clauses.length) ? c1 : c2;
        }
        for (int i = 0; i < c1Clauses.length; i++) {
            if (c1Clauses[i].length != c2Clauses[i].length) {
                return (c1Clauses[i].length < c2Clauses[i].length) ? c1 : c2;
            }
            for (int j = 0; j < c1Clauses[i].length; j++) {
                if (Math.abs(c1Clauses[i][j]) != Math.abs(c2Clauses[i][j])) {
                    return (Math.abs(c1Clauses[i][j]) < Math.abs(c2Clauses[i][j])) ? c1 : c2;
                } else if (c1Clauses[i][j] != c2Clauses[i][j]) {
                    return (c1Clauses[i][j] > c2Clauses[i][j]) ? c1 : c2;
                }
            }
        }
        return c1;
    }
}
