package bcontractor.kernel;

import bcontractor.api.RevisionOperator;
import bcontractor.api.Sentence;
import bcontractor.api.ISet;

/**
 * Implementation of the revision operator based on kernel and incision
 * function.
 * 
 * @author lundberg
 * 
 * @param <S>
 */
public class InternalKernelRevisionOperator<S extends Sentence<S>> implements RevisionOperator<S> {

	private final KernelOperator<S> kernelOperator;

	private final IncisionFunction<S> incisionFunction;

	/**
	 * Constructor
	 * 
	 * @param kernelOperator kernelOperator
	 * @param incisionFunction incisionFunction
	 */
	public InternalKernelRevisionOperator(KernelOperator<S> kernelOperator, IncisionFunction<S> incisionFunction) {
		this.kernelOperator = kernelOperator;
		this.incisionFunction = incisionFunction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISet<S> revise(ISet<S> base, S alpha) {
		return base.minus(this.incision(this.kernel(base, alpha.negate()))).union(alpha);
	}

	/**
	 * Obtains an incision of the kernel
	 * 
	 * @param kernel kernel
	 * @return incision
	 */
	private ISet<S> incision(Kernel<S> kernel) {
		return this.incisionFunction.eval(kernel);
	}

	/**
	 * Obtains the kernel of the base in relation to the sentence.
	 * 
	 * @param base base
	 * @param sentence sentence
	 * @return kernel
	 */
	private Kernel<S> kernel(ISet<S> base, S sentence) {
		return this.kernelOperator.eval(base, sentence);
	}
}
