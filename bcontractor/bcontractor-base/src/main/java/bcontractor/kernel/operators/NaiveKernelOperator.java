package bcontractor.kernel.operators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.base.ISets;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.KernelOperator;

/**
 * Naïve implementation of the kernel operator. Basically finds subsets that
 * entail a given sentence through backtracking and expansion and merges them,
 * stripping those that are found not to be minimal.
 * 
 * @author lundberg
 * 
 */
public class NaiveKernelOperator<S extends Sentence<S>> implements KernelOperator<S> {

	private SATReasoner<S> reasoner;

	/**
	 * Constructor
	 * 
	 * @param reasoner reasoner
	 */
	public NaiveKernelOperator(SATReasoner<S> reasoner) {
		this.reasoner = reasoner;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param base
	 * @param sentence
	 * @return
	 */
	@Override
	public Kernel<S> eval(ISet<S> base, S sentence) {
		List<ISet<S>> elements = this.findElements(sentence, this.sentenceList(base), ISets.<S> empty());
		return new Kernel<S>(ISets.asISet(elements));
	}

	/**
	 * Creates a list of all sentences in the KB.
	 * 
	 * @param base base
	 * @return List of sentences
	 */
	private List<S> sentenceList(ISet<S> base) {
		List<S> sentences = new ArrayList<S>(base.size());
		for (S b : base) {
			sentences.add(b);
		}
		return sentences;
	}

	/**
	 * Finds all kernel elements
	 * 
	 * @param sentence sentence
	 * @param formulas a list of all formulas that must be considered
	 * @param wm a set of sentences being built as a candidate to a kernel
	 *            element
	 * @return List of kernel elements containing all sentences on wm plus any
	 *         of those available through the iterator.
	 */
	private List<ISet<S>> findElements(S sentence, List<S> formulas, ISet<S> wm) {
		boolean entails = this.reasoner.entails(wm, sentence);
		if (entails) {
			return Collections.<ISet<S>> singletonList(wm);
		} else if (formulas.isEmpty()) {
			return Collections.emptyList();
		} else {
			S current = formulas.get(0);
			List<S> remaining = formulas.subList(1, formulas.size());
			return this.merge(this.findElements(sentence, remaining, wm), this.findElements(sentence, remaining, wm.union(current)));
		}
	}

	/**
	 * Merges two sets of kernel elements into one single set, leaving out sets
	 * that are found not to be minimal
	 * 
	 * @param ks1 ks1
	 * @param ks2 ks2
	 * @return set of kernel candidates
	 */
	private List<ISet<S>> merge(List<ISet<S>> ks1, List<ISet<S>> ks2) {
		List<ISet<S>> merged = new ArrayList<ISet<S>>(ks1.size() + ks2.size());
		merged.addAll(ks1);
		merged.addAll(ks2);
		Collections.sort(merged, new Comparator<ISet<S>>() {
			@Override
			public int compare(ISet<S> o1, ISet<S> o2) {
				return o1.size() - o2.size();
			}
		});
		int i = 1;
		int lastSizeEnd = 0;
		while (i < merged.size()) {
			if (merged.get(i).size() != merged.get(i - 1).size()) {
				lastSizeEnd = i;
			}
			if (!this.isMinimal(merged.get(i), merged.subList(0, lastSizeEnd))) {
				merged.remove(i);
			} else {
				i++;
			}
		}
		return merged;
	}

	/**
	 * Checks if the kernel element candidate is minimal among those already
	 * selected
	 * 
	 * @param candidate candidate
	 * @param previous previous
	 * @return boolean
	 */
	private boolean isMinimal(ISet<S> candidate, List<ISet<S>> previous) {
		for (ISet<S> e : previous) {
			if (candidate.containsAll(e)) {
				return false;
			}
		}
		return true;
	}
}
