package bcontractor.kernel.operators;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.base.ISets;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.KernelOperator;

/**
 * Implementation of the KernelOperator that uses the blackbox algorithm to find
 * kernel elements and Reiter's algorithm to guide the search for the kernel
 * elements.<br/>
 * <br/>
 * Algorithm based on Marcio Moretto Ribeiro' doctorate thesis.
 * 
 * @author lundberg
 * 
 */
public class MMRKernelOperator<S extends Sentence<S>> implements KernelOperator<S> {

	private final SATReasoner<S> reasoner;

	/**
	 * Constructor
	 * 
	 * @param reasoner reasoner
	 */
	public MMRKernelOperator(SATReasoner<S> reasoner) {
		this.reasoner = reasoner;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Kernel<S> eval(ISet<S> base, S sentence) {
		return new Kernel<S>(this.kernel(base, sentence));
	}

	/**
	 * Finds all elements of the kernel.
	 * 
	 * @param base base
	 * @param sentence sentence
	 * @return Set of KernelElements
	 */
	private ISet<ISet<S>> kernel(ISet<S> base, S sentence) {
		if (!this.reasoner.entails(base, sentence)) {
			return ISets.empty();
		}
		Set<ISet<S>> cut = new HashSet<ISet<S>>();
		ISet<ISet<S>> kernel = ISets.empty();
		Queue<ISet<S>> queue = new LinkedList<ISet<S>>();
		// Queue set added to prevent queue element repetition
		Set<ISet<S>> queueSet = new HashSet<ISet<S>>();
		queue.add(ISets.<S> empty());

		while (!queue.isEmpty()) {
			ISet<S> hn = queue.remove();
			if (!this.cutContainsSet(cut, hn)) {
				ISet<S> k = this.blackbox(base.minus(hn), sentence);
				if (k.isEmpty()) {
					cut.add(hn);
				} else {
					if (!kernel.contains(k)) {
						kernel = kernel.union(k);
					}
					for (S b : k) {
						ISet<S> nextHn = hn.union(b);
						if (queueSet.add(nextHn)) {
							queue.add(nextHn);
						}
					}
				}
			}
		}
		return kernel;
	}

	/**
	 * Verifies if the cut already contains the set, meaning searching upon it
	 * would be useless.
	 * 
	 * @param cut cut
	 * @param hn hn
	 * @return boolean
	 */
	private boolean cutContainsSet(Set<ISet<S>> cut, ISet<S> hn) {
		for (ISet<S> c : cut) {
			if (hn.containsAll(c)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Finds a minimal subset of the given base that entails the sentence
	 * 
	 * @param base base
	 * @param sentence sentence
	 * @return subset of sentences that entail the sentence
	 */
	private ISet<S> blackbox(ISet<S> base, S sentence) {
		return this.shrink(this.expand(base, sentence), sentence);
	}

	/**
	 * Finds some subset of the given base that entail the sentence. This subset
	 * is created through straight expansion and is not guaranteed to be minimal
	 * 
	 * @param base
	 * @param sentence
	 * @return subset that entails the sentence
	 */
	private ISet<S> expand(ISet<S> base, S sentence) {
		ISet<S> subset = ISets.empty();
		for (S b : base) {
			subset = subset.union(b);
			if (this.reasoner.entails(subset, sentence)) {
				return subset;
			}
		}
		return ISets.empty();
	}

	/**
	 * Receives a subset that entails the sentence and strips it from all
	 * possible sentences, while keeping the entailment. The resulting set is
	 * minimal.
	 * 
	 * @param subset
	 * @param sentence
	 * @return minimal subset that entails the sentence
	 */
	private ISet<S> shrink(ISet<S> subset, S sentence) {
		for (S b : subset) {
			if (this.reasoner.entails(subset.minus(b), sentence)) {
				subset = subset.minus(b);
			}
		}
		return subset;
	}

}
