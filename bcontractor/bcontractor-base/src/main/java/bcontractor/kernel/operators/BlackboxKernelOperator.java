package bcontractor.kernel.operators;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.base.ISets;
import bcontractor.hittingsets.HittingSets;
import bcontractor.hittingsets.HittingSetsMatcher;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.KernelConflictOperator;

/**
 * This algorithm is an implementation of the BlackboxKernelOperator suggested
 * by MMR, but uses an isolated implementation of the minimal subset finding
 * algorithm.
 * 
 * @author lundberg
 * 
 */
public class BlackboxKernelOperator<S extends Sentence<S>> implements KernelConflictOperator<S>{

	private final SATReasoner<S> reasoner;

	/**
	 * Constructor
	 * 
	 * @param reasoner reasoner
	 */
	public BlackboxKernelOperator(SATReasoner<S> reasoner) {
		this.reasoner = reasoner;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Kernel<S> eval(ISet<S> base, S alpha) {
		return new Kernel<S>(this.kernel(base, alpha));
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Kernel<S> eval(ISet<S> base) {
		return new Kernel<S>(this.kernel(base));
	}
	
	
	/**
	 * Finds all elements of the kernel.
	 * 
	 * @param base base
	 * @param alpha sentence
	 * @return Set of KernelElements
	 */
	private ISet<ISet<S>> kernel(ISet<S> base, S alpha) {
		if (!this.reasoner.entails(base, alpha)) {
			return ISets.empty();
		}
		ISet<ISet<S>> kernel = ISets.empty();
		for (ISet<S> alphaKernel : new HittingSets<S>(base, new KernelBlackboxMatcher(alpha))) {
			kernel = kernel.union(alphaKernel);
		}
		return kernel;
	}
	
	
	
	
	private ISet<ISet<S>> kernel(ISet<S> base) {
		if(!this.reasoner.isUnsatisfiable(base)){
			return ISets.empty();
		}
		ISet<ISet<S>> kernel = ISets.empty();
		for (ISet<S> alphaKernel : new HittingSets<S>(base, new KernelBlackboxConflictMatcher())) {
			kernel = kernel.union(alphaKernel);
		}
		return kernel;
	}
	
	
	private class KernelBlackboxConflictMatcher implements HittingSetsMatcher<S>{
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public ISet<S> findHitting(ISet<S> set) {
			return this.shrink(this.expand(set));
		}

		/**
		 * Finds some subset of the given base that entail the sentence. This
		 * subset is created through straight expansion and is not guaranteed to
		 * be minimal. Returns null if there is no subset of the base that
		 * entails the sentence.
		 * 
		 * @param set
		 * @param alpha
		 * @return subset that entails the sentence
		 */
		private ISet<S> expand(ISet<S> set) {
			ISet<S> expanded = ISets.empty();
			for (S b : set) {
				expanded = expanded.union(b);
				if (reasoner.isUnsatisfiable(expanded)) {
					return expanded;
				}
			}
			return null;
		}

		/**
		 * Receives a subset that entails the sentence and strips it from all
		 * possible sentences, while keeping the entailment. The resulting set
		 * is minimal.
		 * 
		 * @param expanded
		 * @param alpha
		 * @return minimal subset that entails the sentence
		 */
		private ISet<S> shrink(ISet<S> expanded) {
			if (expanded == null) {
				return null;
			}
			ISet<S> shrunk = expanded;
			for (S b : expanded) {
				if (reasoner.isUnsatisfiable(shrunk.minus(b))) {
					shrunk = shrunk.minus(b);
				}
			}
			return shrunk;
		}
	}
	
	
	
	

	/**
	 * A minimal subset matcher that finds a kernel element using the blackbox
	 * approach. It first expands an empty set using the given set elements
	 * until it entails the sentence, then it shrinks the set removing all
	 * sentences it can while still keeping the entailment.
	 * 
	 * The result is a minimal element of the kernel, that is a subset of the
	 * given set, or null if such element doesn't exist.
	 * 
	 * @author Lundberg
	 * 
	 */
	private class KernelBlackboxMatcher implements HittingSetsMatcher<S> {

		private final S alpha;

		/**
		 * Constructor
		 * 
		 * @param alpha
		 */
		public KernelBlackboxMatcher(S alpha) {
			this.alpha = alpha;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ISet<S> findHitting(ISet<S> set) {
			return this.shrink(this.expand(set));
		}

		/**
		 * Finds some subset of the given base that entail the sentence. This
		 * subset is created through straight expansion and is not guaranteed to
		 * be minimal. Returns null if there is no subset of the base that
		 * entails the sentence.
		 * 
		 * @param set
		 * @param alpha
		 * @return subset that entails the sentence
		 */
		private ISet<S> expand(ISet<S> set) {
			ISet<S> expanded = ISets.empty();
			for (S b : set) {
				expanded = expanded.union(b);
				if (reasoner.entails(expanded, alpha)) {
					return expanded;
				}
			}
			return null;
		}

		/**
		 * Receives a subset that entails the sentence and strips it from all
		 * possible sentences, while keeping the entailment. The resulting set
		 * is minimal.
		 * 
		 * @param expanded
		 * @param alpha
		 * @return minimal subset that entails the sentence
		 */
		private ISet<S> shrink(ISet<S> expanded) {
			if (expanded == null) {
				return null;
			}
			ISet<S> shrunk = expanded;
			for (S b : expanded) {
				if (reasoner.entails(shrunk.minus(b), alpha)) {
					shrunk = shrunk.minus(b);
				}
			}
			return shrunk;
		}
	}
}
