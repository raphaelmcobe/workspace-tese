package bcontractor.kernel.operators;

import bcontractor.api.ISet;
import bcontractor.api.MUSReasoner;
import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.base.ISets;
import bcontractor.hittingsets.HittingSets;
import bcontractor.hittingsets.HittingSetsMatcher;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.KernelOperator;

/**
 * Implementation of the KernelOperator that uses an UNSAT Reasoner to provide
 * MUSs (Minimal Unsatisfiable Cores) and thus finding efficiently each kernel.
 * Uses Reiter's algorithm to guide the search for the kernel elements, avoiding
 * repetitions and needles searching.<br/>
 * <br/>
 * It uses a single UNSAT call for each kernel.
 * 
 * @author lundberg
 * 
 */
public class UnsatKernelOperator<S extends Sentence<S>> implements KernelOperator<S> {

	private final SATReasoner<S> sat;

	private final MUSReasoner<S> unsat;

	/**
	 * Constructor
	 * 
	 * @param sat sat
	 * @param unsat unsat
	 */
	public UnsatKernelOperator(SATReasoner<S> sat, MUSReasoner<S> unsat) {
		this.sat = sat;
		this.unsat = unsat;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Kernel<S> eval(ISet<S> base, S alpha) {
		return new Kernel<S>(this.kernel(base, alpha));
	}

	/**
	 * Finds all elements of the kernel.
	 * 
	 * @param base base
	 * @param sentence sentence
	 * @return Set of KernelElements
	 */
	private ISet<ISet<S>> kernel(ISet<S> base, S alpha) {
		if (!this.sat.entails(base, alpha)) {
			return ISets.empty();
		}
		ISet<ISet<S>> kernel = ISets.empty();
		for (ISet<S> alphaKernel : new HittingSets<S>(base, new UnsatKernelMatcher(alpha))) {
			kernel = kernel.union(alphaKernel);
		}
		return kernel;
	}

	/**
	 * A hitting set matcher that finds minimal unsatisfiable subsets using an
	 * unsat reasoner and a sat reasoner.
	 * 
	 * @author Lundberg
	 * 
	 */
	private class UnsatKernelMatcher implements HittingSetsMatcher<S> {

		private final S alpha;

		/**
		 * Constructor
		 * 
		 * @param alpha
		 */
		public UnsatKernelMatcher(S alpha) {
			this.alpha = alpha;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ISet<S> findHitting(ISet<S> set) {
			return shrink(unsat.findUnsatisfiableCore(set.union(alpha.negate())).minus(alpha.negate()));
		}

		/**
		 * As the UNSAT operation runs not knowing alpha, it may reach a not so
		 * minimal core, some shrinking may still be necessary.
		 * 
		 * @param unsatResult
		 * @return
		 */
		private ISet<S> shrink(ISet<S> unsatResult) {
			if (unsatResult == null) {
				return null;
			}
			ISet<S> shrunk = unsatResult;
			for (S b : unsatResult) {
				if (sat.entails(shrunk.minus(b), alpha)) {
					shrunk = shrunk.minus(b);
				}
			}
			return shrunk.isEmpty() ? null : shrunk;
		}
	}
}
