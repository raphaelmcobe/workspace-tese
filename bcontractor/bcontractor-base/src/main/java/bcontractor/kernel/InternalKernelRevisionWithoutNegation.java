package bcontractor.kernel;

import bcontractor.api.ISet;
import bcontractor.api.RevisionOperator;
import bcontractor.api.Sentence;

public class InternalKernelRevisionWithoutNegation <S extends Sentence<S>> implements RevisionOperator<S>{
	private final KernelConflictOperator<S> kernelOperator;

	private final IncisionFunction<S> incisionFunction;

	/**
	 * Constructor
	 * 
	 * @param kernelOperator kernelOperator
	 * @param incisionFunction incisionFunction
	 */
	public InternalKernelRevisionWithoutNegation(KernelConflictOperator<S> kernelOperator, IncisionFunction<S> incisionFunction) {
		this.kernelOperator = kernelOperator;
		this.incisionFunction = incisionFunction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISet<S> revise(ISet<S> base, S alpha) {
		return base.minus(incision(kernel(base.union(alpha)))).union(alpha);
	}

	/**
	 * Obtains an incision of the kernel
	 * 
	 * @param kernel kernel
	 * @return incision
	 */
	private ISet<S> incision(Kernel<S> kernel) {
		return this.incisionFunction.eval(kernel);
	}

	/**
	 * Obtains the kernel of the base in relation to the sentence.
	 * 
	 * @param base base
	 * @param sentence sentence
	 * @return kernel
	 */
	private Kernel<S> kernel(ISet<S> base) {
		return this.kernelOperator.eval(base);
	}
	
	
}
