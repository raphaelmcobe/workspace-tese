package bcontractor.kernel;

import bcontractor.api.Sentence;
import bcontractor.api.ISet;

/**
 * Interface for the Kernel operator.
 * 
 * The Kernel operator operates on a sentence set B and sentence \alpha and
 * finds all B' such that B' \subseteq B, B' entails \alpha and for all b \in B'
 * B' \ {b} does not entail \alpha.
 * 
 * @author lundberg
 * 
 * @param <S>
 */
public interface KernelConflictOperator<S extends Sentence<S>> extends KernelOperator<S> {

    /**
     * Finds the smallest inconsistent/incoherent subsets;
     * 
     * 
     * @param sentences
     * 			  knowledge base
     * @return inconsistent kernel
     */
    
    Kernel<S> eval(ISet<S> sentences);
}