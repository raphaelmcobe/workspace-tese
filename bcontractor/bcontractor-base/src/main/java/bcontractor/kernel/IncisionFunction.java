package bcontractor.kernel;

import bcontractor.api.Sentence;
import bcontractor.api.ISet;

/**
 * Incision function, usually needed for operations using kernels.
 * 
 * Selects at least one sentence from each kernel, according to some criteria.
 * 
 * @author lundberg
 * 
 */
public interface IncisionFunction<S extends Sentence<S>> {

    /**
     * Creates a set of sentences containing at least one sentence from each
     * element of the kernel.
     * 
     * @param kernel
     *            kernel
     * @return set of sentences
     */
    ISet<S> eval(Kernel<S> kernel);
}
