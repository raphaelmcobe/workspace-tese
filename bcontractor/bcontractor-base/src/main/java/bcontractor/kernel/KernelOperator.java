package bcontractor.kernel;

import bcontractor.api.Sentence;
import bcontractor.api.ISet;

/**
 * Interface for the Kernel operator.
 * 
 * The Kernel operator operates on a sentence set B and sentence \alpha and
 * finds all B' such that B' \subseteq B, B' entails \alpha and for all b \in B'
 * B' \ {b} does not entail \alpha.
 * 
 * @author lundberg
 * 
 * @param <S>
 */
public interface KernelOperator<S extends Sentence<S>> {

    /**
     * Finds the kernel of a base in relation to a sentence
     * 
     * @param sentences
     *            sentences
     * @param sentence
     *            sentence
     * @return kernel
     */
    Kernel<S> eval(ISet<S> sentences, S sentence);
    
    
}