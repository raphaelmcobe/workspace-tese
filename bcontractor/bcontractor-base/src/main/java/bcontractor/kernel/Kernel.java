package bcontractor.kernel;

import java.util.Iterator;


import bcontractor.api.ISet;
import bcontractor.api.Sentence;

/**
 * Represents a Kernel. A Kernel represents all minimal subsets of a KB that
 * entail a certain sentence.
 * 
 * @author lundberg
 * 
 */
public class Kernel<S extends Sentence<S>> implements Iterable<ISet<S>> {

	private final ISet<ISet<S>> elements;

	/**
	 * Constructor
	 * 
	 * @param elements elements
	 */
	public Kernel(ISet<ISet<S>> elements) {
		this.elements = elements;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<ISet<S>> iterator() {
		return this.elements.iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Kernel<?>)) {
			return false;
		}
		Kernel<?> o = (Kernel<?>) obj;
		return this.elements.equals(o.elements);
	}

	public ISet<ISet<S>> getElements() {
		return elements;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return elements.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Kernel:");
		builder.append("\n\tElements:");
		for (ISet<S> e : this.elements) {
			builder.append("\n\t\t");
			builder.append(e.toString().trim().replaceAll("\n", "\n\t\t"));
		}
		return builder.toString();
	}

	public int getAlphaKernelCount() {
		return this.elements.size();
	}

	public int getMaxAlphaKernelSize() {
		int max = 0;
		for (ISet<S> alphaKernel : this.elements) {
			max = Math.max(max, alphaKernel.size());
		}
		return max;
	}

	public int getMinAlphaKernelSize() {
		int min = Integer.MAX_VALUE;
		for (ISet<S> alphaKernel : this.elements) {
			min = Math.min(min, alphaKernel.size());
		}
		return min;
	}

	public double getMedianAlphaKernelSize() {
		int sum = 0;
		for (ISet<S> alphaKernel : this.elements) {
			sum += alphaKernel.size();
		}
		return (sum * 1.0) / this.elements.size();
	}
}
