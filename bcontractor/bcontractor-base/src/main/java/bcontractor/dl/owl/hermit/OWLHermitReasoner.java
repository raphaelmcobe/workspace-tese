package bcontractor.dl.owl.hermit;

import java.util.HashSet;
import java.util.Set;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.InconsistentOntologyException;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.dl.owl.OWLSentence;

public class OWLHermitReasoner implements SATReasoner<OWLSentence> {

//	private final OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

	@Override
	public boolean entails(ISet<OWLSentence> sentences, OWLSentence sentence) {
		return createHermitReasoner(sentences).isEntailed(sentence.getAxiom());
	}

	@Override
	public boolean isUnsatisfiable(ISet<OWLSentence> sentences) {
		return !isSatisfiable(sentences);
	}

	@Override
	public boolean isSatisfiable(ISet<OWLSentence> sentences) {
//		System.out.println("Size: "+sentences.size());
		long time = System.currentTimeMillis();
		Reasoner reasoner =  createHermitReasoner(sentences);
//		System.out.println("Time: "+(System.currentTimeMillis()-time));
		try{
			reasoner.classifyClasses();
		}catch(InconsistentOntologyException ioex){
			return false;
		}
		return reasoner.getUnsatisfiableClasses().getEntitiesMinusBottom().isEmpty();
	}

	private Reasoner createHermitReasoner(ISet<OWLSentence> sentences) {
		Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();
		for (OWLSentence s : sentences) {
			axioms.add(s.getAxiom());
		}
		Reasoner reasoner;
		try {
			OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
			OWLOntology ontology = manager.createOntology(axioms);
			reasoner = new Reasoner(ontology);
		} catch (OWLOntologyCreationException e) {
			throw new RuntimeException(e);
		}
		return reasoner;
	}

}
