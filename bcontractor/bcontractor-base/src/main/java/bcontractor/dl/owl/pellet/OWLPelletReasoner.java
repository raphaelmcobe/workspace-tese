package bcontractor.dl.owl.pellet;

import java.util.HashSet;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.InconsistentOntologyException;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.dl.owl.OWLSentence;

import com.clarkparsia.modularity.IncrementalClassifier;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

public class OWLPelletReasoner implements SATReasoner<OWLSentence> {

	private final OWLOntologyManager manager = OWLManager.createOWLOntologyManager();


	@Override
	public boolean isSatisfiable(ISet<OWLSentence> sentences) {
		PelletReasoner reasoner = createPelletReasoner(sentences);
		reasoner.precomputeInferences();
		if (!reasoner.isConsistent())
			return false;
		return reasoner.getUnsatisfiableClasses().getSize() == 0;
	}

	private PelletReasoner createPelletReasoner(ISet<OWLSentence> sentences) {
		Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();
		for (OWLSentence s : sentences) {
			axioms.add(s.getAxiom());
		}
		PelletReasoner reasoner;
		try {
			OWLOntology ontology = manager.createOntology(axioms);
			reasoner = PelletReasonerFactory.getInstance().createReasoner(ontology);
		} catch (OWLOntologyCreationException e) {
			throw new RuntimeException(e);
		}
		return reasoner;
	}

	@Override
	public boolean entails(ISet<OWLSentence> sentences, OWLSentence sentence) {
			return createPelletReasoner(sentences).isEntailed(sentence.getAxiom());
	}

	@Override
	public boolean isUnsatisfiable(ISet<OWLSentence> sentences) {
		return !isSatisfiable(sentences);
	}


}
