package bcontractor.dl.owl;

import org.semanticweb.owlapi.model.OWLAxiom;

public class OWLNumberedRestrictionSentence extends OWLSentence {

	public OWLNumberedRestrictionSentence(OWLAxiom axiom) {
		super(axiom);
	}

}
