package bcontractor.dl.owl;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import bcontractor.api.Sentence;

public class OWLSentence implements Sentence<OWLSentence> {

	private final OWLAxiom axiom;

	public OWLSentence(OWLAxiom axiom) {
		super();
		this.axiom = axiom;
	}

	public OWLAxiom getAxiom() {
		return axiom;
	}

	@Override
	public OWLSentence negate() {
		throw new UnsupportedOperationException();
//		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
//		OWLDataFactory df = m.getOWLDataFactory();
//		return new OWLSentence(df.getOWLObjectComplementOf((OWLClassExpression) axiom));
	}
	
	@Override
	public String toString(){
		return this.getAxiom().toString();
	}
}
