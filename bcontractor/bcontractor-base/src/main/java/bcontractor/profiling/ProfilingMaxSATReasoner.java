package bcontractor.profiling;

import bcontractor.api.ISet;
import bcontractor.api.MaxSATReasoner;
import bcontractor.api.Sentence;

public class ProfilingMaxSATReasoner<S extends Sentence<S>> implements MaxSATReasoner<S> {

	private final MaxSATReasoner<S> delegate;

	private long calls = 0;

	public ProfilingMaxSATReasoner(MaxSATReasoner<S> delegate) {
		this.delegate = delegate;
	}

	@Override
	public ISet<S> findMaximumSatisfiableSubset(ISet<S> softSentences, ISet<S> hardSentences) {
		calls++;
		return delegate.findMaximumSatisfiableSubset(softSentences, hardSentences);
	}

	public long getCalls() {
		return calls;
	}
}
