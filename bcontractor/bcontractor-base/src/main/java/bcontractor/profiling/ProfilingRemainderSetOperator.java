package bcontractor.profiling;

import java.lang.management.ManagementFactory;

import bcontractor.api.ISet;
import bcontractor.api.Sentence;
import bcontractor.partialmeet.RemainderSet;
import bcontractor.partialmeet.RemainderSetOperator;

public class ProfilingRemainderSetOperator<S extends Sentence<S>> implements RemainderSetOperator<S> {

	private ProfilingSATReasoner<S> profilingSATReasoner;

	private ProfilingMaxSATReasoner<S> profilingMaxSATReasoner;

	private RemainderSetOperator<S> delegate;

	private long elapsed = 0;

	public ProfilingRemainderSetOperator(RemainderSetOperator<S> delegate, ProfilingSATReasoner<S> profilingSATReasoner,
			ProfilingMaxSATReasoner<S> profilingMaxSATReasoner) {
		super();
		this.profilingSATReasoner = profilingSATReasoner;
		this.profilingMaxSATReasoner = profilingMaxSATReasoner;
		this.delegate = delegate;
	}

	public ProfilingRemainderSetOperator(RemainderSetOperator<S> delegate, ProfilingSATReasoner<S> profilingSATReasoner) {
		this(delegate, profilingSATReasoner, null);
	}

	@Override
	public RemainderSet<S> eval(ISet<S> sentences, S sentence) {
		long start = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime();
		try {
			return delegate.eval(sentences, sentence);
		} finally {
			elapsed += ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime() - start;
		}
	}

	public long getMaxSATCalls() {
		return profilingMaxSATReasoner == null ? 0L : this.profilingMaxSATReasoner.getCalls();
	}

	public long getSATCalls() {
		return this.profilingSATReasoner.getCalls();
	}

	public long getElapsed() {
		return elapsed;
	}
}
