package bcontractor.profiling;

import bcontractor.api.ISet;
import bcontractor.api.Sentence;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.KernelOperator;

public class ProfilingKernelOperator<S extends Sentence<S>> implements KernelOperator<S> {

	private ProfilingSATReasoner<S> profilingSATReasoner;

	private ProfilingMUSReasoner<S> profilingMUSReasoner;

	private KernelOperator<S> delegate;

	private long elapsed = 0;

	public ProfilingKernelOperator(KernelOperator<S> delegate, ProfilingSATReasoner<S> profilingSATReasoner, ProfilingMUSReasoner<S> profilingMUSReasoner) {
		super();
		this.profilingSATReasoner = profilingSATReasoner;
		this.profilingMUSReasoner = profilingMUSReasoner;
		this.delegate = delegate;
	}

	public ProfilingKernelOperator(KernelOperator<S> delegate, ProfilingSATReasoner<S> profilingSATReasoner) {
		this(delegate, profilingSATReasoner, null);
	}

	@Override
	public Kernel<S> eval(ISet<S> sentences, S sentence) {
			return delegate.eval(sentences, sentence);
	}

	public long getMUSCalls() {
		return profilingMUSReasoner == null ? 0L : this.profilingMUSReasoner.getCalls();
	}

	public long getSATCalls() {
		return this.profilingSATReasoner.getCalls();
	}

	public long getElapsed() {
		return elapsed;
	}
}
