package bcontractor.profiling;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;

public class ProfilingSATReasoner<S extends Sentence<S>> implements SATReasoner<S> {

	private final SATReasoner<S> delegate;

	private long calls = 0;

	public ProfilingSATReasoner(SATReasoner<S> delegate) {
		this.delegate = delegate;
	}

	@Override
	public boolean entails(ISet<S> sentences, S sentence) {
		calls++;
		return delegate.entails(sentences, sentence);
	}

	@Override
	public boolean isSatisfiable(ISet<S> sentences) {
		calls++;
		return delegate.isSatisfiable(sentences);
	}

	@Override
	public boolean isUnsatisfiable(ISet<S> sentences) {
		calls++;
		return delegate.isUnsatisfiable(sentences);
	}

	public long getCalls() {
		return calls;
	}
}
