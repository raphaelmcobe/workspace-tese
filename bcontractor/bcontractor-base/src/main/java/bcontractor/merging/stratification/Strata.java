package bcontractor.merging.stratification;

import java.util.List;
import java.util.Iterator;

import bcontractor.api.ISet;
import bcontractor.api.Sentence;

public class Strata<S extends Sentence<S>> implements Iterable<List<S>> {

	private ISet<List<S>> elements;
	
	@Override
	public Iterator<List<S>> iterator() {
		return this.elements.iterator();
	}
	
	public Strata(ISet<List<S>> elements){
		this.elements=elements;
	}
}
