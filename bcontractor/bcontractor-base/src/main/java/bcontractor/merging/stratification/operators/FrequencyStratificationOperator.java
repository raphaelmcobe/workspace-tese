package bcontractor.merging.stratification.operators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bcontractor.api.ISet;
import bcontractor.api.Sentence;
import bcontractor.api.StratificationOperator;
import bcontractor.base.ISets;
import bcontractor.kernel.Kernel;
import bcontractor.merging.stratification.Strata;

public class FrequencyStratificationOperator<S extends Sentence<S>> implements StratificationOperator<S> {

	

	@Override
	public Strata<S> stratify(Kernel<S> kernelSet) {
		ISet<List<S>> strata = ISets.EMPTY_SET;
		Strata<S> toReturn = new Strata<S>(strata);
		Map<S,Integer> sentenceWithFrequency = new HashMap<S,Integer>();
		//Count the number of occurrences of each sentence in each kernel element.
		for (ISet<S> kernel : kernelSet) {
			for (S kernelElement : kernel) {
				if(!(sentenceWithFrequency.containsKey(kernelElement))){
					sentenceWithFrequency.put(kernelElement, 0);
				}
				int frequency = sentenceWithFrequency.get(kernelElement);
				sentenceWithFrequency.put(kernelElement, frequency+1);
			}
		}
		
		//Create a list with the elements of each kernel sorted by their frequencies.
		for (ISet<S> kernel : kernelSet) {
			Map<Integer,S> sentencesFromThisKernelWithTheirFrequencies = new HashMap<Integer, S>();
			for (S kernelElement : kernel) {
				sentencesFromThisKernelWithTheirFrequencies.put(sentenceWithFrequency.get(kernelElement), kernelElement);
			}
			List<Integer> keys = new ArrayList<Integer>(sentencesFromThisKernelWithTheirFrequencies.keySet());
			Collections.sort(keys);
			List<S> stratifiedSentences = new ArrayList<S>();
			for (Integer frequencyCounter : keys) {
				stratifiedSentences.add(sentencesFromThisKernelWithTheirFrequencies.get(frequencyCounter));
			}
			strata.union(stratifiedSentences);
		}
		return toReturn;
	}
	
}
