package bcontractor.merging.stratification.operators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.api.StratificationOperator;
import bcontractor.base.ISets;
import bcontractor.dl.owl.OWLSentence;
import bcontractor.kernel.Kernel;
import bcontractor.merging.stratification.Strata;

public class GeneralityStratificationOperator implements StratificationOperator<OWLSentence> {

	
	private SATReasoner<OWLSentence> reasoner = null;
	private ISet<OWLSentence> knowledgeBase;
	
	public GeneralityStratificationOperator(SATReasoner<OWLSentence> reasoner, ISet<OWLSentence> knowledgeBase){
		this.reasoner=reasoner;
		this.knowledgeBase = knowledgeBase;
	}
	
	@Override
	public Strata<OWLSentence> stratify(Kernel<OWLSentence> kernelSet) {
		ISet<List<OWLSentence>> strata = ISets.empty();
		Strata<OWLSentence> toReturn = new Strata<OWLSentence>(strata);
		for (ISet<OWLSentence> eachKernel : kernelSet) {
			Map<OWLSentence, Integer> map = new HashMap<OWLSentence,Integer>();
			for (OWLSentence owlSentence : eachKernel) {
				OWLAxiom axiom = owlSentence.getAxiom();
				knowledgeBase = knowledgeBase.minus(owlSentence); //Remove axiom from Knowledge Base
				if(axiom.isOfType(AxiomType.SUBCLASS_OF)){
					OWLSubClassOfAxiom subClassAxiom = (OWLSubClassOfAxiom) axiom;
					OWLClassExpression subClass = subClassAxiom.getSubClass(); //get the subclass part of the axiom
					for(OWLSentence toCompare : eachKernel.minus(owlSentence)){
						OWLAxiom axiomToCompare = toCompare.getAxiom();
						if(axiomToCompare.isOfType(AxiomType.SUBCLASS_OF)){
							OWLClassExpression subClassToCompare = ((OWLSubClassOfAxiom)axiomToCompare).getSubClass();
							OWLSubClassOfAxiom newAxiom = OWLManager.createOWLOntologyManager().getOWLDataFactory().getOWLSubClassOfAxiom(subClass, subClassToCompare); //Checks whether C_1\sqsubseteq C_2 and not the other way around;
							if(reasoner.entails(knowledgeBase, new OWLSentence(newAxiom))){
								OWLSubClassOfAxiom newAxiomReverse = OWLManager.createOWLOntologyManager().getOWLDataFactory().getOWLSubClassOfAxiom(subClassToCompare, subClass);
								if(!reasoner.entails(knowledgeBase, new OWLSentence(newAxiomReverse))){
									Integer occurrences = map.get(owlSentence);
									if(occurrences == null) occurrences = 0;
									map.put(owlSentence, occurrences++);
								}
							}
						}else{
							map.put(owlSentence, 0);
						}
					}
				}
				knowledgeBase = knowledgeBase.union(owlSentence);
			}
			List<Integer> values =  Arrays.asList(map.values().toArray(new Integer[0]));
			Collections.sort(values);
			Collections.reverse(values);
			
			//Replace value with key in map
			Map<Integer,Set<OWLSentence>> reverseMap= new HashMap<Integer,Set<OWLSentence>>();
			for (OWLSentence sentence : map.keySet()) {
				
				Integer frequency = map.get(sentence);
				
				Set<OWLSentence> sentences = reverseMap.get(frequency);
				if(sentences == null || sentences.isEmpty()){
					sentences = new HashSet<OWLSentence>();
				}
				sentences.add(sentence);
				reverseMap.put(frequency, sentences);
			}
			
			
			
			
			List<OWLSentence> stratifiedKernel = new ArrayList<OWLSentence>();
			for (Integer frequency : values) {
				stratifiedKernel.addAll(reverseMap.get(frequency));
			}
			strata.union(stratifiedKernel);
		}
		return toReturn;
	}

}
