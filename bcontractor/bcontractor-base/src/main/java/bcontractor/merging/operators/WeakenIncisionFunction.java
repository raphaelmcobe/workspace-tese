package bcontractor.merging.operators;

import bcontractor.api.ISet;
import bcontractor.api.Sentence;
import bcontractor.merging.stratification.Strata;

public abstract class WeakenIncisionFunction<S extends Sentence<S>> {

	public abstract ISet<S> eval(Strata<S> stratifiedKernel);
}
