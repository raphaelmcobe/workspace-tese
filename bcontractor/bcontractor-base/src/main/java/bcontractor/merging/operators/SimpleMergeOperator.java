package bcontractor.merging.operators;

import java.io.File;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import bcontractor.api.ISet;
import bcontractor.api.MergeOperator;
import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.api.StratificationOperator;
import bcontractor.api.WeakenOperator;
import bcontractor.kernel.IncisionFunction;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.KernelConflictOperator;
import bcontractor.merging.stratification.Strata;

public class SimpleMergeOperator<S extends Sentence<S>> implements MergeOperator<S>{

	private KernelConflictOperator<S> conflictOperator;
	private StratificationOperator<S> stratificationOperator;
	private WeakenIncisionFunction<S> weakenIncisionFunction;
	private WeakenOperator<S> weakenOperator;
	private IncisionFunction<S> incisionFunction;
	private SATReasoner<S> reasoner;

	public SimpleMergeOperator(KernelConflictOperator<S> conflictOperator,
			StratificationOperator<S> stratificationOperator,
			WeakenIncisionFunction<S> weakenIncisionFunction,
			WeakenOperator<S> weakenOperator,
			IncisionFunction<S> incisionFunction, SATReasoner<S> reasoner) {
		this.conflictOperator = conflictOperator;
		this.stratificationOperator = stratificationOperator;
		this.weakenIncisionFunction = weakenIncisionFunction;
		this.weakenOperator = weakenOperator;
		this.incisionFunction = incisionFunction;
		this.reasoner = reasoner;
	}

	@Override
	public ISet<S> merge(ISet<S> base1, ISet<S> base2) {
		Kernel<S> kernelSet = this.kernel(base1.union(base2));
		Strata<S> stratifiedKernel = this.stratify(kernelSet);
		ISet<S> cuttingSetToWeaken = this.weakenIncision(stratifiedKernel);
		ISet<S> weakenedSet = this.weaken(cuttingSetToWeaken);
		if(this.reasoner.isSatisfiable(base1.union(base2).minus(cuttingSetToWeaken).union(weakenedSet))){
			return base1.union(base2).minus(cuttingSetToWeaken).union(weakenedSet);
		}
		return base1.union(base2).minus(incision(kernelSet));
	}
	
	
	private ISet<S> incision(Kernel<S> kernel) {
		return this.incisionFunction.eval(kernel); 
	}

	private ISet<S> weaken(ISet<S> cuttingSetToWeaken) {
		return this.weakenOperator.weaken(cuttingSetToWeaken);
	}

	private ISet<S> weakenIncision(Strata<S> stratifiedKernel) {
		return this.weakenIncisionFunction.eval(stratifiedKernel);
	}

	private Strata<S> stratify(Kernel<S> kernelSet) {
		return this.stratificationOperator.stratify(kernelSet);
	}

	public Kernel<S> kernel(ISet<S> sentences){
		return this.conflictOperator.eval(sentences);
	}
	
	
	
	public static void main(String[] args) throws OWLOntologyCreationException{
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		OWLOntology o = m.loadOntologyFromOntologyDocument(new File("/tmp/teste.owl"));
		Set<OWLAxiom> axioms = o.getAxioms();
		for (OWLAxiom owlAxiom : axioms) {
			System.out.println(owlAxiom);
		}
	}

}
