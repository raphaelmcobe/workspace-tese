package bcontractor.merging.operators;

import java.util.List;

import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLObjectMaxCardinality;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;

import bcontractor.api.ISet;
import bcontractor.base.ISets;
import bcontractor.dl.owl.OWLSentence;
import bcontractor.merging.stratification.Strata;

public class NumberedWeakeningIncisionFunction extends WeakenIncisionFunction<OWLSentence> {

	@Override
	public ISet<OWLSentence> eval(Strata<OWLSentence> stratifiedKernel) {
		ISet<OWLSentence> toReturn = ISets.empty();
		
		for (List<OWLSentence> eachKernel : stratifiedKernel) {
			for (OWLSentence owlSentence : eachKernel) {
				OWLAxiom axiom = owlSentence.getAxiom();
				if(axiom.isOfType(AxiomType.SUBCLASS_OF)){
					OWLClassExpression superClassExpression = ((OWLSubClassOfAxiom) axiom).getSuperClass();
					if(superClassExpression.getClassExpressionType().equals(ClassExpressionType.OBJECT_MAX_CARDINALITY)){
						toReturn.union(new OWLSentence(axiom));
					}
				}
				else if(axiom.isOfType(AxiomType.OBJECT_PROPERTY_ASSERTION)){
					toReturn.union(new OWLSentence(axiom));
				}
			}
		}
		
		
		
		
		
		
		return toReturn;
	}

}
