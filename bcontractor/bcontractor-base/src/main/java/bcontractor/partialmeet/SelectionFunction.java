package bcontractor.partialmeet;

import java.util.Set;

import bcontractor.api.Sentence;
import bcontractor.api.ISet;

/**
 * Selection function, usually needed for operations using remainder sets.
 * 
 * Selects a set of 'best' elements among a remainder set, according to some
 * criteria.
 * 
 * @author lundberg
 * 
 */
public interface SelectionFunction<S extends Sentence<S>> {

    /**
     * Chooses a set of elements from the remainder set.
     * 
     * @param remainderset
     *            remainderset
     * @return set of chosen sentence sets
     */
    Set<ISet<S>> eval(RemainderSet<S> remainderSet);
}
