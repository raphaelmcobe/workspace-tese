package bcontractor.partialmeet;

import static bcontractor.base.ISets.intersection;

import java.util.Set;

import bcontractor.api.RevisionOperator;
import bcontractor.api.Sentence;
import bcontractor.api.ISet;

/**
 * Implementation of the revision operation using partial meets between
 * remainder sets
 * 
 * @author lundberg
 * 
 * @param <S>
 */
public class PartialMeetRevisionOperator<S extends Sentence<S>> implements RevisionOperator<S> {

	private final RemainderSetOperator<S> remainderSetOperator;

	private final SelectionFunction<S> selectionFunction;

	/**
	 * Construtor
	 * 
	 * @param remainderSetOperator remainder
	 * @param selectionFunction selection
	 */
	public PartialMeetRevisionOperator(RemainderSetOperator<S> remainderSetOperator, SelectionFunction<S> selectionFunction) {
		this.remainderSetOperator = remainderSetOperator;
		this.selectionFunction = selectionFunction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISet<S> revise(ISet<S> base, S sentence) {
		return intersection(this.selection(this.remainder(base, sentence.negate()))).union(sentence);
	}

	/**
	 * Invokes the remainder set operator
	 * 
	 * @param base base
	 * @param sentence sentence
	 * @return remainder set
	 */
	private RemainderSet<S> remainder(ISet<S> base, S sentence) {
		return this.remainderSetOperator.eval(base, sentence);
	}

	/**
	 * Invokes the selection function
	 * 
	 * @param remainder remainder
	 * @return selected elements from remainder set
	 */
	private Set<ISet<S>> selection(RemainderSet<S> remainder) {
		return this.selectionFunction.eval(remainder);
	}
}
