package bcontractor.partialmeet;

import bcontractor.api.Sentence;
import bcontractor.api.ISet;

/**
 * Interface for the Remainder Set operator.
 * 
 * The remainder set operator operates on a sentence set B and sentence \alpha
 * and finds all B' such that B' \subseteq B, B' doesn't entail \alpha and for
 * all b \in B \ B', B' U {b} entails \alpha.
 * 
 * @author lundberg
 * 
 * @param <S>
 */
public interface RemainderSetOperator<S extends Sentence<S>> {

    /**
     * Finds the remainder set of a base in relation to a sentence
     * 
     * @param base
     *            knowledge base
     * @param sentence
     *            sentence
     * @return kernel
     */
    RemainderSet<S> eval(ISet<S> base, S sentence);
}