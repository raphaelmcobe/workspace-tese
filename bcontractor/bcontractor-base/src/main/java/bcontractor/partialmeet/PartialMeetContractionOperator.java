package bcontractor.partialmeet;

import static bcontractor.base.ISets.intersection;

import java.util.Set;

import bcontractor.api.ContractionOperator;
import bcontractor.api.Sentence;
import bcontractor.api.ISet;

/**
 * Implementation of the contraction operation using partial meets between
 * remainder sets
 * 
 * @author lundberg
 * 
 * @param <S>
 */
public class PartialMeetContractionOperator<S extends Sentence<S>> implements ContractionOperator<S> {

	private final RemainderSetOperator<S> remainderSetOperator;

	private final SelectionFunction<S> selectionFunction;

	/**
	 * Construtor
	 * 
	 * @param remainderSetOperator remainder
	 * @param selectionFunction selection
	 */
	public PartialMeetContractionOperator(RemainderSetOperator<S> remainderSetOperator, SelectionFunction<S> selectionFunction) {
		this.remainderSetOperator = remainderSetOperator;
		this.selectionFunction = selectionFunction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISet<S> contract(ISet<S> base, S sentence) {
		return intersection(this.selection(this.remainder(base, sentence)));
	}

	/**
	 * Invokes the remainder set operator
	 * 
	 * @param base base
	 * @param sentence sentence
	 * @return remainder set
	 */
	private RemainderSet<S> remainder(ISet<S> base, S sentence) {
		return this.remainderSetOperator.eval(base, sentence);
	}

	/**
	 * Invokes the selection function
	 * 
	 * @param remainder remainder
	 * @return selected elements from remainder set
	 */
	private Set<ISet<S>> selection(RemainderSet<S> remainder) {
		return this.selectionFunction.eval(remainder);
	}
}
