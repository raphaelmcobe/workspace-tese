package bcontractor.partialmeet;

import java.util.Iterator;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import bcontractor.api.ISet;
import bcontractor.api.Sentence;

/**
 * Represents a RemainderSet. A remainder set contains all maximal subsets of
 * sentences of a knowledge base that fail to entail a sentence.
 * 
 * @author lundberg
 * 
 */
public class RemainderSet<S extends Sentence<S>> implements Iterable<ISet<S>> {

	private final ISet<ISet<S>> elements;

	/**
	 * Constructor
	 * 
	 * @param elements elements
	 */
	public RemainderSet(ISet<ISet<S>> elements) {
		this.elements = elements;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<ISet<S>> iterator() {
		return this.elements.iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof RemainderSet<?>)) {
			return false;
		}
		RemainderSet<?> o = (RemainderSet<?>) obj;
		return new EqualsBuilder().append(this.elements, o.elements).isEquals();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.elements).toHashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RemainderSet:");
		builder.append("\n\tElements:");
		for (ISet<S> e : this.elements) {
			builder.append("\n\t\t");
			builder.append(e.toString().trim().replaceAll("\n", "\n\t\t"));
		}
		return builder.toString();
	}

	public int getRemaindersCount() {
		return this.elements.size();
	}

	public int getMaxRemainderSize() {
		int max = 0;
		for (ISet<S> remainder : this.elements) {
			max = Math.max(max, remainder.size());
		}
		return max;
	}

	public int getMinRemainderSize() {
		int min = Integer.MAX_VALUE;
		for (ISet<S> remainder : this.elements) {
			min = Math.min(min, remainder.size());
		}
		return min;
	}

	public double getMedianRemainderSize() {
		int sum = 0;
		for (ISet<S> remainder : this.elements) {
			sum += remainder.size();
		}
		return (sum * 1.0) / this.elements.size();
	}
}
