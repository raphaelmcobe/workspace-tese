package bcontractor.partialmeet.operators;

import bcontractor.api.ISet;
import bcontractor.api.MaxSATReasoner;
import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.base.ISets;
import bcontractor.hittingsets.HittingSets;
import bcontractor.hittingsets.HittingSetsMatcher;
import bcontractor.partialmeet.RemainderSet;
import bcontractor.partialmeet.RemainderSetOperator;

/**
 * 
 * Remander set implementation that uses a MaxSAT capable solver to speed up the
 * matching process
 * 
 * @author Lundberg
 * 
 * @param <S> Sentence
 */
public class MaxSATRemainderSetOperator<S extends Sentence<S>> implements RemainderSetOperator<S> {

	private final SATReasoner<S> sat;

	private final MaxSATReasoner<S> maxsat;

	public MaxSATRemainderSetOperator(SATReasoner<S> sat, MaxSATReasoner<S> maxsat) {
		this.sat = sat;
		this.maxsat = maxsat;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RemainderSet<S> eval(ISet<S> base, S sentence) {
		return new RemainderSet<S>(this.findElements(base, sentence));
	}

	/**
	 * 
	 * @param base base
	 * @param sentence sentence
	 * @return set of all remainder sets
	 */
	private ISet<ISet<S>> findElements(ISet<S> base, S sentence) {
		if (!this.sat.entails(base, sentence)) {
			return ISets.singleton(base);
		}
		ISet<ISet<S>> remainderSet = ISets.empty();
		for (ISet<S> remainderComplement : new HittingSets<S>(base, new RemainderBlackboxMatcher(base, sentence))) {
			if (remainderComplement.size() < base.size()) {
				remainderSet = remainderSet.union(base.minus(remainderComplement));
			}
		}
		return remainderSet;
	}

	/**
	 * Matcher for the complement of a remainder set element, using the blackbox
	 * approach.
	 * 
	 * @author Lundberg
	 * 
	 */
	private class RemainderBlackboxMatcher implements HittingSetsMatcher<S> {

		private final ISet<S> base;

		private final ISet<S> baseWithNegate;

		private final S sentence;

		/**
		 * Constructor
		 * 
		 * @param base
		 * @param sentence
		 */
		private RemainderBlackboxMatcher(ISet<S> base, S sentence) {
			this.base = base;
			this.sentence = sentence;
			this.baseWithNegate = base.union(sentence.negate());
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ISet<S> findHitting(ISet<S> set) {
			ISet<S> max = maxsat.findMaximumSatisfiableSubset(set, hardSentences(set)).minus(sentence.negate());
			if (max.isEmpty()) {
				return null;
			}
			return base.minus(max);
		}

		private ISet<S> hardSentences(ISet<S> set) {
			if (set.contains(sentence.negate())) {
				return base.minus(set).union(sentence.negate());
			} else {
				return baseWithNegate.minus(set);
			}
		}
	}
}
