package bcontractor.partialmeet.operators;

import bcontractor.api.ISet;
import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.base.ISets;
import bcontractor.hittingsets.HittingSetsMatcher;
import bcontractor.hittingsets.HittingSets;
import bcontractor.partialmeet.RemainderSet;
import bcontractor.partialmeet.RemainderSetOperator;

/**
 * An implementation of the remainder set operator loosely based on the ideas of
 * the correspondent kernel operator.
 * 
 * This approach uses an algorithm made to search minimal subsets, here employed
 * to find maximal subsets through the search for the minimal complements.
 * 
 * @author lundberg
 * 
 */
public class BlackboxRemainderSetOperator<S extends Sentence<S>> implements RemainderSetOperator<S> {

	private final SATReasoner<S> reasoner;

	/**
	 * Constructor
	 * 
	 * @param reasoner reasoner
	 */
	public BlackboxRemainderSetOperator(SATReasoner<S> reasoner) {
		this.reasoner = reasoner;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RemainderSet<S> eval(ISet<S> base, S sentence) {
		return new RemainderSet<S>(this.findElements(base, sentence));
	}

	/**
	 * 
	 * @param base base
	 * @param sentence sentence
	 * @return set of all remainder sets
	 */
	private ISet<ISet<S>> findElements(ISet<S> base, S sentence) {
		if (!this.reasoner.entails(base, sentence)) {
			return ISets.singleton(base);
		}
		ISet<ISet<S>> remainderSet = ISets.empty();
		for (ISet<S> remainderComplement : new HittingSets<S>(base, new RemainderBlackboxMatcher(base, sentence))) {
			if (remainderComplement.size() < base.size()) {
				remainderSet = remainderSet.union(base.minus(remainderComplement));
			}
		}
		return remainderSet;
	}

	/**
	 * Matcher for the complement of a remainder set element, using the blackbox
	 * approach.
	 * 
	 * @author Lundberg
	 * 
	 */
	private class RemainderBlackboxMatcher implements HittingSetsMatcher<S> {

		private final ISet<S> base;

		private final S sentence;

		/**
		 * Constructor
		 * 
		 * @param base
		 * @param sentence
		 */
		private RemainderBlackboxMatcher(ISet<S> base, S sentence) {
			this.base = base;
			this.sentence = sentence;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ISet<S> findHitting(ISet<S> set) {
			return this.shrink(this.expand(set));
		}

		/**
		 * Finds some subset of the given set such that (base \ expanded) does
		 * not entail the sentence.
		 * 
		 * @param set
		 * @param sentence
		 * @return subset that entails the sentence
		 */
		private ISet<S> expand(ISet<S> set) {
			ISet<S> expanded = ISets.empty();
			for (S b : set) {
				expanded = expanded.union(b);
				if (!reasoner.entails(base.minus(expanded), sentence)) {
					return expanded;
				}
			}
			return null;
		}

		/**
		 * Receives an expanded set, such that (base \ expanded) entails the
		 * sentence, and shrinks it, removing all sentences it can while still
		 * keeping the property.
		 * 
		 * @param expanded
		 * @param sentence
		 * @return the complement of a maximal subset of the knowledge base that
		 *         fails to entail the sentence.
		 */
		private ISet<S> shrink(ISet<S> expanded) {
			if (expanded == null) {
				return null;
			}
			ISet<S> shrunk = expanded;
			for (S b : expanded) {
				if (!reasoner.entails(base.minus(shrunk.minus(b)), sentence)) {
					shrunk = shrunk.minus(b);
				}
			}
			return shrunk;
		}
	}
}
