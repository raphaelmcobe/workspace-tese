package bcontractor.partialmeet.selection;

import java.util.HashSet;
import java.util.Set;

import bcontractor.api.Sentence;
import bcontractor.api.ISet;
import bcontractor.partialmeet.RemainderSet;
import bcontractor.partialmeet.SelectionFunction;

/**
 * A selection function that chooses all elements from a remainder set. This
 * implementation is intended for testing, probably not a good choice for
 * practical purposes.
 * 
 * @author lundberg
 * 
 */
public class FullSelectionFunction<S extends Sentence<S>> implements SelectionFunction<S> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<ISet<S>> eval(RemainderSet<S> remainderSet) {
        Set<ISet<S>> chosen = new HashSet<ISet<S>>();
        for (ISet<S> candidate : remainderSet) {
            chosen.add(candidate);
        }
        return chosen;
    }
}
