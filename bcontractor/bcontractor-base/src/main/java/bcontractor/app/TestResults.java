package bcontractor.app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import bcontractor.api.ReasonerException;

public class TestResults {

	private Set<String> properties = new LinkedHashSet<String>();

	private List<Map<String, Object>> results = Collections.synchronizedList(new LinkedList<Map<String, Object>>());

	public TestResults prepareForNewTest() {
		results.add(new HashMap<String, Object>());
		return this;
	}

	public void add(Map<String, Object> map) {
		properties.addAll(map.keySet());
		results.add(new HashMap<String, Object>(map));
	}
	
	public void store(File file) throws IOException {
		boolean writeHeader = !file.exists();
		Writer out = new OutputStreamWriter(new FileOutputStream(file, true), Charset.forName("UTF-8"));
		try {
			List<String> columns = new ArrayList<String>(this.properties);
			if(writeHeader) {
				out.write(StringUtils.join(columns, ";"));
				out.write("\n");
			}
			for (Map<String, Object> result : this.results) {
				List<String> ordered = new ArrayList<String>();
				for (String column : columns) {
					Object value = result.get(column);
					ordered.add(value == null ? "" : value.toString());
				}
				out.write(StringUtils.join(ordered, ";"));
				out.write("\n");
			}
		} finally {
			IOUtils.closeQuietly(out);
		}
		this.results.clear();
	}
}
