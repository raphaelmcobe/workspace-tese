package bcontractor.app;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;
import java.util.concurrent.TimeUnit;

import bcontractor.api.MUSReasoner;
import bcontractor.api.MaxSATReasoner;
import bcontractor.api.SATReasoner;
import bcontractor.kernel.Kernel;
import bcontractor.kernel.KernelOperator;
import bcontractor.kernel.operators.BlackboxKernelOperator;
import bcontractor.kernel.operators.MMRKernelOperator;
import bcontractor.kernel.operators.UnsatKernelOperator;
import bcontractor.partialmeet.RemainderSet;
import bcontractor.partialmeet.RemainderSetOperator;
import bcontractor.partialmeet.operators.BlackboxRemainderSetOperator;
import bcontractor.partialmeet.operators.MaxSATRemainderSetOperator;
import bcontractor.profiling.ProfilingKernelOperator;
import bcontractor.profiling.ProfilingMUSReasoner;
import bcontractor.profiling.ProfilingMaxSATReasoner;
import bcontractor.profiling.ProfilingRemainderSetOperator;
import bcontractor.profiling.ProfilingSATReasoner;
import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.inputfactory.ArbitraryClauseFactory;
import bcontractor.propositional.inputfactory.ContractionCase;
import bcontractor.propositional.inputfactory.ContractionCaseFactory;
import bcontractor.propositional.inputfactory.SatisfiablePropositionalSentenceSetFactory;
import bcontractor.propositional.inputfactory.SentenceSetFactory;
import bcontractor.propositional.sat4j.PropositionalSAT4JMUSReasoner;
import bcontractor.propositional.sat4j.PropositionalSAT4JMaxSATReasoner;
import bcontractor.propositional.sat4j.PropositionalSAT4JSATReasoner;
import bcontractor.stat.ConstantPicker;
import bcontractor.stat.IncrementalPicker;
import bcontractor.stat.IntegerPicker;
import bcontractor.stat.Picker;

/**
 * Generates scenarios and runs available algorithms against them, logging
 * information about the problem and about the algorithms resource consumption.
 * 
 * @author Lundberg
 * 
 */
public class AlgorithmProfiler {

	private static final String RESULTS_FILE = "scenarios/results_low_variables.csv";

	private static final long TEST_CASE_BATCHES = 200L;
	
	private static final long CASES_PER_BATCH = 1000L;
	
	private static final long RUNS_TIMEOUT = 5000000000L;
	
	private static final long MAX_RUNS = 1L;

	private ThreadPoolExecutor service = new ThreadPoolExecutor(4, 8, 1, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(10000));

	private SATReasoner<PropositionalSentence> satReasoner = new PropositionalSAT4JSATReasoner();

	private MUSReasoner<PropositionalSentence> musReasoner = new PropositionalSAT4JMUSReasoner();

	private MaxSATReasoner<PropositionalSentence> maxSATReasoner = new PropositionalSAT4JMaxSATReasoner();

	public AlgorithmProfiler() {
		service.setRejectedExecutionHandler(new CallerRunsPolicy());
	}

	/**
	 * Creates scenarios
	 */
	private void run() throws Exception {
		// SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
		// Random random = new Random(32452867L);
		Random random = new Random(49979687L);
		// Picker<Integer> clauseSizeMinimal = new IntegerPicker(random, 3, 4);
		// Picker<Integer> clauseSizeRange = new IntegerPicker(random, 0, 1);
		// Picker<Integer> variables = new IntegerPicker(random, 5, 10);
		// Picker<Integer> clauses = new IntegerPicker(random, 5, 40);
		Picker<Integer> clauseSizeMinimal = new ConstantPicker(3);
		Picker<Integer> clauseSizeRange = new ConstantPicker(0);
		Picker<Integer> variables = new ConstantPicker(5);
		Picker<Integer> clauses = new IncrementalPicker().first(5).step(1).repeat(100);

		final TestResults results = new TestResults();
		List<Future<ContractionScenario>> futureCases = new ArrayList<Future<ContractionScenario>>();
		long start = System.currentTimeMillis();
		for (int b = 0; b < TEST_CASE_BATCHES; b++) {
			for (int i = 0; i < CASES_PER_BATCH; i++) {
				futureCases.add(createProblem(random.nextLong(), clauseSizeMinimal.pick(), clauseSizeRange.pick(), variables.pick(), clauses.pick()));
			}
			List<ContractionScenario> cases = new ArrayList<ContractionScenario>();
			for (Future<ContractionScenario> futureCase : futureCases) {
				if (futureCase.get() != null) {
					cases.add(futureCase.get());
				}
			}
//			List<Future<List<Map<String,Object>>>> measurements = new ArrayList<Future<List<Map<String,Object>>>>();
//			for (final ContractionScenario contractionCase : cases) {
//				measurements.add(service.submit(new Callable<List<Map<String,Object>>>() {
//					@Override
//					public List<Map<String,Object>> call() throws Exception {						
//						return measure(contractionCase);
//					}
//				}));
//			}
//			for(Future<List<Map<String,Object>>> measurement : measurements) {
//				for(Map<String,Object> result : measurement.get()) {
//					results.add(result);
//				}
			for (final ContractionScenario contractionCase : cases) {
				for(Map<String,Object> result : measure(contractionCase)) {
					results.add(result);
				}
				results.store(new File(RESULTS_FILE));
			}
		}
		System.out.println("Total: " + (System.currentTimeMillis() - start));
		service.shutdown();
		System.exit(0);
	}

	private List<Map<String,Object>> measure(ContractionScenario scenario) throws Exception {
		List<Map<String,Object>> results = new ArrayList<Map<String,Object>>();
		results.add(measureMaxSAT(scenario));
		results.add(measureKernelMUS(scenario));
		results.add(measureRemainderBlackbox(scenario));
		results.add(measureKernelMMR(scenario));
		results.add(measureKernelBlackbox(scenario));
		return results;

	}

	private Map<String, Object> measureKernelBlackbox(ContractionScenario scenario)
			throws Exception {
		ProfilingSATReasoner<PropositionalSentence> blackbockMeasuringReasoner = new ProfilingSATReasoner<PropositionalSentence>(satReasoner);
		KernelOperator<PropositionalSentence> blackbox = new BlackboxKernelOperator<PropositionalSentence>(blackbockMeasuringReasoner);
		return measureAlgorithm(scenario, "kernel-blackbox", new ProfilingKernelOperator<PropositionalSentence>(blackbox, blackbockMeasuringReasoner));
	}

	private Map<String, Object> measureKernelMMR(ContractionScenario scenario)
			throws Exception {
		ProfilingSATReasoner<PropositionalSentence> mmrMeasuringReasoner = new ProfilingSATReasoner<PropositionalSentence>(satReasoner);
		KernelOperator<PropositionalSentence> mmr = new MMRKernelOperator<PropositionalSentence>(mmrMeasuringReasoner);
		return measureAlgorithm(scenario, "kernel-mmr", new ProfilingKernelOperator<PropositionalSentence>(mmr, mmrMeasuringReasoner));
	}

	private Map<String, Object> measureKernelMUS(ContractionScenario scenario)
			throws Exception {
		ProfilingSATReasoner<PropositionalSentence> musSATMeasuringReasoner = new ProfilingSATReasoner<PropositionalSentence>(satReasoner);
		ProfilingMUSReasoner<PropositionalSentence> musMUSMeasuringReasoner = new ProfilingMUSReasoner<PropositionalSentence>(musReasoner);
		KernelOperator<PropositionalSentence> mus = new UnsatKernelOperator<PropositionalSentence>(musSATMeasuringReasoner, musMUSMeasuringReasoner);
		return measureAlgorithm(scenario, "kernel-mus", new ProfilingKernelOperator<PropositionalSentence>(mus, musSATMeasuringReasoner,
				musMUSMeasuringReasoner));
	}

	private Map<String, Object> measureRemainderBlackbox(ContractionScenario scenario)
			throws Exception {
		ProfilingSATReasoner<PropositionalSentence> blackboxRemainderReasoner = new ProfilingSATReasoner<PropositionalSentence>(satReasoner);
		RemainderSetOperator<PropositionalSentence> blackboxRemainder = new BlackboxRemainderSetOperator<PropositionalSentence>(blackboxRemainderReasoner);
		return measureAlgorithm(scenario, "remainder-blackbox", new ProfilingRemainderSetOperator<PropositionalSentence>(blackboxRemainder,
				blackboxRemainderReasoner));
	}

	private Map<String, Object> measureMaxSAT(ContractionScenario scenario)
			throws Exception {
		ProfilingSATReasoner<PropositionalSentence> maxSATRemainderReasoner = new ProfilingSATReasoner<PropositionalSentence>(satReasoner);
		ProfilingMaxSATReasoner<PropositionalSentence> maxSATMaxRemainderReasoner = new ProfilingMaxSATReasoner<PropositionalSentence>(maxSATReasoner);
		RemainderSetOperator<PropositionalSentence> maxSAT = new MaxSATRemainderSetOperator<PropositionalSentence>(maxSATRemainderReasoner,
				maxSATMaxRemainderReasoner);
		return measureAlgorithm(scenario, "remainder-maxsat", new ProfilingRemainderSetOperator<PropositionalSentence>(maxSAT, maxSATRemainderReasoner,
				maxSATMaxRemainderReasoner));
	}

	private Map<String, Object> measureAlgorithm(ContractionScenario scenario, String algorithm, final ProfilingKernelOperator<PropositionalSentence> operator)
			throws Exception {
		final ContractionCase<PropositionalSentence> contractionCase = scenario.getContractionCase();
		Kernel<PropositionalSentence> kernel = null;
		log("measuring " + algorithm);
		int runs = 0;
		gc();
		Map<String, Object> results = new HashMap<String, Object>();
		long start = System.nanoTime();
		while (shouldRun(runs, start)) {
			long runStart = System.nanoTime();
			kernel = operator.eval(contractionCase.getSentences(), contractionCase.getSentence());
			long runEnd = System.nanoTime();
			if (runs == 0) {
				results.put("operator", "kernel");
				results.put("algorithm", algorithm);
				results.put("seed", scenario.getSeed());
				results.put("variables", scenario.getVariables());
				results.put("clauses", scenario.getClauses());
				results.put("alphaKernelCount", kernel.getAlphaKernelCount());
				results.put("maxAlphaKernelSize", kernel.getMaxAlphaKernelSize());
				results.put("minAlphaKernelSize", kernel.getMinAlphaKernelSize());
				results.put("medianAlphaKernelSize", kernel.getMedianAlphaKernelSize());
				results.put("satCalls", operator.getSATCalls());
				results.put("musCalls", operator.getMUSCalls());
			}
			results.put("elapsed" + runs, runEnd - runStart);
			runs++;
		}
		results.put("runs", runs);
		storeElapsedTimes(results, runs);
		return results;
	}

	private Map<String, Object> measureAlgorithm(ContractionScenario scenario, String algorithm, final ProfilingRemainderSetOperator<PropositionalSentence> operator)
			throws Exception {
		final ContractionCase<PropositionalSentence> contractionCase = scenario.getContractionCase();
		RemainderSet<PropositionalSentence> remainderSet = null;
		log("measuring " + algorithm);
		gc();
		int runs = 0;
		Map<String, Object> results = new HashMap<String, Object>();
		long start = System.nanoTime();
		while (shouldRun(runs, start)) {
			long runStart = System.nanoTime();
			remainderSet = operator.eval(contractionCase.getSentences(), contractionCase.getSentence());
			long runEnd = System.nanoTime();
			if (runs == 0) {
				results.put("operator", "remainderset");
				results.put("algorithm", algorithm);
				results.put("seed", scenario.getSeed());
				results.put("variables", scenario.getVariables());
				results.put("clauses", scenario.getClauses());
				results.put("remainders", remainderSet.getRemaindersCount());
				results.put("maxRemainderSize", remainderSet.getMaxRemainderSize());
				results.put("minRemainderSize", remainderSet.getMinRemainderSize());
				results.put("medianRemainderSize", remainderSet.getMedianRemainderSize());
				results.put("satCalls", operator.getSATCalls());
				results.put("maxSatCalls", operator.getMaxSATCalls());
			}
			results.put("elapsed" + runs, runEnd - runStart);
			runs++;
		}
		results.put("runs", runs);
		storeElapsedTimes(results, runs);
		return results;
	}

	private Future<ContractionScenario> createProblem(final long seed, final Integer clauseSizeMinimal, final Integer clauseSizeRange, final Integer variables,
			final Integer clauses) {
		return service.submit(new Callable<ContractionScenario>() {
			@Override
			public ContractionScenario call() throws Exception {
				try {
					Random random = new Random(seed);
					ArbitraryClauseFactory sentenceFactory = new ArbitraryClauseFactory(random, variables, clauseSizeMinimal, clauseSizeMinimal
							+ clauseSizeRange);
					SentenceSetFactory<PropositionalSentence> sentenceSetFactory = new SatisfiablePropositionalSentenceSetFactory(satReasoner, sentenceFactory,
							variables, clauses);
					ContractionCaseFactory<PropositionalSentence> factory = new ContractionCaseFactory<PropositionalSentence>(sentenceSetFactory,
							sentenceFactory, satReasoner);
					return new ContractionScenario(seed, variables, clauses, factory.create());
				} catch (IllegalArgumentException e) {
					log(String.format("Unable to create problem for seed %s, %s variables, %s clauses.", seed, variables, clauses));
					return null;
				}
			}
		});
	}

	private void gc() {
		// System.gc();
	}

	private boolean shouldRun(int runs, long start) {
		return System.nanoTime() - start < RUNS_TIMEOUT && runs < MAX_RUNS;
	}

	private void storeElapsedTimes(Map<String, Object> map, int runs) {
		for(int i = runs; i < MAX_RUNS; i++) {
			map.put("elapsed" + i, -1L);
		}
	}

	private void log(Object message) {
		System.out.println(Thread.currentThread().getName() + ": " + message);
	}

	public static void main(String... args) throws Exception {
		new AlgorithmProfiler().run();
	}
}
