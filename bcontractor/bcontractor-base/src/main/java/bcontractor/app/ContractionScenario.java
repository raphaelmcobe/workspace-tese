package bcontractor.app;

import bcontractor.propositional.PropositionalSentence;
import bcontractor.propositional.inputfactory.ContractionCase;

public class ContractionScenario {
	private final long seed;
	private final Integer variables;
	private final Integer clauses;
	private final ContractionCase<PropositionalSentence> contractionCase;

	public ContractionScenario(long seed, Integer variables, Integer clauses, ContractionCase<PropositionalSentence> contractionCase) {
		super();
		this.seed = seed;
		this.variables = variables;
		this.clauses = clauses;
		this.contractionCase = contractionCase;
	}

	public long getSeed() {
		return seed;
	}

	public Integer getVariables() {
		return variables;
	}

	public Integer getClauses() {
		return clauses;
	}

	public ContractionCase<PropositionalSentence> getContractionCase() {
		return contractionCase;
	}
}