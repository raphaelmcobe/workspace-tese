package bcontractor.stat;

public class ConstantPicker implements Picker<Integer> {
	
	private final int constant;
	

	
	
	public ConstantPicker(int constant) {
		super();
		this.constant = constant;
	}




	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer pick() {
		return constant;
	}
}
