package bcontractor.stat;

/**
 * Picks random samples
 * 
 * @author Lundberg
 * 
 */
public interface Picker<T> {

	/**
	 * Picks a random sample
	 * 
	 * @return sample
	 */
	T pick();
}
