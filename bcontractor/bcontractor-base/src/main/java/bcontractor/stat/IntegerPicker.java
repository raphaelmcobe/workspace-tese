package bcontractor.stat;

import java.util.Random;

/**
 * Picks random integers in a range
 * 
 * @author Lundberg
 * 
 */
public class IntegerPicker implements Picker<Integer> {

	private final Random random;

	private final int min;

	private final int max;

	/**
	 * Constructor
	 * 
	 * @param random random
	 * @param min min
	 * @param max max
	 */
	public IntegerPicker(Random random, int min, int max) {
		super();
		this.random = random;
		this.min = min;
		this.max = max;
	}

	/**
	 * {@inheritDocn}
	 */
	@Override
	public Integer pick() {
		return random.nextInt(max - min) + min;
	}

}
