package bcontractor.stat;

public class IncrementalPicker implements Picker<Integer> {
	
	private int current = 0;

	private int step = 1;
	
	private int repeat = 1;
	
	private int first = 0;
	
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer pick() {
		return first + (current++ / repeat) * step;
	}
	
	public IncrementalPicker step(int newStep) {
		this.step = newStep;
		return this;
	}
	
	public IncrementalPicker repeat(int newRepeat) {
		this.repeat = newRepeat;
		return this;
	}
	
	public IncrementalPicker first(int newFirst) {
		this.first = newFirst;
		return this;
	}
}
