package bcontractor.builder;

/**
 * Builder interface. Builders should be immutable, and, therefore, thread safe.
 * Building should be an incremental and readable process.
 * 
 * @author lundberg
 * 
 * @param <T>
 *            build object type
 */
public interface Builder<T> extends Cloneable {

    /**
     * Builds the object
     * 
     * @return result
     */
    T build();
}
