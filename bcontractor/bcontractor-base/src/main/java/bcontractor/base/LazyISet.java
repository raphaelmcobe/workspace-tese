package bcontractor.base;

import java.lang.ref.SoftReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import bcontractor.api.ISet;

/**
 * ISet that resolves it's delegate set only when necessary. Resolved delegate
 * is kept as a soft reference, since it can always be recalculated.
 * 
 * @author Lundberg
 * 
 */
public class LazyISet<E> implements ISet<E> {

	private static final int MAX_HEIGHT = 50;

	private final DelegateResolver<E> resolver;

	private Integer hashCode;

	private Integer size;

	private int height;

	private SoftReference<Set<E>> delegateRef = new SoftReference<Set<E>>(null);

	private long[][] fields = new long[9][];

	private int suggestedOrder = -1;

	LazyISet(Set<E> delegate) {
		this(new TrivialResolver<E>(delegate));
	}

	LazyISet(DelegateResolver<E> resolver) {
		int resolverHeight = resolver.height();
		if (resolverHeight > MAX_HEIGHT) {
			this.resolver = new HardReferenceResolver<E>(resolver);
			height = 0;
		} else {
			this.resolver = resolver;
			height = resolverHeight;
		}
	}

	@Override
	public Iterator<E> iterator() {
		return this.getDelegate().iterator();
	}

	@Override
	public ISet<E> union(ISet<E> elements) {
		return new LazyISet<E>(new SetUnionResolver<E>(this, (LazyISet<E>) elements));
	}

	@Override
	public ISet<E> union(E element) {
		return new LazyISet<E>(new SingleUnionResolver<E>(this, element));
	}

	@Override
	@SuppressWarnings("unchecked")
	public ISet<E> minus(ISet<?> elements) {
		return new LazyISet<E>(new SetMinusResolver<E>(this, (LazyISet<E>) elements));
	}

	@Override
	public ISet<E> minus(E element) {
		return new LazyISet<E>(new SingleMinusResolver<E>(this, element));
	}

	@Override
	@SuppressWarnings("unchecked")
	public ISet<E> intersection(ISet<?> elements) {
		return new LazyISet<E>(new IntersectionResolver<E>(this, (LazyISet<E>) elements));
	}

	@Override
	public int size() {
		if (size == null) {
			this.size = this.getDelegate().size();
		}
		return size;
	}

	@Override
	public boolean contains(Object element) {
		return this.getDelegate().contains(element);
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean containsAll(ISet<?> elements) {
		if (this.size() < elements.size()) {
			return false;
		}
		LazyISet<E> o = (LazyISet<E>) elements;
		int order = Math.max(this.getSuggestedOrder(), o.getSuggestedOrder());
		long[] tField = this.getField(order);
		long[] oField = o.getField(order);
		for(int i = 0; i < tField.length; i++) {
			if(((tField[i] ^ oField[i]) & oField[i]) != 0L) {
				return false;
			}
		}
		if(this.getDelegate().containsAll(((LazyISet<?>) elements).getDelegate())) {
			return true;
		} else {
			return false;
		}
	}

	private int getSuggestedOrder() {
		if(suggestedOrder < 0) {
			suggestedOrder = BitFieldUtils.suggestOrder(this.size());
		}
		return suggestedOrder;
	}

	private long[] getField(int order) {
		if(fields[order] == null) {
			fields[order] = BitFieldUtils.createField(order, this);
		}
		return fields[order];
	}



	private Set<E> getDelegate() {
		Set<E> delegate = delegateRef.get();
		if (delegate == null) {
			delegate = this.resolver.resolve();
			size = delegate.size();
			delegateRef = new SoftReference<Set<E>>(delegate);
		}
		return delegate;
	}

	@Override
	public boolean isEmpty() {
		return this.getDelegate().isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof LazyISet)) {
			return false;
		} else if (this == obj) {
			return true;
		}
		LazyISet<?> o = (LazyISet<?>) obj;
		return this.size() == o.size() && this.hashCode() == o.hashCode() && this.containsAll(o);
	}

	@Override
	public int hashCode() {
		if (hashCode == null) {
			this.hashCode = this.getDelegate().hashCode();
		}
		return hashCode;
	}

	private int height() {
		return height;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.getClass().getSimpleName());
		builder.append("\n\tSet:");
		for (E s : this) {
			builder.append("\n\t\t");
			builder.append(s.toString().trim().replaceAll("\n", "\n\t\t"));
		}
		return builder.toString();
	}

	private static interface DelegateResolver<E> {
		Set<E> resolve();

		int height();
	}

	private static class TrivialResolver<E> implements DelegateResolver<E> {
		private final Set<E> set;

		public TrivialResolver(Set<E> set) {
			super();
			this.set = set;
		}

		@Override
		public Set<E> resolve() {
			return set;
		}

		@Override
		public int height() {
			return 0;
		}
	}

	private static class HardReferenceResolver<E> implements DelegateResolver<E> {

		private final Set<E> delegate;

		public HardReferenceResolver(DelegateResolver<E> resolver) {
			super();
			this.delegate = resolver.resolve();
		}

		@Override
		public Set<E> resolve() {
			return delegate;
		}

		@Override
		public int height() {
			return 0;
		}
	}

	private static class SetUnionResolver<E> implements DelegateResolver<E> {
		private final LazyISet<E> set1;

		private final LazyISet<E> set2;

		public SetUnionResolver(LazyISet<E> set1, LazyISet<E> set2) {
			this.set1 = set1;
			this.set2 = set2;
		}

		@Override
		public Set<E> resolve() {
			Set<E> delegate = new HashSet<E>(set1.size() + set2.size());
			Set<E> set1Delegate = set1.getDelegate();
			delegate.addAll(set1Delegate);
			return delegate.addAll(set2.getDelegate()) ? delegate : set1Delegate;
		}

		@Override
		public int height() {
			return Math.max(set1.height(), set2.height()) + 1;
		}
	}

	private static class SingleUnionResolver<E> implements DelegateResolver<E> {
		private final LazyISet<E> set;

		private final E element;

		public SingleUnionResolver(LazyISet<E> set, E element) {
			this.set = set;
			this.element = element;
		}

		@Override
		public Set<E> resolve() {
			Set<E> delegate = new HashSet<E>(set.size() + 1);
			Set<E> setDelegate = set.getDelegate();
			delegate.addAll(setDelegate);
			return delegate.add(element) ? delegate : setDelegate;
		}

		@Override
		public int height() {
			return set.height() + 1;
		}
	}

	private static class SetMinusResolver<E> implements DelegateResolver<E> {
		private final LazyISet<E> set1;

		private final LazyISet<E> set2;

		public SetMinusResolver(LazyISet<E> set1, LazyISet<E> set2) {
			this.set1 = set1;
			this.set2 = set2;
		}

		@Override
		public Set<E> resolve() {
			Set<E> delegate = new HashSet<E>(set1.size() + set2.size());
			Set<E> set1Delegate = set1.getDelegate();
			Set<E> set2Delegate = set2.getDelegate();
			for (E e : set1Delegate) {
				if (!set2Delegate.contains(e)) {
					delegate.add(e);
				}
			}
			return delegate.size() == set1Delegate.size() ? set1Delegate : delegate;
		}

		@Override
		public int height() {
			return Math.max(set1.height(), set2.height()) + 1;
		}
	}

	private static class SingleMinusResolver<E> implements DelegateResolver<E> {
		private final LazyISet<E> set;

		private final E element;

		public SingleMinusResolver(LazyISet<E> set, E element) {
			this.set = set;
			this.element = element;
		}

		@Override
		public Set<E> resolve() {
			Set<E> delegate = new HashSet<E>(set.size() + 1);
			Set<E> setDelegate = set.getDelegate();
			delegate.addAll(setDelegate);
			return delegate.remove(element) ? delegate : setDelegate;
		}

		@Override
		public int height() {
			return set.height() + 1;
		}
	}

	private static class IntersectionResolver<E> implements DelegateResolver<E> {
		private final LazyISet<E> set1;

		private final LazyISet<E> set2;

		public IntersectionResolver(LazyISet<E> set1, LazyISet<E> set2) {
			this.set1 = set1;
			this.set2 = set2;
		}

		@Override
		public Set<E> resolve() {
			Set<E> delegate = new HashSet<E>(set1.size() + set2.size());
			Set<E> set1Delegate = set1.getDelegate();
			delegate.addAll(set1Delegate);
			return delegate.retainAll(set2.getDelegate()) ? delegate : set1Delegate;
		}

		@Override
		public int height() {
			return Math.max(set1.height(), set2.height()) + 1;
		}
	}
}
