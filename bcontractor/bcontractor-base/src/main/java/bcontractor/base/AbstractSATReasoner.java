package bcontractor.base;

import bcontractor.api.SATReasoner;
import bcontractor.api.Sentence;
import bcontractor.api.ISet;

/**
 * Abstract class to ease reasoner implementations.
 * 
 * @author lundberg
 * 
 */
public abstract class AbstractSATReasoner<S extends Sentence<S>> implements SATReasoner<S> {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean entails(ISet<S> sentences, S sentence) {
        return this.isUnsatisfiable(sentences.union(sentence.negate()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUnsatisfiable(ISet<S> sentences) {
        return !this.isSatisfiable(sentences);
    }
}
