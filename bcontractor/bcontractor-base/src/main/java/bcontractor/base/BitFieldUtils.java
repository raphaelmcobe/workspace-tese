package bcontractor.base;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.Arrays;

/**
 * Creates bit fields for very fast, but not conclusive, set representations.
 * A field is represented by an array of long (each with 64 bits). For each element triggers
 * one bit based on it's hash code (multiple elements may have the same hash code, triggering the
 * same bit, thus generating a collision, the bit keeps being one).
 */
public class BitFieldUtils {

	private static final long[] FLAGS = new long[64];

	private static final long[] INDEX_MASKS = new long[9];

	static {
		FLAGS[0] = 0x00000001L;
		for(int i = 1; i < FLAGS.length; i++) {
			FLAGS[i] = FLAGS[i - 1] << 1;
		}
		INDEX_MASKS[0] = 0x00000000L;
		for(int i = 1; i < INDEX_MASKS.length; i++) {
			INDEX_MASKS[i] = (INDEX_MASKS[i-1] << 1) | 0x00000001L;
		}
	}

	/**
	 * Creates a bit field with the number of longs being 2^order. Order must be between 0 and 8.
	 * @param order order, between 0 and 8. 
	 * @param elements elements
	 * @return bit field
	 */
	public static long[] createField(int order, Iterable<?> elements) {
		int length = 1 << order;
		long indexMask = INDEX_MASKS[order];
		long[] field = new long[length];
		Arrays.fill(field, 0x00000000L);
		for(Object e : elements) {
			int hash = e.hashCode();
			int index = (int) ((hash >> 6) & indexMask);
			field[index] = field[index] | FLAGS[hash & 0x003F];
		}
		return field;
	}

	/**
	 * Suggests an order appropriate to the given number of elements.
	 * An appropriate should not waste space, but should still avoid collisions.
	 * @param elements elements
	 * @return suggested order
	 */
	public static int suggestOrder(int elements) {
		if(elements == 0) {
			return 0;
		}
		int highestBit = Integer.numberOfTrailingZeros(Integer.highestOneBit(elements));
		int proposedOrder = highestBit - 4;
		return max(0, min(proposedOrder, 8));
	}
}