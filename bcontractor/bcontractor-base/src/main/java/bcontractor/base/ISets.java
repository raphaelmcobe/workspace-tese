package bcontractor.base;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import bcontractor.api.ISet;
import bcontractor.api.Sentence;

/**
 * Utility class for set manipulation operations.
 * 
 * @author lundberg
 * 
 */
public final class ISets {

	public static <S extends Sentence<S>> ISet<S> intersection(Iterable<ISet<S>> sets) {
		Iterator<ISet<S>> it = sets.iterator();
		if (!it.hasNext()) {
			return empty();
		}
		ISet<S> result = it.next();
		while (it.hasNext()) {
			result = result.intersection(it.next());
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static <E> ISet<E> empty() {
		return EMPTY_SET;
	}

	public static <E> ISet<E> asISet(Collection<E> collection) {
		return asISet(new HashSet<E>(collection));
	}

	// LazyISet + fields
	 @SuppressWarnings({ "rawtypes", "unchecked" })
	 public static final ISet EMPTY_SET = new
	 LazyISet(Collections.emptySet());
	
	 public static <E> ISet<E> asISet(Set<E> set) {
	 return new LazyISet<E>(set);
	 }
	
	 public static <E> ISet<E> singleton(E element) {
	 return new LazyISet<E>(Collections.singleton(element));
	 }

	// COW
	// @SuppressWarnings({ "rawtypes", "unchecked" })
	// public static final ISet EMPTY_SET = new
	// CopyOnWriteSet(Collections.emptySet());
	//
	// public static <E> ISet<E> asISet(Set<E> set) {
	// return new CopyOnWriteSet<E>(set);
	// }
	//
	// public static <E> ISet<E> singleton(E element) {
	// return new CopyOnWriteSet<E>(Collections.singleton(element));
	// }

	// LazyISet
//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	public static final ISet EMPTY_SET = new LazyISet84(Collections.emptySet());
//
//	public static <E> ISet<E> asISet(Set<E> set) {
//		return new LazyISet84<E>(set);
//	}
//
//	public static <E> ISet<E> singleton(E element) {
//		return new LazyISet84<E>(Collections.singleton(element));
//	}
}
