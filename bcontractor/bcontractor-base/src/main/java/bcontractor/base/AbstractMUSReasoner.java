package bcontractor.base;

import bcontractor.api.Sentence;
import bcontractor.api.MUSReasoner;

/**
 * Abstract class for implementations of unsatisfiability reasoners.
 * 
 * @author lundberg
 * 
 * @param <S>
 */
public abstract class AbstractMUSReasoner<S extends Sentence<S>> implements MUSReasoner<S> {

}
