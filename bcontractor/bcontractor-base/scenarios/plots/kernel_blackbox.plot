set autoscale
set xtic auto
set ytic auto
set ztic auto
set title "Kernel - Blackbox"
set xlabel "Variables"
set ylabel "Clauses"
set zlabel "Time"
splot "kernel_blackbox.dat"
