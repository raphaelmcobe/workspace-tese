set autoscale
set xtic auto
set ytic auto
set ztic auto
set title "Kernel - MUS"
set xlabel "Variables"
set ylabel "Clauses"
set zlabel "Time"
splot "kernel_mus.dat"
