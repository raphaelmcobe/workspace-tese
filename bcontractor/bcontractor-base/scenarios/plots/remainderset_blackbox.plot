set autoscale
set xtic auto
set ytic auto
set ztic auto
set title "Remainder Set - Blackbox"
set xlabel "Variables"
set ylabel "Clauses"
set zlabel "Time"
splot "remainderset_blackbox.dat"
