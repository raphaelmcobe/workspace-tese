import csv
import pylab

records = csv.reader(open('results.csv'), delimiter = ';')

header = records.next()

clauses = header.index('clauses')
elapsed = header.index('elapsed0')

results = {}

for r in records:
    r_clauses = int(r[clauses])
    if not results.has_key(r_clauses):
        results[r_clauses] = 0
    results[r_clauses] = results[r_clauses] + int(r[elapsed])

domain = results.keys()
domain.sort()
image = []
for d in domain:
    image.append(results[d])

print domain
print image
pylab.plot(domain, image)
pylab.xlabel('time (s)')
pylab.ylabel('voltage (mV)')
pylab.title('About as simple as it gets, folks')
pylab.grid(True)
pylab.savefig('simple_plot')
pylab.show()
