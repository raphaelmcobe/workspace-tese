import csv
import pylab

def plot(file, domain, initial, source, merge, to_image):
    records = csv.reader(open(file), delimiter = ';')

    header = records.next()
    
    results = {}
    
    for r in records:
        d = domain(header, r)
        if not results.has_key(d):
            results[d] = initial
        results[d] = merge(results[d], source(header, r))
    
    domain = results.keys()
    domain.sort()
    image = []
    for d in domain:
        image.append(to_image(results[d]))
    
    pylab.plot(domain, image)
    pylab.xlabel('time (s)')
    pylab.ylabel('voltage (mV)')
    pylab.title('About as simple as it gets, folks')
    pylab.grid(True)
    pylab.savefig('simple_plot')
    pylab.show()

plot(file='results.csv',
     initial = (0, 0),
     domain = lambda h, r: int(r[h.index('clauses')]),
     source = lambda h, r: (1, int(r[h.index('elapsed0')])),
     merge = lambda x, y: (x[0]+y[0],x[1]+y[1]),
     to_image = lambda x: x[1]/x[0]
     )

plot(file='results_low_variables.csv',
     initial = (0, 0),
     domain = lambda h, r: int(r[h.index('clauses')]),
     source = lambda h, r: (1, int(r[h.index('elapsed0')])),
     merge = lambda x, y: (x[0]+y[0],x[1]+y[1]),
     to_image = lambda x: x[1]/x[0]
     )