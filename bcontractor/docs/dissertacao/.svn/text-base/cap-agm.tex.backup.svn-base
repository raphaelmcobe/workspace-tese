%% ------------------------------------------------------------------------- %%
\chapter{O Paradigma AGM}
\label{cap:paradigma_agm}

O paradigma que norteou os estudos na �rea nas �ltimas d�cadas foi estabelecido em
\cite{AGM85}, onde foram definidas as opera��es b�sicas sobre conjuntos de cren�as,
assim como uma s�rie de postulados que os operadores devem atender para serem
considerados racionais. Al�m de definir um arcabou�o te�rico para o estudo
destes operadores, tamb�m � apresentada uma forma para constru��o da opera��o
utilizando o conceito de {\it partial meet}.

O arcabou�o assume a exist�ncia de um \emph{operador de consequ�ncia} $Cn$, que leva um conjunto de proposi��es a outro, tal que para quaisquer conjuntos de proposi��es $X$ e $Y$ onde $X \subseteq Y$, vale que $X \subseteq Cn(X)$, $Cn(X) = Cn(Cn(X))$ e $Cn(X) \subseteq Cn(Y)$. � assumido que $Cn$ cont�m as tautologias l�gicas, � compacto (se $\beta \in Cn(X)$, $\beta \in Cn(X')$ para algum $X'$ subconjunto finito de $X$), e satisfaz a regra de introdu��o de disjun��es nas premissas (se $\beta \in Cn(X \cup \{\alpha_1\})$ e $\beta \in Cn(X \cup \{\alpha_2\})$, segue que $\beta \in Cn(X \cup \{\alpha_1 \vee \alpha_2\})$).

Uma \emph{teoria} � um conjunto de proposi��es fechado sobre $Cn$.

� dito que um conjunto de proposi��es � consistente se n�o existe proposi��o $\beta$
tal que $\beta \in Cn(X)$ e $\neg \beta \in Cn(X)$.

A opera��o mais b�sica a ser feita sobre uma teoria � denominada expans�o, em que
uma proposi��o � adicionada a um conjunto de proposi��es, que � em seguida fechado
logicamente pelo operador $Cn$ ($A + \alpha) = Cn(A \cup {\alpha})$. Essa opera��o pode resultar em um conjunto
inconsistente.

Com base nestas defini��es s�o estabelecidas as opera��es de contra��o e revis�o.

\section{Contra��o}
\label{sec:contracao}

O operador de contra��o $\contraction$ opera sobre uma teoria $A$ e uma
proposi��o $\alpha$. A intui��o � que o resultado seja um uma teoria consistente que
n�o contenha $\alpha$. Formalmente o operador deve atender aos seguintes postulados \cite{AM82}
para ser considerado racional:

\begin{itemize}
\item ($\contraction$ 1) $A \contraction \alpha$ � uma teoria quando $A$ � uma teoria (\emph{fecho}).

\item ($\contraction$ 2) Se $\alpha \notin Cn(\varnothing)$, ent�o $\alpha \notin Cn(A \contraction \alpha)$ (\emph{sucesso}).

\item ($\contraction$ 3) $A \contraction \alpha \subseteq A$ (\emph{inclus�o}).

\item ($\contraction$ 4) Se $\alpha \notin Cn(A)$, ent�o $A \contraction \alpha = A$ (\emph{vacuidade}).

\item ($\contraction$ 5) $A \subseteq Cn((A \contraction \alpha) \cup \{\alpha\})$ quando $A$ � uma teoria (\emph{recupera��o}).

\item ($\contraction$ 6) Se $Cn(\alpha) = Cn(\beta)$, ent�o $A \contraction \alpha = A \contraction \beta$ (\emph{extensionalidade}).
\end{itemize}

O postulado de sucesso garante que caso $\alpha$ n�o seja uma tautologia o resultado da contra��o n�o implicar� $\alpha$. A inclus�o garante que as implica��es da teoria resultante da contra��o est�o contidas na teoria original, portanto n�o houve gera��o de outros conhecimentos, apenas remo��o. A vacuidade garante que se n�o � necess�rio remover nenhuma senten�a, pois a teoria j� n�o implica em $\alpha$, nada � feito. 

\section{Revis�o}
\label{sec:revisao}

O operador de revis�o $\revision$ atua sobre uma teoria $A$ e uma proposi��o $\alpha$, resultando em uma teoria consistente que cont�m $\alpha$. Formalmente o operador deve atender aos seguintes postulados\cite{Gar88}:

\begin{itemize} 

\item ($\revision$ 1) $A \revision \alpha$ � sempre uma teoria. (\emph{fecho})

\item ($\revision$ 2) $\alpha \in A \revision \alpha$. (\emph{sucesso})

\item ($\revision$ 3) $A \revision \alpha \subseteq Cn(A \cup \{\alpha\})$. (\emph{inclus�o})

\item ($\revision$ 4) Se $\neg \alpha \notin A$, ent�o $Cn(A \cup \{\alpha\}) \subseteq A \revision \alpha$. (\emph{preserva��o})

\item ($\revision$ 5) Se $\neg \alpha \notin Cn(\varnothing)$, ent�o $A \revision \alpha$ � consistente sob $Cn$. (\emph{consist�ncia})

\item ($\revision$ 6) Se $Cn(\alpha) = Cn(\beta)$, ent�o $A \revision \alpha = A \revision \beta$. (\emph{equival�ncia})

\end{itemize}

Estas propriedades descrevem com rigor matem�tico o comportamento esperado dos operadores, sem definir como chegar ao resultado.

Os postulados de sucesso e fecho garantem que o resultado da revis�o � uma teoria que cont�m $\alpha$, o postulado de consist�ncia afirma que desde que $\neg \alpha$ n�o seja uma tautologia o resultado ser� consistente. O postulado da preserva��o diz que informa��o n�o deve ser removida da teoria sem necessidade, pois caso a teoria n�o seja contr�ria a $\alpha$ a revis�o e a expans�o possuem o mesmo resultado.

\section{Identidades}
\label{sec:identidades}

Em \cite{Gar88} � demonstrado que os operadores podem ser definidos em termos um do outro, atrav�s de duas equa��es de identidade:

Identidade de Levi: $A \revision \alpha = (K \contraction \neg \alpha) + \alpha$.

Identidade de Harper: $A \contraction \alpha = (K \revision \neg \alpha) \cap A$.

Um operador de contra��o que atenda aos postulados gera, atrav�s destas identidades, um operador de revis�o que tamb�m atende �s propriedades desejadas (e vice-versa).

\section{Partial meet}
\label{sec:partial_meet}

Os postulados, apesar de definir as propriedades que um operador deve atender, n�o definem um resultado �nico: uma contra��o sobre uma mesma teoria e mesma senten�a pode ter como resultado diversas teorias diferentes, e ainda assim atendendo as postulados. Os postulados tamb�m n�o afirmam nada sobre o algoritmo para se realizar a contra��o, deixando indefinido o caminho para se chegar ao resultado.

\cite{AGM85} define, entretanto, uma constru��o para os operadores. Seja $A$ uma teoria e $\alpha$ uma proposi��o. Primeiramente definimos o conceito de \emph{conjunto res�duo} de uma teoria $A$ com rela��o a uma senten�a $\alpha$, $A \bot \alpha$, como sendo o conjunto de todos os subconjuntos maximais $B$ de $A$ tal que $\alpha \notin Cn(B)$. Uma \emph{fun��o de sele��o} $\gamma$, tal que $\gamma(A \bot \alpha)$ � um subconjunto n�o vazio de $A \bot \alpha$, se este n�o for vazio, e $\{A\}$ caso contr�rio.

A partir destas opera��es podemos definir uma fun��o de contra��o:

$A \contraction \alpha = \bigcap{\gamma(A \bot \alpha)}$.

Intuitivamente, esta constru��o toma os maiores conjuntos de informa��o do conjunto que n�o implicam aquilo que se quer remover e faz uma intersec��o dos conjuntos mais importantes. A id�ia � reter o m�ximo de informa��o no ato da contra��o, lidando sempre com conjuntos maximais.

� demonstrado em \cite{AGM85} que esta constru��o define qualquer operador de contra��o poss�vel: qualquer operador constru�do que satisfaz os postulados � equivalente a um operador constru�do por \emph{partial meet} para alguma fun��o de sele��o.

As mesmas afirma��es tamb�m s�o v�lidas para o operador de revis�o constru�do com base nesta contra��o.

\section{Inconveni�ncias}
\label{sec:inconveniencias}

O paradigma mostrado brevemente acima, apesar de ser elegante do ponto de vista te�rico, implica em s�rios problemas quanto a sua computabilidade.

Em primeiro lugar, o arcabou�o opera sempre em conjuntos logicamente fechados, que s�o com muita frequ�ncia infinitos e por consequ�ncia de dif�cil manipula��o e implementa��o. O pr�prio fecho l�gico, utilizado extensamente, � por si s� uma opera��o extremamente cara do ponto de vista computacional.

Outro ponto negativo desta abordagem resulta da homogeneidade das senten�as em um conjunto, n�o h� como identificar senten�as mais fundamentais e que deveriam ter prefer�ncia sobre outras em uma eventual contra��o. Al�m disso, o fecho l�gico de uma teoria inconsistente � sempre o mesmo conjunto de senten�as, composto por toda a linguagem, enquanto na pr�tica � de muita valia a an�lise de diferentes tipos de inconsist�ncia.

