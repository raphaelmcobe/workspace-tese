\begin{thebibliography}{AGM85}

\bibitem[AGM82]{AM82}
Carlos~E. Alchourr{\'o}n, Peter G{\"a}rdenfors e David Makinson.
\newblock {On the logic of theory change: Contraction functions and their
  associated revision functions}.
\newblock {\em Theoria}, 48:14--37, 1982.

\bibitem[AGM85]{AGM85}
Carlos~E. Alchourr{\'o}n, Peter G{\"a}rdenfors e David Makinson.
\newblock {On the Logic of Theory Change: Partial Meet Contraction and Revision
  Functions}.
\newblock {\em The Journal of Symbolic Logic}, 50(2):510--530, 1985.

\bibitem[FFI06]{FFK06}
Marcelo~A. Falappa, Eduardo~L. Ferm{\'e} e Gabriele~K. Isberner.
\newblock {On the Logic of Theory Change: Relations between Incision and
  Selection Functions}.
\newblock Em {\em Proceeding of the 2006 conference on ECAI 2006}, p\'{a}ginas
  402--406, Amsterdam, The Netherlands, The Netherlands, 2006. IOS Press.

\bibitem[G{\"a}r88]{Gar88}
Peter G{\"a}rdenfors.
\newblock {\em {Knowledge in flux: Modeling the dynamics of epistemic states}}.
\newblock Cambridge University Press, Cambridge, Massachusetts, United States
  of America (USA), 1988.

\bibitem[GSW89]{GSW89}
Russell Greiner, Barbara~A. Smith e Ralph~W. Wilkerson.
\newblock A correction to the algorithm in reiter's theory of diagnosis.
\newblock {\em Artificial Intelligence}, 41:79--88, 1989.

\bibitem[Han91]{Han91}
Sven~Ove Hansson.
\newblock {\em Belief Base Dynamics}.
\newblock Tese de Doutorado, Uppsala University, 1991.

\bibitem[Han92]{Han92}
Sven~Ove Hansson.
\newblock Reversing the levi identity.
\newblock {\em Journal of Philosophical Logic}, 22(6):637--639, 1992.

\bibitem[Han94]{Han94}
Sven~Ove Hansson.
\newblock {Kernel Contraction}.
\newblock {\em J. of Symbolic Logic}, 59(3):845--859, 1994.

\bibitem[Han99]{Han99}
Sven~Ove Hansson.
\newblock {\em {A Textbook of Belief Dynamics : Theory Change and Database
  Updating (Applied Logic Series)}}.
\newblock Springer, Mar\c{c}o 1999.

\bibitem[Kal06]{Kal06}
Aditya Kalyanpur.
\newblock {\em Debugging and repair of owl ontologies}.
\newblock Tese de Doutorado, College Park, MD, USA, 2006.

\bibitem[LS08]{MLKS07}
Mark~H. Liffiton e Karem~A. Sakallah.
\newblock Algorithms for computing minimal unsatisfiable subsets of
  constraints.
\newblock {\em J. Autom. Reason.}, 40:1--33, January 2008.

\bibitem[Rei87]{Rei87}
Raymond Reiter.
\newblock A theory of diagnosis from first principles.
\newblock {\em Artif. Intell.}, 32:57--95, April 1987.

\bibitem[Rib10]{Rib10}
M{\'a}rcio~Moretto Ribeiro.
\newblock {\em Revis{\~a}o de cren\c{c}as em l{\'o}gicas de descri\c{c}{\~a}o e
  outras l{\'o}gicas n{\~a}o cl{\'a}ssicas}.
\newblock Tese de Doutorado, Universidade de S{\~a}o Paulo, 2010.

\bibitem[Was00]{Was00}
Renata Wassermann.
\newblock {Resource-Bounded Belief Revision}.
\newblock 2000.

\end{thebibliography}
