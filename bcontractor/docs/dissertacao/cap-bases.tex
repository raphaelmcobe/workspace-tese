%% ------------------------------------------------------------------------- %%
\chapter{Bases de Crenças}
\label{cap:bases_de_crencas}

Uma alternativa proposta como solução para os pontos negativos do paradigma AGM é a utilização de \emph{bases de crenças}.

Uma base de crenças é um conjunto finito de sentenças que, juntamente com algum operador de consequência lógica, cria o conjunto de crenças de um agente. Note que não é necessário computar o conjunto de crenças, potencialmente infinito, basta representá-lo com um conjunto de sentenças. Note também que é comum que exista mais de uma base possível para o mesmo conjunto de crenças.

Uma base de crenças pode ser vista estritamente como uma representação do conjunto de crenças, permitindo a armazenagem eficiente de um conjunto potencialmente infinito utilizando uma base finita.

Entretanto uma base de crenças é capaz de guardar mais informações do que um conjunto de crenças fechado logicamente, pois armazenando sentenças explícitas podemos tratá-las de maneira distinta de sentenças meramente derivadas, fornecendo dados para a elaboração de operadores lógicos com resultados mais intuitivos.

As implementações tratadas neste trabalho irão utilizar o modelo teórico construído sobre bases de crenças em \cite{Han91} e \cite{Han92}, tendo como fundamento o paradigma estabelecido para conjuntos de crenças em \cite{AGM85}.

\section{Partial meet sobre bases}
\label{sec:partial_meet_sobre_bases}

As mesmas idéias propostas para a construção de operações sobre conjunto de crenças utilizando o conceito de partial meet podem ser aplicadas para construir operações sobre bases de crenças, com algumas adaptações e propriedades distintas.

Hansson \cite{Han91} define o operador de contração de bases por partial meet com função de seleção $\gamma$, designado $\pmbcontraction$, que atua sobre uma base $B$ e uma sentença $\alpha$ e resulta em uma outra base, como sendo:

$B \pmbcontraction \alpha = \bigcap \gamma (B \bot \alpha)$

Em \cite{Han92} são definidas propriedades desta contração, nos mesmos moldes que os postulados em \cite{AGM85}.

\begin{itemize}
 \item Se $\alpha \notin Cn(\varnothing)$, então $\alpha \notin Cn(B \pmbcontraction \alpha)$ (\emph{sucesso})
 
 \item $B \pmbcontraction \alpha \subseteq B$ (\emph{inclusão})

 \item Se $\beta \in B \setminus (B \pmbcontraction \alpha)$, então existe $B'$ tal que $B \pmbcontraction \alpha \subseteq B' \subseteq B$, $\alpha \notin Cn(B')$ e $\alpha \in Cn(B' \cup \{\beta\})$ (\emph{relevância})

 \item Se para todos os subconjuntos $B'$ de $B$, $\alpha \in Cn(B')$ se e somente se $\beta \in Cn(B')$, então $B \pmbcontraction \alpha = B \pmbcontraction \beta$ (\emph{uniformidade})
\end{itemize}

Em \cite{AGM85} o operador de revisão é construído a partir da contração utilizando a Identidade de Levi. Hansson definiu a aplicação direta desta identidade em bases de crenças como sendo uma revisão interna de base por partial meet:

$B \ipmbrevision \alpha = \bigcap{\gamma(B \bot \neg \alpha)} \cup \{\alpha\}$

A operação descrita acima primeiro contrai, removendo da base elementos para evitar inconsistências com $\alpha$, e em seguida efetua a expansão.

O operador de revisão interna de base por partial meet atende às seguintes propriedades:

\begin{itemize}
 \item Se $\neg \alpha \notin Cn(\varnothing)$, então $\neg \alpha \notin Cn(B \ipmbrevision \alpha)$ (\emph{não-contradição})

 \item $B \ipmbrevision \alpha \subseteq B \cup \{ \alpha \}$ (\emph{inclusão})

 \item Se $\beta \in B \setminus B \ipmbrevision \alpha$, então existe $B'$ tal que $B \ipmbrevision \alpha \subseteq B' \subseteq B \cup \{ \alpha \}$, $\neg \alpha \notin Cn(B')$, mas $\neg \alpha \in Cn(B \cup \{\beta\})$ (\emph{relevância})

 \item $\alpha \in B \ipmbrevision \alpha$ (\emph{sucesso})

 \item Se para todo $B' \subseteq B$, $\neg \alpha \in Cn(B')$ se e somente se $\neg \beta \in Cn(B')$, então $B \cap (B \ipmbrevision \alpha) = B \cap (B \ipmbrevision \beta)$ (\emph{uniformidade})

\end{itemize}

Diferente do que ocorre quando tratamos de conjuntos de sentenças, uma base pode se tornar inconsistente sem que haja perda de informação, o que permite que a operação de revisão possa ser escrita de outra forma, denominada revisão externa de base por partial meet \cite{Han92}, $\epmbrevision$.

$B \epmbrevision \alpha = \bigcap{\gamma{((B \cup \{ \alpha \}) \bot \neg \alpha)}}$

A revisão externa, por sua vez, é caracterizada pelas seguintes propriedades, descritas em \cite{Han92}

\begin{itemize}
 \item Se $\neg \alpha \notin Cn(\varnothing)$, então $\neg \alpha \notin Cn(B \epmbrevision \alpha)$ (\emph{não-contradição})

 \item $B \epmbrevision \alpha \subseteq B \cup \{ \alpha \}$ (\emph{inclusão})

 \item Se $\beta \in B \setminus B \epmbrevision \alpha$, então existe $B'$ tal que $B \epmbrevision \alpha \subseteq B' \subseteq B \cup \{ \alpha \}$ tal que $\neg \alpha \notin Cn(B')$ e $\neg \alpha \in Cn(B' \cup \{ \beta \})$ (\emph{relevância})

 \item $\alpha \in B \epmbrevision \alpha$ (\emph{sucesso})

 \item Se $\alpha$ e $\beta$ são elementos de $B$ e é verdade que para todo $B' \subseteq B$ tal que $\neg \alpha \in Cn(B')$ se e somente se $\neg \beta \in Cn(B')$, então $B \cap (B \epmbrevision \alpha) = B \cap (B \epmbrevision \beta)$ (\emph{uniformidade fraca})

 \item $B + \alpha \epmbrevision \alpha = B \epmbrevision \alpha$ (\emph{pré-expansão})
\end{itemize}

As propriedades da revisão externa e interna são um pouco diferentes, e mesmo o resultado das operações pode mudar dependendo do caso. Na prática, existem casos em que uma é preferível à outra, mas este assunto não será abordado aqui.


\section{Kernel}
\label{sec:kernel}

Hansson propôs em \cite{Han94} uma construção diferente para o operador de contração. A idéia é que se tomarmos todos os conjuntos mínimos de uma base $B$ que implicam em uma sentença $\alpha$ e removermos ao menos uma sentença de cada, obteremos uma base que não implica $\alpha$. Por se tratar de conjuntos mínimos, é intuitivo esperar que computacionalmente esta abordagem seja mais eficiente.

Definimos a operação \emph{kernel} tal que para cada conjunto de sentenças $B$ e uma sentença $\alpha$ obtemos um conjunto de $\alpha \mhyphen$kernels $X$ tal que  $X \in B \kernel \alpha$ se e somente se:

\begin{itemize}
 \item $X \subseteq B$
 \item $\alpha \in Cn(X)$
 \item $Y$, se $Y \subset X$ segue que $\alpha \notin Cn(Y)$
\end{itemize}

Uma \emph{função de incisão} $\sigma$ para uma base $B$ e sentença $\alpha$ é tal que: 

\begin{itemize}
 \item $\sigma (B \kernel \alpha) \subseteq \bigcup{(B \kernel \alpha)}$.
 \item Se $X \neq \varnothing$ e $X \in B \kernel \alpha$, então $X \cap \sigma (B \kernel \alpha) \neq \varnothing$.
\end{itemize}

Utilizando o operador kernel e uma função de incisão, podemos construir o operador de \emph{contração kernel} $\kbcontraction$:

$B \kbcontraction \alpha = B \setminus \sigma(B \kernel \alpha)$

As propriedades deste operador foram definidas em \cite{Han94}:

\begin{itemize}
 \item Se $\alpha \in Cn(\varnothing)$, então $\alpha \notin Cn(B \kbcontraction \alpha)$ (\emph{sucesso})

 \item $B \kbcontraction \alpha \subseteq B$ (\emph{inclusão})

 \item Se $\beta \in B \setminus B \kbcontraction \alpha$, então existe $B' \subseteq B$ tal que $\alpha \notin Cn(B')$ e $\alpha \in Cn(B' \cup \{\beta\})$ (\emph{core-retainment})

 \item Se para todos os subconjuntos $B'$ de $B$, $\alpha \in Cn(B')$ se e somente se $\beta \in Cn(B')$, então $B \kbcontraction \alpha = B \kbcontraction \beta$ (\emph{uniformidade})

\end{itemize}

Como foi observado em \cite{Was00}, esta caracterização da operação de contração kernel torna-a um caso mais geral da contração por partial meet, portanto todo operador de contração kernel é um operador de contração por partial meet, mas nem toda contração por partial meet é uma contração kernel.

Também podemos construir os operadores de revisão interna utilizando a contração kernel:

$B \ikbrevision \alpha = (B \setminus \sigma(B \kernel \neg \alpha)) \cup \{ \alpha \}$

De acordo com \cite{Was00}, assumindo um operador de consequência $Cn$ monotônico e compacto, a revisão kernel interna pode ser caracterizada de acordo com as seguintes propriedades:

\begin{itemize}
 \item Se $\neg \alpha \notin Cn(\varnothing)$, então $\neg \alpha \notin Cn(B \ikbrevision \alpha)$ (\emph{não-contradição})

 \item $B \ikbrevision \alpha \subseteq B \cup \{ \alpha \}$ (\emph{inclusão})

 \item Se $\beta \in B \setminus B \ikbrevision \alpha$, então existe $B' \subseteq B$ tal que $\neg \alpha \notin Cn(B')$ e $\neg \alpha \in Cn(B' \cup \{ \beta \})$ (\emph{core-retainment})

 \item $\alpha \in B \ikbrevision \alpha$ (\emph{sucesso})

 \item Se para todos os subconjuntos $B'$ de $B$, $\neg \alpha \in Cn(B')$ se e somente se $\neg \beta \in Cn(B')$, então $B \cap (B \ikbrevision \alpha) = B \cap (B \ikbrevision \beta)$ (\emph{uniformidade})
\end{itemize}

Também podemos definir o operador de revisão kernel externa, $\ekbrevision$, como:

$B \ekbrevision \alpha = (B \cup \{ \alpha \}) \setminus \sigma{((B \cup \{ \alpha \}) \kernel \neg \alpha)}$

Este operador, de acordo com \cite{Was00}, para um operador de consequência $Cn$ monotônico e compacto, satisfaz as seguintes propriedades:

\begin{itemize}
 \item  Se $\neg \alpha \notin Cn(\varnothing)$, então $\neg \alpha \notin Cn(B \ekbrevision \alpha)$ (\emph{não-contradição})

 \item $B \ekbrevision \alpha \subseteq B \cup \{ \alpha \}$ (\emph{inclusão})

 \item Se $\beta \in B \setminus B \ekbrevision \alpha$, então existe $B' \subseteq B \cup \{ \alpha \}$ tal que $\neg \alpha \notin Cn(B')$ e $\neg \alpha \in Cn(B' \cup \{ \beta \})$ (\emph{core-retainment})

 \item $\alpha \in B \ekbrevision \alpha$ (\emph{sucesso})

 \item Se $\alpha$ e $\beta$ são elementos de $B$ e para todo $B' \subseteq B$ tal que que $\neg \alpha \in Cn(B')$ se e somente se $\neg \beta \in Cn(B')$, então $B \cap (B \ekbrevision \alpha) = B \cap (B \ekbrevision \beta)$ (\emph{uniformidade fraca})

 \item $B + \alpha \ekbrevision \alpha = B \ekbrevision \alpha$ (\emph{pré-expansão})

\end{itemize}

