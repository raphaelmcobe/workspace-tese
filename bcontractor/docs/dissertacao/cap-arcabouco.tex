%% ------------------------------------------------------------------------- %%
\chapter{Arcabouço}
\label{cap:arcabouco}


\section{Arcabouço}

O arcabouço é escrito em Java e define através de interfaces diversos conceitos acerca de revisão de crenças. A seguir são expostos alguns exemplos de como foi estruturado e os benefícios obtidos. Para maior clareza foram deixados de lado documentação e métodos auxiliares.

Como os algoritmos fazem uso intenso de conjuntos, foi criada uma estrutura de dados para representação de conjuntos imutáveis e mais próxima do modelo matemático, promovendo aumento de expressividade e aproximando a implementação da teoria.

\begin{lstlisting}[frame=trbl]
public interface ISet<E> extends Iterable<E> {
	ISet<E> union(ISet<? extends E> elements);
	ISet<E> union(E element);
	ISet<E> minus(ISet<?> elements);
	ISet<E> minus(E element);
	ISet<E> intersection(ISet<?> elements);
	boolean contains(Object element);
	boolean containsAll(Iterable<?> elements);
	boolean isEmpty();
	int size();
}
\end{lstlisting}

Também são definidas interfaces unificadas para as operações de contração e revisão:

\begin{lstlisting}[frame=trbl]
public interface ContractionOperator<S extends Sentence<S>> {
    ISet<S> contract(ISet<S> base, S alpha);
}

public interface RevisionOperator<S extends Sentence<S>> {
    ISet<S> revise(ISet<S> base, S alpha);
}
\end{lstlisting}

Estas definições permitem que múltiplas implementações possam coexistir, permitindo fácil intercâmbio. Para construir a revisão kernel interna, por exemplo, é utilizada a mesma abstração de operador kernel e função de incisão existentes na teoria: $B \ikbrevision \alpha = (B \setminus \sigma(B \kernel \neg \alpha)) \cup \{ \alpha \}$.

\begin{lstlisting}[frame=trbl]
public class InternalKernelRevisionOperator<S extends Sentence<S>> implements RevisionOperator<S> {
	private KernelOperator<S> kernelOperator;
	private IncisionFunction<S> incisionFunction;
	ISet<S> revise(ISet<S> base, S alpha) {
		return base.minus(incision(kernel(base, alpha.negate()))).union(alpha);
	}
}
\end{lstlisting}

O exemplo acima é uma evidência de como a estruturação do arcabouço mantendo proximidade com a teoria permite uma maior clareza. A teoria sempre define diversos operadores sobre bases de crenças utilizando um pequeno número de funções e conceitos mais elementares. Em termos práticos, basta que existam implementações eficientes para as funções básicas para que diversas operações possam ser construídas. As funções básicas, que também possuem comportamento bem definido, também possuem uma interface própria e permitem diversas implementações possíveis.

Além do exemplo acima, também é definida revisão externa e contração kernel, utilizando o operador kernel e a função de incisão. De forma análoga existem definições para o operador de conjunto resíduo e função de seleção, com os operadores sobre bases de crenças construídos a partir deles.


\begin{figure}[!h]
  \centering
  \includegraphics[width=.80\textwidth]{contraction_class_diagram} 
  \caption{Diagrama de classes com diversas implementações de contração.}
  \label{fig:contraction} 
\end{figure}

Além dos operadores, o próprio provador de teoremas é encapsulado para permitir o uso de diversos provadores de teoremas e de diversas lógicas. Para os testes propostos será utilizada a lógica proposicional.

\begin{figure}[!h]
  \centering
  \includegraphics[width=.60\textwidth]{reasoner_class_diagram} 
  \caption{Diagrama de classes com diversas implementações de provadores.}
  \label{fig:reasoners} 
\end{figure}

\section{Black-box}

O algoritmo black-box, descrito em \cite{Rib10} com base em \cite{Kal06}, utiliza apenas a verificação de satisfatibilidade do provador de teoremas, o que o torna funcional para um grande número de casos.

Primeiramente é necessário definir como identificar um dos elementos do kernel, o que é feito através do procedimento denominado expande-encolhe. A idéia deste procedimento é selecionar elementos de uma base até que se obtenha um subconjunto inconsistente, em seguida este conjunto é contraído mantendo a inconsistência até que não seja possível continuar. Ao final deste procedimento o conjunto resultante é um elemento do kernel.

\begin{lstlisting}[frame=trbl]
ISet<S> findHitting(ISet<S> set) {
	return shrink(expand(set));
}
ISet<S> expand(ISet<S> set) {
	ISet<S> expanded = ISets.empty();
	for (S b : set) {
		expanded = expanded.union(b);
		if (reasoner.entails(expanded, alpha)) {
			return expanded;
		}
	}
	return null;
}
ISet<S> shrink(ISet<S> expanded) {
	if (expanded == null) {
		return null;
	}
	ISet<S> shrunk = expanded;
	for (S b : expanded) {
		if (reasoner.entails(shrunk.minus(b), alpha)) {
			shrunk = shrunk.minus(b);
		}
	}
	return shrunk;
}
\end{lstlisting}

O procedimento expande-encolhe encontra um elemento, mas não é apropriado para encontrar todos os elementos de uma forma organizada. O algoritmo de Reiter para computação de cortes mínimos em uma classe de conjuntos, descrito em \cite{Rei87} e corrigido em \cite{GSW89}, pode ser utilizado para guiar esta busca, permitindo que todos os elementos conflitantes sejam encontrados.

Apesar do algoritmo acima ser intuitivamente mais adequado para encontrar subconjuntos mínimos, também é possível utilizar o algoritmo de Reiter e o expande-encolhe para encontrar os conjuntos resíduo, que são maximais. Abaixo está um trecho do código que utiliza o expande-encolhe para encontrar o complemento de um dos conjuntos resíduo.

\begin{lstlisting}[frame=trbl]
ISet<S> findHitting(ISet<S> set) {
	return shrink(expand(set));
}
ISet<S> expand(ISet<S> set) {
	ISet<S> expanded = ISets.empty();
	for (S b : set) {
		expanded = expanded.union(b);
		if (!reasoner.entails(base.minus(expanded), sentence)) {
			return expanded;
		}
	}
	return null;
}
ISet<S> shrink(ISet<S> expanded) {
	if (expanded == null) {
		return null;
	}
	ISet<S> shrunk = expanded;
	for (S b : expanded) {
		if (!reasoner.entails(base.minus(shrunk.minus(b)), sentence)) {
			shrunk = shrunk.minus(b);
		}
	}
	return shrunk;
}
\end{lstlisting}

Ao final do algoritmo de Reiter para cortes mínimos utilizando o expande-encolhe acima, basta tomar os complementos de todos os subconjuntos encontrados para obter todos os conjuntos resíduo.

\section{Glass-box}

Em \cite{Rib10} e \cite{Kal06} é descrito um método chamado glass-box, que utilizaria uma versão pesadamente modificada do provador de teoremas para conseguir extrair mais informações sobre a consulta, como as sentenças utilizadas para chegar a uma inconsistência. Com essas informações seria possível implementar métodos muito mais eficientes para encontrar os elementos do Kernel.

No caso proposicional, o procedimento descrito se aproxima muito do problema do núcleo insatisfatível, cuja implementação é muito comum em provadores de teoremas modernos devido às vastas aplicações, como verificação automática de hardware e software, design de hardware e para desenvolvimento de sistemas que forneçam explicações sobre determinados problemas \cite{MLKS07}.

O uso da operação do provador de teoremas para resolução do núcleo insatisfatível permite a implementação do operador kernel utilizando um número muito menor de chamadas, efetuando boa parte da busca dentro do próprio provador. O custo da operação, entretanto, é conhecidamente mais caro.

Outra funcionalidade que vem se tornando comum em provadores de teoremas é o problema da satisfatibilidade máxima, que foi integrado ao arcabouço para gerar o conjunto resíduo.

\section{Cronograma}
\label{sec:cronograma}

O plano de trabalho está dividido em algumas etapas principais:

\begin{enumerate}
 \item Finalização da implementação

 \item Elaboração dos testes

 \item Elaboração dos critérios testados

 \item Execução e análise dos testes

 \item Redação da dissertação.
\end{enumerate}

Estas etapas estão planejadas para execução de acordo com o seguinte cronograma:

\begin{tabular}{ | c | c | c | c | c | c | c | c | c | c | c | c}
\hline
Atividade & dezembro & janeiro  & fevereiro & março & abril & maio & junho & julho & agosto\\ \hline
1         &    *     &    *     &           &       &       &      &       &       &       \\ \hline
2         &          &          &   *       &   *   &   *   &      &       &       &       \\ \hline
3         &          &          &   *       &   *   &   *   &      &       &       &       \\ \hline
4         &          &          &   *       &   *   &   *   &  *   &   *   &       &       \\ \hline
5         &          &          &           &   *   &   *   &  *   &   *   &   *   &   *   \\ \hline
\end{tabular} 
