\select@language {brazil}
\contentsline {chapter}{Lista de Figuras}{vii}{section*.5}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Organiza\IeC {\c c}\IeC {\~a}o do Trabalho}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Nota\IeC {\c c}\IeC {\~a}o}{2}{section.1.3}
\contentsline {chapter}{\numberline {2}O Paradigma AGM}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Contra\IeC {\c c}\IeC {\~a}o}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Revis\IeC {\~a}o}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Identidades}{4}{section.2.3}
\contentsline {section}{\numberline {2.4}Partial meet}{4}{section.2.4}
\contentsline {section}{\numberline {2.5}Inconveni\IeC {\^e}ncias}{5}{section.2.5}
\contentsline {chapter}{\numberline {3}Bases de Cren\IeC {\c c}as}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Partial meet sobre bases}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Kernel}{8}{section.3.2}
\contentsline {chapter}{\numberline {4}Rela\IeC {\c c}\IeC {\~o}es entre constru\IeC {\c c}\IeC {\~o}es}{11}{chapter.4}
\contentsline {chapter}{\numberline {5}Metodologia}{13}{chapter.5}
\contentsline {chapter}{\numberline {6}Arcabou\IeC {\c c}o}{15}{chapter.6}
\contentsline {section}{\numberline {6.1}Arcabou\IeC {\c c}o}{15}{section.6.1}
\contentsline {section}{\numberline {6.2}Black-box}{16}{section.6.2}
\contentsline {section}{\numberline {6.3}Glass-box}{17}{section.6.3}
\contentsline {section}{\numberline {6.4}Cronograma}{18}{section.6.4}
\contentsline {chapter}{\numberline {7}Gera\IeC {\c c}\IeC {\~a}o de Cen\IeC {\'a}rios}{19}{chapter.7}
\contentsline {chapter}{\numberline {8}Perfil de execu\IeC {\c c}\IeC {\~a}o}{21}{chapter.8}
\contentsline {section}{\numberline {8.1}Cen\IeC {\'a}rio de teste}{21}{section.8.1}
\contentsline {section}{\numberline {8.2}Pontos cr\IeC {\'\i }ticos}{21}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Copy-on-Write}{22}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}LazyISet}{22}{subsection.8.2.2}
\contentsline {subsection}{\numberline {8.2.3}LazyISet + Bit Fields}{22}{subsection.8.2.3}
\contentsline {chapter}{\numberline {9}Compara\IeC {\c c}\IeC {\~a}o de desempenho}{23}{chapter.9}
\contentsline {chapter}{\numberline {10}Conclus\IeC {\~a}o}{25}{chapter.10}
\contentsline {chapter}{Refer\^encias Bibliogr\'aficas}{27}{section*.9}
\contentsfinish 
