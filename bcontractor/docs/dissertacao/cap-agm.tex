%% ------------------------------------------------------------------------- %%
\chapter{O Paradigma AGM}
\label{cap:paradigma_agm}

O paradigma que norteou os estudos na área nas últimas décadas foi estabelecido em
\cite{AGM85}, onde foram definidas as operações básicas sobre conjuntos de crenças,
assim como uma série de postulados que os operadores devem atender para serem
considerados racionais. Além de definir um arcabouço teórico para o estudo
destes operadores, também é apresentada uma forma para construção da operação
utilizando o conceito de {\it partial meet}.

O arcabouço assume a existência de um \emph{operador de consequência} $Cn$, que leva um conjunto de proposições a outro, tal que para quaisquer conjuntos de proposições $X$ e $Y$ onde $X \subseteq Y$, vale que $X \subseteq Cn(X)$, $Cn(X) = Cn(Cn(X))$ e $Cn(X) \subseteq Cn(Y)$. É assumido que $Cn$ contém as tautologias lógicas, é compacto (se $\beta \in Cn(X)$, $\beta \in Cn(X')$ para algum $X'$ subconjunto finito de $X$), e satisfaz a regra de introdução de disjunções nas premissas (se $\beta \in Cn(X \cup \{\alpha_1\})$ e $\beta \in Cn(X \cup \{\alpha_2\})$, segue que $\beta \in Cn(X \cup \{\alpha_1 \vee \alpha_2\})$).

Uma \emph{teoria} é um conjunto de proposições fechado sobre $Cn$.

É dito que um conjunto de proposições é consistente se não existe proposição $\beta$
tal que $\beta \in Cn(X)$ e $\neg \beta \in Cn(X)$.

A operação mais básica a ser feita sobre uma teoria é denominada expansão, em que
uma proposição é adicionada a um conjunto de proposições, que é em seguida fechado
logicamente pelo operador $Cn$ ($A + \alpha) = Cn(A \cup {\alpha})$. Essa operação pode resultar em um conjunto
inconsistente.

Com base nestas definições são estabelecidas as operações de contração e revisão.

\section{Contração}
\label{sec:contracao}

O operador de contração $\contraction$ opera sobre uma teoria $A$ e uma
proposição $\alpha$. A intuição é que o resultado seja um uma teoria consistente que
não contenha $\alpha$. Formalmente o operador deve atender aos seguintes postulados \cite{AM82}
para ser considerado racional:

\begin{itemize}
\item ($\contraction$ 1) $A \contraction \alpha$ é uma teoria quando $A$ é uma teoria (\emph{fecho}).

\item ($\contraction$ 2) Se $\alpha \notin Cn(\varnothing)$, então $\alpha \notin Cn(A \contraction \alpha)$ (\emph{sucesso}).

\item ($\contraction$ 3) $A \contraction \alpha \subseteq A$ (\emph{inclusão}).

\item ($\contraction$ 4) Se $\alpha \notin Cn(A)$, então $A \contraction \alpha = A$ (\emph{vacuidade}).

\item ($\contraction$ 5) $A \subseteq Cn((A \contraction \alpha) \cup \{\alpha\})$ quando $A$ é uma teoria (\emph{recuperação}).

\item ($\contraction$ 6) Se $Cn(\alpha) = Cn(\beta)$, então $A \contraction \alpha = A \contraction \beta$ (\emph{extensionalidade}).
\end{itemize}

O postulado de sucesso garante que caso $\alpha$ não seja uma tautologia o resultado da contração não implicará $\alpha$. A inclusão garante que as implicações da teoria resultante da contração estão contidas na teoria original, portanto não houve geração de outros conhecimentos, apenas remoção. A vacuidade garante que se não é necessário remover nenhuma sentença, pois a teoria já não implica em $\alpha$, nada é feito.
A recuperação implica que uma contração seguida por uma expansão deverá conter a teoria original. A extensionalidade garante que a contração por proposições logicamente equivalentes geram resultado idêntico.

\section{Revisão}
\label{sec:revisao}

O operador de revisão $\revision$ atua sobre uma teoria $A$ e uma proposição $\alpha$, resultando em uma teoria consistente que contém $\alpha$. Formalmente o operador deve atender aos seguintes postulados\cite{Gar88}:

\begin{itemize} 

\item ($\revision$ 1) $A \revision \alpha$ é sempre uma teoria. (\emph{fecho})

\item ($\revision$ 2) $\alpha \in A \revision \alpha$. (\emph{sucesso})

\item ($\revision$ 3) $A \revision \alpha \subseteq Cn(A \cup \{\alpha\})$. (\emph{inclusão})

\item ($\revision$ 4) Se $\neg \alpha \notin A$, então $Cn(A \cup \{\alpha\}) \subseteq A \revision \alpha$. (\emph{preservação})

\item ($\revision$ 5) Se $\neg \alpha \notin Cn(\varnothing)$, então $A \revision \alpha$ é consistente sob $Cn$. (\emph{consistência})

\item ($\revision$ 6) Se $Cn(\alpha) = Cn(\beta)$, então $A \revision \alpha = A \revision \beta$. (\emph{equivalência})

\end{itemize}

Estas propriedades descrevem com rigor matemático o comportamento esperado dos operadores, sem definir como chegar ao resultado.

Os postulados de sucesso e fecho garantem que o resultado da revisão é uma teoria que contém $\alpha$, a inclusão garante que a teoria resultante implica em $\alpha$, o postulado de consistência afirma que desde que $\neg \alpha$ não seja uma tautologia o resultado será consistente. O postulado da preservação diz que informação não deve ser removida da teoria sem necessidade, pois caso a teoria não seja contrária a $\alpha$ a revisão e a expansão possuem o mesmo resultado. A equivalência garante que a revisão por proposições logicamente equivalentes geram resultado idêntico.

\section{Identidades}
\label{sec:identidades}

Em \cite{Gar88} é demonstrado que os operadores podem ser definidos em termos um do outro, através de duas equações de identidade:

Identidade de Levi: $A \revision \alpha = (K \contraction \neg \alpha) + \alpha$.

Identidade de Harper: $A \contraction \alpha = (K \revision \neg \alpha) \cap A$.

Um operador de contração que atenda aos postulados gera, através destas identidades, um operador de revisão que também atende às propriedades desejadas (e vice-versa).

\section{Partial meet}
\label{sec:partial_meet}

Os postulados, apesar de definir as propriedades que um operador deve atender, não definem um resultado único: uma contração sobre uma mesma teoria e mesma sentença pode ter como resultado diversas teorias diferentes, e ainda assim atendendo as postulados. Os postulados também não afirmam nada sobre o algoritmo para se realizar a contração, deixando indefinido o caminho para se chegar ao resultado.

\cite{AGM85} define, entretanto, uma construção para os operadores. Seja $A$ uma teoria e $\alpha$ uma proposição. Primeiramente definimos o conceito de \emph{conjunto resíduo} de uma teoria $A$ com relação a uma sentença $\alpha$, $A \bot \alpha$, como sendo o conjunto de todos os subconjuntos maximais $B$ de $A$ tal que $\alpha \notin Cn(B)$. Uma \emph{função de seleção} $\gamma$, tal que $\gamma(A \bot \alpha)$ é um subconjunto não vazio de $A \bot \alpha$, se este não for vazio, e $\{A\}$ caso contrário.

A partir destas operações podemos definir uma função de contração:

$A \contraction \alpha = \bigcap{\gamma(A \bot \alpha)}$.

Intuitivamente, esta construção toma os maiores conjuntos de informação do conjunto que não implicam aquilo que se quer remover e faz uma intersecção dos conjuntos mais importantes. A idéia é reter o máximo de informação no ato da contração, lidando sempre com conjuntos maximais.

É demonstrado em \cite{AGM85} que esta construção define qualquer operador de contração possível: qualquer operador construído que satisfaz os postulados é equivalente a um operador construído por \emph{partial meet} para alguma função de seleção.

As mesmas afirmações também são válidas para o operador de revisão construído com base nesta contração.

\section{Inconveniências}
\label{sec:inconveniencias}

O paradigma mostrado brevemente acima, apesar de ser elegante do ponto de vista teórico, implica em sérios problemas quanto a sua computabilidade.

Em primeiro lugar, o arcabouço opera sempre em conjuntos logicamente fechados, que são sempre infinitos e por consequência de difícil manipulação e implementação. O próprio fecho lógico, utilizado extensamente, é por si só uma operação extremamente cara do ponto de vista computacional.

Outro ponto negativo desta abordagem resulta da homogeneidade das sentenças em um conjunto, não há como identificar sentenças mais fundamentais e que deveriam ter preferência sobre outras em uma eventual contração. Além disso, o fecho lógico de uma teoria inconsistente é sempre o mesmo conjunto de sentenças, composto por toda a linguagem, enquanto na prática é de muita valia a análise de diferentes tipos de inconsistência.

