package bcontractor.api;

/**
 * Interface for a revision operation upon a sentence set and a sentence
 * 
 * @author lundberg
 * 
 * @param <S>
 */
public interface RevisionOperator<S extends Sentence<S>> {

	/**
	 * Revises a given set of sentences, removing necessary sentences to add a
	 * sentence.
	 * 
	 * @param base base of sentences that must be revised
	 * @param alpha sentence that should be added
	 * @return revision result
	 */
	ISet<S> revise(ISet<S> base, S alpha);
}
