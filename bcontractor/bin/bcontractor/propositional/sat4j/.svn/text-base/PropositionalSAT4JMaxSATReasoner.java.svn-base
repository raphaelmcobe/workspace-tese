package bcontractor.propositional.sat4j;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.sat4j.ExitCode;
import org.sat4j.core.VecInt;
import org.sat4j.maxsat.WeightedMaxSatDecorator;
import org.sat4j.pb.PseudoOptDecorator;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IOptimizationProblem;
import org.sat4j.specs.IVecInt;
import org.sat4j.specs.TimeoutException;

import bcontractor.api.ISet;
import bcontractor.api.MaxSATReasoner;
import bcontractor.base.ISets;
import bcontractor.propositional.PropositionalSentence;

/**
 * MaxSAT implementation using SAT4J. INCOMPLETE
 * 
 * @author Lundberg
 * 
 */
public class PropositionalSAT4JMaxSATReasoner implements MaxSATReasoner<PropositionalSentence> {

	private SAT4JSolverProvider provider = SAT4JSolverProvider.getInstance();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISet<PropositionalSentence> findMaximumSatisfiableSubset(ISet<PropositionalSentence> softSentences, ISet<PropositionalSentence> hardSentences) {
		int maxvar = 0;
		ISet<PropositionalSentence> allSentences = softSentences.union(hardSentences);
		for (PropositionalSentence sentence : allSentences) {
			maxvar = Math.max(maxvar, sentence.getMaxvar());
		}
		WeightedMaxSatDecorator decorator = new WeightedMaxSatDecorator(provider.getPBSolver());
		PseudoOptDecorator asolver = new PseudoOptDecorator(decorator);
		IOptimizationProblem optproblem = asolver;
		try {
			asolver.newVar(maxvar);
			for (PropositionalSentence sentence : softSentences) {
				for (int[] clause : sentence.getClauses()) {
					decorator.addSoftClause(toIVecInt(clause));
				}
			}
			for (PropositionalSentence sentence : hardSentences) {
				for (int[] clause : sentence.getClauses()) {
					decorator.addHardClause(toIVecInt(clause));
				}
			}
			ExitCode code = solve(optproblem);
			if (code.equals(ExitCode.UNSATISFIABLE)) {
				return ISets.empty();
			}
			Set<PropositionalSentence> core = new HashSet<PropositionalSentence>();
			int[] model = asolver.model();
			for (PropositionalSentence sentence : allSentences) {
				if (satisfies(sentence, model)) {
					core.add(sentence);
				}
			}
			return ISets.asISet(core);
		} catch (TimeoutException e) {
			throw new IllegalStateException("Shouldn't timeout.", e);
		} catch (ContradictionException e) {
			// Hard sentences not satisfiable
			return ISets.empty();
		}
	}

	private IVecInt toIVecInt(int[] clause) {
		int[] lit = new int[clause.length];
		System.arraycopy(clause, 0, lit, 0, clause.length);
		IVecInt vec = new VecInt(lit);
		return vec;
	}

	private ExitCode solve(IOptimizationProblem optproblem) throws TimeoutException {
		boolean isSatisfiable = false;
		try {
			while (optproblem.admitABetterSolution()) {
				if (!isSatisfiable) {
					if (optproblem.nonOptimalMeansSatisfiable()) {
						if (optproblem.hasNoObjectiveFunction()) {
							return ExitCode.SATISFIABLE;
						}
					}
					isSatisfiable = true;
				}
				optproblem.discardCurrentSolution();
			}
			if (isSatisfiable) {
				return ExitCode.OPTIMUM_FOUND;
			}
			return ExitCode.UNSATISFIABLE;
		} catch (ContradictionException ex) {
			assert isSatisfiable;
			return ExitCode.OPTIMUM_FOUND;
		}
	}

	private boolean satisfies(PropositionalSentence sentence, int[] model) {
		for (int[] clause : sentence.getClauses()) {
			for (int m : model) {
				if (Arrays.binarySearch(clause, m) >= 0) {
					return true;
				}
			}
		}
		return false;
	}
}
